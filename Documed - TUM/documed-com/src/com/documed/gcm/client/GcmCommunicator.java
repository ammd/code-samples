package com.documed.gcm.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import com.documed.main.Communicator;

/**
 * Supplies an application with a GCM communication channel to and from
 * an application server. In this HTTP-based scenario, the application
 * communicates via HTTP with the application server, and the server
 * communicates via GCM servers, which address the application directly.
 */
public class GcmCommunicator extends Communicator {
	
	// TODO: move some constants to server?
	
	//  Registration endpoint on application server
	public static final String PROPERTY_REGISTER = "echo";

	// Registration id property
	public static final String PROPERTY_REG_ID = "regId";
	
    // Use IP 10.0.2.2 if working from emulator, otherwise IP of machine
    // on current network. May require to turn off / configure firewall
	public static final String SERVER_BASE_URL = "http://192.168.0.13:8080/documed/documed/Main";
	
	// Property name for registration id
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    
    // Project number retrieved from Google Developers API Console
    private static final String SENDER_ID = "966012878086";
        
    // GCM objects
    private GoogleCloudMessaging gcm;

    // Store registration id for application + device;
    private String regid;
    
    // Message IDs required for XMPP ACK/NACK
    AtomicInteger msgId = new AtomicInteger();
    
    /**
     * Constructor, set basic constants. Bootstrap must be called separately,
     * because it might be the case that the calling activity may need to execute
     * a its callback. 
     */
    public GcmCommunicator(Context appcontext, String appVersion, String name){
    	super(appcontext, appVersion, name);
    }
        
    /**
     * Bootstrap this GcmCommunicator, looks for Google Play Services availability
     * within the application context (see {@code checkPlayServices(}).
     * 
     * If available, attempts to register the client with the GCM and application servers.
     * Must be called from within an async context
     * TODO: enable calling from non async context
     * 
     * 
     * @return null iff nothing is to be run by the calling activity, i.e. services were found,
     * otherwise returns callback relayed from {@code checkPlayServices()}, should be executed 
     * as follows: .execute(this), where this is a reference to the calling activity.
     */
    // TODO: define type for standard async task?
    public AsyncTask<Activity, Integer, String> bootstrap(){
    	
    	AsyncTask<Activity, Integer, String> result = checkPlayServices();
        if (result == null) {
            gcm = GoogleCloudMessaging.getInstance(appInterface.appcontext);
            regid = getRegistrationId();
            if (regid.isEmpty()) {
            	register();
            } else {
                Log.i(TAG, "Device already registered with GCM, ID: " + regid);
                Log.i(TAG, "Registering device on application server...");
    			send(new ArrayList<NameValuePair>(0), PROPERTY_REGISTER);
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
        return result;
    }
    
    
    /**
     * Check the device to make sure Google Play Services APK are available. 
     * If this is not the case, display a dialog that allows users to 
     * download the APK from the Google Play Store or enable it in the 
     * device's system settings.
     * 
     * Returns a task that displays this dialog on the calling activity.
     * This return value should be called with .execute(this), where this
     * refers to the calling activity.
     * 
     * In case everything is OK and nothing should be run on the activity, null
     * is returned.
     */
    // TODO: try with Runnable, explicitly stating it should be run on the UI thread
    // TODO: define type for standard async task?
    public AsyncTask<Activity, Integer, String> checkPlayServices() {

    	final int resultCode = GooglePlayServicesUtil.
    			isGooglePlayServicesAvailable(appInterface.appcontext);
        Log.i(TAG, "checkGooglePlayServices, resultCode= "+ resultCode);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
            
                class ShowGooglePlayDialogTask extends AsyncTask<Activity, Integer, String>{

                    @Override
                    protected String doInBackground(Activity... acts) {
                    	GooglePlayServicesUtil.getErrorDialog(resultCode, acts[0],
                                PLAY_SERVICES_RESOLUTION_REQUEST).show();
                    	return "Google Play Services Error Dialog shown.";
                    }

                    @Override
                    protected void onPostExecute(String msg) {
                    	Log.i(TAG, msg);
                    }
                }   // End ShowGooglePlayDialogTask
                
                ShowGooglePlayDialogTask showDialog = new ShowGooglePlayDialogTask();
                return showDialog;
                
            } else {
                Log.i(TAG, "This device is not supported.");
                class ActivityFinishTask extends AsyncTask<Activity, Integer, String>{

                    @Override
                    protected String doInBackground(Activity... acts) {
                    	acts[0].finish();
                    	return "Called finish on activity";
                    }

                    @Override
                    protected void onPostExecute(String msg) {
                    	Log.i(TAG, msg);
                    }
                }   // End ActivityFinishTask
                
                ActivityFinishTask finish = new ActivityFinishTask();
                return finish;
            }
        }
       
        // Nothing to be run from the activity
        return null;
    }
    
    /**
     * Gets the current GCM registration ID from application SharedPreferences
     * Registration is required iff result is empty (i.e. no registration id
     * was found in SharedPreferences), or if the application has been updated
     * from a previous version.  
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId() {

        final SharedPreferences prefs = appInterface.getPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // application version.
        int registeredVersion = prefs.getInt(appInterface.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = appInterface.getAppVersion();
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }
    
    
    /**
     * Return the GCM registration id, authenticates a user / device to send messages
     * via GCM
     */
    public String getRegId(){
    	return regid;
    } 
    
    /**
     * Stores the registration ID and app versionCode in the application's
     * SharedPreferences.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId() {

    	if (regid == null)
    	{
    		Log.i(TAG, "Error: trying to save null registration id ");
    		return;
    	}
        final SharedPreferences prefs = appInterface.getPreferences();
        int appVersion = appInterface.getAppVersion();
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regid);
        editor.putInt(appInterface.PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }
    
    /**
     * Registers the application with GCM servers asynchronously.
     * Stores the registration ID and versionCode in the application's
     * SharedPreferences.
     */
    /*
    private void registerAsync() {

        class RegistrationTask extends AsyncTask<Void, Integer, String>{

            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(appInterface.appcontext);
                    }
                    Log.i(TAG, "Registering device on GCM server...");
                    regid = gcm.register(SENDER_ID);
                    Log.i(TAG, "Device registered with GCM, registration ID=" + regid);
        			send(new ArrayList<NameValuePair>(0), PROPERTY_REGISTER);
                    storeRegistrationId();
                    msg = "Registration complete";
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();

                    // TODO
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            	Log.i(TAG, msg);
            }
        }   // End RegistrationTask

        new RegistrationTask().execute();
    }*/
    
    /**
     * Registers the application with GCM servers synchronously.
     * Stores the registration ID and versionCode in the application's
     * SharedPreferences.
     */
    private void register() {

    	try {
    		if (gcm == null) {
    			gcm = GoogleCloudMessaging.getInstance(appInterface.appcontext);
            }
            Log.i(TAG, "Registering device on GCM server...");
            regid = gcm.register(SENDER_ID);
            Log.i(TAG, "Device registered with GCM, registration ID=" + regid);
            send(new ArrayList<NameValuePair>(0), PROPERTY_REGISTER);
            storeRegistrationId();
            
         } catch (IOException ex) {
        	 Log.i(TAG, "Error registering: " + ex.getMessage());

             // TODO
             // If there is an error, don't just keep trying to register.
             // Require the user to click a button again, or perform
             // exponential back-off.
         }
    }
    
    /**
	 * Sends an upstream message via XMPP composed with the given data.
	 * Requires application server running XMPP
	 * Note: method sends asynchronously
	 * 
	 * @param data - Bundle with message data
	 */
	public void send(final Bundle data){

		final class SendTask extends AsyncTask<Void, Void, String> {

	    	@Override
	    	protected String doInBackground(Void... params) {
                String msg = "";
                try {
                	data.putString(PROPERTY_REG_ID, regid);
                    String id = Integer.toString(msgId.incrementAndGet());
                    gcm.send(SENDER_ID + "@gcm.googleapis.com", id, data);
                    msg = "Sent message";
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
	    	}

	    	@Override
	    	protected void onPostExecute(String msg) {
	    		Log.i(TAG, msg);
	    	}
	    }
	    new SendTask().execute();
	}
        
    /**
	 * Sends an upstream message via HTTP composed with the given data
	 * to the server on the specified paths (i.e. servlet entry points).
	 * Note: method sends asynchronously
	 * 
	 * @param data - list of key value pairs containing the data
	 */
    // TODO: use encryption to scramble parameters
	public void send(final List<NameValuePair> data, final String... paths){
		for (int i = 0; i < paths.length; i++){
			
			final int j = i;
			final class SendPostTask extends AsyncTask<Void, Void, HttpResponse> {

	    		@Override
	    		protected HttpResponse doInBackground(Void... params) {
	    			return sendPOST(data, paths[j]);
	    		}

	    		@Override
	    		protected void onPostExecute(HttpResponse response) {
	    			if (response == null) {
	    				Log.i(TAG, "Send: HttpResponse is null");
	    				return;
	    			}

	    			StatusLine httpStatus = response.getStatusLine();
	    			if (httpStatus.getStatusCode() != 200) {
	    				Log.i(TAG, "Status: " + httpStatus.getStatusCode());
	    				return;
	    			}
	    		}
	    	}
	    	new SendPostTask().execute();
		}
	}
	
	
    /**
     * Send POST request to backend via HTTP.
     * Note: call to backend is not async. This method is meant to be called
     * from within an asynchronous channel / method
     * 
     * @return HTTP response from server
     */
    public HttpResponse sendPOST(List<NameValuePair> data, String path) {
    	
    	// Make sure server receives the registration Id of the requesting device
    	data.add(new BasicNameValuePair(PROPERTY_REG_ID, regid));

        String url = SERVER_BASE_URL+"/"+path;
        HttpPost httppost = new HttpPost(url);
        try {
        	httppost.setEntity(new UrlEncodedFormEntity(data));
            HttpClient httpclient = AndroidHttpClient.newInstance("Android");
            HttpResponse response = httpclient.execute(httppost);
            httpclient.getConnectionManager().shutdown();
            return response;

        } catch (ClientProtocolException e) {
            Log.i(TAG, e.getMessage(), e);
        } catch (IOException e) {
            Log.i(TAG, e.getMessage(), e);
        }
        return null;
    }
}
