package com.documed.main;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;


/**
 * Parent class for all communicator classes, provides functionality for the
 * service that doesn't directly deal with communication, such as persistence
 * on (android) application level
 */
public class AppInterface {
	            
    // The application version, needed to check if app has been upgraded since last save
    public String PROPERTY_APP_VERSION;
    
    // The simple name of the main activity of the application using this communicator
    public String PROPERTY_MAIN_NAME;
    
    // Android application context, ComService context
    public Context appcontext;
    
    
    /**
     * Constructor, set basic application related constants.
     */
    public AppInterface(Context appcontext, String appVersion, String name){
    	
    	this.PROPERTY_APP_VERSION = appVersion;
    	this.PROPERTY_MAIN_NAME = name;
    	this.appcontext = appcontext;
    }
            
    /**
     * @return Application's SharedPreferences
     */
    public SharedPreferences getPreferences() {
        return appcontext.getSharedPreferences(PROPERTY_MAIN_NAME, Context.MODE_PRIVATE);
    }
    
    
    /**
     * @return Application's version code from the PackageManager
     */
    public int getAppVersion() {

        try {
            PackageInfo packageInfo = appcontext.getPackageManager()
                    .getPackageInfo(appcontext.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}
