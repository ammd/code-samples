package com.documed.main;

import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import android.content.Context;


/**
 * Parent class for all communicator classes, provides abstract send and receive
 * functionality. All communicator classes must support, at the very least,
 * an HTTP based send method.
 */
public abstract class Communicator {
	    
    // Logger tag
    protected static final String TAG = "COM@DOCUMED";
            
    // Interface with application properties
    protected com.documed.main.AppInterface appInterface;
    
    // The listener for incoming server messages
    // TODO: does this belong with the Communicator or with GcmCommunicator?
    protected com.documed.main.ComListener listener;
    
    /**
     * Constructor, set basic application related constants.
     */
    public Communicator(Context appcontext, String appVersion, String name){
    	this.appInterface = new com.documed.main.AppInterface(appcontext, appVersion, name);
    }
    
    /**
     * Binds a message listener to this Communicator
     * @param listener - the listener to bind to
     */
	protected void bindListener(com.documed.main.ComListener listener){
		com.documed.gcm.client.ComIntentService.bindListener(listener);
	}
	
	/**
	 * Sends an upstream message via HTTP composed with the given data
	 * to the server on the specified paths (i.e. servlet entry point).
	 * Note: method sends asynchronously
	 * 
	 * @param data - Bundle with message data
	 */
	public abstract void send(List<NameValuePair> data, String... path);
}
