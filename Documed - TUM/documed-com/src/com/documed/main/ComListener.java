package com.documed.main;

import android.os.Bundle;

/**
 * This is a listener designed to be injected into a receiving Intent Service
 * or BroadcastReceiver, so that the application remains agnostic as to the particular
 * types of received messages. As different message types become available, this interface
 * may be expanded, for instance with methods for success and fail message reception.
 */
public interface ComListener {

	// General message received
	public void onMessageReceived(Bundle bundle);
}
