package com.documed.main;

import java.util.List;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;

import com.documed.gcm.client.GcmCommunicator;;

public class ComService extends Service {
	
	public static final String PROPERTY_APP_VERSION = "appVersion";
	public static final String PROPERTY_ACT_SIMPLE = "ActivitySimpleName";
	private GcmCommunicator gcm;
	
	// Binder exposing interface to external components, returned on onBind
	private final IBinder mBinder = new LocalBinder();
	public class LocalBinder extends Binder {
        public ComService getService() {
            return ComService.this;
        }
    }
	
	// Called when a components binds this service to itself
	@Override
    public IBinder onBind(Intent intent) {
		
		String actName = intent.getStringExtra(PROPERTY_ACT_SIMPLE);
		gcm = new GcmCommunicator(getApplicationContext(), PROPERTY_APP_VERSION, actName);
        return mBinder;
    }
	
	/**
	 * Bootstraps the communicator, performing any preprocessing checks
	 * and setup procedures.
	 * 
	 * @param asyncContext - true iff this method is called from an 
	 * asynchronous context, i.e. from within an AsyncTask.
	 * 
	 * @return If non null, task to be executed by the calling activity with 
	 * .execute(this), where this refers to the calling activity. If null, 
	 * everything succeeded and no further steps must be taken by the 
	 * calling activity  
	 */
	public AsyncTask<Activity, Integer, String> bootstrap() {
	    return gcm.bootstrap();
	}
	
	/**
	 * Binds an application's message listener to this ComService
	 * @param listener - reference to the application listener
	 */
	public void bindListener(com.documed.main.ComListener listener){
		gcm.bindListener(listener);
	}
	
	/**
	 * Returns a receipt to the bound activity regarding its connection
	 * with this ComService
	 * @return string token as receipt
	 */
	// TODO: return directly in bootstrap?
	public String getRegistrationToken(){
		return gcm.getRegId();
	}
	
	/**
	 * Sends an upstream message via HTTP composed of the given data
	 * to the server on the specified paths (i.e. servlet entry point).
	 * Note: method sends asynchronously
	 */
	public void send(List<NameValuePair> data, String... paths){
		gcm.send(data, paths);
	}
	
	/**
	 * Sends an upstream message via XMPP composed of the given data.
	 * Requires application server running XMPP.
	 * Note: method sends asynchronously
	 */
	public void send(Bundle data){
		gcm.send(data);
	}
}