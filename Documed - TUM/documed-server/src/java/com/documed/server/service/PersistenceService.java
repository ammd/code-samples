package com.documed.server.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.documed.server.dao.DepartmentDAO;
import com.documed.server.dao.DiaryAnamnesisDAO;
import com.documed.server.dao.DiaryDAO;
import com.documed.server.dao.Diary_QThreadDAO;
import com.documed.server.dao.DoctorDAO;
import com.documed.server.dao.Doctor_QThreadDAO;
import com.documed.server.dao.PatientDAO;
import com.documed.server.dao.PicturePointDAO;
import com.documed.server.dao.QThreadAnamnesisDAO;
import com.documed.server.dao.QThreadDAO;
import com.documed.server.dao.QuestionDAO;
import com.documed.server.dao.QuestionnaireDAO;
import com.documed.server.dao.Questionnaire_QThreadDAO;
import com.documed.server.dao.SelectionAnswerDAO;
import com.documed.server.dao.SelectionDAO;
import com.documed.server.dao.SliderDAO;
import com.documed.server.domain.Diary;
import com.documed.server.domain.DiaryAnamnesis;
import com.documed.server.domain.Diary_QThread;
import com.documed.server.domain.Doctor;
import com.documed.server.domain.Doctor_QThread;
import com.documed.server.domain.Patient;
import com.documed.server.domain.PicturePoint;
import com.documed.server.domain.QThread;
import com.documed.server.domain.QThreadAnamnesis;
import com.documed.server.domain.Question;
import com.documed.server.domain.QuestionType;
import com.documed.server.domain.Questionnaire;
import com.documed.server.domain.Selection;
import com.documed.server.domain.SelectionAnswer;
import com.documed.server.dto.DoctorRegistrationTokenDTO;
import com.documed.server.dto.DoctorThreadAssignmentDTO;
import com.documed.server.dto.PatientRegistrationTokenDTO;

/**
 * Service which handles all persistence task by DAO's
 * 
 * @author .
 */
@Service
public class PersistenceService {

	private final Log log = LogFactory.getLog(PersistenceService.class);

	@Autowired
	private DepartmentDAO departmentDAO;

	@Autowired
	private Diary_QThreadDAO diary_QThreadDAO;

	@Autowired
	private DiaryAnamnesisDAO diaryAnamnesisDAO;

	@Autowired
	private DiaryDAO diaryDAO;

	@Autowired
	private Doctor_QThreadDAO doctor_QThreadDAO;

	@Autowired
	private DoctorDAO doctorDAO;

	@Autowired
	private PatientDAO patientDAO;

	@Autowired
	private PicturePointDAO picturePointDAO;

	@Autowired
	private QThreadAnamnesisDAO qThreadAnamnesisDAO;

	@Autowired
	private QThreadDAO qThreadDAO;

	@Autowired
	private QuestionDAO questionDAO;

	@Autowired
	private Questionnaire_QThreadDAO questionnaire_QThreadDAO;

	@Autowired
	private QuestionnaireDAO questionnaireDAO;

	@Autowired
	private SelectionAnswerDAO selectionAnswerDAO;

	@Autowired
	private SelectionDAO selectionDAO;

	@Autowired
	private SliderDAO sliderDAO;

	/**
	 * 
	 * Called when patient app gets started for the first time. Creates new patient and corresponding diary entry.
	 * 
	 * @param deviceId
	 * @return PatientRegistrationTokenDTO or null if not successful
	 */
	public PatientRegistrationTokenDTO registerNewPatient(String deviceId) {

		log.info("register new patient");

		PatientRegistrationTokenDTO rtrn = null;

		try {

			// remove surrounding quotes
			if (deviceId != null && deviceId.startsWith("\"")) {
				deviceId = deviceId.substring(1, deviceId.length() - 1);
				deviceId = deviceId.replace("\\\"", "\"");
			}

			rtrn = new PatientRegistrationTokenDTO();

			Patient patient = new Patient();
			patient.setDeviceId(deviceId);
			if (patientDAO.saveOrUpdate(patient)) {

				Diary diary = new Diary();
				diary.setPatientId(patient.getId());

				if (diaryDAO.saveOrUpdate(diary)) {

					rtrn.setPatientId(patient.getId());
					rtrn.setDiaryId(diary.getId());
					log.info("new patient registered by id " + patient.getId());
				}

			} else {
				throw new Exception("patient was not registered");
			}

		} catch (Exception _e) {
			rtrn = null;
			_e.printStackTrace();
			log.error("exception occured @registerNewPatient", _e);
		}

		return rtrn;
	}

	/**
	 * 
	 * Called when doctgor app gets started for the first time.
	 * 
	 * @param registrationToken
	 * @return doctorId or 0 if not successful
	 */
	public int registerNewDoctor(DoctorRegistrationTokenDTO registrationToken) {

		log.info("register new doctor");

		int doctorId = 0;

		try {

			// device id must be given
			if (registrationToken != null && registrationToken.getDeviceId() != null && !registrationToken.getDeviceId().trim().isEmpty()) {

				Doctor doc = new Doctor();
				doc.setDeviceId(registrationToken.getDeviceId());
				doc.setActive(true);
				doc.setAddress(registrationToken.getAddress());
				doc.setEmail(registrationToken.getEmail());
				doc.setName(registrationToken.getName());
				doc.setOhMonday(registrationToken.getOhMonday());
				doc.setOhTuesday(registrationToken.getOhTuesday());
				doc.setOhWednesday(registrationToken.getOhWednesday());
				doc.setOhThursday(registrationToken.getOhThursday());
				doc.setOhFriday(registrationToken.getOhFriday());
				doc.setOhSaturday(registrationToken.getOhSaturday());
				doc.setOhSunday(registrationToken.getOhSunday());

				if (doctorDAO.saveOrUpdate(doc)) {

					doctorId = doc.getId();
					log.info("new doctor registered by id " + doctorId);

				} else {
					throw new Exception("doctor was not registered");
				}
			} else {
				throw new Exception("device id is mandatory for doctor registration");
			}

		} catch (Exception _e) {
			doctorId = 0;
			_e.printStackTrace();
			log.error("exception occured @registerNewDoctor", _e);
		}

		return doctorId;
	}
	
	/**
	 * 
	 * Updates doctor instance
	 * 
	 * @param registrationToken
	 * @return doctorId or 0 if not successful
	 */
	public boolean updateDoctor(Doctor doctor) {

		boolean rtrn = false;

		try {

			// doctor id must be given
			if (doctor != null && doctor.getId() > 0) {
				
				if (doctorDAO.saveOrUpdate(doctor)) {
					
					log.info("doctor updated by id " + doctor.getId());
					rtrn = true;

				} else {
					throw new Exception("doctor was not updated");
				}
			} else {
				throw new Exception("doctor id is mandatory for doctor update");
			}

		} catch (Exception _e) {
			rtrn = false;
			_e.printStackTrace();
			log.error("exception occured @updateDoctor", _e);
		}

		return rtrn;
	}

	/**
	 * @return list of all diary questions
	 */
	public List<Question> loadDiaryQuestions() {

		log.info("load all diary questions");

		List<Question> diaryQuestions = null;

		try {

			diaryQuestions = questionDAO.readAllDiaryQuestions();

			if (diaryQuestions != null) {
				log.info(diaryQuestions.size() + " diary questions found");

				// load and assign corresponding question types
				for (Question question : diaryQuestions) {

					if (question.getQuestionType() == QuestionType.TEXT) {
						// do nothing
					} else if (question.getQuestionType() == QuestionType.SLIDER) {
						question.setSlider(sliderDAO.readById(question.getSliderId()));
					} else if (question.getQuestionType() == QuestionType.PICTUREPOINT) {
						question.setPicturePoint(picturePointDAO.readById(question.getPicturePointId()));
					} else if (question.getQuestionType() == QuestionType.SELECTION) {

						Selection selection = selectionDAO.readById(question.getSelectionId());
						List<SelectionAnswer> answers = selectionAnswerDAO.readAllBySelection(selection.getId());

						selection.setAnswers(answers);
						question.setSelection(selection);
					} else {
						log.error(question.getQuestionType() + " is not suppoted");
					}
				}
			}

		} catch (Exception _e) {
			diaryQuestions = null;
			_e.printStackTrace();
			log.error("exception occured @loadDiaryQuestions", _e);
		}

		return diaryQuestions;
	}

	/**
	 * Adds question to diary
	 * 
	 * @param diaryId
	 * @param questionId
	 * @return whether successfully or not
	 */
	public boolean addQuestionToDiary(int diaryId, int questionId) {

		log.info("add question " + questionId + " to diary " + diaryId);

		boolean success = false;

		try {

			success = diaryDAO.addQuestionToDiary(diaryId, questionId);

		} catch (Exception _e) {
			success = false;
			_e.printStackTrace();
			log.error("exception occured @addQuestionToDiary", _e);
		}

		return success;
	}

	/**
	 * Removes question from diary
	 * 
	 * @param diaryId
	 * @param questionId
	 * @return whether successfully or not
	 */
	public boolean removeQuestionFromDiary(int diaryId, int questionId) {

		log.info("add question " + questionId + " to diary " + diaryId);

		boolean success = false;

		try {

			success = diaryDAO.removeQuestionFromDiary(diaryId, questionId);

		} catch (Exception _e) {
			success = false;
			_e.printStackTrace();
			log.error("exception occured @addQuestionToDiary", _e);
		}

		return success;
	}

	/**
	 * Submits diary anamnesis
	 * 
	 * @param diaryId
	 * @param questionId
	 * @param answer
	 * @return whether successfully or not
	 */
	public boolean submitDiaryAnamnesis(int diaryId, int questionId, String answer) {

		log.info("submit diary anamnesis " + answer);

		boolean success = false;

		try {

			// remove surrounding quotes
			if (answer != null && answer.startsWith("\"")) {
				answer = answer.substring(1, answer.length() - 1);
				answer = answer.replace("\\\"", "\"");
			}

			DiaryAnamnesis diaryAnamnesis = new DiaryAnamnesis();
			diaryAnamnesis.setSubmitted(true);
			diaryAnamnesis.setAnswer(answer);
			diaryAnamnesis.setDiaryId(diaryId);
			diaryAnamnesis.setQuestionId(questionId);
			diaryAnamnesis.setTime(new Date().getTime());

			if (diaryAnamnesisDAO.saveOrUpdate(diaryAnamnesis)) {
				log.info("diary anamnesis stored by id " + diaryAnamnesis.getId());
				success = true;
			} else {
				throw new Exception("diary anamnesis was not stored");
			}

		} catch (Exception _e) {
			success = false;
			_e.printStackTrace();
			log.error("exception occured @submitDiaryAnamnesis", _e);
		}

		return success;
	}

	/**
	 * 
	 * @param patientId
	 * @return list of all threads by given patientId
	 */
	public List<QThread> loadPatientThreads(int patientId) {

		log.info("load all threads by patient " + patientId);

		List<QThread> patientThreads = null;

		try {

			patientThreads = qThreadDAO.readAllByPatient(patientId);

			if (patientThreads != null) {
				log.info(patientThreads.size() + " threads found");

				// load and assign doctor/history doctors
				for (QThread thread : patientThreads) {

					if (thread.getDoctorId() > 0) {
						thread.setDoctor(doctorDAO.readById(thread.getDoctorId()));
					}

					thread.setHistoryDoctors(doctor_QThreadDAO.readAllByThreadId(thread.getId()));

					for (Doctor_QThread doctor_QThread : thread.getHistoryDoctors()) {
						doctor_QThread.setDoctor(doctorDAO.readById(doctor_QThread.getDoctorId()));
					}
				}
			}

		} catch (Exception _e) {
			patientThreads = null;
			_e.printStackTrace();
			log.error("exception occured @loadPatientThreads", _e);
		}

		return patientThreads;
	}

	/**
	 * 
	 * @param threadId
	 * @return list of all questionnaires by given threadId
	 */
	public List<Questionnaire> loadQuestionnairesByThread(int threadId) {

		log.info("load all questionnaires by thread " + threadId);

		List<Questionnaire> threadQuestionnaires = null;

		try {

			threadQuestionnaires = questionnaireDAO.readAllByThread(threadId);

			if (threadQuestionnaires != null) {
				log.info(threadQuestionnaires.size() + " questionnaires found");

				// load and assign questions
				for (Questionnaire questionnaire : threadQuestionnaires) {

					List<Question> questionnaireQuestions = questionDAO.readAllQuestionsByQuestionnaireId(questionnaire.getId());

					// load and assign corresponding question types
					for (Question question : questionnaireQuestions) {

						if (question.getQuestionType() == QuestionType.TEXT) {
							// do nothing
						} else if (question.getQuestionType() == QuestionType.SLIDER) {
							question.setSlider(sliderDAO.readById(question.getSliderId()));
						} else if (question.getQuestionType() == QuestionType.PICTUREPOINT) {
							question.setPicturePoint(picturePointDAO.readById(question.getPicturePointId()));
						} else if (question.getQuestionType() == QuestionType.SELECTION) {

							Selection selection = selectionDAO.readById(question.getSelectionId());
							List<SelectionAnswer> answers = selectionAnswerDAO.readAllBySelection(selection.getId());

							selection.setAnswers(answers);
							question.setSelection(selection);
						} else {
							log.error(question.getQuestionType() + " is not suppoted");
						}
					}

					questionnaire.setQuestions(questionnaireQuestions);
				}
			}

		} catch (Exception _e) {
			threadQuestionnaires = null;
			_e.printStackTrace();
			log.error("exception occured @loadQuestionnairesByThread", _e);
		}

		return threadQuestionnaires;
	}

	/**
	 * Submits thread anamnesis
	 * 
	 * @param threadAnamnesisList
	 * @return whether successfully or not
	 */
	public boolean submitThreadAnamnesis(List<QThreadAnamnesis> threadAnamnesisList) {

		log.info("submit thread anamnesis, size " + threadAnamnesisList != null ? threadAnamnesisList.size() : 0);

		boolean success = false;

		try {

			for (QThreadAnamnesis tAnamnesis : threadAnamnesisList) {
				tAnamnesis.setSubmitted(true);
				tAnamnesis.setTime(new Date().getTime());

				// remove surrounding quotes
				if (tAnamnesis.getAnswer() != null && tAnamnesis.getAnswer().startsWith("\"")) {
					tAnamnesis.setAnswer(tAnamnesis.getAnswer().substring(1, tAnamnesis.getAnswer().length() - 1));
					tAnamnesis.setAnswer(tAnamnesis.getAnswer().replace("\\\"", "\""));
				}
			}

			success = qThreadAnamnesisDAO.saveOrUpdate(threadAnamnesisList);

		} catch (Exception _e) {
			success = false;
			_e.printStackTrace();
			log.error("exception occured @submitDiaryAnamnesis", _e);
		}

		return success;
	}

	/**
	 * 
	 * @param patientId
	 * @return patient instance by given id
	 */
	public Patient loadPatientById(int patientId) {

		log.info("load patient by id " + patientId);

		Patient patient = null;

		try {

			patient = patientDAO.readById(patientId);

		} catch (Exception _e) {
			patient = null;
			_e.printStackTrace();
			log.error("exception occured @loadPatientById", _e);
		}

		return patient;
	}
	
	/**
	 * 
	 * @param threadId
	 * @return patient instance by given thread-id
	 */
	public Patient loadPatientByThreadId(int threadId) {

		log.info("load patient by thread-id " + threadId);

		QThread thread = null;
		Patient patient = null;

		try {
			thread = qThreadDAO.readById(threadId);
			patient = patientDAO.readById(thread.getPatientId());
		} catch (Exception _e) {
			patient = null;
			_e.printStackTrace();
			log.error("exception occured @loadPatientById", _e);
		}

		return patient;
	}

	/**
	 * 
	 * @param doctorId
	 * @return doctor instance by given id
	 */
	public Doctor loadDoctorById(int doctorId) {

		log.info("load doctor by id " + doctorId);

		Doctor doc = null;

		try {

			doc = doctorDAO.readById(doctorId);

		} catch (Exception _e) {
			doc = null;
			_e.printStackTrace();
			log.error("exception occured @loadDoctorById", _e);
		}

		return doc;
	}

	/**
	 * 
	 * @param qThreadAnamnesisId
	 * @return qThreadAnamnesis instance by given id
	 */
	public QThreadAnamnesis loadQThreadAnamnesisById(int qThreadAnamnesisId) {

		log.info("load qThreadAnamnesis by id " + qThreadAnamnesisId);

		QThreadAnamnesis anamnesis = null;

		try {

			anamnesis = qThreadAnamnesisDAO.readById(qThreadAnamnesisId);

		} catch (Exception _e) {
			anamnesis = null;
			_e.printStackTrace();
			log.error("exception occured @loadQThreadAnamnesisById", _e);
		}

		return anamnesis;
	}

	/**
	 * Saves or updates thread anamnesis
	 * 
	 * @param anamnesis
	 * @return whether successfully or not
	 */
	public boolean saveOrUpdateQThreadAnamnesis(QThreadAnamnesis anamnesis) {

		log.info("save or update thread anamnesis " + anamnesis);

		boolean success = false;

		try {

			// remove surrounding quotes
			if (anamnesis != null && anamnesis.getAnswer() != null && anamnesis.getAnswer().startsWith("\"")) {
				anamnesis.setAnswer(anamnesis.getAnswer().substring(1, anamnesis.getAnswer().length() - 1));
				anamnesis.setAnswer(anamnesis.getAnswer().replace("\\\"", "\""));
			}

			if (qThreadAnamnesisDAO.saveOrUpdate(anamnesis)) {
				log.info("thread anamnesis stored by id " + anamnesis.getId());
				success = true;
			} else {
				throw new Exception("thread anamnesis was not stored");
			}

		} catch (Exception _e) {
			success = false;
			_e.printStackTrace();
			log.error("exception occured @saveOrUpdateQThreadAnamnesis", _e);
		}

		return success;
	}

	/**
	 * Assigns doctor and thread, if threadId is given update, othwerwise create and assign new thread
	 * 
	 * @param dta
	 * @return threadId or 0 if not successful
	 */
	public int assignDoctorToThread(DoctorThreadAssignmentDTO dta) {

		int threadId = 0;

		// existing thread
		if (dta.getThreadId() > 0) {

			log.info("assign doctor " + dta.getDoctorId() + " to existing thread " + dta.getThreadId() + " to diary (0 if diary not allowed) " + dta.getDiaryId() + " of patient " + dta.getPatientId());

			try {

				QThread thread = qThreadDAO.readById(dta.getThreadId());

				if (thread.getDoctorId() != dta.getDoctorId()) {

					// write history entry for 'old' doctor
					Doctor_QThread doctor_QThread = new Doctor_QThread();
					doctor_QThread.setDoctorId(thread.getDoctorId());
					doctor_QThread.setThreadId(dta.getThreadId());
					doctor_QThread.setTimeTo(new Date().getTime());

					// assign 'new' doctor to thread
					thread.setDoctorId(dta.getDoctorId());

					if (qThreadDAO.saveOrUpdate(thread)) {
						log.info("thread stored");

						// diary allowed
						if (dta.getDiaryId() > 0) {
							// no relation between diary and thread, so create
							if (diary_QThreadDAO.readById(dta.getThreadId(), dta.getDiaryId()) == null) {
								Diary_QThread dq = new Diary_QThread();
								dq.setDiaryId(dta.getDiaryId());
								dq.setThreadId(dta.getThreadId());
								dq.setTime(new Date().getTime());

								if (diary_QThreadDAO.save(dq)) {
									log.info("diary-qthread relation stored");
								} else {
									throw new Exception("diary-qthread relation was not stored");
								}
							}
						}
						// diary not allowed
						else {

							Diary_QThread dq = diary_QThreadDAO.readById(dta.getThreadId(), dta.getDiaryId());

							// relation between diary and thread, so delete
							if (dq != null) {

								if (diary_QThreadDAO.delete(dq)) {
									log.info("diary-qthread relation deleted");
								} else {
									throw new Exception("diary-qthread relation was not deleted");
								}
							}
						}

						threadId = thread.getId();
					} else {
						throw new Exception("thread was not stored");
					}

				} else {
					throw new Exception("doctor " + dta.getDoctorId() + " is already assigned to thread " + dta.getThreadId());
				}

			} catch (Exception _e) {
				threadId = 0;
				_e.printStackTrace();
				log.error("exception occured @assignDoctorToThread (existing thread)", _e);
			}
		}
		// new thread
		else {

			log.info("assign doctor " + dta.getDoctorId() + " to new thread of patient " + dta.getPatientId());

			try {

				QThread thread = new QThread();
				thread.setBegin(new Date().getTime());
				thread.setDoctorId(dta.getDoctorId());
				thread.setPatientId(dta.getPatientId());
				thread.setTitle(dta.getTitle());

				if (qThreadDAO.saveOrUpdate(thread)) {
					log.info("thread stored by id " + thread.getId());

					// diary allowed
					if (dta.getDiaryId() > 0) {
						// no relation between diary and thread, so create
						if (diary_QThreadDAO.readById(thread.getId(), dta.getDiaryId()) == null) {
							Diary_QThread dq = new Diary_QThread();
							dq.setDiaryId(dta.getDiaryId());
							dq.setThreadId(thread.getId());
							dq.setTime(new Date().getTime());

							if (diary_QThreadDAO.save(dq)) {
								log.info("diary-qthread relation stored");
							} else {
								throw new Exception("diary-qthread relation was not stored");
							}
						}
					}

					threadId = thread.getId();
				} else {
					throw new Exception("thread was not stored");
				}

			} catch (Exception _e) {
				threadId = 0;
				_e.printStackTrace();
				log.error("exception occured @submitDiaryAnamnesis (new thread)", _e);
			}
		}

		return threadId;
	}

	/**
	 * 
	 * @return all available questionnaire instances
	 */
	public List<Questionnaire> loadAllQuestionnaires() {
		
		log.info("load all questionnaires");

		List<Questionnaire> questionnaires = null;

		try {

			questionnaires = questionnaireDAO.readAll();

			if (questionnaires != null) {
				log.info(questionnaires.size() + " questionnaires found");

				// load and assign questions
				for (Questionnaire questionnaire : questionnaires) {

					List<Question> questionnaireQuestions = questionDAO.readAllQuestionsByQuestionnaireId(questionnaire.getId());

					// load and assign corresponding question types
					for (Question question : questionnaireQuestions) {

						if (question.getQuestionType() == QuestionType.TEXT) {
							// do nothing
						} else if (question.getQuestionType() == QuestionType.SLIDER) {
							question.setSlider(sliderDAO.readById(question.getSliderId()));
						} else if (question.getQuestionType() == QuestionType.PICTUREPOINT) {
							question.setPicturePoint(picturePointDAO.readById(question.getPicturePointId()));
						} else if (question.getQuestionType() == QuestionType.SELECTION) {

							Selection selection = selectionDAO.readById(question.getSelectionId());
							List<SelectionAnswer> answers = selectionAnswerDAO.readAllBySelection(selection.getId());

							selection.setAnswers(answers);
							question.setSelection(selection);
						} else {
							log.error(question.getQuestionType() + " is not suppoted");
						}
					}

					questionnaire.setQuestions(questionnaireQuestions);
				}
			}

		} catch (Exception _e) {
			questionnaires = null;
			_e.printStackTrace();
			log.error("exception occured @loadAllQuestionnaires", _e);
		}

		return questionnaires;
	}

	/**
	 * Defines questionnaires for given thread
	 * 
	 * @param q
	 * @param questionnaires
	 * @return whether successfully or not
	 */
	public boolean setThreadQuestionnaires(int threadId, List<Integer> questionnaireIds) {

		boolean success = false;

		log.info("set questionnaires for thread " + threadId);

		try {

			success = questionnaire_QThreadDAO.setThreadQuestionnaires(threadId, questionnaireIds);

		} catch (Exception _e) {
			success = false;
			_e.printStackTrace();
			log.error("exception occured @setThreadQuestionnaires", _e);
		}

		return success;
	}

	/**
	 * @return first picturepoint
	 */
	public PicturePoint loadFirstPicturePoint() {

		log.info("load first picturepoint");

		PicturePoint rtrn = null;

		try {

			List<PicturePoint> ppList = picturePointDAO.readAll();
			if (ppList != null && !ppList.isEmpty()) {
				rtrn = ppList.get(0);
			}

		} catch (Exception _e) {
			rtrn = null;
			_e.printStackTrace();
			log.error("exception occured @loadPicturePoint", _e);
		}

		return rtrn;
	}

	/**
	 * 
	 * @param patientId
	 * @return diary anamnesis list by patientId
	 */
	public List<DiaryAnamnesis> loadDiaryAnamnesisListByPatient(int patientId) {

		log.info("load diary anamnesis list by patient " + patientId);

		List<DiaryAnamnesis> diaryAnamnesisList = null;

		// store fetched questions in map for better performance (cache)
		Map<Integer, Question> questionsMap = new HashMap<Integer, Question>();

		try {

			int diaryId = diaryDAO.readByPatientId(patientId).getId();

			diaryAnamnesisList = diaryAnamnesisDAO.readAllByDiary(diaryId);

			if (diaryAnamnesisList != null) {
				log.info(diaryAnamnesisList.size() + " diary anamnesis items found");

				// load and assign question
				for (DiaryAnamnesis diaryAnamnesis : diaryAnamnesisList) {

					Question question = null;
					// question is in map
					if (questionsMap.containsKey(diaryAnamnesis.getQuestionId())) {
						question = questionsMap.get(diaryAnamnesis.getQuestionId());
					}
					// question not in map, load from db
					else {

						question = questionDAO.readById(diaryAnamnesis.getQuestionId());
						if (question.getQuestionType() == QuestionType.TEXT) {
							// do nothing
						} else if (question.getQuestionType() == QuestionType.SLIDER) {
							question.setSlider(sliderDAO.readById(question.getSliderId()));
						} else if (question.getQuestionType() == QuestionType.PICTUREPOINT) {
							question.setPicturePoint(picturePointDAO.readById(question.getPicturePointId()));
						} else if (question.getQuestionType() == QuestionType.SELECTION) {

							Selection selection = selectionDAO.readById(question.getSelectionId());
							List<SelectionAnswer> answers = selectionAnswerDAO.readAllBySelection(selection.getId());

							selection.setAnswers(answers);
							question.setSelection(selection);
						} else {
							log.error(question.getQuestionType() + " is not suppoted");
						}

						questionsMap.put(question.getId(), question);
					}

					diaryAnamnesis.setQuestion(question);
				}
			}

		} catch (Exception _e) {
			diaryAnamnesisList = null;
			_e.printStackTrace();
			log.error("exception occured @loadDiaryAnamnesisListByPatient", _e);
		}

		return diaryAnamnesisList;
	}

	/**
	 * 
	 * @param patientId
	 * @param threadId
	 * @return diary anamnesis list by patientId
	 */
	public List<DiaryAnamnesis> loadDiaryAnamnesisListByPatientAndThread(int patientId, int threadId) {

		log.info("load diary anamnesis list by patient " + patientId + " and thread " + threadId);

		List<DiaryAnamnesis> diaryAnamnesisList = null;

		// store fetched questions in map for better performance (cache)
		Map<Integer, Question> questionsMap = new HashMap<Integer, Question>();

		try {

			int diaryId = diaryDAO.readByPatientId(patientId).getId();

			// check if thread is allowed to read diary
			if (diary_QThreadDAO.readById(threadId, diaryId) != null) {

				diaryAnamnesisList = diaryAnamnesisDAO.readAllByDiary(diaryId);

				if (diaryAnamnesisList != null) {
					log.info(diaryAnamnesisList.size() + " diary anamnesis items found");

					// load and assign question
					for (DiaryAnamnesis diaryAnamnesis : diaryAnamnesisList) {

						Question question = null;
						// question is in map
						if (questionsMap.containsKey(diaryAnamnesis.getQuestionId())) {
							question = questionsMap.get(diaryAnamnesis.getQuestionId());
						}
						// question not in map, load from db
						else {

							question = questionDAO.readById(diaryAnamnesis.getQuestionId());
							if (question.getQuestionType() == QuestionType.TEXT) {
								// do nothing
							} else if (question.getQuestionType() == QuestionType.SLIDER) {
								question.setSlider(sliderDAO.readById(question.getSliderId()));
							} else if (question.getQuestionType() == QuestionType.PICTUREPOINT) {
								question.setPicturePoint(picturePointDAO.readById(question.getPicturePointId()));
							} else if (question.getQuestionType() == QuestionType.SELECTION) {

								Selection selection = selectionDAO.readById(question.getSelectionId());
								List<SelectionAnswer> answers = selectionAnswerDAO.readAllBySelection(selection.getId());

								selection.setAnswers(answers);
								question.setSelection(selection);
							} else {
								log.error(question.getQuestionType() + " is not suppoted");
							}

							questionsMap.put(question.getId(), question);
						}

						diaryAnamnesis.setQuestion(question);
					}
				}
			} else {
				log.error("thread " + threadId + " is not allowed to read diary " + diaryId);
			}

		} catch (Exception _e) {
			diaryAnamnesisList = null;
			_e.printStackTrace();
			log.error("exception occured @loadDiaryAnamnesisListByPatientAndThread", _e);
		}

		return diaryAnamnesisList;
	}

	/**
	 * 
	 * @param threadId
	 * @return thread anamnesis list by threadId
	 */
	public List<QThreadAnamnesis> loadThreadAnamnesisList(int threadId) {

		log.info("load anamnesis list by thread " + threadId);

		List<QThreadAnamnesis> threadAnamnesisList = null;

		// store fetched questions in map for better performance (cache)
		Map<Integer, Question> questionsMap = new HashMap<Integer, Question>();
		Map<Integer, QThread> threadsMap = new HashMap<Integer, QThread>();

		try {

			threadAnamnesisList = qThreadAnamnesisDAO.readAllByThread(threadId);

			if (threadAnamnesisList != null) {
				log.info(threadAnamnesisList.size() + " thread anamnesis items found");

				// load and assign question and thread
				for (QThreadAnamnesis threadAnamnesis : threadAnamnesisList) {

					Question question = null;
					// question is in map
					if (questionsMap.containsKey(threadAnamnesis.getQuestionId())) {
						question = questionsMap.get(threadAnamnesis.getQuestionId());
					}
					// question not in map, load from db
					else {

						question = questionDAO.readById(threadAnamnesis.getQuestionId());
						if (question.getQuestionType() == QuestionType.TEXT) {
							// do nothing
						} else if (question.getQuestionType() == QuestionType.SLIDER) {
							question.setSlider(sliderDAO.readById(question.getSliderId()));
						} else if (question.getQuestionType() == QuestionType.PICTUREPOINT) {
							question.setPicturePoint(picturePointDAO.readById(question.getPicturePointId()));
						} else if (question.getQuestionType() == QuestionType.SELECTION) {

							Selection selection = selectionDAO.readById(question.getSelectionId());
							List<SelectionAnswer> answers = selectionAnswerDAO.readAllBySelection(selection.getId());

							selection.setAnswers(answers);
							question.setSelection(selection);
						} else {
							log.error(question.getQuestionType() + " is not suppoted");
						}

						questionsMap.put(question.getId(), question);
					}

					QThread thread = null;
					// thread is in map
					if (threadsMap.containsKey(threadAnamnesis.getThreadId())) {
						thread = threadsMap.get(threadAnamnesis.getThreadId());
					}
					// thread not in map, load from db
					else {

						thread = qThreadDAO.readById(threadAnamnesis.getThreadId());
						thread.setDoctor(doctorDAO.readById(thread.getDoctorId()));
						threadsMap.put(thread.getId(), thread);
					}

					threadAnamnesis.setQuestion(question);
					threadAnamnesis.setThread(thread);
				}
			}

		} catch (Exception _e) {
			threadAnamnesisList = null;
			_e.printStackTrace();
			log.error("exception occured @loadThreadAnamnesisList", _e);
		}

		return threadAnamnesisList;
	}

	/**
	 * Closes thread if doctor is allowed to do
	 * 
	 * @param doctorId
	 * @param threadId
	 * @return whether successfully or not
	 */
	public boolean closeThread(int doctorId, int threadId) {

		boolean success = false;

		log.info("close thread " + threadId + " by doctor " + doctorId);

		try {

			QThread thread = qThreadDAO.readById(threadId);

			if (thread.getDoctorId() == doctorId) {

				thread.setActive(false);
				thread.setEnd(new Date().getTime());

				if (qThreadDAO.saveOrUpdate(thread)) {
					log.info("thread stored");
					success = true;
				} else {
					throw new Exception("thread was not stored");
				}

			} else {
				throw new Exception("doctor " + doctorId + " is not allowed to close thread " + threadId);
			}

		} catch (Exception _e) {
			success = false;
			_e.printStackTrace();
			log.error("exception occured @closeThread", _e);
		}

		return success;
	}

	public void setDepartmentDAO(DepartmentDAO departmentDAO) {
		this.departmentDAO = departmentDAO;
	}

	public void setDiary_QThreadDAO(Diary_QThreadDAO diary_QThreadDAO) {
		this.diary_QThreadDAO = diary_QThreadDAO;
	}

	public void setDiaryAnamnesisDAO(DiaryAnamnesisDAO diaryAnamnesisDAO) {
		this.diaryAnamnesisDAO = diaryAnamnesisDAO;
	}

	public void setDiaryDAO(DiaryDAO diaryDAO) {
		this.diaryDAO = diaryDAO;
	}

	public void setDoctor_QThreadDAO(Doctor_QThreadDAO doctor_QThreadDAO) {
		this.doctor_QThreadDAO = doctor_QThreadDAO;
	}

	public void setDoctorDAO(DoctorDAO doctorDAO) {
		this.doctorDAO = doctorDAO;
	}

	public void setPatientDAO(PatientDAO patientDAO) {
		this.patientDAO = patientDAO;
	}

	public void setPicturePointDAO(PicturePointDAO picturePointDAO) {
		this.picturePointDAO = picturePointDAO;
	}

	public void setqThreadAnamnesisDAO(QThreadAnamnesisDAO qThreadAnamnesisDAO) {
		this.qThreadAnamnesisDAO = qThreadAnamnesisDAO;
	}

	public void setqThreadDAO(QThreadDAO qThreadDAO) {
		this.qThreadDAO = qThreadDAO;
	}

	public void setQuestionDAO(QuestionDAO questionDAO) {
		this.questionDAO = questionDAO;
	}

	public void setQuestionnaire_QThreadDAO(Questionnaire_QThreadDAO questionnaire_QThreadDAO) {
		this.questionnaire_QThreadDAO = questionnaire_QThreadDAO;
	}

	public void setQuestionnaireDAO(QuestionnaireDAO questionnaireDAO) {
		this.questionnaireDAO = questionnaireDAO;
	}

	public void setSelectionAnswerDAO(SelectionAnswerDAO selectionAnswerDAO) {
		this.selectionAnswerDAO = selectionAnswerDAO;
	}

	public void setSelectionDAO(SelectionDAO selectionDAO) {
		this.selectionDAO = selectionDAO;
	}

	public void setSliderDAO(SliderDAO sliderDAO) {
		this.sliderDAO = sliderDAO;
	}
}
