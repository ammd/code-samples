package com.documed.server.comm;

/**
 * Holds common values 
 * 
 * @author .
 */
public class CommonValues {

	// SERVER PROPERTIES
	public static final String SP_RESPONSE_TOKEN = "response";
	public static final String SP_WARDROUND_TOKEN = "wardroundToken";
	public static final String SP_RELOAD_TOKEN = "reloadToken";
	public static final String SP_QUESTIONNAIRE_READ_TOKEN = "questionnaireReadToken";
}
