package com.documed.server.dto;

/**
 * DTO-Class for doctor-thread assignment
 * 
 * @author .
 */
public class DoctorThreadAssignmentDTO {
	
	private int doctorId;
	private int patientId;
	private int threadId;
	private int diaryId;
	private String title;

	public int getDoctorId() {
		return doctorId;
	}

	public int getPatientId() {
		return patientId;
	}

	public int getThreadId() {
		return threadId;
	}

	public int getDiaryId() {
		return diaryId;
	}

	public String getTitle() {
		return title;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public void setThreadId(int threadId) {
		this.threadId = threadId;
	}

	public void setDiaryId(int diaryId) {
		this.diaryId = diaryId;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
