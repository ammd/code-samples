package com.documed.server.dto;

/**
 * Token for patient-registration
 * 
 * @author .
 */
public class PatientRegistrationTokenDTO {

	private int patientId;
	private int diaryId;

	public int getPatientId() {
		return patientId;
	}

	public int getDiaryId() {
		return diaryId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public void setDiaryId(int diaryId) {
		this.diaryId = diaryId;
	}

}
