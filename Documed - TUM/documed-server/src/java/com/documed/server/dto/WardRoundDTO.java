package com.documed.server.dto;

/**
 * Class for WardRound DTO
 * 
 * @author .
 */
public class WardRoundDTO {

	private long time;
	private String doctor;
	private String subject;
	private String phoneNo;
	private String email;
	private String address;
	private String ohMonday;
	private String ohTuesday;
	private String ohWednesday;
	private String ohThursday;
	private String ohFriday;
	private String ohSaturday;
	private String ohSunday;

	public WardRoundDTO() {
	}

	public WardRoundDTO(long time, String doctor, String subject, String phoneNo, String email, String address, String ohMonday, String ohTuesday, String ohWednesday, String ohThursday, String ohFriday, String ohSaturday, String ohSunday) {
		this.time = time;
		this.doctor = doctor;
		this.subject = subject;
		this.phoneNo = phoneNo;
		this.email = email;
		this.address = address;
		this.ohMonday = ohMonday;
		this.ohTuesday = ohTuesday;
		this.ohWednesday = ohWednesday;
		this.ohThursday = ohThursday;
		this.ohFriday = ohFriday;
		this.ohSaturday = ohSaturday;
		this.ohSunday = ohSunday;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getDoctor() {
		return doctor;
	}

	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOhMonday() {
		return ohMonday;
	}

	public String getOhTuesday() {
		return ohTuesday;
	}

	public String getOhWednesday() {
		return ohWednesday;
	}

	public String getOhThursday() {
		return ohThursday;
	}

	public String getOhFriday() {
		return ohFriday;
	}

	public String getOhSaturday() {
		return ohSaturday;
	}

	public String getOhSunday() {
		return ohSunday;
	}

	public void setOhMonday(String ohMonday) {
		this.ohMonday = ohMonday;
	}

	public void setOhTuesday(String ohTuesday) {
		this.ohTuesday = ohTuesday;
	}

	public void setOhWednesday(String ohWednesday) {
		this.ohWednesday = ohWednesday;
	}

	public void setOhThursday(String ohThursday) {
		this.ohThursday = ohThursday;
	}

	public void setOhFriday(String ohFriday) {
		this.ohFriday = ohFriday;
	}

	public void setOhSaturday(String ohSaturday) {
		this.ohSaturday = ohSaturday;
	}

	public void setOhSunday(String ohSunday) {
		this.ohSunday = ohSunday;
	}

}
