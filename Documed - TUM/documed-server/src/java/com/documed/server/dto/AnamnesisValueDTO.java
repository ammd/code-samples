package com.documed.server.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for anamnesis value DTO
 * 
 * @author .
 */
public class AnamnesisValueDTO {

	// Question type TEXT
	private String textAnswer = "";

	// Question type SELECTION
	private List<Integer> selectionAnswers = new ArrayList<Integer>();

	// Question type SLIDER
	private int sliderAnswer;

	// Question type PICTUREPOINT
	private float picturePointXCoordinateAnswer;
	private float picturePointYCoordinateAnswer;

	public String getTextAnswer() {
		return textAnswer;
	}

	public List<Integer> getSelectionAnswers() {
		return selectionAnswers;
	}

	public int getSliderAnswer() {
		return sliderAnswer;
	}

	public float getPicturePointXCoordinateAnswer() {
		return picturePointXCoordinateAnswer;
	}

	public float getPicturePointYCoordinateAnswer() {
		return picturePointYCoordinateAnswer;
	}

	public void setTextAnswer(String textAnswer) {
		this.textAnswer = textAnswer;
	}

	public void setSelectionAnswers(List<Integer> selectionAnswers) {
		this.selectionAnswers = selectionAnswers;
	}

	public void setSliderAnswer(int sliderAnswer) {
		this.sliderAnswer = sliderAnswer;
	}

	public void setPicturePointXCoordinateAnswer(float picturePointXCoordinateAnswer) {
		this.picturePointXCoordinateAnswer = picturePointXCoordinateAnswer;
	}

	public void setPicturePointYCoordinateAnswer(float picturePointYCoordinateAnswer) {
		this.picturePointYCoordinateAnswer = picturePointYCoordinateAnswer;
	}
}
