package com.documed.server.dto;

/**
 * DTO-Class for doctor registration
 * 
 * @author .
 */
public class DoctorRegistrationTokenDTO {

	private String name;
	private String address;
	private String phoneNo;
	private String email;
	private String openinHours;
	private String deviceId;
	private String ohMonday;
	private String ohTuesday;
	private String ohWednesday;
	private String ohThursday;
	private String ohFriday;
	private String ohSaturday;
	private String ohSunday;

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public String getEmail() {
		return email;
	}

	public String getOpeninHours() {
		return openinHours;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setOpeninHours(String openinHours) {
		this.openinHours = openinHours;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getOhMonday() {
		return ohMonday;
	}

	public String getOhTuesday() {
		return ohTuesday;
	}

	public String getOhWednesday() {
		return ohWednesday;
	}

	public String getOhThursday() {
		return ohThursday;
	}

	public String getOhFriday() {
		return ohFriday;
	}

	public String getOhSaturday() {
		return ohSaturday;
	}

	public String getOhSunday() {
		return ohSunday;
	}

	public void setOhMonday(String ohMonday) {
		this.ohMonday = ohMonday;
	}

	public void setOhTuesday(String ohTuesday) {
		this.ohTuesday = ohTuesday;
	}

	public void setOhWednesday(String ohWednesday) {
		this.ohWednesday = ohWednesday;
	}

	public void setOhThursday(String ohThursday) {
		this.ohThursday = ohThursday;
	}

	public void setOhFriday(String ohFriday) {
		this.ohFriday = ohFriday;
	}

	public void setOhSaturday(String ohSaturday) {
		this.ohSaturday = ohSaturday;
	}

	public void setOhSunday(String ohSunday) {
		this.ohSunday = ohSunday;
	}

}
