package com.documed.server.domain;

import java.io.Serializable;

/**
 * 
 * Parent class for all documed entities
 * 
 * @author .
 */
public abstract class BaseDomain implements Serializable {

	private static final long serialVersionUID = 851434973763596950L;
	
	protected int id;
	protected String createdBy;
	protected String modifiedBy;

	protected long createdAt;
	protected long modifiedAt;

	protected boolean active = true;
	
	public BaseDomain() {		
	}
		
	public BaseDomain(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active) {
		this.id = id;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.createdAt = createdAt;
		this.modifiedAt = modifiedAt;
		this.active = active;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public long getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(long modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseDomain other = (BaseDomain) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
