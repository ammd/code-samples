package com.documed.server.domain;

import java.io.Serializable;

/**
 * Domain for entity 'thread_tagebuch'
 * 
 * @author .
 */
public class Questionnaire_QThread implements Serializable {

	private static final long serialVersionUID = 2068551922033683679L;

	private int questionnaireId;
	private Questionnaire questionnaire;
	private int threadId;
	private QThread thread;

	private String warningConstraints;
	private long time;

	public Questionnaire_QThread() {
	}

	public Questionnaire_QThread(int questionnaireId, Questionnaire questionnaire, int threadId, QThread thread, String warningConstraints, long time) {
		super();
		this.questionnaireId = questionnaireId;
		this.questionnaire = questionnaire;
		this.threadId = threadId;
		this.thread = thread;
		this.warningConstraints = warningConstraints;
		this.time = time;
	}

	public int getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(int questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public int getThreadId() {
		return threadId;
	}

	public void setThreadId(int threadId) {
		this.threadId = threadId;
	}

	public Questionnaire getQuestionnaire() {
		return questionnaire;
	}

	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}

	public QThread getThread() {
		return thread;
	}

	public void setThread(QThread thread) {
		this.thread = thread;
	}

	public String getWarningConstraints() {
		return warningConstraints;
	}

	public void setWarningConstraints(String warningConstraints) {
		this.warningConstraints = warningConstraints;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + questionnaireId;
		result = prime * result + threadId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Questionnaire_QThread other = (Questionnaire_QThread) obj;
		if (questionnaireId != other.questionnaireId)
			return false;
		if (threadId != other.threadId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Questionnaire_QThread [questionnaireId=" + questionnaireId + ", questionnaire=" + questionnaire + ", threadId=" + threadId + ", thread=" + thread + ", warningConstraints=" + warningConstraints + ", time=" + time + "]";
	}

}
