package com.documed.server.domain;

import java.io.Serializable;

/**
 * Domain for entity 'arzt_thread'
 * 
 * @author .
 */
public class Doctor_QThread implements Serializable {

	private static final long serialVersionUID = -7082188676302225826L;

	private int doctorId;
	private Doctor doctor;
	private int threadId;
	private QThread thread;

	private long timeTo;

	public Doctor_QThread() {
	}

	public Doctor_QThread(int doctorId, Doctor doctor, int threadId, QThread thread, long timeTo) {
		super();
		this.doctorId = doctorId;
		this.doctor = doctor;
		this.threadId = threadId;
		this.thread = thread;
		this.timeTo = timeTo;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public int getThreadId() {
		return threadId;
	}

	public void setThreadId(int threadId) {
		this.threadId = threadId;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public QThread getThread() {
		return thread;
	}

	public void setThread(QThread thread) {
		this.thread = thread;
	}

	public long getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(long timeTo) {
		this.timeTo = timeTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + doctorId;
		result = prime * result + threadId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Doctor_QThread other = (Doctor_QThread) obj;
		if (doctorId != other.doctorId)
			return false;
		if (threadId != other.threadId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Doctor_QThread [doctorId=" + doctorId + ", doctor=" + doctor + ", threadId=" + threadId + ", thread=" + thread + ", timeTo=" + timeTo + "]";
	}

}
