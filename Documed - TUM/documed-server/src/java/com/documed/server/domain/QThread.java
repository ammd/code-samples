package com.documed.server.domain;

import java.util.List;

/**
 * Domain for entity 'thread'
 * 
 * @author .
 */
public class QThread extends BaseDomain {

	private static final long serialVersionUID = -672920880124450315L;

	private String title;
	private long begin;
	private long end;

	private int doctorId;
	private Doctor doctor;
	private int patientId;
	private Patient patient;
	private List<Doctor_QThread> historyDoctors;
	private List<Diary_QThread> threads;
	private List<Questionnaire_QThread> questionnaires;
	private List<QThreadAnamnesis> anamnesis;

	public QThread() {
	}

	public QThread(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, String title, long begin, long end, int doctorId, Doctor doctor, int patientId, Patient patient, List<Doctor_QThread> historyDoctors, List<Diary_QThread> threads, List<Questionnaire_QThread> questionnaires, List<QThreadAnamnesis> anamnesis) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.title = title;
		this.begin = begin;
		this.end = end;
		this.doctorId = doctorId;
		this.doctor = doctor;
		this.patientId = patientId;
		this.patient = patient;
		this.historyDoctors = historyDoctors;
		this.threads = threads;
		this.questionnaires = questionnaires;
		this.anamnesis = anamnesis;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}

	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public List<Doctor_QThread> getHistoryDoctors() {
		return historyDoctors;
	}

	public void setHistoryDoctors(List<Doctor_QThread> historyDoctors) {
		this.historyDoctors = historyDoctors;
	}

	public List<Diary_QThread> getThreads() {
		return threads;
	}

	public void setThreads(List<Diary_QThread> threads) {
		this.threads = threads;
	}

	public List<Questionnaire_QThread> getQuestionnaires() {
		return questionnaires;
	}

	public void setQuestionnaires(List<Questionnaire_QThread> questionnaires) {
		this.questionnaires = questionnaires;
	}

	public List<QThreadAnamnesis> getAnamnesis() {
		return anamnesis;
	}

	public void setAnamnesis(List<QThreadAnamnesis> anamnesis) {
		this.anamnesis = anamnesis;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getBegin() {
		return begin;
	}

	public void setBegin(long begin) {
		this.begin = begin;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	@Override
	public String toString() {
		return "Thread [title=" + title + ", begin=" + begin + ", end=" + end + ", id=" + id + ", active=" + active + "]";
	}
}
