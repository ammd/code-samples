package com.documed.server.domain;

/**
 * @author .
 */
public enum TimeInterval {
	DAILY, WEEKLY, MONTHLY, YEARLY;
}
