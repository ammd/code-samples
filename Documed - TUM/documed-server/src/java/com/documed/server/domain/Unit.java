package com.documed.server.domain;

/**
 * 
 * @author .
 */
public enum Unit {
	
	POINTS("%"), DEGREES("C°"), MMHG("mmHg"), CM("cm"), MM("mm");

	private String label;

	private Unit(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

}
