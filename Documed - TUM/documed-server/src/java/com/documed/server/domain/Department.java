package com.documed.server.domain;

import java.util.List;

/**
 * Domain for entity 'fachbereich'
 * 
 * @author .
 */
public class Department extends BaseDomain {

	private static final long serialVersionUID = -5507096526864060843L;

	private String name;

	private List<Questionnaire> questionnaires;
	private List<Doctor> doctors;

	public Department() {
	}

	public Department(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, String name, List<Questionnaire> questionnaires, List<Doctor> doctors) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.name = name;
		this.questionnaires = questionnaires;
		this.doctors = doctors;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Questionnaire> getQuestionnaires() {
		return questionnaires;
	}

	public void setQuestionnaires(List<Questionnaire> questionnaires) {
		this.questionnaires = questionnaires;
	}

	public List<Doctor> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<Doctor> doctors) {
		this.doctors = doctors;
	}

	@Override
	public String toString() {
		return "Department [name=" + name + ", id=" + id + ", active=" + active + "]";
	}
}
