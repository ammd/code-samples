package com.documed.server.domain;

import java.util.List;

/**
 * Domain for entity 'standardfragebogen'
 * 
 * @author .
 */
public class Questionnaire extends BaseDomain {

	private static final long serialVersionUID = -5635598629895552603L;

	private String title;
	private String source;

	private TimeInterval timeInterval = TimeInterval.DAILY;

	private int departmentId;
	private Department department;
	private List<Questionnaire_QThread> threads;
	private List<QThreadAnamnesis> anamnesis;
	private List<Question> questions;

	public Questionnaire() {
	}

	public Questionnaire(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, String title, String source, TimeInterval timeInterval, int departmentId, Department department, List<Questionnaire_QThread> threads, List<QThreadAnamnesis> anamnesis, List<Question> questions) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.title = title;
		this.source = source;
		this.timeInterval = timeInterval;
		this.departmentId = departmentId;
		this.department = department;
		this.threads = threads;
		this.anamnesis = anamnesis;
		this.questions = questions;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public TimeInterval getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(TimeInterval timeInterval) {
		this.timeInterval = timeInterval;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public List<Questionnaire_QThread> getThreads() {
		return threads;
	}

	public void setThreads(List<Questionnaire_QThread> threads) {
		this.threads = threads;
	}

	public List<QThreadAnamnesis> getAnamnesis() {
		return anamnesis;
	}

	public void setAnamnesis(List<QThreadAnamnesis> anamnesis) {
		this.anamnesis = anamnesis;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	@Override
	public String toString() {
		return "Questionnaire [title=" + title + ", source=" + source + ", timeInterval=" + timeInterval + ", id=" + id + ", active=" + active + "]";
	}

}
