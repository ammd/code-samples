package com.documed.server.domain;

import java.util.List;

/**
 * Domain for entity 'slider'
 * 
 * @author .
 */
public class Slider extends BaseDomain {

	private static final long serialVersionUID = -6754282736947962945L;

	private int minimum;
	private int maximum;
	private String labelMinimum;
	private String labelMaximum;
	private Unit unit = Unit.POINTS;
	private double stepResolution;
	private int warningMinimum;
	private int warningMaximum;

	private List<Question> questions;

	public Slider() {
	}

	public Slider(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, int minimum, int maximum, String labelMinimum, String labelMaximum, Unit unit, double stepResolution, int warningMinimum, int warningMaximum, List<Question> questions) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.minimum = minimum;
		this.maximum = maximum;
		this.labelMinimum = labelMinimum;
		this.labelMaximum = labelMaximum;
		this.unit = unit;
		this.stepResolution = stepResolution;
		this.warningMinimum = warningMinimum;
		this.warningMaximum = warningMaximum;
		this.questions = questions;
	}

	public int getMinimum() {
		return minimum;
	}

	public void setMinimum(int minimum) {
		this.minimum = minimum;
	}

	public int getMaximum() {
		return maximum;
	}

	public void setMaximum(int maximum) {
		this.maximum = maximum;
	}

	public String getLabelMinimum() {
		return labelMinimum;
	}

	public void setLabelMinimum(String labelMinimum) {
		this.labelMinimum = labelMinimum;
	}

	public String getLabelMaximum() {
		return labelMaximum;
	}

	public void setLabelMaximum(String labelMaximum) {
		this.labelMaximum = labelMaximum;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public double getStepResolution() {
		return stepResolution;
	}

	public void setStepResolution(double stepResolution) {
		this.stepResolution = stepResolution;
	}

	public int getWarningMinimum() {
		return warningMinimum;
	}

	public void setWarningMinimum(int warningMinimum) {
		this.warningMinimum = warningMinimum;
	}

	public int getWarningMaximum() {
		return warningMaximum;
	}

	public void setWarningMaximum(int warningMaximum) {
		this.warningMaximum = warningMaximum;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	@Override
	public String toString() {
		return "Slider [minimum=" + minimum + ", maximum=" + maximum + ", labelMinimum=" + labelMinimum + ", labelMaximum=" + labelMaximum + ", unit=" + unit + ", stepResolution=" + stepResolution + ", warningMinimum=" + warningMinimum + ", warningMaximum=" + warningMaximum + "]";
	}
}
