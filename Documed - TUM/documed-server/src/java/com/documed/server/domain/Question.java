package com.documed.server.domain;

import java.util.List;

/**
 * Domain for entity 'frage'
 * 
 * @author .
 */
public class Question extends BaseDomain {

	private static final long serialVersionUID = -851729630273163403L;

	private String title;
	private String source;

	private QuestionType questionType = QuestionType.SLIDER;

	private boolean timeDependent;
	private boolean diaryQuestion;

	private int selectionId;
	private Selection selection;
	private int picturePointId;
	private PicturePoint picturePoint;
	private int sliderId;
	private Slider slider;

	private List<Questionnaire> questionnaires;
	private List<QThreadAnamnesis> threadAnamnesis;
	private List<DiaryAnamnesis> diaryAnamnesis;
	private List<Diary> diaries;

	public Question() {
	}

	public Question(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, String title, String source, QuestionType questionType, boolean timeDependent, boolean diaryQuestion, int selectionId, Selection selection, int picturePointId, PicturePoint picturePoint, int sliderId, Slider slider, List<Questionnaire> questionnaires, List<QThreadAnamnesis> threadAnamnesis, List<DiaryAnamnesis> diaryAnamnesis, List<Diary> diaries) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.title = title;
		this.source = source;
		this.questionType = questionType;
		this.timeDependent = timeDependent;
		this.diaryQuestion = diaryQuestion;
		this.selectionId = selectionId;
		this.selection = selection;
		this.picturePointId = picturePointId;
		this.picturePoint = picturePoint;
		this.sliderId = sliderId;
		this.slider = slider;
		this.questionnaires = questionnaires;
		this.threadAnamnesis = threadAnamnesis;
		this.diaryAnamnesis = diaryAnamnesis;
		this.diaries = diaries;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public QuestionType getQuestionType() {
		return questionType;
	}

	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}

	public boolean isTimeDependent() {
		return timeDependent;
	}

	public void setTimeDependent(boolean timeDependent) {
		this.timeDependent = timeDependent;
	}

	public boolean isDiaryQuestion() {
		return diaryQuestion;
	}

	public void setDiaryQuestion(boolean diaryQuestion) {
		this.diaryQuestion = diaryQuestion;
	}

	public List<Questionnaire> getQuestionnaires() {
		return questionnaires;
	}

	public void setQuestionnaires(List<Questionnaire> questionnaires) {
		this.questionnaires = questionnaires;
	}

	public List<QThreadAnamnesis> getThreadAnamnesis() {
		return threadAnamnesis;
	}

	public void setThreadAnamnesis(List<QThreadAnamnesis> threadAnamnesis) {
		this.threadAnamnesis = threadAnamnesis;
	}

	public List<DiaryAnamnesis> getDiaryAnamnesis() {
		return diaryAnamnesis;
	}

	public void setDiaryAnamnesis(List<DiaryAnamnesis> diaryAnamnesis) {
		this.diaryAnamnesis = diaryAnamnesis;
	}

	public List<Diary> getDiaries() {
		return diaries;
	}

	public void setDiaries(List<Diary> diaries) {
		this.diaries = diaries;
	}

	public Slider getSlider() {
		return slider;
	}

	public void setSlider(Slider slider) {
		this.slider = slider;
	}

	public PicturePoint getPicturePoint() {
		return picturePoint;
	}

	public void setPicturePoint(PicturePoint picturePoint) {
		this.picturePoint = picturePoint;
	}

	public Selection getSelection() {
		return selection;
	}

	public void setSelection(Selection selection) {
		this.selection = selection;
	}

	public int getSelectionId() {
		return selectionId;
	}

	public void setSelectionId(int selectionId) {
		this.selectionId = selectionId;
	}

	public int getPicturePointId() {
		return picturePointId;
	}

	public void setPicturePointId(int picturePointId) {
		this.picturePointId = picturePointId;
	}

	public int getSliderId() {
		return sliderId;
	}

	public void setSliderId(int sliderId) {
		this.sliderId = sliderId;
	}

	@Override
	public String toString() {
		return "Question [title=" + title + ", source=" + source + ", questionType=" + questionType + ", timeDependent=" + timeDependent + ", id=" + id + ", active=" + active + "]";
	}
}
