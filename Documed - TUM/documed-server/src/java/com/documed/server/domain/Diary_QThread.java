package com.documed.server.domain;

import java.io.Serializable;

/**
 * Domain for entity 'thread_tagebuch'
 * 
 * @author .
 */
public class Diary_QThread implements Serializable {

	private static final long serialVersionUID = -8145311864962480125L;

	private int diaryId;
	private Diary diary;
	private int threadId;
	private QThread thread;

	private String warningConstraints;
	private long time;

	public Diary_QThread() {
	}

	public Diary_QThread(int diaryId, Diary diary, int threadId, QThread thread, String warningConstraints, long time) {
		super();
		this.diaryId = diaryId;
		this.diary = diary;
		this.threadId = threadId;
		this.thread = thread;
		this.warningConstraints = warningConstraints;
		this.time = time;
	}

	public Diary getDiary() {
		return diary;
	}

	public void setDiary(Diary diary) {
		this.diary = diary;
	}

	public QThread getThread() {
		return thread;
	}

	public void setThread(QThread thread) {
		this.thread = thread;
	}

	public String getWarningConstraints() {
		return warningConstraints;
	}

	public void setWarningConstraints(String warningConstraints) {
		this.warningConstraints = warningConstraints;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public int getDiaryId() {
		return diaryId;
	}

	public void setDiaryId(int diaryId) {
		this.diaryId = diaryId;
	}

	public int getThreadId() {
		return threadId;
	}

	public void setThreadId(int threadId) {
		this.threadId = threadId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + diaryId;
		result = prime * result + threadId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Diary_QThread other = (Diary_QThread) obj;
		if (diaryId != other.diaryId)
			return false;
		if (threadId != other.threadId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Diary_QThread [diaryId=" + diaryId + ", diary=" + diary + ", threadId=" + threadId + ", thread=" + thread + ", warningConstraints=" + warningConstraints + ", time=" + time + "]";
	}

}
