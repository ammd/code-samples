package com.documed.server.domain;

import java.util.List;

/**
 * Domain for entity 'tagebuch'
 * 
 * @author .
 */
public class Diary extends BaseDomain {

	private static final long serialVersionUID = -115757800628690022L;

	private int patientId;
	private Patient patient;
	private List<Diary_QThread> threads;
	private List<DiaryAnamnesis> anamnesis;
	private List<Question> questions;

	public Diary() {
	}

	public Diary(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, int patientId, Patient patient, List<Diary_QThread> threads, List<DiaryAnamnesis> anamnesis, List<Question> questions) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.patientId = patientId;
		this.patient = patient;
		this.threads = threads;
		this.anamnesis = anamnesis;
		this.questions = questions;
	}

	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public List<Diary_QThread> getThreads() {
		return threads;
	}

	public void setThreads(List<Diary_QThread> threads) {
		this.threads = threads;
	}

	public List<DiaryAnamnesis> getAnamnesis() {
		return anamnesis;
	}

	public void setAnamnesis(List<DiaryAnamnesis> anamnesis) {
		this.anamnesis = anamnesis;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	@Override
	public String toString() {
		return "Diary [id=" + id + ", active=" + active + "]";
	}
}
