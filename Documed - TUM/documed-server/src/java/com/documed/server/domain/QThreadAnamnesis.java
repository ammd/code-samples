package com.documed.server.domain;

/**
 * Domain for entity 'threadanamnese'
 * 
 * @author .
 */
public class QThreadAnamnesis extends BaseDomain {

	private static final long serialVersionUID = 8074803355936639091L;

	private long time;
	private boolean submitted;
	private boolean read;
	private String answer;

	private int questionId;
	private Question question;
	private int questionnaireId;
	private Questionnaire questionnaire;
	private int threadId;
	private QThread thread;

	public QThreadAnamnesis() {
	}

	public QThreadAnamnesis(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, long time, boolean submitted, boolean read, String answer, int questionId, Question question, int questionnaireId, Questionnaire questionnaire, int threadId, QThread thread) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.time = time;
		this.submitted = submitted;
		this.read = read;
		this.answer = answer;
		this.questionId = questionId;
		this.question = question;
		this.questionnaireId = questionnaireId;
		this.questionnaire = questionnaire;
		this.threadId = threadId;
		this.thread = thread;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Questionnaire getQuestionnaire() {
		return questionnaire;
	}

	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}

	public QThread getThread() {
		return thread;
	}

	public void setThread(QThread thread) {
		this.thread = thread;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public int getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(int questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public int getThreadId() {
		return threadId;
	}

	public void setThreadId(int threadId) {
		this.threadId = threadId;
	}

	@Override
	public String toString() {
		return "ThreadAnamnesis [time=" + time + ", submitted=" + submitted + ", read=" + read + ", answer=" + answer + ", id=" + id + ", active=" + active + "]";
	}
}
