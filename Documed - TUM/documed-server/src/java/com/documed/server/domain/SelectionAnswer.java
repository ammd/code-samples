package com.documed.server.domain;

import java.util.List;

/**
 * Domain for entity 'auswahlantwort'
 * 
 * @author .
 */
public class SelectionAnswer extends BaseDomain {

	private static final long serialVersionUID = -2490659979123946554L;

	private String title;
	private byte[] image;
	private boolean warningIfTrue;
	private boolean warningIfFalse;

	private List<Selection> selections;

	public SelectionAnswer() {
	}

	public SelectionAnswer(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, String title, byte[] image, boolean warningIfTrue, boolean warningIfFalse, List<Selection> selections) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.title = title;
		this.image = image;
		this.warningIfTrue = warningIfTrue;
		this.warningIfFalse = warningIfFalse;
		this.selections = selections;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public boolean isWarningIfTrue() {
		return warningIfTrue;
	}

	public void setWarningIfTrue(boolean warningIfTrue) {
		this.warningIfTrue = warningIfTrue;
	}

	public boolean isWarningIfFalse() {
		return warningIfFalse;
	}

	public void setWarningIfFalse(boolean warningIfFalse) {
		this.warningIfFalse = warningIfFalse;
	}

	public List<Selection> getSelections() {
		return selections;
	}

	public void setSelections(List<Selection> selections) {
		this.selections = selections;
	}

	@Override
	public String toString() {
		return "SelectionAnswer [title=" + title + ", warningIfTrue=" + warningIfTrue + ", warningIfFalse=" + warningIfFalse + ", id=" + id + ", active=" + active + "]";
	}

}
