package com.documed.server.domain;

/**
 * Domain for entity 'tagebuchanamnese'
 * 
 * @author .
 */
public class DiaryAnamnesis extends BaseDomain {

	private static final long serialVersionUID = -2938032289368719214L;

	private long time;
	private boolean submitted;
	private boolean read;
	private String answer;

	private int diaryId;
	private Diary diary;
	private int questionId;
	private Question question;

	public DiaryAnamnesis() {
	}

	public DiaryAnamnesis(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, long time, boolean submitted, boolean read, String answer, int diaryId, Diary diary, int questionId, Question question) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.time = time;
		this.submitted = submitted;
		this.read = read;
		this.answer = answer;
		this.diaryId = diaryId;
		this.diary = diary;
		this.questionId = questionId;
		this.question = question;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Diary getDiary() {
		return diary;
	}

	public void setDiary(Diary diary) {
		this.diary = diary;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public int getDiaryId() {
		return diaryId;
	}

	public void setDiaryId(int diaryId) {
		this.diaryId = diaryId;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	@Override
	public String toString() {
		return "DiaryAnamnesis [time=" + time + ", submitted=" + submitted + ", read=" + read + ", answer=" + answer + ", id=" + id + ", active=" + active + "]";
	}
}
