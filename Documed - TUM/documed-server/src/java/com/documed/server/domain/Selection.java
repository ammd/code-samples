package com.documed.server.domain;

import java.util.List;

/**
 * Domain for entity 'auswahl'
 * 
 * @author .
 */
public class Selection extends BaseDomain {

	private static final long serialVersionUID = 6474659258121864426L;

	private boolean multiselect;

	private List<SelectionAnswer> answers;
	private List<Question> questions;

	public Selection() {
	}

	public Selection(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, boolean multiselect, List<SelectionAnswer> answers, List<Question> questions) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.multiselect = multiselect;
		this.answers = answers;
		this.questions = questions;
	}

	public boolean isMultiselect() {
		return multiselect;
	}

	public void setMultiselect(boolean multiselect) {
		this.multiselect = multiselect;
	}

	public List<SelectionAnswer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<SelectionAnswer> answers) {
		this.answers = answers;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	@Override
	public String toString() {
		return "Selection [multiselect=" + multiselect + ", id=" + id + ", active=" + active + "]";
	}

}
