package com.documed.server.domain;

import java.util.Arrays;
import java.util.List;

/**
 * Domain for entity 'bildpunkt'
 * 
 * @author .
 */
public class PicturePoint extends BaseDomain {

	private static final long serialVersionUID = 4981652574558689452L;

	private byte[] image;

	private List<Question> questions;

	public PicturePoint() {
	}

	public PicturePoint(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, byte[] image, List<Question> questions) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.image = image;
		this.questions = questions;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	@Override
	public String toString() {
		return "PicturePoint [image=" + Arrays.toString(image) + ", id=" + id + ", active=" + active + "]";
	}

}
