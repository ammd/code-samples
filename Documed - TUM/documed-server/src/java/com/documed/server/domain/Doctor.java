package com.documed.server.domain;

import java.util.List;

/**
 * Domain for entity 'arzt'
 * 
 * @author .
 */
public class Doctor extends BaseDomain {

	private static final long serialVersionUID = -3571115792948975595L;

	private String name;
	private String address;
	private String phoneNo;
	private String email;
	private String ohMonday;
	private String ohTuesday;
	private String ohWednesday;
	private String ohThursday;
	private String ohFriday;
	private String ohSaturday;
	private String ohSunday;
	private String deviceId;

	private List<Department> departments;
	private List<QThread> currentThreads;
	private List<Doctor_QThread> historyThreads;

	public Doctor() {
	}

	public Doctor(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, String name, String address, String phoneNo, String email, String ohMonday, String ohTuesday, String ohWednesday, String ohThursday, String ohFriday, String ohSaturday, String ohSunday, String deviceId, List<Department> departments, List<QThread> currentThreads, List<Doctor_QThread> historyThreads) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.name = name;
		this.address = address;
		this.phoneNo = phoneNo;
		this.email = email;
		this.ohMonday = ohMonday;
		this.ohTuesday = ohTuesday;
		this.ohWednesday = ohWednesday;
		this.ohThursday = ohThursday;
		this.ohFriday = ohFriday;
		this.ohSaturday = ohSaturday;
		this.ohSunday = ohSunday;
		this.deviceId = deviceId;
		this.departments = departments;
		this.currentThreads = currentThreads;
		this.historyThreads = historyThreads;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public String getEmail() {
		return email;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public List<QThread> getCurrentThreads() {
		return currentThreads;
	}

	public void setCurrentThreads(List<QThread> currentThreads) {
		this.currentThreads = currentThreads;
	}

	public List<Doctor_QThread> getHistoryThreads() {
		return historyThreads;
	}

	public void setHistoryThreads(List<Doctor_QThread> historyThreads) {
		this.historyThreads = historyThreads;
	}

	public String getOhMonday() {
		return ohMonday;
	}

	public String getOhTuesday() {
		return ohTuesday;
	}

	public String getOhWednesday() {
		return ohWednesday;
	}

	public String getOhThursday() {
		return ohThursday;
	}

	public String getOhFriday() {
		return ohFriday;
	}

	public String getOhSaturday() {
		return ohSaturday;
	}

	public String getOhSunday() {
		return ohSunday;
	}

	public void setOhMonday(String ohMonday) {
		this.ohMonday = ohMonday;
	}

	public void setOhTuesday(String ohTuesday) {
		this.ohTuesday = ohTuesday;
	}

	public void setOhWednesday(String ohWednesday) {
		this.ohWednesday = ohWednesday;
	}

	public void setOhThursday(String ohThursday) {
		this.ohThursday = ohThursday;
	}

	public void setOhFriday(String ohFriday) {
		this.ohFriday = ohFriday;
	}

	public void setOhSaturday(String ohSaturday) {
		this.ohSaturday = ohSaturday;
	}

	public void setOhSunday(String ohSunday) {
		this.ohSunday = ohSunday;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public String toString() {
		return "Doctor [name=" + name + ", address=" + address + ", id=" + id + ", active=" + active + "]";
	}

}
