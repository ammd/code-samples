package com.documed.server.domain;

/**
 * 
 * @author .
 */
public enum QuestionType {
	TEXT, SELECTION, SLIDER, PICTUREPOINT;
}
