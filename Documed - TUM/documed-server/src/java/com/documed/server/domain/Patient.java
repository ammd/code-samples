package com.documed.server.domain;

import java.util.List;

/**
 * Domain for entity 'patient'
 * 
 * @author .
 */
public class Patient extends BaseDomain {

	private static final long serialVersionUID = -6683867049817320342L;

	private String deviceId;

	private Diary diary;
	private List<QThread> threads;

	public Patient() {
	}

	public Patient(int id, String createdBy, String modifiedBy, long createdAt, long modifiedAt, boolean active, String deviceId, Diary diary, List<QThread> threads) {
		super(id, createdBy, modifiedBy, createdAt, modifiedAt, active);
		this.deviceId = deviceId;
		this.diary = diary;
		this.threads = threads;
	}

	public Diary getDiary() {
		return diary;
	}

	public void setDiary(Diary diary) {
		this.diary = diary;
	}

	public List<QThread> getThreads() {
		return threads;
	}

	public void setThreads(List<QThread> threads) {
		this.threads = threads;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", active=" + active + "]";
	}
}
