package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.SelectionDAO;
import com.documed.server.domain.Selection;

/**
 * Test case for Selection DAO
 * 
 * @author .
 */
public class SelectionDAOTest {

	private static ApplicationContext context;
	private static SelectionDAO dao;
	private static Selection mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(SelectionDAO.class);
		assert (dao != null);

		mockObject = new Selection(0, null, null, 0L, 0L, true, false, null, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setActive(false);
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<Selection> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		Selection readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.isActive(), false);

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
