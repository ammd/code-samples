package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.SelectionAnswerDAO;
import com.documed.server.domain.SelectionAnswer;

/**
 * Test case for SelectionAnswer DAO
 * 
 * @author .
 */
public class SelectionAnswerDAOTest {

	private static ApplicationContext context;
	private static SelectionAnswerDAO dao;
	private static SelectionAnswer mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(SelectionAnswerDAO.class);
		assert (dao != null);

		mockObject = new SelectionAnswer(0, null, null, 0L, 0L, true, "asv1", new byte[]{}, false, false, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setTitle("asv2");
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<SelectionAnswer> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		SelectionAnswer readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.getTitle(), "asv2");

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
