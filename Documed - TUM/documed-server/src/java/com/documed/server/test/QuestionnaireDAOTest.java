package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.QuestionnaireDAO;
import com.documed.server.domain.Questionnaire;
import com.documed.server.domain.TimeInterval;

/**
 * Test case for Questionnaire DAO
 * 
 * @author .
 */
public class QuestionnaireDAOTest {

	private static ApplicationContext context;
	private static QuestionnaireDAO dao;
	private static Questionnaire mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(QuestionnaireDAO.class);
		assert (dao != null);

		mockObject = new Questionnaire(0, null, null, 0L, 0L, true, "fb1", "", TimeInterval.MONTHLY, 0, null, null, null, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setTimeInterval(TimeInterval.YEARLY);
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<Questionnaire> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		Questionnaire readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.getTimeInterval().name(), TimeInterval.YEARLY.name());

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
