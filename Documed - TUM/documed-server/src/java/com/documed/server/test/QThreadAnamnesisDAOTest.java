package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.QThreadAnamnesisDAO;
import com.documed.server.domain.QThreadAnamnesis;

/**
 * Test case for QThreadAnamnesis DAO
 * 
 * @author .
 */
public class QThreadAnamnesisDAOTest {

	private static ApplicationContext context;
	private static QThreadAnamnesisDAO dao;
	private static QThreadAnamnesis mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(QThreadAnamnesisDAO.class);
		assert (dao != null);

		mockObject = new QThreadAnamnesis(0, null, null, 0L, 0L, true, new Date().getTime(), true, false, "answer1", 0, null, 0, null, 0, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setAnswer("answer2");
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<QThreadAnamnesis> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		QThreadAnamnesis readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.getAnswer(), "answer2");

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
