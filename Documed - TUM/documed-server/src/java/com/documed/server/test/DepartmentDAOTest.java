package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.DepartmentDAO;
import com.documed.server.domain.Department;

/**
 * Test case for Department DAO
 * 
 * @author .
 */
public class DepartmentDAOTest {

	private static ApplicationContext context;
	private static DepartmentDAO dao;
	private static Department mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(DepartmentDAO.class);
		assert (dao != null);

		mockObject = new Department(0, null, null, 0L, 0L, true, "allgemein", null, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setName("allgemein2");
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<Department> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		Department readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.getName(), "allgemein2");

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
