package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.DoctorDAO;
import com.documed.server.domain.Doctor;

/**
 * Test case for Doctor DAO
 * 
 * @author .
 */
public class DoctorDAOTest {

	private static ApplicationContext context;
	private static DoctorDAO dao;
	private static Doctor mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(DoctorDAO.class);
		assert (dao != null);

		mockObject = new Doctor(0, null, null, 0L, 0L, true, "fornaro", "", "0176/99638267", "fornaro@doc.de", null, null, null, null, "08:00-12:00;15:00-17:00", null, "adfaf023945u0faw", null, null, null, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setName("soelder");
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<Doctor> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		Doctor readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.getName(), "soelder");

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
