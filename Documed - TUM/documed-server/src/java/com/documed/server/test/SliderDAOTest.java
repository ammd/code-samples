package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.SliderDAO;
import com.documed.server.domain.Slider;
import com.documed.server.domain.Unit;

/**
 * Test case for Slider DAO
 * 
 * @author .
 */
public class SliderDAOTest {

	private static ApplicationContext context;
	private static SliderDAO dao;
	private static Slider mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(SliderDAO.class);
		assert (dao != null);

		mockObject = new Slider(0, null, null, 0L, 0L, true, 0, 10, "a", "b", Unit.MM, 1.0, 0, 0, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setLabelMaximum("c");
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<Slider> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		Slider readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.getLabelMaximum(), "c");

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
