package com.documed.server.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * TestSuite for all tests
 * 
 * NOTE: All Foreign key checks must be disabled to perform these tests
 * 
 * @author .
 */
@RunWith(Suite.class)
@SuiteClasses({ DepartmentDAOTest.class, Diary_QThreadDAOTest.class, DiaryAnamnesisDAOTest.class, DiaryDAOTest.class, Doctor_QThreadDAOTest.class, DoctorDAOTest.class, PatientDAOTest.class, PicturePointDAOTest.class, QThreadAnamnesisDAOTest.class, QThreadDAOTest.class, QuestionDAOTest.class, Questionnaire_QThreadDAOTest.class, QuestionnaireDAOTest.class, SelectionAnswerDAOTest.class, SelectionDAOTest.class, SliderDAOTest.class })
public class AllTests {

}
