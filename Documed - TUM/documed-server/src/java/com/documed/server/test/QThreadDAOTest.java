package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.QThreadDAO;
import com.documed.server.domain.QThread;

/**
 * Test case for QThread DAO
 * 
 * @author .
 */
public class QThreadDAOTest {

	private static ApplicationContext context;
	private static QThreadDAO dao;
	private static QThread mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(QThreadDAO.class);
		assert (dao != null);

		mockObject = new QThread(0, null, null, 0L, 0L, true, "knie", new Date().getTime(), 0L, 0, null, 0, null, null, null, null, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setTitle("r�cken");
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<QThread> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		QThread readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.getTitle(), "r�cken");

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
