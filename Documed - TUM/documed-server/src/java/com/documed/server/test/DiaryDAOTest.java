package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.DiaryDAO;
import com.documed.server.domain.Diary;

/**
 * Test case for Diary DAO
 * 
 * @author .
 */
public class DiaryDAOTest {

	private static ApplicationContext context;
	private static DiaryDAO dao;
	private static Diary mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(DiaryDAO.class);
		assert (dao != null);

		mockObject = new Diary(0, null, null, 0L, 0L, true, 0, null, null, null, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setActive(false);
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<Diary> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		Diary readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.isActive(), false);

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
