package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.PatientDAO;
import com.documed.server.domain.Patient;

/**
 * Test case for Patient DAO
 * 
 * @author .
 */
public class PatientDAOTest {

	private static ApplicationContext context;
	private static PatientDAO dao;
	private static Patient mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(PatientDAO.class);
		assert (dao != null);

		mockObject = new Patient(0, null, null, 0L, 0L, true, "adfaf023945u0faw", null, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setActive(false);
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<Patient> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		Patient readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.isActive(), false);

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
