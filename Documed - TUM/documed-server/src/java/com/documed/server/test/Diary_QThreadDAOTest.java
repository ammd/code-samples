package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.Diary_QThreadDAO;
import com.documed.server.domain.Diary_QThread;

/**
 * Test case for Diary_QThread DAO
 * 
 * @author .
 */
public class Diary_QThreadDAOTest {

	private static ApplicationContext context;
	private static Diary_QThreadDAO dao;
	private static Diary_QThread mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		
		dao = context.getBean(Diary_QThreadDAO.class);
		assert (dao != null);

		mockObject = new Diary_QThread(1, null, 2, null, "cs", new Date().getTime());
	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.save(mockObject), true);

		// Update
		mockObject.setWarningConstraints("cs2");
		assertEquals(dao.update(mockObject), true);

		// Read All
		List<Diary_QThread> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		Diary_QThread readObject = dao.readById(readObjects.get(0).getThreadId(), readObjects.get(0).getDiaryId());
		assertEquals(readObject.getWarningConstraints(), "cs2");

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
