package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.Doctor_QThreadDAO;
import com.documed.server.domain.Doctor_QThread;

/**
 * Test case for Doctor_QThread DAO
 * 
 * @author .
 */
public class Doctor_QThreadDAOTest {

	private static ApplicationContext context;
	private static Doctor_QThreadDAO dao;
	private static Doctor_QThread mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		
		dao = context.getBean(Doctor_QThreadDAO.class);
		assert (dao != null);

		mockObject = new Doctor_QThread(1, null, 2, null, 0L);
	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.save(mockObject), true);

		// Update
		long time = new Date().getTime();
		mockObject.setTimeTo(time);
		assertEquals(dao.update(mockObject), true);

		// Read All
		List<Doctor_QThread> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		Doctor_QThread readObject = dao.readById(readObjects.get(0).getThreadId(), readObjects.get(0).getDoctorId());
		assertEquals(readObject.getTimeTo(), time);

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
