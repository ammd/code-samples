package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.DiaryAnamnesisDAO;
import com.documed.server.domain.DiaryAnamnesis;

/**
 * Test case for DiaryAnamnesis DAO
 * 
 * @author .
 */
public class DiaryAnamnesisDAOTest {

	private static ApplicationContext context;
	private static DiaryAnamnesisDAO dao;
	private static DiaryAnamnesis mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(DiaryAnamnesisDAO.class);
		assert (dao != null);

		mockObject = new DiaryAnamnesis(0, null, null, 0L, 0L, true, new Date().getTime(), true, false, "test", 0, null, 0, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setAnswer("test2");
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<DiaryAnamnesis> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		DiaryAnamnesis readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.getAnswer(), "test2");

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
