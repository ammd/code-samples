package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.PicturePointDAO;
import com.documed.server.domain.PicturePoint;

/**
 * Test case for PicturePoint DAO
 * 
 * @author .
 */
public class PicturePointDAOTest {

	private static ApplicationContext context;
	private static PicturePointDAO dao;
	private static PicturePoint mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(PicturePointDAO.class);
		assert (dao != null);
		mockObject = new PicturePoint(0, null, null, 0L, 0L, true, new byte[]{}, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setActive(false);
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<PicturePoint> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		PicturePoint readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.isActive(), false);

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
