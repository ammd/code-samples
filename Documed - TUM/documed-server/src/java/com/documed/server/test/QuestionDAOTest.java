package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.QuestionDAO;
import com.documed.server.domain.Question;
import com.documed.server.domain.QuestionType;

/**
 * Test case for Question DAO
 * 
 * @author .
 */
public class QuestionDAOTest {

	private static ApplicationContext context;
	private static QuestionDAO dao;
	private static Question mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		dao = context.getBean(QuestionDAO.class);
		assert (dao != null);

		mockObject = new Question(0, null, null, 0L, 0L, true, "q1", "", QuestionType.SLIDER, false, false, 0, null, 0, null, 0, null, null, null, null, null);

	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Update
		mockObject.setTitle("q2");
		assertEquals(dao.saveOrUpdate(mockObject), true);

		// Read All
		List<Question> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		Question readObject = dao.readById(readObjects.get(0).getId());
		assertEquals(readObject.getTitle(), "q2");

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
