package com.documed.server.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.documed.server.dao.Questionnaire_QThreadDAO;
import com.documed.server.domain.Questionnaire_QThread;

/**
 * Test case for Questionnaire_QThread DAO
 * 
 * @author .
 */
public class Questionnaire_QThreadDAOTest {

	private static ApplicationContext context;
	private static Questionnaire_QThreadDAO dao;
	private static Questionnaire_QThread mockObject;

	@BeforeClass
	public static void testSetup() {
		context = new FileSystemXmlApplicationContext(new String[] { "webapps/WEB-INF/applicationContext.xml" });
		
		dao = context.getBean(Questionnaire_QThreadDAO.class);
		assert (dao != null);

		mockObject = new Questionnaire_QThread(1, null, 2, null, "cs", new Date().getTime());
	}

	@AfterClass
	public static void testCleanup() {
		context = null;
	}

	@Test
	public void testCRUD() {

		// Create
		assertEquals(dao.save(mockObject), true);

		// Update
		mockObject.setWarningConstraints("cs2");
		assertEquals(dao.update(mockObject), true);

		// Read All
		List<Questionnaire_QThread> readObjects = dao.readAll();
		assertEquals(readObjects.size(), 1);

		// Read
		Questionnaire_QThread readObject = dao.readById(readObjects.get(0).getThreadId(), readObjects.get(0).getQuestionnaireId());
		assertEquals(readObject.getWarningConstraints(), "cs2");

		// Delete
		assertEquals(dao.delete(mockObject), true);
		readObjects = dao.readAll();
		assertEquals(readObjects.size(), 0);
	}

}
