package com.documed.server.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.documed.server.comm.CommonValues;
import com.documed.server.comm.Message;
import com.documed.server.comm.Result;
import com.documed.server.comm.Sender;
import com.documed.server.domain.DiaryAnamnesis;
import com.documed.server.domain.Doctor;
import com.documed.server.domain.Patient;
import com.documed.server.domain.PicturePoint;
import com.documed.server.domain.QThread;
import com.documed.server.domain.QThreadAnamnesis;
import com.documed.server.domain.Question;
import com.documed.server.domain.Questionnaire;
import com.documed.server.dto.DoctorRegistrationTokenDTO;
import com.documed.server.dto.DoctorThreadAssignmentDTO;
import com.documed.server.dto.PatientRegistrationTokenDTO;
import com.documed.server.dto.WardRoundDTO;
import com.documed.server.service.PersistenceService;
import com.google.gson.Gson;

/**
 * Handles each request to the server
 * 
 * @author .
 */
@Controller
@RequestMapping("/Main")
public class MainController {

	private final Log log = LogFactory.getLog(MainController.class);

	// GCM Constants
	private static final int MULTICAST_SIZE = 1000;
	private Sender sender;
	private static final Executor threadPool = Executors.newFixedThreadPool(5);

	// Controller
	public MainController() {

		// Create Sender, which holds the connection configuration for communication with the GCM servers
		// TODO: consider loading API key dynamically from project resources, as key should remain private
		// and therefore should not directly appear in the source code
		sender = new Sender("AIzaSyDxeezAYEp1qDpEaW5_jvjp63V-2iaVamg");
		log.info("***DocUMed Server Up and Running***");
	}

	@Autowired
	private PersistenceService persistenceService;

	/**
	 * 
	 * @throws IOException
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/echo")
	public @ResponseBody void echoMessage(@RequestParam("regId") String regid) throws IOException {

		Message message = new Message.Builder().addData(CommonValues.SP_RESPONSE_TOKEN, "Youve reached the echo server ...").build();
		Result result = sender.send(message, regid, 5);
		log.info("Sent message to device: " + result);
	}

	// @RequestMapping(method = RequestMethod.POST, value = "/registerPatient")
	// @ResponseBody
	// public void registerNewPatient(@RequestParam("deviceId") String deviceId) {
	//
	// try {
	//
	// PatientRegistrationTokenDTO regToken = persistenceService.registerNewPatient(deviceId);
	//
	// if (regToken != null) {
	//
	// String jsonToken = new Gson().toJson(regToken, PatientRegistrationTokenDTO.class);
	// Message message = new Message.Builder().addData(CommonValues.SP_REGISTRATION_TOKEN, jsonToken).build();
	//
	// Result result = sender.send(message, deviceId, 5);
	// log.info("Sent message to device: " + result);
	//
	// } else {
	// throw new Exception("Registration not successful: regToken is null");
	// }
	// } catch (IOException _e) {
	// log.error("Failed to send message", _e);
	// _e.printStackTrace();
	// } catch (Exception _e) {
	// log.error("Error @registerNewPatient", _e);
	// _e.printStackTrace();
	// }
	// }

	/**
	 * 
	 * Called when patient app gets started for the first time. Creates new patient and corresponding diary entry.
	 * 
	 * @param deviceId
	 * @return PatientRegistrationTokenDTO or null if not successful
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/RegisterPatient")
	public @ResponseBody PatientRegistrationTokenDTO registerNewPatient(@RequestBody String deviceId) {
		return persistenceService.registerNewPatient(deviceId);
	}

	/**
	 * @return list of all diary questions
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/LoadDiaryQuestions")
	public @ResponseBody List<Question> loadDiaryQuestions() {
		return persistenceService.loadDiaryQuestions();
	}

	/**
	 * Adds question to diary
	 * 
	 * @param diaryId
	 * @param questionId
	 * @return whether successfully or not
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/AddQuestionToDiary/{diaryId}")
	public @ResponseBody boolean addQuestionToDiary(@PathVariable int diaryId, @RequestParam int questionId) {
		return persistenceService.addQuestionToDiary(diaryId, questionId);
	}

	/**
	 * Removes question from diary
	 * 
	 * @param diaryId
	 * @param questionId
	 * @return whether successfully or not
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/RemoveQuestionFromDiary/{diaryId}")
	public @ResponseBody boolean removeQuestionFromDiary(@PathVariable int diaryId, @RequestParam int questionId) {
		return persistenceService.removeQuestionFromDiary(diaryId, questionId);
	}

	/**
	 * Submits diary anamnesis
	 * 
	 * @param diaryId
	 * @param questionId
	 * @param answer
	 * @return whether successfully or not
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/SubmitDiaryAnamnesis/{diaryId}")
	public @ResponseBody boolean submitDiaryAnamnesis(@PathVariable int diaryId, @RequestParam int questionId, @RequestBody String answer) {
		return persistenceService.submitDiaryAnamnesis(diaryId, questionId, answer);
	}

	/**
	 * 
	 * @param patientId
	 * @return list of all threads by given patientId
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/LoadPatientThreads")
	public @ResponseBody List<QThread> loadPatientThreads(@RequestParam int patientId) {
		return persistenceService.loadPatientThreads(patientId);
	}

	/**
	 * 
	 * @param threadId
	 * @return list of all questionnaires by given threadId
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/LoadQuestionnairesByThread")
	public @ResponseBody List<Questionnaire> loadQuestionnairesByThread(@RequestParam int threadId) {
		return persistenceService.loadQuestionnairesByThread(threadId);
	}

	/**
	 * Submits thread anamnesis
	 * 
	 * @param threadAnamnesisList
	 * @return whether successfully or not
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/SubmitThreadAnamnesis")
	public @ResponseBody boolean submitThreadAnamnesis(@RequestBody List<QThreadAnamnesis> threadAnamnesisList) {
		return persistenceService.submitThreadAnamnesis(threadAnamnesisList);
	}

	/**
	 * 
	 * Called when doctor app gets started for the first time. Creates new doctor entry.
	 * 
	 * @param deviceId
	 * @return doctorId or 0 if not successful
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/RegisterDoctor")
	public @ResponseBody int registerNewDoctor(@RequestBody DoctorRegistrationTokenDTO registrationToken) {
		return persistenceService.registerNewDoctor(registrationToken);
	}
	
	/**
	 * 
	 * Updates doctor instance
	 * 
	 * @param doctor
	 * @return whether successfully or not
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/UpdateDoctor")
	public @ResponseBody boolean updateDoctor(@RequestBody Doctor doctor) {
		return persistenceService.updateDoctor(doctor);
	}

	/**
	 * Assigns doctor and thread, if threadId is given update, othwerwise (threadId = 0) create and assign new thread if diary is not allowed, diaryId must be !!! 0
	 * 
	 * @param dta
	 * @return threadId or 0 if not successful
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/AssignDoctorToThread")
	public @ResponseBody int assignDoctorToThread(@RequestBody DoctorThreadAssignmentDTO dta) {
		return persistenceService.assignDoctorToThread(dta);
	}

	/**
	 * 
	 * @return all available questionnaire instances
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/LoadAllQuestionnaires")
	public @ResponseBody List<Questionnaire> loadAllQuestionnaires() {
		return persistenceService.loadAllQuestionnaires();
	}	

	/**
	 * Defines questionnaires for given thread
	 * 
	 * @param threadId
	 * @param questionnaires
	 * @return whether successfully or not
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/SetThreadQuestionnaires/{threadId}")
	public @ResponseBody boolean setThreadQuestionnaires(@PathVariable int threadId, @RequestBody List<Integer> questionnaireIds) {
		
		boolean rtrn = persistenceService.setThreadQuestionnaires(threadId, questionnaireIds);
		Patient patient = persistenceService.loadPatientByThreadId(threadId);
		
		try {
			if (rtrn && patient != null) {
				// push reload notification to patient
				Message message = new Message.Builder().addData(CommonValues.SP_RELOAD_TOKEN, "true").build();
				Result result = sender.send(message, patient.getDeviceId(), 5);
				log.info("Sent message to device: " + result);
			}
		} catch (IOException _e) {			
			log.error("Failed to send message", _e);
			_e.printStackTrace();
		}
		
		return rtrn;
	}

	/**
	 * USED BY PATIENT-APP
	 * 
	 * @param patientId
	 * @return diary anamnesis list by patientId
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/LoadFirstPicturePoint")
	public @ResponseBody PicturePoint loadFirstPicturePoint() {
		return persistenceService.loadFirstPicturePoint();
	}

	/**
	 * USED BY PATIENT-APP
	 * 
	 * @param patientId
	 * @return diary anamnesis list by patientId
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/LoadDiaryAnamnesisListByPatient")
	public @ResponseBody List<DiaryAnamnesis> loadDiaryAnamnesisListByPatient(@RequestParam int patientId) {
		return persistenceService.loadDiaryAnamnesisListByPatient(patientId);
	}

	/**
	 * USED BY DOCTOR-APP
	 * 
	 * @param patientId
	 * @param threadId
	 * @return diary anamnesis list by patientId
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/LoadDiaryAnamnesisListByPatient/{patientId}")
	public @ResponseBody List<DiaryAnamnesis> loadDiaryAnamnesisListByPatientAndThread(@PathVariable int patientId, @RequestParam int threadId) {
		return persistenceService.loadDiaryAnamnesisListByPatientAndThread(patientId, threadId);
	}

	/**
	 * 
	 * @param threadId
	 * @return thread anamnesis list by threadId
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/LoadThreadAnamnesisList")
	public @ResponseBody List<QThreadAnamnesis> loadThreadAnamnesisList(@RequestParam int threadId) {
		return persistenceService.loadThreadAnamnesisList(threadId);
	}

	/**
	 * submits wardround notification from doctor to patient
	 * 
	 * @param doctorId
	 * @param patientId
	 * @param subject
	 * @return whether successfully or not
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/NotifyWardround")
	public @ResponseBody boolean notifyWardround(@RequestParam int doctorId, @RequestParam int patientId, @RequestBody String subject) {

		log.info("wardround notification received doctor " + doctorId + " patient " + patientId + " subject " + subject);

		boolean success = false;

		try {
			
			// remove surrounding quotes
			if (subject != null && subject.startsWith("\"")) {
				subject = subject.substring(1, subject.length() - 1);
				subject = subject.replace("\\\"", "\"");
			}

			Doctor doctor = persistenceService.loadDoctorById(doctorId);
			Patient patient = persistenceService.loadPatientById(patientId);

			if (doctor != null && patient != null) {

				WardRoundDTO wardround = new WardRoundDTO();
				wardround.setAddress(doctor.getAddress());
				wardround.setDoctor(doctor.getName());
				wardround.setEmail(doctor.getEmail());
				wardround.setPhoneNo(doctor.getPhoneNo());
				wardround.setSubject(subject);
				wardround.setTime(new Date().getTime());
				wardround.setOhMonday(doctor.getOhMonday());
				wardround.setOhTuesday(doctor.getOhTuesday());
				wardround.setOhWednesday(doctor.getOhWednesday());
				wardround.setOhThursday(doctor.getOhThursday());
				wardround.setOhFriday(doctor.getOhFriday());
				wardround.setOhSaturday(doctor.getOhSaturday());
				wardround.setOhSunday(doctor.getOhSunday());

				// push wardround to patient
				String jsonToken = new Gson().toJson(wardround, WardRoundDTO.class);
				Message message = new Message.Builder().addData(CommonValues.SP_WARDROUND_TOKEN, jsonToken).build();

				Result result = sender.send(message, patient.getDeviceId(), 5);
				log.info("Sent message to device: " + result);
				
				success = true;
				
			} else {
				throw new Exception("Doctor or patient was not found in db");
			}
			
		} catch (IOException _e) {
			success = false;
			log.error("Failed to send message", _e);
			_e.printStackTrace();
		} catch (Exception _e) {
			success = false;
			_e.printStackTrace();
			log.error("exception occured @notifyWardround", _e);
		}

		return success;
	}

	/**
	 * notification that thread anamnesis was read by doctor
	 * 
	 * @param patientId
	 * @param threadId
	 * @param subject
	 * @return whether successfully or not
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/NotifyThreadAnamnesisRead/{threadId}")
	public @ResponseBody boolean notifyThreadAnamnesisRead(@PathVariable int threadId, @RequestParam int patientId) {

		log.info("anamnesis of thread " + threadId + " was read by doctor");

		boolean success = false;

		try {

			Patient patient = persistenceService.loadPatientById(patientId);
			
			if (patient != null) {
				// push questionnaire-read notification to patient
				Message message = new Message.Builder().addData(CommonValues.SP_QUESTIONNAIRE_READ_TOKEN, Integer.toString(threadId)).build();
				Result result = sender.send(message, patient.getDeviceId(), 5);
				log.info("Sent message to device: " + result);
				
				success = true;
			} else {
				throw new Exception("Patient not found by id " + patientId);
			}

		} catch (IOException _e) {	
			success = false;
			log.error("Failed to send message", _e);
			_e.printStackTrace();
		} catch (Exception _e) {
			success = false;
			_e.printStackTrace();
			log.error("exception occured @notifyThreadAnamnesisRead", _e);
		}

		return success;
	}

	/**
	 * Closes thread if doctor is allowed to do
	 * 
	 * @param doctorId
	 * @param threadId
	 * @return whether successfully or not
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/CloseThread/{threadId}")
	public @ResponseBody boolean closeThread(@PathVariable int threadId, @RequestParam int doctorId) {
		return persistenceService.closeThread(doctorId, threadId);
	}

	public PersistenceService getPersistenceService() {
		return persistenceService;
	}

	public void setPersistenceService(PersistenceService persistenceService) {
		this.persistenceService = persistenceService;
	}
}
