package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.Question;
import com.documed.server.domain.QuestionType;

/**
 * DAO for question
 * 
 * @author .
 */
@Repository
public class QuestionDAO extends BaseDAO<Question> {

	private final Log log = LogFactory.getLog(QuestionDAO.class);
	private static final String TABLE_NAME = "frage";
	private static final String TABLE_NAME_QUESTION_QUESTIONNAIRE = "frage_bogen";

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
	}

	/**
	 * Saves or updates question instance
	 * 
	 * @param question
	 * @return whether successfully or not
	 */
	@Override
	public boolean saveOrUpdate(Question question) {

		log.info("try to save question " + question);

		boolean success = false;

		try {

			// insert
			if (question.getId() == 0) {

				question.setCreatedAt(new Date().getTime());
				question.setCreatedBy(SYSTEM_NAME);

				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(11);
				params.put("erstellt_am", new Timestamp(question.getCreatedAt()));
				params.put("erstellt_von", question.getCreatedBy());
				params.put("aktiv", question.isActive());
				params.put("titel", question.getTitle());
				params.put("fragetyp", question.getQuestionType().name());
				params.put("zeitabhaengig", question.isTimeDependent());
				params.put("tagebuchfrage", question.isDiaryQuestion());
				params.put("quelle", question.getSource());
				params.put("auswahlid", question.getSelectionId());
				params.put("bildpunktid", question.getPicturePointId());
				params.put("sliderid", question.getSliderId());

				question.setId(insertTemplate.executeAndReturnKey(params).intValue());
				success = true;

				log.info("question inserted with id " + question.getId());
			}
			// update
			else {

				question.setModifiedAt(new Date().getTime());
				question.setModifiedBy(SYSTEM_NAME);

				jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ? , aktiv = ?, titel = ?, fragetyp = ?, zeitabhaengig = ?, tagebuchfrage = ?, quelle = ?, auswahlid = ?, bildpunktid = ?, sliderid = ? WHERE id = ?", new Object[] { new Timestamp(question.getModifiedAt()), question.getModifiedBy(), question.isActive(), question.getTitle(), question.getQuestionType().name(), question.isTimeDependent(), question.isDiaryQuestion(), question.getSource(), question.getSelectionId(), question.getPicturePointId(), question.getSliderId(), question.getId() });
				success = true;

				log.info("question updated with id " + question.getId());
			}

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads question by id
	 * 
	 * @param id
	 * @return question instance
	 */
	@Override
	public Question readById(int id) {

		log.info("try to read question by id " + id);

		Question rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, titel, fragetyp, zeitabhaengig, tagebuchfrage, quelle, auswahlid, bildpunktid, sliderid FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<Question>() {
				@Override
				public Question mapRow(ResultSet rs, int rownum) throws SQLException {

					QuestionType qType = rs.getString("fragetyp") != null && !rs.getString("fragetyp").isEmpty() ? QuestionType.valueOf(rs.getString("fragetyp")) : QuestionType.SLIDER;

					return new Question(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("titel"), rs.getString("quelle"), qType, rs.getBoolean("zeitabhaengig"), rs.getBoolean("tagebuchfrage"), rs.getInt("auswahlid"), null, rs.getInt("bildpunktid"), null, rs.getInt("sliderid"), null, null, null, null, null);
				}
			}, new Object[] { id });

			log.info("question read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all questions
	 * 
	 * @return list of questions
	 */
	@Override
	public List<Question> readAll() {

		log.info("try to read all questions");

		List<Question> rtrn = null;

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, titel, fragetyp, zeitabhaengig, tagebuchfrage, quelle, auswahlid, bildpunktid, sliderid FROM " + TABLE_NAME + " ORDER BY titel", new RowMapper<Question>() {

				@Override
				public Question mapRow(ResultSet rs, int rownum) throws SQLException {
					
					QuestionType qType = rs.getString("fragetyp") != null && !rs.getString("fragetyp").isEmpty() ? QuestionType.valueOf(rs.getString("fragetyp")) : QuestionType.SLIDER;

					return new Question(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("titel"), rs.getString("quelle"), qType, rs.getBoolean("zeitabhaengig"), rs.getBoolean("tagebuchfrage"), rs.getInt("auswahlid"), null, rs.getInt("bildpunktid"), null, rs.getInt("sliderid"), null, null, null, null, null);
				}
			});

			log.info("return " + rtrn.size() + " questions");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}
	
	/**
	 * Reads all diary questions
	 * 
	 * @return list of diary questions
	 */
	public List<Question> readAllDiaryQuestions() {

		log.info("try to read all diary questions");

		List<Question> rtrn = null;

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, titel, fragetyp, zeitabhaengig, tagebuchfrage, quelle, auswahlid, bildpunktid, sliderid FROM " + TABLE_NAME + " WHERE tagebuchfrage = true ORDER BY titel", new RowMapper<Question>() {

				@Override
				public Question mapRow(ResultSet rs, int rownum) throws SQLException {
					
					QuestionType qType = rs.getString("fragetyp") != null && !rs.getString("fragetyp").isEmpty() ? QuestionType.valueOf(rs.getString("fragetyp")) : QuestionType.SLIDER;

					return new Question(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("titel"), rs.getString("quelle"), qType, rs.getBoolean("zeitabhaengig"), rs.getBoolean("tagebuchfrage"), rs.getInt("auswahlid"), null, rs.getInt("bildpunktid"), null, rs.getInt("sliderid"), null, null, null, null, null);
				}
			});

			log.info("return " + rtrn.size() + " diary questions");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}
	
	/**
	 * Reads all questionnaire questions by questionnaire id
	 * 
	 * @param questionnaireId
	 * @return list of questionnaire questions
	 */
	public List<Question> readAllQuestionsByQuestionnaireId(final int questionnaireId) {

		log.info("try to read all questions by questionnaireId " + questionnaireId);

		List<Question> rtrn = null;

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT q.id qid, q.erstellt_am qerstellt_am, q.erstellt_von qerstellt_von, q.veraendert_am qveraendert_am, q.veraendert_von qveraendert_von, q.aktiv qaktiv, q.titel qtitel, q.fragetyp qfragetyp, q.zeitabhaengig qzeitabhaengig, q.tagebuchfrage qtagebuchfrage, q.quelle qquelle, q.auswahlid qauswahlid, q.bildpunktid qbildpunktid, q.sliderid qsliderid FROM " + TABLE_NAME + " q JOIN " + TABLE_NAME_QUESTION_QUESTIONNAIRE + " qq ON q.id = qq.frageid WHERE qq.bogenid = ? ORDER BY q.titel", new RowMapper<Question>() {

				@Override
				public Question mapRow(ResultSet rs, int rownum) throws SQLException {
					
					QuestionType qType = rs.getString("qfragetyp") != null && !rs.getString("qfragetyp").isEmpty() ? QuestionType.valueOf(rs.getString("qfragetyp")) : QuestionType.SLIDER;

					return new Question(rs.getInt("qid"), rs.getString("qerstellt_von"), rs.getString("qveraendert_von"), rs.getTimestamp("qerstellt_am") != null ? rs.getTimestamp("qerstellt_am").getTime() : 0L, rs.getTimestamp("qveraendert_am") != null ? rs.getTimestamp("qveraendert_am").getTime() : 0L, rs.getBoolean("qaktiv"), rs.getString("qtitel"), rs.getString("qquelle"), qType, rs.getBoolean("qzeitabhaengig"), rs.getBoolean("qtagebuchfrage"), rs.getInt("qauswahlid"), null, rs.getInt("qbildpunktid"), null, rs.getInt("qsliderid"), null, null, null, null, null);
				}
			}, new Object[] { questionnaireId });

			log.info("return " + rtrn.size() + " questions");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes question
	 * 
	 * @param question
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(Question question) {

		log.info("try to delete question " + question);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { question.getId() });
			success = true;

			log.info("question deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}
}
