package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.Slider;
import com.documed.server.domain.Unit;

/**
 * DAO for slider
 * 
 * @author .
 */
@Repository
public class SliderDAO extends BaseDAO<Slider> {

	private final Log log = LogFactory.getLog(SliderDAO.class);
	private static final String TABLE_NAME = "slider";	

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
	}

	/**
	 * Saves or updates slider instance
	 * 
	 * @param slider
	 * @return whether successfully or not
	 */
	@Override
	public boolean saveOrUpdate(Slider slider) {

		log.info("try to save slider " + slider);

		boolean success = false;

		try {

			// insert
			if (slider.getId() == 0) {

				slider.setCreatedAt(new Date().getTime());
				slider.setCreatedBy(SYSTEM_NAME);

				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(11);
				params.put("erstellt_am", new Timestamp(slider.getCreatedAt()));
				params.put("erstellt_von", slider.getCreatedBy());
				params.put("aktiv", slider.isActive());
				params.put("minimum", slider.getMinimum());
				params.put("maximum", slider.getMaximum());
				params.put("einheit", slider.getUnit().name());
				params.put("aufloesung", slider.getStepResolution());
				params.put("warnung_min", slider.getWarningMinimum());
				params.put("warnung_max", slider.getWarningMaximum());
				params.put("label_min", slider.getLabelMinimum());
				params.put("label_max", slider.getLabelMaximum());

				slider.setId(insertTemplate.executeAndReturnKey(params).intValue());
				success = true;

				log.info("slider inserted with id " + slider.getId());
			}
			// update
			else {

				slider.setModifiedAt(new Date().getTime());
				slider.setModifiedBy(SYSTEM_NAME);

				jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ? , aktiv = ?, minimum = ?, maximum = ?, einheit = ?, aufloesung = ?, warnung_min = ?, warnung_max = ?, label_min = ?, label_max = ? WHERE id = ?", new Object[] { new Timestamp(slider.getModifiedAt()), slider.getModifiedBy(), slider.isActive(), slider.getMinimum(), slider.getMaximum(), slider.getUnit().name(), slider.getStepResolution(), slider.getWarningMinimum(), slider.getWarningMaximum(), slider.getLabelMinimum(), slider.getLabelMaximum(), slider.getId() });
				success = true;

				log.info("slider updated with id " + slider.getId());
			}

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads slider by id
	 * 
	 * @param id
	 * @return slider instance
	 */
	@Override
	public Slider readById(int id) {

		log.info("try to read slider by id " + id);

		Slider rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, minimum, maximum, einheit, aufloesung, warnung_min, warnung_max, label_min, label_max FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<Slider>() {
				@Override
				public Slider mapRow(ResultSet rs, int rownum) throws SQLException {
					
					Unit unit = rs.getString("einheit") != null && !rs.getString("einheit").isEmpty() ? Unit.valueOf(rs.getString("einheit")) : Unit.POINTS;
					
					return new Slider(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getInt("minimum"), rs.getInt("maximum"), rs.getString("label_min"), rs.getString("label_max"), unit, rs.getDouble("aufloesung"), rs.getInt("warnung_min"), rs.getInt("warnung_max"), null);
				}
			}, new Object[] { id });

			log.info("slider read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all sliders
	 * 
	 * @return list of sliders
	 */
	@Override
	public List<Slider> readAll() {

		log.info("try to read all sliders");

		List<Slider> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, minimum, maximum, einheit, aufloesung, warnung_min, warnung_max, label_min, label_max FROM " + TABLE_NAME + " ORDER BY id", new RowMapper<Slider>() {

				@Override
				public Slider mapRow(ResultSet rs, int rownum) throws SQLException {

					Unit unit = rs.getString("einheit") != null && !rs.getString("einheit").isEmpty() ? Unit.valueOf(rs.getString("einheit")) : Unit.POINTS;
					
					return new Slider(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getInt("minimum"), rs.getInt("maximum"), rs.getString("label_min"), rs.getString("label_max"), unit, rs.getDouble("aufloesung"), rs.getInt("warnung_min"), rs.getInt("warnung_max"), null);
				}
			});			

			log.info("return " + rtrn.size() + " sliders");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes slider
	 * 
	 * @param slider
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(Slider slider) {

		log.info("try to delete slider " + slider);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { slider.getId() });
			success = true;

			log.info("slider deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}	
}
