package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.Diary_QThread;

/**
 * DAO for diary_qthread
 * 
 * @author .
 */
@Repository
public class Diary_QThreadDAO extends BaseDAO<Diary_QThread> {

	private final Log log = LogFactory.getLog(Diary_QThreadDAO.class);
	private static final String TABLE_NAME = "thread_tagebuch";

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME);
	}

	/**
	 * Saves or updates diary_qthread instance
	 * 
	 * @param diary_qthread
	 * @return whether successfully or not
	 */
	@Override
	@Deprecated
	public boolean saveOrUpdate(Diary_QThread diary_qthread) {
		return false;
	}

	/**
	 * Saves diary_qthread instance
	 * 
	 * @param diary_qthread
	 * @return whether successfully or not
	 */
	public boolean save(Diary_QThread diary_qthread) {
		log.info("try to save diary_qthread " + diary_qthread);

		boolean success = false;

		try {

			// generate insert map
			Map<String, Object> params = new HashMap<String, Object>(4);
			params.put("threadid", diary_qthread.getThreadId());
			params.put("tagebuchid", diary_qthread.getDiaryId());
			params.put("warnungsconstraints", diary_qthread.getWarningConstraints());
			params.put("zeitpunkt", diary_qthread.getTime() == 0 ? null : new Timestamp(diary_qthread.getTime()));

			insertTemplate.execute(params);
			success = true;

			log.info("diary_qthread inserted with threadid " + diary_qthread.getThreadId() + " and diaryid " + diary_qthread.getDiaryId());

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Updates diary_qthread instance
	 * 
	 * @param diary_qthread
	 * @return whether successfully or not
	 */
	public boolean update(Diary_QThread diary_qthread) {
		log.info("try to update diary_qthread " + diary_qthread);

		boolean success = false;

		try {

			jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET warnungsconstraints = ?, zeitpunkt = ? WHERE threadid = ? AND tagebuchid = ?", new Object[] { diary_qthread.getWarningConstraints(), diary_qthread.getTime() == 0 ? null : new Timestamp(diary_qthread.getTime()), diary_qthread.getThreadId(), diary_qthread.getDiaryId() });
			success = true;

			log.info("diary_qthread updated with threadid " + diary_qthread.getThreadId() + " and diaryid " + diary_qthread.getDiaryId());

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads diary_qthread by id
	 * 
	 * @param id
	 * @return diary_qthread instance
	 */
	@Override
	@Deprecated
	public Diary_QThread readById(int id) {
		return null;
	}

	/**
	 * Reads diary_qthread by threadId and diaryId
	 * 
	 * @param threadId
	 *            , diaryId
	 * @return diary_qthread instance
	 */
	public Diary_QThread readById(int threadId, int diaryId) {

		log.info("try to read diary_qthread by threadid " + threadId + " and diaryid " + diaryId);

		Diary_QThread rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT threadid, tagebuchid, warnungsconstraints, zeitpunkt FROM " + TABLE_NAME + " WHERE threadid = ? AND tagebuchid = ?", new RowMapper<Diary_QThread>() {
				@Override
				public Diary_QThread mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Diary_QThread(rs.getInt("tagebuchid"), null, rs.getInt("threadid"), null, rs.getString("warnungsconstraints"), rs.getTimestamp("zeitpunkt") != null ? rs.getTimestamp("zeitpunkt").getTime() : 0L);
				}
			}, new Object[] { threadId, diaryId });

			log.info("diary_qthread read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all diary_qthreads
	 * 
	 * @return list of diary_qthreads
	 */
	@Override
	public List<Diary_QThread> readAll() {

		log.info("try to read all diary_qthreads");

		List<Diary_QThread> rtrn = null;

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT threadid, tagebuchid, warnungsconstraints, zeitpunkt FROM " + TABLE_NAME + " ORDER BY zeitpunkt", new RowMapper<Diary_QThread>() {

				@Override
				public Diary_QThread mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Diary_QThread(rs.getInt("tagebuchid"), null, rs.getInt("threadid"), null, rs.getString("warnungsconstraints"), rs.getTimestamp("zeitpunkt") != null ? rs.getTimestamp("zeitpunkt").getTime() : 0L);
				}
			});

			log.info("return " + rtrn.size() + " diary_qthreads");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes diary_qthread
	 * 
	 * @param diary_qthread
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(Diary_QThread diary_qthread) {

		log.info("try to delete diary_qthread " + diary_qthread);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE threadid = ? AND tagebuchid = ?", new Object[] { diary_qthread.getThreadId(), diary_qthread.getDiaryId() });
			success = true;

			log.info("diary_qthread deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}
}
