package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.Doctor;

/**
 * DAO for doctor
 * 
 * @author .
 */
@Repository
public class DoctorDAO extends BaseDAO<Doctor> {

	private final Log log = LogFactory.getLog(DoctorDAO.class);
	private static final String TABLE_NAME = "arzt";	

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
	}

	/**
	 * Saves or updates doctor instance
	 * 
	 * @param doctor
	 * @return whether successfully or not
	 */
	@Override
	public boolean saveOrUpdate(Doctor doctor) {

		log.info("try to save doctor " + doctor);

		boolean success = false;

		try {

			// insert
			if (doctor.getId() == 0) {

				doctor.setCreatedAt(new Date().getTime());
				doctor.setCreatedBy(SYSTEM_NAME);

				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(5);
				params.put("erstellt_am", new Timestamp(doctor.getCreatedAt()));
				params.put("erstellt_von", doctor.getCreatedBy());
				params.put("aktiv", doctor.isActive());
				params.put("name", doctor.getName());
				params.put("adresse", doctor.getAddress());
				params.put("telefon", doctor.getPhoneNo());
				params.put("email", doctor.getEmail());
				params.put("oz_mo", doctor.getOhMonday());
				params.put("oz_di", doctor.getOhTuesday());
				params.put("oz_mi", doctor.getOhWednesday());
				params.put("oz_do", doctor.getOhThursday());
				params.put("oz_fr", doctor.getOhFriday());
				params.put("oz_sa", doctor.getOhSaturday());
				params.put("oz_so", doctor.getOhSunday());
				params.put("device_id", doctor.getDeviceId());

				doctor.setId(insertTemplate.executeAndReturnKey(params).intValue());
				success = true;

				log.info("doctor inserted with id " + doctor.getId());
			}
			// update
			else {

				doctor.setModifiedAt(new Date().getTime());
				doctor.setModifiedBy(SYSTEM_NAME);

				jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ? , aktiv = ?, name = ?, adresse = ?, telefon = ?, email = ?, oz_mo = ?, oz_di = ?, oz_mi = ?, oz_do = ?, oz_fr = ?, oz_sa = ?, oz_so = ?, device_id = ? WHERE id = ?", new Object[] { new Timestamp(doctor.getModifiedAt()), doctor.getModifiedBy(), doctor.isActive(), doctor.getName(), doctor.getAddress(), doctor.getPhoneNo(), doctor.getEmail(), doctor.getOhMonday(), doctor.getOhTuesday(), doctor.getOhWednesday(), doctor.getOhThursday(), doctor.getOhFriday(), doctor.getOhSaturday(), doctor.getOhSunday(), doctor.getDeviceId(), doctor.getId() });
				success = true;

				log.info("doctor updated with id " + doctor.getId());
			}

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads doctor by id
	 * 
	 * @param id
	 * @return doctor instance
	 */
	@Override
	public Doctor readById(int id) {

		log.info("try to read doctor by id " + id);

		Doctor rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, name, adresse, telefon, email, oz_mo, oz_di, oz_mi, oz_do, oz_fr, oz_sa, oz_so, device_id FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<Doctor>() {
				@Override
				public Doctor mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Doctor(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("name"), rs.getString("adresse"), rs.getString("telefon"), rs.getString("email"), rs.getString("oz_mo"), rs.getString("oz_di"), rs.getString("oz_mi"), rs.getString("oz_do"), rs.getString("oz_fr"), rs.getString("oz_sa"), rs.getString("oz_so"), rs.getString("device_id"), null, null, null);
				}
			}, new Object[] { id });

			log.info("doctor read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all doctors
	 * 
	 * @return list of doctors
	 */
	@Override
	public List<Doctor> readAll() {

		log.info("try to read all doctors");

		List<Doctor> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, name, adresse, telefon, email, oz_mo, oz_di, oz_mi, oz_do, oz_fr, oz_sa, oz_so, device_id FROM " + TABLE_NAME + " ORDER BY name", new RowMapper<Doctor>() {

				@Override
				public Doctor mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Doctor(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("name"), rs.getString("adresse"), rs.getString("telefon"), rs.getString("email"), rs.getString("oz_mo"), rs.getString("oz_di"), rs.getString("oz_mi"), rs.getString("oz_do"), rs.getString("oz_fr"), rs.getString("oz_sa"), rs.getString("oz_so"), rs.getString("device_id"), null, null, null);
				}
			});			

			log.info("return " + rtrn.size() + " doctors");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes doctor
	 * 
	 * @param doctor
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(Doctor doctor) {

		log.info("try to delete doctor " + doctor);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { doctor.getId() });
			success = true;

			log.info("doctor deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}	
}
