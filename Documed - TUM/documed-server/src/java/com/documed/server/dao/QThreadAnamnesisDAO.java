package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.documed.server.domain.QThreadAnamnesis;

/**
 * DAO for threadanamnesis
 * 
 * @author .
 */
@Repository
public class QThreadAnamnesisDAO extends BaseDAO<QThreadAnamnesis> {

	private final Log log = LogFactory.getLog(QThreadAnamnesisDAO.class);
	private static final String TABLE_NAME = "threadanamnese";

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
	}

	/**
	 * Saves or updates threadanamnesis instance
	 * 
	 * @param threadanamnesis
	 * @return whether successfully or not
	 */
	@Override
	@Transactional
	public boolean saveOrUpdate(QThreadAnamnesis threadanamnesis) {

		log.info("try to save threadanamnesis " + threadanamnesis);

		boolean success = false;

		// insert
		if (threadanamnesis.getId() == 0) {

			threadanamnesis.setCreatedAt(new Date().getTime());
			threadanamnesis.setCreatedBy(SYSTEM_NAME);

			// generate insert map
			Map<String, Object> params = new HashMap<String, Object>(10);
			params.put("erstellt_am", new Timestamp(threadanamnesis.getCreatedAt()));
			params.put("erstellt_von", threadanamnesis.getCreatedBy());
			params.put("aktiv", threadanamnesis.isActive());
			params.put("zeitpunkt", threadanamnesis.getTime() == 0 ? null : new Timestamp(threadanamnesis.getTime()));
			params.put("uebertragen", threadanamnesis.isSubmitted());
			params.put("gelesen", threadanamnesis.isRead());
			params.put("antwort", threadanamnesis.getAnswer());
			params.put("bogenid", threadanamnesis.getQuestionnaireId());
			params.put("frageid", threadanamnesis.getQuestionId());
			params.put("threadid", threadanamnesis.getThreadId());

			threadanamnesis.setId(insertTemplate.executeAndReturnKey(params).intValue());
			success = true;

			log.info("threadanamnesis inserted with id " + threadanamnesis.getId());
		}
		// update
		else {

			threadanamnesis.setModifiedAt(new Date().getTime());
			threadanamnesis.setModifiedBy(SYSTEM_NAME);

			jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ? , aktiv = ?, zeitpunkt = ?, uebertragen = ?, gelesen = ?, antwort = ?, bogenid = ?, frageid = ?, threadid = ? WHERE id = ?", new Object[] { new Timestamp(threadanamnesis.getModifiedAt()), threadanamnesis.getModifiedBy(), threadanamnesis.isActive(), threadanamnesis.getTime() == 0 ? null : new Timestamp(threadanamnesis.getTime()), threadanamnesis.isSubmitted(), threadanamnesis.isRead(), threadanamnesis.getAnswer(), threadanamnesis.getQuestionnaireId(), threadanamnesis.getQuestionId(), threadanamnesis.getThreadId(), threadanamnesis.getId() });
			success = true;

			log.info("threadanamnesis updated with id " + threadanamnesis.getId());
		}

		return success;
	}

	/**
	 * saves thread-anamnesis list as batch with transaction
	 * 
	 * @param threadanamnesisList
	 * @return whether successfully or not
	 */
	@Transactional
	public boolean saveOrUpdate(List<QThreadAnamnesis> threadanamnesisList) throws Exception {
		for (QThreadAnamnesis threadAnamnesis : threadanamnesisList) {
			if (!saveOrUpdate(threadAnamnesis)) {
				throw new Exception("threadAnamnesis not stored");
			}
		}
		return true;
	}

	/**
	 * Reads threadanamnesis by id
	 * 
	 * @param id
	 * @return threadanamnesis instance
	 */
	@Override
	public QThreadAnamnesis readById(int id) {

		log.info("try to read threadanamnesis by id " + id);

		QThreadAnamnesis rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, zeitpunkt, uebertragen, gelesen, antwort, bogenid, frageid, threadid FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<QThreadAnamnesis>() {
				@Override
				public QThreadAnamnesis mapRow(ResultSet rs, int rownum) throws SQLException {
					return new QThreadAnamnesis(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getTimestamp("zeitpunkt") != null ? rs.getTimestamp("zeitpunkt").getTime() : 0L, rs.getBoolean("uebertragen"), rs.getBoolean("gelesen"), rs.getString("antwort"), rs.getInt("frageid"), null, rs.getInt("bogenid"), null, rs.getInt("threadid"), null);
				}
			}, new Object[] { id });

			log.info("threadanamnesis read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all threadanamnesis
	 * 
	 * @return list of threadanamnesis
	 */
	@Override
	public List<QThreadAnamnesis> readAll() {

		log.info("try to read all threadanamnesis");

		List<QThreadAnamnesis> rtrn = null;

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, zeitpunkt, uebertragen, gelesen, antwort, bogenid, frageid, threadid FROM " + TABLE_NAME + " ORDER BY zeitpunkt", new RowMapper<QThreadAnamnesis>() {

				@Override
				public QThreadAnamnesis mapRow(ResultSet rs, int rownum) throws SQLException {
					return new QThreadAnamnesis(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getTimestamp("zeitpunkt") != null ? rs.getTimestamp("zeitpunkt").getTime() : 0L, rs.getBoolean("uebertragen"), rs.getBoolean("gelesen"), rs.getString("antwort"), rs.getInt("frageid"), null, rs.getInt("bogenid"), null, rs.getInt("threadid"), null);
				}
			});

			log.info("return " + rtrn.size() + " threadanamnesis");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}
	
	/**
	 * Reads all threadanamnesis by thread
	 * 
	 * @param threadId
	 * @return list of threadanamnesis
	 */
	public List<QThreadAnamnesis> readAllByThread(final int threadId) {

		log.info("try to read all threadanamnesis by thread " + threadId);

		List<QThreadAnamnesis> rtrn = null;

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, zeitpunkt, uebertragen, gelesen, antwort, bogenid, frageid, threadid FROM " + TABLE_NAME + " WHERE threadid = ? ORDER BY zeitpunkt", new RowMapper<QThreadAnamnesis>() {

				@Override
				public QThreadAnamnesis mapRow(ResultSet rs, int rownum) throws SQLException {
					return new QThreadAnamnesis(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getTimestamp("zeitpunkt") != null ? rs.getTimestamp("zeitpunkt").getTime() : 0L, rs.getBoolean("uebertragen"), rs.getBoolean("gelesen"), rs.getString("antwort"), rs.getInt("frageid"), null, rs.getInt("bogenid"), null, rs.getInt("threadid"), null);
				}
			}, new Object[] { threadId });

			log.info("return " + rtrn.size() + " threadanamnesis");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes threadanamnesis
	 * 
	 * @param threadanamnesis
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(QThreadAnamnesis threadanamnesis) {

		log.info("try to delete threadanamnesis " + threadanamnesis);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { threadanamnesis.getId() });
			success = true;

			log.info("threadanamnesis deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}
}
