package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.Doctor_QThread;

/**
 * DAO for doctor_qthread
 * 
 * @author .
 */
@Repository
public class Doctor_QThreadDAO extends BaseDAO<Doctor_QThread> {

	private final Log log = LogFactory.getLog(Doctor_QThreadDAO.class);
	private static final String TABLE_NAME = "arzt_thread";

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME);
	}

	/**
	 * Saves or updates doctor_qthread instance
	 * 
	 * @param doctor_qthread
	 * @return whether successfully or not
	 */
	@Override
	@Deprecated
	public boolean saveOrUpdate(Doctor_QThread doctor_qthread) {
		return false;
	}

	/**
	 * Saves doctor_qthread instance
	 * 
	 * @param doctor_qthread
	 * @return whether successfully or not
	 */
	public boolean save(Doctor_QThread doctor_qthread) {
		
		log.info("try to save doctor_qthread " + doctor_qthread);

		boolean success = false;

		try {

			// generate insert map
			Map<String, Object> params = new HashMap<String, Object>(4);
			params.put("threadid", doctor_qthread.getThreadId());
			params.put("arztid", doctor_qthread.getDoctorId());
			params.put("zeitpunkt_bis", doctor_qthread.getTimeTo() == 0 ? null : new Timestamp(doctor_qthread.getTimeTo()));

			insertTemplate.execute(params);
			success = true;

			log.info("doctor_qthread inserted with threadid " + doctor_qthread.getThreadId() + " and arztid " + doctor_qthread.getDoctorId());

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Updates doctor_qthread instance
	 * 
	 * @param doctor_qthread
	 * @return whether successfully or not
	 */
	public boolean update(Doctor_QThread doctor_qthread) {
		log.info("try to update doctor_qthread " + doctor_qthread);

		boolean success = false;

		try {

			jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET zeitpunkt_bis = ? WHERE threadid = ? AND arztid = ?", new Object[] {doctor_qthread.getTimeTo() == 0 ? null : new Timestamp(doctor_qthread.getTimeTo()), doctor_qthread.getThreadId(), doctor_qthread.getDoctorId() });
			success = true;

			log.info("doctor_qthread updated with threadid " + doctor_qthread.getThreadId() + " and doctorid " + doctor_qthread.getDoctorId());

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads doctor_qthread by id
	 * 
	 * @param id
	 * @return doctor_qthread instance
	 */
	@Override
	@Deprecated
	public Doctor_QThread readById(int id) {
		return null;
	}

	/**
	 * Reads doctor_qthread by threadId and doctorId
	 * 
	 * @param threadId, doctorId
	 * @return doctor_qthread instance
	 */
	public Doctor_QThread readById(int threadId, int doctorId) {

		log.info("try to read doctor_qthread by threadid " + threadId + " and doctorid " + doctorId);

		Doctor_QThread rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT threadid, arztid, zeitpunkt_bis FROM " + TABLE_NAME + " WHERE threadid = ? AND arztid = ?", new RowMapper<Doctor_QThread>() {
				@Override
				public Doctor_QThread mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Doctor_QThread(rs.getInt("arztid"), null, rs.getInt("threadid"), null, rs.getTimestamp("zeitpunkt_bis") != null ? rs.getTimestamp("zeitpunkt_bis").getTime() : 0L);
				}
			}, new Object[] { threadId, doctorId });

			log.info("doctor_qthread read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all doctor_qthreads
	 * 
	 * @return list of doctor_qthreads
	 */
	@Override
	public List<Doctor_QThread> readAll() {

		log.info("try to read all doctor_qthreads");

		List<Doctor_QThread> rtrn = null;

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT threadid, arztid, zeitpunkt_bis FROM " + TABLE_NAME + " ORDER BY zeitpunkt_bis", new RowMapper<Doctor_QThread>() {

				@Override
				public Doctor_QThread mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Doctor_QThread(rs.getInt("arztid"), null, rs.getInt("threadid"), null, rs.getTimestamp("zeitpunkt_bis") != null ? rs.getTimestamp("zeitpunkt_bis").getTime() : 0L);
				}
			});

			log.info("return " + rtrn.size() + " doctor_qthreads");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}
	
	/**
	 * Reads all doctor_qthreads by given threadId
	 *
	 * @param threadId
	 * @return list of doctor_qthreads
	 */
	public List<Doctor_QThread> readAllByThreadId(int threadId) {

		log.info("try to read all doctor_qthreads by threadId " + threadId);

		List<Doctor_QThread> rtrn = null;

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT threadid, arztid, zeitpunkt_bis FROM " + TABLE_NAME + " WHERE threadid = ? ORDER BY zeitpunkt_bis", new RowMapper<Doctor_QThread>() {

				@Override
				public Doctor_QThread mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Doctor_QThread(rs.getInt("arztid"), null, rs.getInt("threadid"), null, rs.getTimestamp("zeitpunkt_bis") != null ? rs.getTimestamp("zeitpunkt_bis").getTime() : 0L);
				}
			}, new Object[] { threadId });

			log.info("return " + rtrn.size() + " doctor_qthreads");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes doctor_qthread
	 * 
	 * @param doctor_qthread
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(Doctor_QThread doctor_qthread) {

		log.info("try to delete doctor_qthread " + doctor_qthread);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE threadid = ? AND arztid = ?", new Object[] { doctor_qthread.getThreadId(), doctor_qthread.getDoctorId() });
			success = true;

			log.info("doctor_qthread deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}
}
