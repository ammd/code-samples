package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.Department;

/**
 * DAO for department
 * 
 * @author .
 */
@Repository
public class DepartmentDAO extends BaseDAO<Department> {

	private final Log log = LogFactory.getLog(DepartmentDAO.class);
	private static final String TABLE_NAME = "fachbereich";	
		
	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
	}

	/**
	 * Saves or updates department instance
	 * 
	 * @param department
	 * @return whether successfully or not
	 */
	@Override
	public boolean saveOrUpdate(Department department) {

		log.info("try to save department " + department);

		boolean success = false;

		try {

			// insert
			if (department.getId() == 0) {

				department.setCreatedAt(new Date().getTime());
				department.setCreatedBy(SYSTEM_NAME);

				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(4);
				params.put("erstellt_am", new Timestamp(department.getCreatedAt()));
				params.put("erstellt_von", department.getCreatedBy());
				params.put("aktiv", department.isActive());
				params.put("name", department.getName());

				department.setId(insertTemplate.executeAndReturnKey(params).intValue());
				success = true;

				log.info("department inserted with id " + department.getId());
			}
			// update
			else {

				department.setModifiedAt(new Date().getTime());
				department.setModifiedBy(SYSTEM_NAME);

				jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ? , aktiv = ?, name = ? WHERE id = ?", new Object[] { new Timestamp(department.getModifiedAt()), department.getModifiedBy(), department.isActive(), department.getName(), department.getId() });
				success = true;

				log.info("department updated with id " + department.getId());
			}

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads department by id
	 * 
	 * @param id
	 * @return department instance
	 */
	@Override
	public Department readById(int id) {

		log.info("try to read department by id " + id);

		Department rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, name FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<Department>() {
				@Override
				public Department mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Department(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("name"), null, null);
				}
			}, new Object[] { id });

			log.info("department read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all departments
	 * 
	 * @return list of departments
	 */
	@Override
	public List<Department> readAll() {

		log.info("try to read all departments");

		List<Department> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, name FROM " + TABLE_NAME + " ORDER BY name", new RowMapper<Department>() {

				@Override
				public Department mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Department(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("name"), null, null);
				}
			});			

			log.info("return " + rtrn.size() + " departments");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes department
	 * 
	 * @param department
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(Department department) {

		log.info("try to delete department " + department);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { department.getId() });
			success = true;

			log.info("department deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}	
}
