package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.DiaryAnamnesis;

/**
 * DAO for diaryanamnesis
 * 
 * @author .
 */
@Repository
public class DiaryAnamnesisDAO extends BaseDAO<DiaryAnamnesis> {

	private final Log log = LogFactory.getLog(DiaryAnamnesisDAO.class);
	private static final String TABLE_NAME = "tagebuchanamnese";	

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
	}

	/**
	 * Saves or updates diaryanamnesis instance
	 * 
	 * @param diaryanamnesis
	 * @return whether successfully or not
	 */
	@Override
	public boolean saveOrUpdate(DiaryAnamnesis diaryanamnesis) {

		log.info("try to save diaryanamnesis " + diaryanamnesis);

		boolean success = false;

		try {

			// insert
			if (diaryanamnesis.getId() == 0) {

				diaryanamnesis.setCreatedAt(new Date().getTime());
				diaryanamnesis.setCreatedBy(SYSTEM_NAME);

				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(9);
				params.put("erstellt_am", new Timestamp(diaryanamnesis.getCreatedAt()));
				params.put("erstellt_von", diaryanamnesis.getCreatedBy());
				params.put("aktiv", diaryanamnesis.isActive());
				params.put("zeitpunkt", diaryanamnesis.getTime() == 0 ? null : new Timestamp(diaryanamnesis.getTime()));
				params.put("uebertragen", diaryanamnesis.isSubmitted());
				params.put("gelesen", diaryanamnesis.isRead());
				params.put("antwort", diaryanamnesis.getAnswer());
				params.put("frageid", diaryanamnesis.getQuestionId());
				params.put("tagebuchid", diaryanamnesis.getDiaryId());

				diaryanamnesis.setId(insertTemplate.executeAndReturnKey(params).intValue());
				success = true;

				log.info("diaryanamnesis inserted with id " + diaryanamnesis.getId());
			}
			// update
			else {

				diaryanamnesis.setModifiedAt(new Date().getTime());
				diaryanamnesis.setModifiedBy(SYSTEM_NAME);

				jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ? , aktiv = ?, zeitpunkt = ?, uebertragen = ?, gelesen = ?, antwort = ?, frageid = ?, tagebuchid = ? WHERE id = ?", new Object[] { new Timestamp(diaryanamnesis.getModifiedAt()), diaryanamnesis.getModifiedBy(), diaryanamnesis.isActive(), diaryanamnesis.getTime() == 0 ? null : new Timestamp(diaryanamnesis.getTime()), diaryanamnesis.isSubmitted(), diaryanamnesis.isRead(), diaryanamnesis.getAnswer(), diaryanamnesis.getQuestionId(), diaryanamnesis.getDiaryId(), diaryanamnesis.getId() });
				success = true;

				log.info("diaryanamnesis updated with id " + diaryanamnesis.getId());
			}

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads diaryanamnesis by id
	 * 
	 * @param id
	 * @return diaryanamnesis instance
	 */
	@Override
	public DiaryAnamnesis readById(int id) {

		log.info("try to read diaryanamnesis by id " + id);

		DiaryAnamnesis rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, zeitpunkt, uebertragen, gelesen, antwort, frageid, tagebuchid FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<DiaryAnamnesis>() {
				@Override
				public DiaryAnamnesis mapRow(ResultSet rs, int rownum) throws SQLException {
					return new DiaryAnamnesis(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getTimestamp("zeitpunkt") != null ? rs.getTimestamp("zeitpunkt").getTime() : 0L, rs.getBoolean("uebertragen"), rs.getBoolean("gelesen"), rs.getString("antwort"), rs.getInt("tagebuchid"), null, rs.getInt("frageid"), null);
				}
			}, new Object[] { id });

			log.info("diaryanamnesis read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all diaryanamnesis
	 * 
	 * @return list of diaryanamnesis
	 */
	@Override
	public List<DiaryAnamnesis> readAll() {

		log.info("try to read all diaryanamnesis");

		List<DiaryAnamnesis> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, zeitpunkt, uebertragen, gelesen, antwort, frageid, tagebuchid FROM " + TABLE_NAME + " ORDER BY zeitpunkt", new RowMapper<DiaryAnamnesis>() {

				@Override
				public DiaryAnamnesis mapRow(ResultSet rs, int rownum) throws SQLException {
					return new DiaryAnamnesis(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getTimestamp("zeitpunkt") != null ? rs.getTimestamp("zeitpunkt").getTime() : 0L, rs.getBoolean("uebertragen"), rs.getBoolean("gelesen"), rs.getString("antwort"), rs.getInt("tagebuchid"), null, rs.getInt("frageid"), null);
				}
			});			

			log.info("return " + rtrn.size() + " diaryanamnesis");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}
	
	/**
	 * Reads all diaryanamnesis by diaryId
	 * 
	 * @param diaryId
	 * @return list of diaryanamnesis
	 */
	public List<DiaryAnamnesis> readAllByDiary(final int diaryId) {

		log.info("try to read all diaryanamnesis by diary " + diaryId);

		List<DiaryAnamnesis> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, zeitpunkt, uebertragen, gelesen, antwort, frageid, tagebuchid FROM " + TABLE_NAME + " WHERE tagebuchid = ? ORDER BY zeitpunkt", new RowMapper<DiaryAnamnesis>() {

				@Override
				public DiaryAnamnesis mapRow(ResultSet rs, int rownum) throws SQLException {
					return new DiaryAnamnesis(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getTimestamp("zeitpunkt") != null ? rs.getTimestamp("zeitpunkt").getTime() : 0L, rs.getBoolean("uebertragen"), rs.getBoolean("gelesen"), rs.getString("antwort"), rs.getInt("tagebuchid"), null, rs.getInt("frageid"), null);
				}
			}, new Object[] { diaryId });

			log.info("return " + rtrn.size() + " diaryanamnesis");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes diaryanamnesis
	 * 
	 * @param diaryanamnesis
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(DiaryAnamnesis diaryanamnesis) {

		log.info("try to delete diaryanamnesis " + diaryanamnesis);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { diaryanamnesis.getId() });
			success = true;

			log.info("diaryanamnesis deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}	
}
