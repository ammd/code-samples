package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.Selection;

/**
 * DAO for selection
 * 
 * @author .
 */
@Repository
public class SelectionDAO extends BaseDAO<Selection> {

	private final Log log = LogFactory.getLog(SelectionDAO.class);
	private static final String TABLE_NAME = "auswahl";	

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
	}

	/**
	 * Saves or updates selection instance
	 * 
	 * @param selection
	 * @return whether successfully or not
	 */
	@Override
	public boolean saveOrUpdate(Selection selection) {

		log.info("try to save selection " + selection);

		boolean success = false;

		try {

			// insert
			if (selection.getId() == 0) {

				selection.setCreatedAt(new Date().getTime());
				selection.setCreatedBy(SYSTEM_NAME);

				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(4);
				params.put("erstellt_am", new Timestamp(selection.getCreatedAt()));
				params.put("erstellt_von", selection.getCreatedBy());
				params.put("aktiv", selection.isActive());
				params.put("multiselektierbar", selection.isMultiselect());

				selection.setId(insertTemplate.executeAndReturnKey(params).intValue());
				success = true;

				log.info("selection inserted with id " + selection.getId());
			}
			// update
			else {

				selection.setModifiedAt(new Date().getTime());
				selection.setModifiedBy(SYSTEM_NAME);

				jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ? , aktiv = ?, multiselektierbar = ? WHERE id = ?", new Object[] { new Timestamp(selection.getModifiedAt()), selection.getModifiedBy(), selection.isActive(), selection.isMultiselect(), selection.getId() });
				success = true;

				log.info("selection updated with id " + selection.getId());
			}

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads selection by id
	 * 
	 * @param id
	 * @return selection instance
	 */
	@Override
	public Selection readById(int id) {

		log.info("try to read selection by id " + id);

		Selection rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, multiselektierbar FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<Selection>() {
				@Override
				public Selection mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Selection(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getBoolean("multiselektierbar"), null, null);
				}
			}, new Object[] { id });

			log.info("selection read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all selections
	 * 
	 * @return list of selections
	 */
	@Override
	public List<Selection> readAll() {

		log.info("try to read all selections");

		List<Selection> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, multiselektierbar FROM " + TABLE_NAME + " ORDER BY id", new RowMapper<Selection>() {

				@Override
				public Selection mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Selection(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getBoolean("multiselektierbar"), null, null);
				}
			});			

			log.info("return " + rtrn.size() + " selections");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes selection
	 * 
	 * @param selection
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(Selection selection) {

		log.info("try to delete selection " + selection);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { selection.getId() });
			success = true;

			log.info("selection deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}	
}
