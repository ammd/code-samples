package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.QThread;

/**
 * DAO for thread
 * 
 * @author .
 */
@Repository
public class QThreadDAO extends BaseDAO<QThread> {

	private final Log log = LogFactory.getLog(QThreadDAO.class);
	private static final String TABLE_NAME = "thread";	

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
	}

	/**
	 * Saves or updates thread instance
	 * 
	 * @param thread
	 * @return whether successfully or not
	 */
	@Override
	public boolean saveOrUpdate(QThread thread) {

		log.info("try to save thread " + thread);

		boolean success = false;

		try {

			// insert
			if (thread.getId() == 0) {

				thread.setCreatedAt(new Date().getTime());
				thread.setCreatedBy(SYSTEM_NAME);

				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(8);
				params.put("erstellt_am", new Timestamp(thread.getCreatedAt()));
				params.put("erstellt_von", thread.getCreatedBy());
				params.put("aktiv", thread.isActive());
				params.put("titel", thread.getTitle());
				params.put("beginn", thread.getBegin() == 0 ? null : new Timestamp(thread.getBegin()));
				params.put("ende", thread.getEnd() == 0 ? null : new Timestamp(thread.getEnd()));
				params.put("arztid", thread.getDoctorId());
				params.put("patientid", thread.getPatientId());

				thread.setId(insertTemplate.executeAndReturnKey(params).intValue());
				success = true;

				log.info("thread inserted with id " + thread.getId());
			}
			// update
			else {

				thread.setModifiedAt(new Date().getTime());
				thread.setModifiedBy(SYSTEM_NAME);

				jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ? , aktiv = ?, titel = ?, beginn = ?, ende = ?, arztid = ?, patientid = ? WHERE id = ?", new Object[] { new Timestamp(thread.getModifiedAt()), thread.getModifiedBy(), thread.isActive(), thread.getTitle(), thread.getBegin() == 0 ? null : new Timestamp(thread.getBegin()), thread.getEnd() == 0 ? null : new Timestamp(thread.getEnd()), thread.getDoctorId(), thread.getPatientId(), thread.getId() });
				success = true;

				log.info("thread updated with id " + thread.getId());
			}

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads thread by id
	 * 
	 * @param id
	 * @return thread instance
	 */
	@Override
	public QThread readById(int id) {

		log.info("try to read thread by id " + id);

		QThread rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, titel, beginn, ende, arztid, patientid FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<QThread>() {
				@Override
				public QThread mapRow(ResultSet rs, int rownum) throws SQLException {
					return new QThread(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("titel"), rs.getTimestamp("beginn") != null ? rs.getTimestamp("beginn").getTime() : 0L, rs.getTimestamp("ende") != null ? rs.getTimestamp("ende").getTime() : 0L, rs.getInt("arztid"), null, rs.getInt("patientid"), null, null, null, null, null);
				}
			}, new Object[] { id });

			log.info("thread read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all threads
	 * 
	 * @return list of threads
	 */
	@Override
	public List<QThread> readAll() {

		log.info("try to read all threads");

		List<QThread> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, titel, beginn, ende, arztid, patientid FROM " + TABLE_NAME + " ORDER BY beginn", new RowMapper<QThread>() {

				@Override
				public QThread mapRow(ResultSet rs, int rownum) throws SQLException {
					return new QThread(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("titel"), rs.getTimestamp("beginn") != null ? rs.getTimestamp("beginn").getTime() : 0L, rs.getTimestamp("ende") != null ? rs.getTimestamp("ende").getTime() : 0L, rs.getInt("arztid"), null, rs.getInt("patientid"), null, null, null, null, null);
				}
			});			

			log.info("return " + rtrn.size() + " threads");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}
	
	/**
	 * Reads all threads by given patient
	 * 
	 * @param patientId
	 * @return list of threads
	 */
	public List<QThread> readAllByPatient(final int patientId) {

		log.info("try to read all threads by patient " + patientId);

		List<QThread> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, titel, beginn, ende, arztid, patientid FROM " + TABLE_NAME + " WHERE patientid = ? ORDER BY beginn", new RowMapper<QThread>() {

				@Override
				public QThread mapRow(ResultSet rs, int rownum) throws SQLException {
					return new QThread(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("titel"), rs.getTimestamp("beginn") != null ? rs.getTimestamp("beginn").getTime() : 0L, rs.getTimestamp("ende") != null ? rs.getTimestamp("ende").getTime() : 0L, rs.getInt("arztid"), null, rs.getInt("patientid"), null, null, null, null, null);
				}
			}, new Object[] { patientId });

			log.info("return " + rtrn.size() + " threads");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes thread
	 * 
	 * @param thread
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(QThread thread) {

		log.info("try to delete thread " + thread);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { thread.getId() });
			success = true;

			log.info("thread deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}	
}
