package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.documed.server.domain.Questionnaire_QThread;

/**
 * DAO for questionnaire_qthread
 * 
 * @author .
 */
@Repository
public class Questionnaire_QThreadDAO extends BaseDAO<Questionnaire_QThread> {

	private final Log log = LogFactory.getLog(Questionnaire_QThreadDAO.class);
	private static final String TABLE_NAME = "bogen_thread";

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME);
	}

	/**
	 * Saves or updates questionnaire_qthread instance
	 * 
	 * @param questionnaire_qthread
	 * @return whether successfully or not
	 */
	@Override
	@Deprecated
	public boolean saveOrUpdate(Questionnaire_QThread questionnaire_qthread) {
		return false;
	}

	/**
	 * Saves questionnaire_qthread instance
	 * 
	 * @param questionnaire_qthread
	 * @return whether successfully or not
	 */
	@Transactional
	public boolean save(Questionnaire_QThread questionnaire_qthread) {

		log.info("try to save questionnaire_qthread " + questionnaire_qthread);

		boolean success = false;

		try {

			// generate insert map
			Map<String, Object> params = new HashMap<String, Object>(4);
			params.put("threadid", questionnaire_qthread.getThreadId());
			params.put("bogenid", questionnaire_qthread.getQuestionnaireId());
			params.put("warnungsconstraints", questionnaire_qthread.getWarningConstraints());
			params.put("zeitpunkt", questionnaire_qthread.getTime() == 0 ? null : new Timestamp(questionnaire_qthread.getTime()));

			insertTemplate.execute(params);
			success = true;

			log.info("questionnaire_qthread inserted with threadid " + questionnaire_qthread.getThreadId() + " and bogenid " + questionnaire_qthread.getQuestionnaireId());

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Updates questionnaire_qthread instance
	 * 
	 * @param questionnaire_qthread
	 * @return whether successfully or not
	 */
	public boolean update(Questionnaire_QThread questionnaire_qthread) {
		log.info("try to update questionnaire_qthread " + questionnaire_qthread);

		boolean success = false;

		try {

			jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET warnungsconstraints = ?, zeitpunkt = ? WHERE threadid = ? AND bogenid = ?", new Object[] { questionnaire_qthread.getWarningConstraints(), questionnaire_qthread.getTime() == 0 ? null : new Timestamp(questionnaire_qthread.getTime()), questionnaire_qthread.getThreadId(), questionnaire_qthread.getQuestionnaireId() });
			success = true;

			log.info("questionnaire_qthread updated with threadid " + questionnaire_qthread.getThreadId() + " and bogenid " + questionnaire_qthread.getQuestionnaireId());

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads questionnaire_qthread by id
	 * 
	 * @param id
	 * @return questionnaire_qthread instance
	 */
	@Override
	@Deprecated
	public Questionnaire_QThread readById(int id) {
		return null;
	}

	/**
	 * Reads questionnaire_qthread by threadId and bogenId
	 * 
	 * @param threadId
	 *            , bogenId
	 * @return questionnaire_qthread instance
	 */
	public Questionnaire_QThread readById(int threadId, int bogenId) {

		log.info("try to read questionnaire_qthread by threadid " + threadId + " and bogenid " + bogenId);

		Questionnaire_QThread rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT threadid, bogenid, warnungsconstraints, zeitpunkt FROM " + TABLE_NAME + " WHERE threadid = ? AND bogenid = ?", new RowMapper<Questionnaire_QThread>() {
				@Override
				public Questionnaire_QThread mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Questionnaire_QThread(rs.getInt("bogenid"), null, rs.getInt("threadid"), null, rs.getString("warnungsconstraints"), rs.getTimestamp("zeitpunkt") != null ? rs.getTimestamp("zeitpunkt").getTime() : 0L);
				}
			}, new Object[] { threadId, bogenId });

			log.info("questionnaire_qthread read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all questionnaire_qthreads
	 * 
	 * @return list of questionnaire_qthreads
	 */
	@Override
	public List<Questionnaire_QThread> readAll() {

		log.info("try to read all questionnaire_qthreads");

		List<Questionnaire_QThread> rtrn = null;

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT threadid, bogenid, warnungsconstraints, zeitpunkt FROM " + TABLE_NAME + " ORDER BY zeitpunkt", new RowMapper<Questionnaire_QThread>() {

				@Override
				public Questionnaire_QThread mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Questionnaire_QThread(rs.getInt("bogenid"), null, rs.getInt("threadid"), null, rs.getString("warnungsconstraints"), rs.getTimestamp("zeitpunkt") != null ? rs.getTimestamp("zeitpunkt").getTime() : 0L);
				}
			});

			log.info("return " + rtrn.size() + " questionnaire_qthreads");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all questionnaire_qthreads by threadId
	 * 
	 * @param threadId
	 * @return list of questionnaire_qthreads
	 */
	public List<Questionnaire_QThread> readAllByThreadId(final int threadId) {

		log.info("try to read all questionnaire_qthreads by threadId " + threadId);

		List<Questionnaire_QThread> rtrn = null;

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT threadid, bogenid, warnungsconstraints, zeitpunkt FROM " + TABLE_NAME + " WHERE threadid = ? ORDER BY zeitpunkt", new RowMapper<Questionnaire_QThread>() {

				@Override
				public Questionnaire_QThread mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Questionnaire_QThread(rs.getInt("bogenid"), null, rs.getInt("threadid"), null, rs.getString("warnungsconstraints"), rs.getTimestamp("zeitpunkt") != null ? rs.getTimestamp("zeitpunkt").getTime() : 0L);
				}
			}, new Object[] { threadId });

			log.info("return " + rtrn.size() + " questionnaire_qthreads");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Defines questionnaires for given thread
	 * 
	 * @param threadId
	 * @param questionnaires
	 * @return whether successfully or not
	 */
	@Transactional
	public boolean setThreadQuestionnaires(int threadId, List<Integer> questionnaireIds) throws Exception {

		log.info("try to set questionnaires for thread " + threadId);
		
		List<Questionnaire_QThread> currentQuestionnaires = readAllByThreadId(threadId);
		for (Questionnaire_QThread currentQuestionnaire : currentQuestionnaires) {

			if (questionnaireIds.contains(currentQuestionnaire.getQuestionnaireId())) {
				// currentQuestionnaire is still needed so delete from list (to prevent redundant insert)
				questionnaireIds.remove(currentQuestionnaire.getQuestionnaireId());
			} else {
				// currentQuestionnaire is obsolete so remove from db
				if (!delete(currentQuestionnaire)) {
					throw new Exception("currentQuestionnaire not deleted");
				}
			}
		}

		// insert remaining questionnaireIds (new ones)
		for (Integer questionnaireId : questionnaireIds) {
			Questionnaire_QThread rel_questionnaire = new Questionnaire_QThread();
			rel_questionnaire.setThreadId(threadId);
			rel_questionnaire.setQuestionnaireId(questionnaireId);
			rel_questionnaire.setTime(new Date().getTime());
			if (!save(rel_questionnaire)) {
				throw new Exception("rel_questionnaire not saved");
			}
		}

		return true;
	}

	/**
	 * Deletes questionnaire_qthread
	 * 
	 * @param questionnaire_qthread
	 * @return whether successfully or not
	 */
	@Override
	@Transactional
	public boolean delete(Questionnaire_QThread questionnaire_qthread) {

		log.info("try to delete questionnaire_qthread " + questionnaire_qthread);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE threadid = ? AND bogenid = ?", new Object[] { questionnaire_qthread.getThreadId(), questionnaire_qthread.getQuestionnaireId() });
			success = true;

			log.info("questionnaire_qthread deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}
}
