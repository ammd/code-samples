package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.PicturePoint;

/**
 * DAO for picturepoint
 * 
 * @author .
 */
@Repository
public class PicturePointDAO extends BaseDAO<PicturePoint> {

	private final Log log = LogFactory.getLog(PicturePointDAO.class);
	private static final String TABLE_NAME = "bildpunkt";	

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
	}

	/**
	 * Saves or updates picturepoint instance
	 * 
	 * @param picturepoint
	 * @return whether successfully or not
	 */
	@Override
	public boolean saveOrUpdate(PicturePoint picturepoint) {

		log.info("try to save picturepoint " + picturepoint);

		boolean success = false;

		try {

			// insert
			if (picturepoint.getId() == 0) {

				picturepoint.setCreatedAt(new Date().getTime());
				picturepoint.setCreatedBy(SYSTEM_NAME);

				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(4);
				params.put("erstellt_am", new Timestamp(picturepoint.getCreatedAt()));
				params.put("erstellt_von", picturepoint.getCreatedBy());
				params.put("aktiv", picturepoint.isActive());
				params.put("abbildung", picturepoint.getImage());

				picturepoint.setId(insertTemplate.executeAndReturnKey(params).intValue());
				success = true;

				log.info("picturepoint inserted with id " + picturepoint.getId());
			}
			// update
			else {

				picturepoint.setModifiedAt(new Date().getTime());
				picturepoint.setModifiedBy(SYSTEM_NAME);

				jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ? , aktiv = ?, abbildung = ? WHERE id = ?", new Object[] { new Timestamp(picturepoint.getModifiedAt()), picturepoint.getModifiedBy(), picturepoint.isActive(), picturepoint.getImage(), picturepoint.getId() });
				success = true;

				log.info("picturepoint updated with id " + picturepoint.getId());
			}

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads picturepoint by id
	 * 
	 * @param id
	 * @return picturepoint instance
	 */
	@Override
	public PicturePoint readById(int id) {

		log.info("try to read picturepoint by id " + id);

		PicturePoint rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, abbildung FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<PicturePoint>() {
				@Override
				public PicturePoint mapRow(ResultSet rs, int rownum) throws SQLException {
					return new PicturePoint(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getBytes("abbildung"), null);
				}
			}, new Object[] { id });

			log.info("picturepoint read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all picturepoints
	 * 
	 * @return list of picturepoints
	 */
	@Override
	public List<PicturePoint> readAll() {

		log.info("try to read all picturepoints");

		List<PicturePoint> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, abbildung FROM " + TABLE_NAME + " ORDER BY id", new RowMapper<PicturePoint>() {
			
				@Override
				public PicturePoint mapRow(ResultSet rs, int rownum) throws SQLException {
					return new PicturePoint(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getBytes("abbildung"), null);
				}
			});			

			log.info("return " + rtrn.size() + " picturepoints");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes picturepoint
	 * 
	 * @param picturepoint
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(PicturePoint picturepoint) {

		log.info("try to delete picturepoint " + picturepoint);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { picturepoint.getId() });
			success = true;

			log.info("picturepoint deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}	
}
