package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.Questionnaire;
import com.documed.server.domain.TimeInterval;

/**
 * DAO for questionnaire
 * 
 * @author .
 */
@Repository
public class QuestionnaireDAO extends BaseDAO<Questionnaire> {

	private final Log log = LogFactory.getLog(QuestionnaireDAO.class);
	private static final String TABLE_NAME = "standardfragebogen";
	private static final String TABLE_NAME_QUESTIONNAIRE_THREAD = "bogen_thread";

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
	}

	/**
	 * Saves or updates questionnaire instance
	 * 
	 * @param questionnaire
	 * @return whether successfully or not
	 */
	@Override
	public boolean saveOrUpdate(Questionnaire questionnaire) {

		log.info("try to save questionnaire " + questionnaire);

		boolean success = false;

		try {

			// insert
			if (questionnaire.getId() == 0) {

				questionnaire.setCreatedAt(new Date().getTime());
				questionnaire.setCreatedBy(SYSTEM_NAME);

				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(7);
				params.put("erstellt_am", new Timestamp(questionnaire.getCreatedAt()));
				params.put("erstellt_von", questionnaire.getCreatedBy());
				params.put("aktiv", questionnaire.isActive());
				params.put("titel", questionnaire.getTitle());
				params.put("quelle", questionnaire.getSource());
				params.put("zeitintervall", questionnaire.getTimeInterval().name());
				params.put("fachbereichid", questionnaire.getDepartmentId());

				questionnaire.setId(insertTemplate.executeAndReturnKey(params).intValue());
				success = true;

				log.info("questionnaire inserted with id " + questionnaire.getId());
			}
			// update
			else {

				questionnaire.setModifiedAt(new Date().getTime());
				questionnaire.setModifiedBy(SYSTEM_NAME);

				jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ? , aktiv = ?, titel = ?, quelle = ?, zeitintervall = ?, fachbereichid = ? WHERE id = ?", new Object[] { new Timestamp(questionnaire.getModifiedAt()), questionnaire.getModifiedBy(), questionnaire.isActive(), questionnaire.getTitle(), questionnaire.getSource(), questionnaire.getTimeInterval().name(), questionnaire.getDepartmentId(), questionnaire.getId() });
				success = true;

				log.info("questionnaire updated with id " + questionnaire.getId());
			}

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads questionnaire by id
	 * 
	 * @param id
	 * @return questionnaire instance
	 */
	@Override
	public Questionnaire readById(int id) {

		log.info("try to read questionnaire by id " + id);

		Questionnaire rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, titel, quelle, zeitintervall, fachbereichid FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<Questionnaire>() {
				@Override
				public Questionnaire mapRow(ResultSet rs, int rownum) throws SQLException {

					TimeInterval interval = rs.getString("zeitintervall") != null && !rs.getString("zeitintervall").isEmpty() ? TimeInterval.valueOf(rs.getString("zeitintervall")) : TimeInterval.DAILY;

					return new Questionnaire(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("titel"), rs.getString("quelle"), interval, rs.getInt("fachbereichid"), null, null, null, null);
				}
			}, new Object[] { id });

			log.info("questionnaire read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all questionnaires
	 * 
	 * @return list of questionnaires
	 */
	@Override
	public List<Questionnaire> readAll() {

		log.info("try to read all questionnaires");

		List<Questionnaire> rtrn = null;

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, titel, quelle, zeitintervall, fachbereichid FROM " + TABLE_NAME + " ORDER BY titel", new RowMapper<Questionnaire>() {

				@Override
				public Questionnaire mapRow(ResultSet rs, int rownum) throws SQLException {

					TimeInterval interval = rs.getString("zeitintervall") != null && !rs.getString("zeitintervall").isEmpty() ? TimeInterval.valueOf(rs.getString("zeitintervall")) : TimeInterval.DAILY;

					return new Questionnaire(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("titel"), rs.getString("quelle"), interval, rs.getInt("fachbereichid"), null, null, null, null);
				}
			});

			log.info("return " + rtrn.size() + " questionnaires");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all questionnaires by given threadId
	 * 
	 * @param threadId
	 * @return list of questionnaires
	 */
	public List<Questionnaire> readAllByThread(final int threadId) {

		log.info("try to read all questionnaires by thread " + threadId);

		List<Questionnaire> rtrn = null;

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT q.id qid, q.erstellt_am qerstellt_am, q.erstellt_von qerstellt_von, q.veraendert_am qveraendert_am, q.veraendert_von qveraendert_von, q.aktiv qaktiv, q.titel qtitel, q.quelle qquelle, q.zeitintervall qzeitintervall, q.fachbereichid qfachbereichid FROM " + TABLE_NAME + " q JOIN " + TABLE_NAME_QUESTIONNAIRE_THREAD + " qt ON q.id = qt.bogenid WHERE qt.threadid = ? ORDER BY q.titel", new RowMapper<Questionnaire>() {

				@Override
				public Questionnaire mapRow(ResultSet rs, int rownum) throws SQLException {

					TimeInterval interval = rs.getString("qzeitintervall") != null && !rs.getString("qzeitintervall").isEmpty() ? TimeInterval.valueOf(rs.getString("qzeitintervall")) : TimeInterval.DAILY;

					return new Questionnaire(rs.getInt("qid"), rs.getString("qerstellt_von"), rs.getString("qveraendert_von"), rs.getTimestamp("qerstellt_am") != null ? rs.getTimestamp("qerstellt_am").getTime() : 0L, rs.getTimestamp("qveraendert_am") != null ? rs.getTimestamp("qveraendert_am").getTime() : 0L, rs.getBoolean("qaktiv"), rs.getString("qtitel"), rs.getString("qquelle"), interval, rs.getInt("qfachbereichid"), null, null, null, null);
				}
			}, new Object[] { threadId });

			log.info("return " + rtrn.size() + " questionnaires");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes questionnaire
	 * 
	 * @param questionnaire
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(Questionnaire questionnaire) {

		log.info("try to delete questionnaire " + questionnaire);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { questionnaire.getId() });
			success = true;

			log.info("questionnaire deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}
}
