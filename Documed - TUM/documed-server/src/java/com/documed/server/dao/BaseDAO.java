package com.documed.server.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

/**
 * Parent class for all DAOs
 * 
 * @author .
 */
@Repository
public abstract class BaseDAO<E> {	

	protected static final String SYSTEM_NAME = "documed_server";

	protected JdbcTemplate jdbcTemplate;
	protected SimpleJdbcInsert insertTemplate;	

	@Autowired
	public abstract void setDataSource(DataSource dataSource);
	
	public abstract boolean saveOrUpdate(E entity);
	
	public abstract E readById(int id);
	
	public abstract List<E> readAll();
	
	public abstract boolean delete(E entity);		
}
