package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.Patient;

/**
 * DAO for patient
 * 
 * @author .
 */
@Repository
public class PatientDAO extends BaseDAO<Patient> {

	private final Log log = LogFactory.getLog(PatientDAO.class);
	private static final String TABLE_NAME = "patient";	

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
	}

	/**
	 * Saves or updates patient instance
	 * 
	 * @param patient
	 * @return whether successfully or not
	 */
	@Override
	public boolean saveOrUpdate(Patient patient) {

		log.info("try to save patient " + patient);

		boolean success = false;

		try {

			// insert
			if (patient.getId() == 0) {

				patient.setCreatedAt(new Date().getTime());
				patient.setCreatedBy(SYSTEM_NAME);

				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(3);
				params.put("erstellt_am", new Timestamp(patient.getCreatedAt()));
				params.put("erstellt_von", patient.getCreatedBy());
				params.put("aktiv", patient.isActive());
				params.put("device_id", patient.getDeviceId());

				patient.setId(insertTemplate.executeAndReturnKey(params).intValue());
				success = true;

				log.info("patient inserted with id " + patient.getId());
			}
			// update
			else {

				patient.setModifiedAt(new Date().getTime());
				patient.setModifiedBy(SYSTEM_NAME);

				jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ?, aktiv = ?, device_id = ? WHERE id = ?", new Object[] { new Timestamp(patient.getModifiedAt()), patient.getModifiedBy(), patient.isActive(), patient.getDeviceId(), patient.getId() });
				success = true;

				log.info("patient updated with id " + patient.getId());
			}

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads patient by id
	 * 
	 * @param id
	 * @return patient instance
	 */
	@Override
	public Patient readById(int id) {

		log.info("try to read patient by id " + id);

		Patient rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, device_id FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<Patient>() {
				@Override
				public Patient mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Patient(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("device_id"), null, null);
				}
			}, new Object[] { id });

			log.info("patient read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all patients
	 * 
	 * @return list of patients
	 */
	@Override
	public List<Patient> readAll() {

		log.info("try to read all patients");

		List<Patient> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, device_id FROM " + TABLE_NAME + " ORDER BY id", new RowMapper<Patient>() {

				@Override
				public Patient mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Patient(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("device_id"), null, null);
				}
			});			

			log.info("return " + rtrn.size() + " patients");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes patient
	 * 
	 * @param patient
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(Patient patient) {

		log.info("try to delete patient " + patient);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { patient.getId() });
			success = true;

			log.info("patient deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}	
}
