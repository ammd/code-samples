package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.SelectionAnswer;

/**
 * DAO for selectionanswer
 * 
 * @author .
 */
@Repository
public class SelectionAnswerDAO extends BaseDAO<SelectionAnswer> {

	private final Log log = LogFactory.getLog(SelectionAnswerDAO.class);
	private static final String TABLE_NAME = "auswahlantwort";	
	private static final String TABLE_NAME_SELECTION_SELECTIONANSWER = "auswahl_antwort";	

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
	}

	/**
	 * Saves or updates selectionanswer instance
	 * 
	 * @param selectionanswer
	 * @return whether successfully or not
	 */
	@Override
	public boolean saveOrUpdate(SelectionAnswer selectionanswer) {

		log.info("try to save selectionanswer " + selectionanswer);

		boolean success = false;

		try {

			// insert
			if (selectionanswer.getId() == 0) {

				selectionanswer.setCreatedAt(new Date().getTime());
				selectionanswer.setCreatedBy(SYSTEM_NAME);

				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(7);
				params.put("erstellt_am", new Timestamp(selectionanswer.getCreatedAt()));
				params.put("erstellt_von", selectionanswer.getCreatedBy());
				params.put("aktiv", selectionanswer.isActive());
				params.put("titel", selectionanswer.getTitle());
				params.put("abbildung", selectionanswer.getImage());
				params.put("warnung_ja", selectionanswer.isWarningIfTrue());
				params.put("warnung_nein", selectionanswer.isWarningIfFalse());

				selectionanswer.setId(insertTemplate.executeAndReturnKey(params).intValue());
				success = true;

				log.info("selectionanswer inserted with id " + selectionanswer.getId());
			}
			// update
			else {

				selectionanswer.setModifiedAt(new Date().getTime());
				selectionanswer.setModifiedBy(SYSTEM_NAME);

				jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ? , aktiv = ?, titel = ?, abbildung = ?, warnung_ja = ?, warnung_nein = ? WHERE id = ?", new Object[] { new Timestamp(selectionanswer.getModifiedAt()), selectionanswer.getModifiedBy(), selectionanswer.isActive(), selectionanswer.getTitle(), selectionanswer.getImage(), selectionanswer.isWarningIfTrue(), selectionanswer.isWarningIfFalse(), selectionanswer.getId() });
				success = true;

				log.info("selectionanswer updated with id " + selectionanswer.getId());
			}

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads selectionanswer by id
	 * 
	 * @param id
	 * @return selectionanswer instance
	 */
	@Override
	public SelectionAnswer readById(int id) {

		log.info("try to read selectionanswer by id " + id);

		SelectionAnswer rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, titel, abbildung, warnung_ja, warnung_nein FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<SelectionAnswer>() {
				@Override
				public SelectionAnswer mapRow(ResultSet rs, int rownum) throws SQLException {
					return new SelectionAnswer(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("titel"), rs.getBytes("abbildung"), rs.getBoolean("warnung_ja"), rs.getBoolean("warnung_nein"), null);
				}
			}, new Object[] { id });

			log.info("selectionanswer read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all selectionanswers
	 * 
	 * @return list of selectionanswers
	 */
	@Override
	public List<SelectionAnswer> readAll() {

		log.info("try to read all selectionanswers");

		List<SelectionAnswer> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, titel, abbildung, warnung_ja, warnung_nein FROM " + TABLE_NAME + " ORDER BY titel", new RowMapper<SelectionAnswer>() {

				@Override
				public SelectionAnswer mapRow(ResultSet rs, int rownum) throws SQLException {
					return new SelectionAnswer(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getString("titel"), rs.getBytes("abbildung"), rs.getBoolean("warnung_ja"), rs.getBoolean("warnung_nein"), null);
				}
			});			

			log.info("return " + rtrn.size() + " selectionanswers");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}
	
	/**
	 * Reads all selectionanswers by given selection id
	 * 
	 * @param selectionId
	 * @return list of selectionanswers
	 */
	public List<SelectionAnswer> readAllBySelection(int selectionId) {

		log.info("try to read all selectionanswers by selection " + selectionId);

		List<SelectionAnswer> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT aa.id aaid, aa.erstellt_am aaerstellt_am, aa.erstellt_von aaerstellt_von, aa.veraendert_am aaveraendert_am, aa.veraendert_von aaveraendert_von, aa.aktiv aaaktiv, aa.titel aatitel, aa.abbildung aaabbildung, aa.warnung_ja aawarnung_ja, aa.warnung_nein aawarnung_nein FROM " + TABLE_NAME + " aa JOIN " + TABLE_NAME_SELECTION_SELECTIONANSWER + " sa ON aa.id = sa.antwortid WHERE sa.auswahlid = ? ORDER BY aa.titel", new RowMapper<SelectionAnswer>() {

				@Override
				public SelectionAnswer mapRow(ResultSet rs, int rownum) throws SQLException {
					return new SelectionAnswer(rs.getInt("aaid"), rs.getString("aaerstellt_von"), rs.getString("aaveraendert_von"), rs.getTimestamp("aaerstellt_am") != null ? rs.getTimestamp("aaerstellt_am").getTime() : 0L, rs.getTimestamp("aaveraendert_am") != null ? rs.getTimestamp("aaveraendert_am").getTime() : 0L, rs.getBoolean("aaaktiv"), rs.getString("aatitel"), rs.getBytes("aaabbildung"), rs.getBoolean("aawarnung_ja"), rs.getBoolean("aawarnung_nein"), null);
				}
			}, new Object[] { selectionId });		

			log.info("return " + rtrn.size() + " selectionanswers");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes selectionanswer
	 * 
	 * @param selectionanswer
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(SelectionAnswer selectionanswer) {

		log.info("try to delete selectionanswer " + selectionanswer);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { selectionanswer.getId() });
			success = true;

			log.info("selectionanswer deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}	
}
