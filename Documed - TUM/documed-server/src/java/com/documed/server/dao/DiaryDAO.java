package com.documed.server.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.documed.server.domain.Diary;

/**
 * DAO for diary
 * 
 * @author .
 */
@Repository
public class DiaryDAO extends BaseDAO<Diary> {

	private final Log log = LogFactory.getLog(DiaryDAO.class);
	private static final String TABLE_NAME = "tagebuch";	
	private static final String TABLE_NAME_DIARY_QUESTION = "frage_tagebuch";	
	
	protected SimpleJdbcInsert insertDiaryQuestionTemplate;	

	@Autowired
	@Override
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.insertTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).usingGeneratedKeyColumns("id");
		this.insertDiaryQuestionTemplate = new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME_DIARY_QUESTION);
	}

	/**
	 * Saves or updates diary instance
	 * 
	 * @param diary
	 * @return whether successfully or not
	 */
	@Override
	public boolean saveOrUpdate(Diary diary) {

		log.info("try to save diary " + diary);

		boolean success = false;

		try {

			// insert
			if (diary.getId() == 0) {

				diary.setCreatedAt(new Date().getTime());
				diary.setCreatedBy(SYSTEM_NAME);

				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(4);
				params.put("erstellt_am", new Timestamp(diary.getCreatedAt()));
				params.put("erstellt_von", diary.getCreatedBy());
				params.put("aktiv", diary.isActive());
				params.put("patientid", diary.getPatientId());

				diary.setId(insertTemplate.executeAndReturnKey(params).intValue());
				success = true;

				log.info("diary inserted with id " + diary.getId());
			}
			// update
			else {

				diary.setModifiedAt(new Date().getTime());
				diary.setModifiedBy(SYSTEM_NAME);

				jdbcTemplate.update("UPDATE " + TABLE_NAME + " SET veraendert_am = ?, veraendert_von = ? , aktiv = ?, patientid = ? WHERE id = ?", new Object[] { new Timestamp(diary.getModifiedAt()), diary.getModifiedBy(), diary.isActive(), diary.getPatientId(), diary.getId() });
				success = true;

				log.info("diary updated with id " + diary.getId());
			}

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}
	
	/**
	 * Adds question-diary relation
	 * 
	 * @param diaryId
	 * @param questionId
	 * @return whether successfully or not
	 */
	public boolean addQuestionToDiary(final int diaryId, final int questionId) {

		log.info("try to save question " + questionId + " and diary " + diaryId + " relation");

		boolean success = false;

		try {

			if (diaryId > 0 && questionId > 0) {
				
				// generate insert map
				Map<String, Object> params = new HashMap<String, Object>(2);
				params.put("frageid", questionId);
				params.put("tagebuchid", diaryId);
				
				insertDiaryQuestionTemplate.execute(params);
				success = true;

				log.info("question-diary relation saved");
				
			} else {
				log.error("diaryId and questionId must be > 0");
				throw new Exception("diaryId and questionId must be > 0");
			}			

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}

	/**
	 * Reads diary by id
	 * 
	 * @param id
	 * @return diary instance
	 */
	@Override
	public Diary readById(int id) {

		log.info("try to read diary by id " + id);

		Diary rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, patientid FROM " + TABLE_NAME + " WHERE id = ?", new RowMapper<Diary>() {
				@Override
				public Diary mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Diary(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getInt("patientid"), null, null, null, null);
				}
			}, new Object[] { id });

			log.info("diary read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}
	
	/**
	 * Reads diary by patient id
	 * 
	 * @param patientId
	 * @return diary instance
	 */
	public Diary readByPatientId(final int patientId) {

		log.info("try to read diary by patient-id " + patientId);

		Diary rtrn = null;

		try {

			rtrn = jdbcTemplate.queryForObject("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, patientid FROM " + TABLE_NAME + " WHERE patientid = ?", new RowMapper<Diary>() {
				@Override
				public Diary mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Diary(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getInt("patientid"), null, null, null, null);
				}
			}, new Object[] { patientId });

			log.info("diary read successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Reads all diarys
	 * 
	 * @return list of diarys
	 */
	@Override
	public List<Diary> readAll() {

		log.info("try to read all diaries");

		List<Diary> rtrn = null;		

		try {

			// load all entities
			rtrn = jdbcTemplate.query("SELECT id, erstellt_am, erstellt_von, veraendert_am, veraendert_von, aktiv, patientid FROM " + TABLE_NAME + " ORDER BY id", new RowMapper<Diary>() {

				@Override
				public Diary mapRow(ResultSet rs, int rownum) throws SQLException {
					return new Diary(rs.getInt("id"), rs.getString("erstellt_von"), rs.getString("veraendert_von"), rs.getTimestamp("erstellt_am") != null ? rs.getTimestamp("erstellt_am").getTime() : 0L, rs.getTimestamp("veraendert_am") != null ? rs.getTimestamp("veraendert_am").getTime() : 0L, rs.getBoolean("aktiv"), rs.getInt("patientid"), null, null, null, null);
				}
			});			

			log.info("return " + rtrn.size() + " diaries");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return rtrn;
	}

	/**
	 * Deletes diary
	 * 
	 * @param diary
	 * @return whether successfully or not
	 */
	@Override
	public boolean delete(Diary diary) {

		log.info("try to delete diary " + diary);

		boolean success = false;

		try {
			jdbcTemplate.update("DELETE FROM " + TABLE_NAME + " WHERE id = ?", new Object[] { diary.getId() });
			success = true;

			log.info("diary deleted successfully");

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}	
	
	/**
	 * Removes question-diary relation
	 * 
	 * @param diaryId
	 * @param questionId
	 * @return whether successfully or not
	 */
	public boolean removeQuestionFromDiary(final int diaryId, final int questionId) {

		log.info("try to remove question " + questionId + " and diary " + diaryId + " relation");

		boolean success = false;

		try {

			if (diaryId > 0 && questionId > 0) {			
				
				jdbcTemplate.update("DELETE FROM " + TABLE_NAME_DIARY_QUESTION + " WHERE frageid = ? AND tagebuchid = ?", new Object[] { questionId, diaryId });
				success = true;

				log.info("question-diary relation removed");
				
			} else {
				log.error("diaryId and questionId must be > 0");
				throw new Exception("diaryId and questionId must be > 0");
			}			

		} catch (Exception _e) {
			_e.printStackTrace();
		}

		return success;
	}
}
