# Google Contacts Chrome Extension #

A Google Chrome extension designed to query, filter and edit Google Contacts on a particular account.

### Current functionality ###

Query, search and filter data according to domain/account specific logic, making it compatible only with
the Agelab's volunteer account.

### Future functionality ###

* OAuth 2.0
* Increase performance (make paging unnecessary)
* Automatic edit tags (i.e. in place) to filtered contacts, POST results to account.
* Mailto functionality, personalized emails: email link on name tag

### Contributors ###

* Mauricio Munoz (ammd@mit.edu)