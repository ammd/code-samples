'use strict';

/* Services */

var contactsServices = angular.module('agelabContactsServices', ['ngResource']);

contactsServices.factory('RESTService', function() {

    var RESTService = {};

		RESTService.fetchContacts = function(blockoffset, blocksize, callback){

      var url = "https://www.google.com/m8/feeds/contacts/default/full";

      // This call is async
      chrome.extension.getBackgroundPage().oauth.sendSignedRequest(url, callback, {
            'method' : 'GET',
            'parameters' : { 'alt' : 'json', 'max-results' : blocksize, 'start-index' : blockoffset, 'v' : 3.0 }
      });
		};

    return RESTService;
});