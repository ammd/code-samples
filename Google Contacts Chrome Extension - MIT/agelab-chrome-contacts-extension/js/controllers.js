// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

var contactsControllers = angular.module('agelabContactsControllers', []);

contactsControllers.controller('ContactsCtrl', ['$scope', 'RESTService', function ($scope, RESTService) {

    // Pagination tradeoff. The larger the number the more results per page and fewer request,
    // however the lower the responsiveness of the application when searching and filtering
    $scope.pageSize = 2000;
    $scope.currentPage = 0; 
    $scope.contacts = [];
    $scope.loaded = 0;
    $scope.viewLoading =  true;
    $scope.truthy = true;
    $scope.hideExports = true;

    var startIndex = 1;

    // Pagination
    $scope.setCurrentPage = function(currentPage) {
        $scope.currentPage = currentPage;
    }

    $scope.getNumberAsArray = function (num) {
        return new Array(num);
    };

    $scope.numberOfPages = function() {
        return Math.ceil($scope.contacts.length/ $scope.pageSize);
    };

    // Signout button functionality
    $scope.signout = function(){
       chrome.extension.getBackgroundPage().logout();
    }

    // Export contacts button functionality
    $scope.toggleExport = function(){
      $scope.hideExports = !$scope.hideExports;
    }

    // Checkbox filters to check for empty values, memberMap values must match keys on contact
    $scope.emptyfilters  =  [ { title: 'Email',           memberMap:'emails',      value: false},
                              { title: 'Phone Number',    memberMap:'phoneNumber', value: false},
                              { title: 'Address',         memberMap:'address',     value: false},
                              { title: 'Birthday',        memberMap:'birthday',    value: false},
                              { title: 'Highrise',        memberMap:'highrise',    value: false},
                              { title: 'Notes',           memberMap:'notes',       value: false}];
                        
    // checkbox filters requiring specific validation logic, memberMap values must match keys on contact
    $scope.contentfilters  =  [ { title: 'Gender - Male',      memberMap:'gender',   value: false},
                                { title: 'Gender - Female',    memberMap:'gender',   value: false}];              

    // Sort options
    $scope.sortOptions = { name:       { title: 'Name',         value: 'fullName'},
                           birthday:   { title: 'Birthday',     value: 'birthday'},
                           added:      { title: 'Added',        value: 'added'},
                           drives:     { title: 'Drives/Week',  value: 'drives'}};

    // Maximize performance: use !== undefined vs. .hasOwnProperty, as well as a custom json parser.
    function onGoogleContactFetch(text, xhr) {

      console.log("entered callback");

      var data = jsonParse(text);
      if (data.feed.entry === undefined){

        console.log("data is undefined");
        $scope.viewLoading = false;
        $scope.$apply();
        
        /*console.log("data is undefined -> no more requests");
        $scope.viewLoading = false;*/
        return;
      } 
      //console.log("data fetched: ", data);
      var numentries = data.feed.entry.length;
      for (var i = 0, entry; entry = data.feed.entry[i]; i++) {
    
          // Containers
          var emailsSimple = [], emailsDesc = [], phoneNumber = [], address = [];
          var birthday = '', highrise = '', added='', hearabout='', contact='', drives = '';
          var fullName = (entry.gd$name !== undefined ? entry.gd$name.gd$fullName.$t : '');
          var gender = (entry.gContact$gender !== undefined? entry.gContact$gender.value : '');
          var notes = (entry.content !== undefined ? entry.content.$t.replace("Notes: ","").replace("Notes:", "") : '');

          // user defined variables map
          var userDefinedField = entry.gContact$userDefinedField;
          if (userDefinedField){
             for (var j = 0, item; item = userDefinedField[j]; j++) {
                switch(item.key.toUpperCase()){
                  case 'BIRTHDAY': birthday = item.value; break;
                  case 'GENDER': gender = gender == '' ? item.value : gender; break;
                  case 'HIGHRISE TAGS': highrise = item.value; break;
                  case 'ADDED': added = item.value; break;
                  case 'HEAR ABOUT US': hearabout = item.value; break;
                  case 'CONTACT': contact = item.value; break;
                  case 'DRIVES/WEEK': drives = item.value; break;
                  default: break;
                }
             }
          }

          // fill array containers
          if (entry.gd$email) {
            var em = entry.gd$email;
            for (var j = 0, e; e = em[j]; j++) {
              if (!e.address) continue;
              emailsDesc.push(e.address + ' ('+e.rel.substring(e.rel.lastIndexOf('#')+1)+')');
              emailsSimple.push(e.address);
            }
          }
          if (entry.gd$phoneNumber) {
            var phones = entry.gd$phoneNumber;
            for (var j = 0, phone; phone = phones[j]; j++) {
              phoneNumber.push(phone.$t + 
                ' ('+phone.rel.substring(phone.rel.lastIndexOf('#')+1)+')');
            }
          }
            if (entry.gd$structuredPostalAddress) {
            var list = entry.gd$structuredPostalAddress;
            for (var j = 0, ad; ad = list[j]; j++) {
              address.push(ad.gd$formattedAddress.$t + 
                ' ('+ad.rel.substring(ad.rel.lastIndexOf('#')+1)+')');
            }
          }

          // map to contact object
          var contact = {
            'emailsSimple' : emailsSimple.join("; "),
            'emails'       : emailsDesc.join(";  "),
            'fullName'     : fullName,
            'phoneNumber'  : phoneNumber.join(";  "),
            'address'      : address.join(";  "),
            'birthday'     : birthday,
            'gender'       : gender,
            'highrise'     : highrise,
            'added'        : added,
            'hearabout'    : hearabout,
            'contact'      : contact,
            'drives'       : drives,
            'notes'        : notes
          };

          $scope.contacts.push(contact);
      }

      $scope.loaded += numentries;
      //if (numentries < $scope.pageSize) $scope.viewLoading = false;
      $scope.$apply();

      console.log("fetching ...");
      RESTService.fetchContacts(startIndex += numentries, $scope.pageSize, onGoogleContactFetch);
    };

    console.log("fetching ...");
    RESTService.fetchContacts(startIndex, $scope.pageSize, onGoogleContactFetch);
}]);