'use strict';

/* App Module */

// Angular application dependencies
var agelabContactsApp = angular.module('agelabContactsApp', [
  'agelabContactsControllers',
  'agelabContactsFilters',
  'agelabContactsServices',
  'pasvaz.bindonce'
]);

// Authorization mechanism init
var oauth = ChromeExOAuth.initBackgroundPage({
    'request_url' : 'https://www.google.com/accounts/OAuthGetRequestToken',
    'authorize_url' : 'https://www.google.com/accounts/OAuthAuthorizeToken',
    'access_url' : 'https://www.google.com/accounts/OAuthGetAccessToken',
    'consumer_key' : 'anonymous',
    'consumer_secret' : 'anonymous',
    'scope' : 'http://www.google.com/m8/feeds/',
    'app_name' : 'AgeLab Volunteer Contacts'
  });


// Id of the window from which the plugin in spawned, 
// plus its original size (will revert to  these dimensions later)
var spawnWinID = null;
var spawnWinWidth =  null;
var spawnWinLeft = null;
var spawnWinTop = null;
var spawnWinHeight =  null;

// Display connection / authorization status in chrome bar
function setIcon() {
  if (oauth.hasToken()) {
    chrome.browserAction.setIcon({ 'path' : '../img/agelab-on.png'});
  } else {
    chrome.browserAction.setIcon({ 'path' : '../img/agelab-off.png'});
  }
};

// Clear authorization token, reset workspace
function logout() {
  oauth.clearTokens();

  // If dimensions did not change (user was able to work with given space), return to original dimensions
  chrome.windows.get(spawnWinID, function(window) {
      if ((window.left == spawnWinLeft && window.top == spawnWinTop && 
      	   window.height == spawnWinHeight))
        chrome.windows.update(spawnWinID, { 'width' : spawnWinWidth});
  });

  // Close side bar
  chrome.windows.getCurrent(function(window) {
    chrome.windows.remove(window.id, setIcon);
  });
};

function configureSidebar(){
  chrome.windows.getCurrent(function(window) {

      spawnWinID = window.id;
      spawnWinWidth = window.width;
      spawnWinHeight = window.height;
      spawnWinTop = window.top;
      spawnWinLeft = window.left;

      var w = Math.round(window.width - window.width / 1.618);
      var h = Math.round(window.height);
      var left = Math.round(window.left - w + window.width);
      var top = window.top;

      chrome.windows.update(spawnWinID, { 'width': Math.round(window.width-w)});
      chrome.windows.create({ 'url' : '../partials/contacts.html', 'type' : 'popup', 
                              'width': w, 'height': h, 'left': left, 'top': top});
  });
};

function getAuthorization() {

  // TODO
  //if(oath.hasToken()) return;
  oauth.authorize(function() {
    setIcon();
    configureSidebar();
  });
};

chrome.browserAction.onClicked.addListener(getAuthorization);





