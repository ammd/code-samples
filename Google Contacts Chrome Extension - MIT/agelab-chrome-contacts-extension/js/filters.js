'use strict';

/* Filters */

var filters = angular.module('agelabContactsFilters', []);

filters.filter('orderObjBy', function(){
 return function(input, attribute) {
    if (!angular.isObject(input) || attribute === undefined || attribute.length < 1) return input;

    // Only display elements with non empty and non null value for the given attribute
    var array = [];
    for(var objectKey in input) {
        if(input[objectKey][attribute] && input[objectKey][attribute] !== "") 
          array.push(input[objectKey]);
    }

    // pick predicate function according to type of input (test order matters)
    var pred = function(a, b){
      var ma = moment(a, "MM/DD/YYYY");
      if (ma.isValid()){
        var mb = moment(b, "MM/DD/YYYY");
        return ma.isBefore(mb) ? 1 : (ma.isSame(mb) ? 0 : -1);
      } else if (!isNaN(parseInt(a))){ return a - b; } 
        else { return a.localeCompare(b); }
    }

    array.sort(function(a, b){
        a = a[attribute];
        b = b[attribute];
        return pred(a,b);
    });
    return array;
 }
});

filters.filter('afterDate', function() {
  return function(input, date) {
  	  if (date == null || date === undefined || !moment(date).isValid() || moment(date).isBefore("Jan 1, 1900")) return input;
  	  var filtered = [];
      for (var i = 0; i < input.length; i++) {
      	if (moment(input[i].birthday).isAfter(moment(date))) filtered.push(input[i]);
      }
      return filtered;
  };
});

filters.filter('beforeDate', function() {
  return function(input, date) {
      if (date == null || date === undefined || !moment(date).isValid() || moment(date).isBefore("Jan 1, 1900")) return input;
      var filtered = [];
      for (var i = 0; i < input.length; i++) {
        if (moment(input[i].birthday).isBefore(moment(date))) filtered.push(input[i]);
      }
      return filtered;
  };
});

filters.filter('genderFilter', function() {
  return function(input, male, female) {
      if (!male && !female) return input;
      var filtered = [];
      for (var i = 0; i < input.length; i++) {
        //if (male && input[i].gender.toUpperCase().lastIndexOf("M", 0) === 0) filtered.push(input[i]);
        //if (female && input[i].gender.toUpperCase().lastIndexOf("F", 0) === 0) filtered.push(input[i]);
        if (male && ((/^M/).test(input[i].gender) || (/^m/).test(input[i].gender))) filtered.push(input[i]);
        if (female && ((/^F/).test(input[i].gender) || (/^f/).test(input[i].gender))) filtered.push(input[i]);
      }
      return filtered;
  };
});

filters.filter('emptyFilter', function() {
  return function(input, filters) {

    if (!angular.isObject(input)) return input;
    var array = [];

    // Only display elements with non empty and non null value for the given attribute
    for(var objectKey in input) {

        var obj = input[objectKey];

        var fail = false;
        for (var i = 0; i < filters.length; i++){
          var filter = filters[i];
          var attribute = filter.memberMap;
          if(attribute === undefined) return input;

          // Skip filters which have no mapping to the input object
          if (attribute.length < 1 || obj[attribute] === undefined) continue;

          // Fail case
          if (filter.value && obj[attribute] !== undefined && obj[attribute].length < 1) { fail  = true; break; }
        }
        if (!fail) array.push(obj);
    }
    return array;
  };
});


filters.filter('startFrom', function() {
    return function(input, start) {         
        return input.slice(start);
}
});
