///////////////////////////////////////////////////////////////////////////////
// GlobalRFClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#define ARMA_DONT_USE_CXX11

#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <omp.h>
#include "armadillo"
#include "LocalRFClassifier.h"
#include "../com/util.h"
#include <opencv2/opencv.hpp>

using namespace arma;
using namespace cv;
using namespace std;

// ____________________________________________________________________________
bool LocalRFClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;

    if (stoi(p[0]) < 0 || stoi(p[0]) > 1) return false;
    _trainForest = stoi(p[0]) == 0 ? false : true;

    if (stoi(p[1]) < 1) return false;
    _forestsize = stoi(p[1]);

    if (stoi(p[2]) < 1) return false;
    _depth = stoi(p[2]);

    if (stoi(p[3]) < 1) return false;
    _numerical = stoi(p[3]) == 1 ? true : false;

    if (stoi(p[4]) < 0) return false;
    _priors = stoi(p[4]);

    std::vector<std::string> v(p.begin()+5, p.end());
    if(!Classifier::bootstrap(v)) return false;

    cout << "Bootstrapping RandomForest Classifier ..." << endl;
    cout << "Forest size = " << _forestsize << endl;
    cout << "Depth = " << _depth << endl;
    cout << "Priors: " << _priors << endl;

    // Get number of classes -> 1 boosted tree per class
    arma::mat uClasses = unique(_trainannot);
    _rfs = new vector<CvRTrees>();
    _rfs->resize(uClasses.n_rows);

    // Compute and store fresh classifiers
    if (_trainForest)
    {
        // Define as classification problem
        cv::Mat types(1, _train.n_cols + 1, CV_8UC1);
        types.setTo(cv::Scalar(_numerical ? CV_VAR_NUMERICAL : 
                                            CV_VAR_CATEGORICAL));
        types.at<char>(0, _train.n_cols) = CV_VAR_CATEGORICAL;


        // Using set of class labels, build a random forest for each class,
        // using the corresponding feature vectors
        int counter = 0;
        int threads = 1; //omp_get_max_threads();
        #pragma omp parallel num_threads(threads)
        {
            #pragma omp for schedule(dynamic)
                for (int k = 0; k < (int)uClasses.n_rows; k++)
                {
                    counter++;
                    if (threads == 1)
                    {
                        if (counter > 0) printf("\r                        \r");
                        printf("Computing RFs. Progress: %.1lf%%", 
                              ((float)counter/(float)uClasses.n_rows)*100);
                        fflush(stdout);
                    }

                    // Build prior probabilities. Each iteration is a binary
                    // split of the data, therefore priors vector has length 2
                    vector<float> priors(2);

                    // Use equal priors
                    if (_priors == 1)
                        
                        std::for_each(priors.begin(), priors.end(), 
                            [&](float &n){ n = 0.5f; });

                    // Use adaptive priors - use the current binary class split
                    // to compute the priors
                    else if (_priors == 2)
                    {
                        arma::uvec classMatches =  
                        find(_trainannot == uClasses(k,0));

                        // What order are the priors supposed to be in???
                        // -> check confusion matrix -> positive class in index 1
                        priors[0] = _train.n_rows - classMatches.n_rows;
                        priors[1] = classMatches.n_rows;

                        // Under-represented classes get higher probability 
                        // to balance out bias
                        std::for_each(priors.begin(), priors.end(), 
                                [&](float &n){ n = 1.0f - 
                                (float)(n/(float)_train.n_rows); });
                        
                    }

                    // Build params
                    float minsample = _train.n_rows * 0.01f; 
                    float* priorPtr = _priors == 0 ? nullptr : priors.data();

                    CvRTParams rfparams(
                        _depth,         // max depth
                        minsample,      // # samples required for node split,
                                        // i.e. 1% of data
                        0.01f,          // regression accuracy
                        true,           // use surrogates, needed for 
                                        // variable importance
                        10,             // max categories, not needed for
                                        // binary/numerical classification
                        priorPtr,       // priors
                        true,           // calc var importance
                        0,              // n active vars, number of features
                                        // to select for each split, 
                                        // set to sqrt(feat length)
                        _forestsize,    // num trees
                        0.01f,          // forest accuracy
                        CV_TERMCRIT_ITER | CV_TERMCRIT_EPS // termcrit type
                    );

                    // Get feature vectors for current class - transform n-way
                    // classification problem into a series of binary 
                    // classification problems.
                    cv::Mat* data = new cv::Mat(_train.n_rows,
                                                _train.n_cols, CV_32F);
                    cv::Mat* resp = new cv::Mat(_train.n_rows, 1, CV_32SC1);
                    for (int i = 0; i < (int)_trainannot.size(); i++)
                    {
                        // Check if current annotation matches the class
                        // the classifier should be trained on.
                        resp->at<int>(i,0) = _trainannot[i] == uClasses(k,0) ? 1:0;
                        for (int j = 0; j < (int)_train.n_cols; j++)
                            data->at<float>(i,j) = _train(i,j);
                    }

                    // Train the random forest for the current class
                    _rfs->at(k).clear();
                    _rfs->at(k).train(*data, CV_ROW_SAMPLE, *resp, 
                                      cv::Mat(),cv::Mat(),types,
                                      cv::Mat(), rfparams);

                    // write models
                    string save = "./output/rf" + std::to_string(k) + ".xml";
                    _rfs->at(k).save(save.c_str());
                    delete data;
                    delete resp;
                }
        }

        printf("\r                                    \r");
        printf("Computing RFs. Progress: 100.0%%");
        cout << endl << flush;
    }

    // Load random forests
    for (int i = 0; i < (int)uClasses.n_rows; i++)
    {
        string rfname = "./output/rf" + std::to_string(i) + ".xml";
        _rfs->at(i).load(rfname.c_str());
    }
    return true;
}

// ____________________________________________________________________________
vector<int> LocalRFClassifier::classify()
{
    // Data to OpenCV
    cv::Mat sample(_test.n_rows, _test.n_cols, CV_32F);
    for (int j = 0; j < (int)_test.n_rows; j++)
        for (int k =0; k < (int)_test.n_cols; k++)
             sample.at<float>(j, k) = _test(j, k);


    // Classify each sample
    arma::mat uClasses = unique(_trainannot);
    vector<int> res;
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads)
    {
          #pragma omp for schedule(dynamic)
            for (int k = 0; k <  (int)_test.n_rows; k++)
            {
                // Run the sample through every random forest, each of which
                // returns the probability that the sample does not belong to the
                // class it was trained on. Therefore, pick the random forest
                // and thus class with the highest returned probability.
                vector<float> probs;
                for(size_t j = 0; j < _rfs->size(); j++)
                    // Note: predict_prob only works with binary classification
                    // problems, so model has been configured correctly.
                    probs.push_back(_rfs->at(j).predict_prob(sample.row(k)));
                
                int index = std::distance(probs.begin(), 
                    std::max_element(probs.begin(), probs.end()));

                res.push_back(floor(uClasses(index,0)+0.5f));            
            }
    }
    return res;
}