///////////////////////////////////////////////////////////////////////////////
// GlobalGMMClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#define ARMA_DONT_USE_CXX11

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <omp.h>
#include "armadillo"
#include "GlobalGMMClassifier.h"
#include "../com/util.h"
#include <opencv2/opencv.hpp>

using namespace arma;
using namespace cv;
using namespace std;

// ____________________________________________________________________________
bool GlobalGMMClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;

    if (stoi(p[0]) < 0 || stoi(p[0]) > 1) return false;
    _trainModels = stoi(p[0]) == 0 ? false : true;

    if (stoi(p[1]) < 0 || stoi(p[1]) > 1) return false;
    _assign = stoi(p[1]);

    std::vector<std::string> v(p.begin()+2, p.end());

    if(!Classifier::bootstrap(v)) return false;

    cout << "Bootstrapping GlobalGMMClassifier ..." << endl;

    // Use to get number of classes, this is the number of clusters with
    // which the GMM is initialized.
    arma::mat uClasses = unique(_trainannot);
    
    // Compute and store a fresh classifier
    cv::Mat* data = new cv::Mat(_train.n_rows,_train.n_cols, CV_64F);
    
    // Data to OpenCV format
    for (int i = 0; i < (int)_train.n_rows; i++)
        for (int j = 0; j < (int)_train.n_cols; j++)
            data->at<double>(i,j) = (double)_train(i,j);
           
    if (_trainModels)
    {         
        // Train GMM
        cv::EM em(uClasses.n_rows);
        em.train(*data);
    
        // write model
        cv::FileStorage fs("./output/GMM.xml",cv::FileStorage::WRITE);
        em.write(fs);
    }

    // Load GMM model.
    string outpath = "./output/GMM.xml";
    const cv::FileStorage fs(outpath, cv::FileStorage::READ);
    if (fs.isOpened()) {
        const FileNode& fn = fs[outpath];
        _model = new cv::EM(uClasses.n_rows);
        _model->read(fn);
    } 
    if (!_model->isTrained()) 
    {
        cout << "Stored GMM Model not trained! ";
        exit(1);
    }

    // Compute component assignments:
    // Use training data to figure out which component corresponds to which 
    // class corresponds to which cluster in the trained model by checking the
    // distribution of classes for each component.
    //
    // For each cluster index, store the annotations of the samples
    // in the training set that were assigned to that cluster.
    // Inner vector holds annotations
    vector<vector<int>> clusterIds(uClasses.n_rows);
    for (int k = 0; k < (int)data->rows; k++)
    {
        int cluster = _model->predict(data->row(k))[1];
        clusterIds[cluster].push_back(_trainannot(k,0));
    }

    // Get total number of elements for each class
    vector<float> classCount;
    for (int i = 0; i < (int)uClasses.n_rows; i++)
    {
        arma::uvec classMatches =  find(_trainannot == uClasses(i,0));
        classCount.push_back(classMatches.n_rows);
    }


    // Map each component of the GMM to a class label. 
    // 
    // Variant 1: The class label is the class that maximizes: 
    // [(#class i assigned to comp. j)/ (# samples in comp j)] /
    // [(# samples in class i) / (# samples in training set)]
    //
    // Intuitively, compute how much of each component is each class
    // as a percentage, and normalize that value by how well that class
    // is represented in the training set. Classes that are better represented
    // (have a higher percentage) will lead to a smaller overall outcome, thus
    // allowing other classes to "take ownership" for the cluster.
    _clusterLabels.resize(uClasses.n_rows);
    if (_assign == 0)
    {
        for (int i = 0; i < (int)uClasses.n_rows; i++)
        {
            // Values for each class, find index of max element.
            vector<float> classDist;
            float inSet = (float)classCount[i] /(float)_train.n_rows;
            for (int k = 0; k < (int)clusterIds.size(); k++)    
            {
                arma::imat annotMat(clusterIds[k]);
                arma::uvec submat = arma::find(annotMat == uClasses(i,0));
                float inCluster = (float)submat.n_rows / annotMat.size();
                classDist.push_back(inCluster / inSet);
            }

            auto result = std::max_element(classDist.begin(), classDist.end());
            int label = uClasses(std::distance(classDist.begin(),result),0);
            _clusterLabels[i] = label;
        }
    }

    // Variant 2: Consider all possible assignments, take the one with the 
    // highest classification accuracy on the training set.
    else if (_assign == 1)
    {
        // Get all permutation of classes
        vector<int> classes;
        vector<vector<int>> perms;
        for (int i = 0; i < (int)uClasses.n_rows; i++)
            classes.push_back(uClasses(i,0));
        do {
            perms.push_back(classes);
        } while(std::next_permutation(classes.begin(), classes.end()));

        // Get classification results on training data
        vector<int> clusterIdx;
        for (int k = 0; k < (int)data->rows; k++)
        
            // Zero element is log likelihood of sample,
            // First element of result is an index of the most probable 
            // mixture component for the given sample.
            clusterIdx.push_back(_model->predict(data->row(k))[1]);

        // Find the most suitable permutation by computing the training error
        int maxHits = 0, maxPerm = -1;
        for (int k = 0; k < (int)perms.size(); k++)
        {
            int numHits = 0;
            for (int j = 0; j < (int)clusterIdx.size(); j++)
            {
                int cClass = (perms[k])[clusterIdx[j]];
                numHits += (cClass == floor(_trainannot[j]+0.5f)) ? 1:0;
            }
            if (numHits > maxHits)
            {
                maxHits = numHits;
                maxPerm = k;
            }
        }

        // Best permutation found, copy to global variable
        _clusterLabels = perms[maxPerm];
    }   

    for (int i = 0; i < (int)_clusterLabels.size(); i++)
        cout << "Cluster with index " << i << 
                " was assigned to class label " << _clusterLabels[i] << endl;    
    
    delete data;
    return true;
}

// ____________________________________________________________________________
vector<int> GlobalGMMClassifier::classify()
{
    vector<int> res;
    arma::mat uClasses = unique(_trainannot);

    // To OpenCV format
    cv::Mat* data = new cv::Mat(_test.n_rows,_test.n_cols, CV_32F);
    for (int j = 0; j < (int)_test.n_rows; j++)
        for (int l =0; l < (int)_test.n_cols; l++)
            data->at<float>(j, l) = _test(j, l);

    // Loop over all classes / trained models
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads)
    {
          #pragma omp for schedule(dynamic)
            for (int k = 0; k < (int)data->rows; k++)
            {
                // Zero element is log likelihood of sample,
                // First element of result is an index of the most probable 
                // mixture component for the given sample.
                int clusterIdx = _model->predict(data->row(k))[1];
                res.push_back(_clusterLabels[clusterIdx]);
            }
    }
    delete data;
    return res;
}