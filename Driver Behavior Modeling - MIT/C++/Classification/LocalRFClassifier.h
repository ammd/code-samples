
///////////////////////////////////////////////////////////////////////////////
// LocalRFClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef LOCALRFCLASSIFIER_H
#define LOCALRFCLASSIFIER_H

#include <vector>
#include <string>
#include "Classifier.h"
#include <opencv2/opencv.hpp>

using namespace cv; 


// Classify track features using a random forest
class LocalRFClassifier : public Classifier
{
public:

    // Constructor, destructor.
    LocalRFClassifier(): Classifier() { _hasParams = 8; }
    ~LocalRFClassifier() { delete _rfs; }

    // Validate given arguments, train and save a random forest for the given
    // training set. A training sample consists of a conglomerate feature vector
    // for a frame
    bool bootstrap(std::vector<std::string> params);

    // Assign label according the random forest (one per class) that claims each sample
    // with the highest probability. The output vector is as long as the number of
    // elements in the test set
    std::vector<int> classify();

private:

    // The number of trees to train
    int _forestsize;

    // The depth of each tree
    int _depth;

    // Whether or not to train the classifier (or use an available one)
    bool _trainForest;

    // The type of data we are working with, used for parameter optimization
    // true = numerical data, false work with categorical
    bool _numerical;

    // How to set priors for the classifier. 0 = do not use priors,
    // 1 = use equal priors, 2 = use proportional priors
    int _priors;

    // The random forests, one per class. Each random forest therefore
    // acts as a binary classifier.
    std::vector<CvRTrees>* _rfs;
};

#endif  // LOCALRFCLASSIFIER_H

