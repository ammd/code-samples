///////////////////////////////////////////////////////////////////////////////
// LocalGMMClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#define ARMA_DONT_USE_CXX11

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <omp.h>
#include "armadillo"
#include "LocalGMMClassifier.h"
#include "../com/util.h"
#include <opencv2/opencv.hpp>

using namespace arma;
using namespace cv;
using namespace std;

// ____________________________________________________________________________
bool LocalGMMClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;

    if (stoi(p[0]) < 0 || stoi(p[0]) > 1) return false;
    _trainModels = stoi(p[0]) == 0 ? false : true;

    if (stoi(p[1]) < 1) return false;
    _gaussians = stoi(p[1]);

    std::vector<std::string> v(p.begin()+2, p.end());
    if(!Classifier::bootstrap(v)) return false;

    cout << "Bootstrapping LocalGMMClassifier ..." << endl;
    cout << "Gaussians: " << _gaussians << endl;
    
    // Get number of classes -> 1 GMM per class
    arma::mat uClasses = unique(_trainannot);
    _models = new vector<cv::EM>();
    _models->resize(uClasses.n_rows);

    // Compute and store fresh classifiers
    if (_trainModels)
    {
        int counter = 0;
        int threads = 1; //omp_get_max_threads();
        #pragma omp parallel num_threads(threads)
        {
            #pragma omp for schedule(dynamic)
                for (int k = 0; k < (int)uClasses.n_rows; k++)
                {
            	    counter++;
                    if (threads == 1)
                    {
                        if (counter > 0) printf("\r                        \r");
                        printf("Computing GMMs. Progress: %.1lf%%", 
                              ((float)counter/(float)uClasses.n_rows)*100);
                        fflush(stdout);
                    }
    
                    // Get feature vectors for current class - transform n-way
                    // classification problem into a series of binary 
                    // classification problems.
                    cv::Mat* data = new cv::Mat(_train.n_rows,
                                                _train.n_cols, CV_64F);
                    for (int i = 0; i < (int)_train.n_rows; i++)
                        for (int j = 0; j < (int)_train.n_cols; j++)
                            data->at<double>(i,j) = (double)_train(i,j);
                    
                    // Train GMM
                    cv::EM em(_gaussians);
                    em.train(*data);
    
                    // write model
                    cv::FileStorage fs("./output/GMM"+
                    to_string(uClasses(k,0))+".xml",cv::FileStorage::WRITE);
                    em.write(fs);
                    delete data;
                }
        }
        printf("\r                                    \r");
        printf("Computing GMMs. Progress: 100.0%%");
        cout << endl << flush;
    }

    // Load GMM models. This should be done here, as there will most likely
    // not be that many distinct classes, so the memory cost does not scale
    // linearly with the size of the training set.
    for (int k = 0; k < (int)uClasses.n_rows; k++) 
    {
        // Get stored GMM
        string outpath = "./output/GMM"+to_string(uClasses(k,0))+".xml";
        const cv::FileStorage fs(outpath, cv::FileStorage::READ);
        cv::EM model;
        if (fs.isOpened()) {
            const FileNode& fn = fs[outpath];
            model.read(fn);
        } 
        if (!model.isTrained()) 
        {
            cout << "Stored GMM Model not trained! ";
            cout << to_string(uClasses(k,0)) << endl;
            exit(1);
        }
        _models->at(k) = model;
    }
    return true;
}

// ____________________________________________________________________________
vector<int> LocalGMMClassifier::classify()
{ 
    // Store prediction scores for all genres
    arma::mat uClasses = unique(_trainannot);
    vector<int> res;

    // To OpenCV format
    cv::Mat* data = new cv::Mat(_test.n_rows,_test.n_cols, CV_32F);
    for (int j = 0; j < (int)_test.n_rows; j++)
        for (int l =0; l < (int)_test.n_cols; l++)
            data->at<float>(j, l) = _test(j, l);

    // Loop over all classes / trained models
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads)
    {
          #pragma omp for schedule(dynamic)
            for (int k = 0; k < (int)data->rows; k++)
            {
                // Store prediction score
                vector<double> scores(uClasses.n_rows, 0.0);

                // For each sample: 
                // A GMM is a a probability distribution composed of a linear 
                // combination of gaussian components. The likelihood score returned
                // by the predict function refers to the likelihood of the sample
                // with respect to the PDF of the GMM, saying intuitively how 
                // likely it is that the GMM "owns" the sample.
                for (int i = 0; i < (int)uClasses.n_rows; i++)
                {
                    cv::Mat postProb(1, _gaussians, CV_64F);
                    auto prob = _models->at(i).predict(data->row(k),postProb);
                    scores[i] = prob[0];
                }
                
                // Get index of winning model for the current sample
                auto it = max_element(scores.begin(), scores.end());
                auto index = distance(scores.begin(), it); 
                res.push_back(uClasses(index,0));
            }
    }
    delete data;
    return res;
}