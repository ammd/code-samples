
///////////////////////////////////////////////////////////////////////////////
// KNNClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <queue>
#include <math.h>
#include <time.h>
#include <omp.h>
#include <unordered_map>
#include "KNNClassifier.h"
#include "../com/util.h"

using namespace cv;
using namespace std;

// ____________________________________________________________________________
bool KNNClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;
    if (stoi(p[0]) < 1) return false; // Number of neighbors
    _k = stoi(p[0]);

    std::vector<std::string> v(p.begin()+1, p.end());
    if (!Classifier::bootstrap(v)) return false;

    cout << "Bootstrapping KNNClassifier ..." << endl;
    cout << "Number of neighbors: " << _k << endl;
    
    _model = new CvKNearest();
    cv::Mat* data = new cv::Mat(_train.n_rows,_train.n_cols, CV_32F);
    cv::Mat* resp = new cv::Mat(_train.n_rows, 1, CV_32SC1);
    for (int j = 0; j < (int)_train.n_rows; j++)
    {
        resp->at<int>(j,0) = _trainannot(j,0);
        for (int k = 0; k < (int)_train.n_cols; k++)
             data->at<float>(j, k) = (float)_train(j, k);
    }
    _model->train(*data,*resp,cv::Mat(),false,30,false);
    cout << " done." << endl;
    delete data;
    delete resp;
    return true;
}

// ____________________________________________________________________________
vector<int> KNNClassifier::classify()
{
    cv::Mat* data = new cv::Mat(_test.n_rows,_test.n_cols, CV_32F);
    cv::Mat* results = new cv::Mat(_test.n_rows,1, CV_32SC1);
    cv::Mat* resp = new cv::Mat(_test.n_rows, 1, CV_32SC1);
    for (int j = 0; j < (int)_test.n_rows; j++)
    {
        resp->at<int>(j,0) = _testannot(j,0);
        for (int k = 0; k < (int)_test.n_cols; k++)
             data->at<float>(j, k) = (float)_test(j, k);
    }
    _model->find_nearest(*data,_k,results);
    vector<int> res(results->rows);
    for (int j = 0; j < (int)results->rows; j++)
        res[j] = floor(results->at<float>(j,0) +0.5f);

    delete results;
    delete data;
    delete resp;
    return res;
}