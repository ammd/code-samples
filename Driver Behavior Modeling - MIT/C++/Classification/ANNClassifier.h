
///////////////////////////////////////////////////////////////////////////////
// ANNClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef ANNCLASSIFIER_H
#define ANNCLASSIFIER_H

#include <vector>
#include <string>
#include <unordered_map>
#include "Classifier.h"
#include <opencv2/opencv.hpp>

using namespace cv; 


// Classify track features using a random forest
class ANNClassifier : public Classifier
{
public:

    // Constructor, destructor.
    ANNClassifier(): Classifier() { _hasParams = 5; }
    ~ANNClassifier() { delete _network; }

    // Compute the weights of the neural network using the given training data.
    bool bootstrap(std::vector<std::string> params);

    // Feed each input feature to the input layer of the neural network and
    // look at the results at the output layer to assign each sample to a
    // class. 
    std::vector<int> classify();

private:

    // The neural network
    CvANN_MLP* _network;

    // Whether to train or load a trained network
    bool _trainNetwork;

    // 0 to use no priors (weights given from training data), 1 to use equal
    // priors, 2 to use inverse of presence percentage in training set
    int _priors;
};

#endif  // ANNCLASSIFIER_H

