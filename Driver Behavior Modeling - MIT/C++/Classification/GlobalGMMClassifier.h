
///////////////////////////////////////////////////////////////////////////////
// GlobalGMMClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef GLOBALGMMCLASSIFIER_H
#define GLOBALGMMCLASSIFIER_H

#include <vector>
#include <string>
#include <unordered_map>
#include "Classifier.h"
#include <opencv2/opencv.hpp>

using namespace cv;


// Classify track features using a nearest neighbor classifier with SSD metrics
class GlobalGMMClassifier : public Classifier
{
public:

    // Constructor, destructor.
    GlobalGMMClassifier(): Classifier() { _hasParams = 5; }
    ~GlobalGMMClassifier() { delete _model; }

    // Validate given arguments, build and save GMM models for every class
    // in the training set. 
    bool bootstrap(std::vector<std::string> params);

    // Assign the label corresponding to the training model with the highest 
    // log likelihood
    std::vector<int>  classify();

private:

    // Train models for each track in the training set
    bool _trainModels;

    // Assignment strategy - 0 to compute normalized probability of sample
    // belonging to cluster, 1 to set assignment by maximizing the
    // classification quality on the training set.
    int _assign;

    // Mapping from cluster ID (the index) to class label, learn this during training
    std::vector<int> _clusterLabels;

    // The GMM model
    cv::EM* _model;
};

#endif  // GLOBALGMMCLASSIFIER_H

