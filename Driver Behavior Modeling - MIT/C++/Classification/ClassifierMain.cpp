
///////////////////////////////////////////////////////////////////////////////
// ClassifierMain.cpp
///////////////////////////////////////////////////////////////////////////////

#define ARMA_DONT_USE_CXX11

#include "Classifier.h"
#include "KNNClassifier.h"
#include "GlobalGMMClassifier.h"
#include "LocalGMMClassifier.h"
#include "GlobalRFClassifier.h"
#include "LocalRFClassifier.h"
#include "ANNClassifier.h"
#include "BoostClassifier.h"
#include "../com/util.h"
#include "MainState.h"
#include "armadillo"
#include <boost/program_options.hpp>
#include <stdio.h>
#include <iostream>
#include <iterator>
#include <fstream>
#include <stdlib.h>
#include <getopt.h>
#include <vector>
#include <string>

using namespace arma;
using namespace std;
namespace bpo = boost::program_options;

// Prints usage (parameters + options) of the program, and then exits
void printUsage(string assetsPath)
{   
    string printcmd = "groff -Tascii -man "+assetsPath+"/usage.1";
    if (system(printcmd.c_str()) < 0) {}
    exit(1);
}

// Parse command line arguments using Boost
vector<string> parseCmdLine(int argc, char** argv, MainState* state, 
                            Classifier** classifier)
{
    bpo::options_description optDes("");
    optDes.add_options()
        ("help,h",                 "")
        ("dump,d",                 "")
        ("knearestneighbor,n",     "")
        ("localgmm,l",             "")
        ("globalgmm,g",            "")
        ("globalrandomforest,r",   "")
        ("localrandomforest,f",    "")
        ("neuralnetwork,a",        "")
        ("boost,b",                "")
        ("params",    bpo::value< vector<string> >(), "");

    bpo::positional_options_description posDes;
    posDes.add("params", -1);

    bpo::variables_map vm;
    try{
        bpo::store(bpo::command_line_parser(argc, argv).
            options(optDes).positional(posDes).run(), vm);
        bpo::notify(vm);
    } catch(std::exception& ex){
        cout << ex.what() << endl;   
    }
    
    // Handle General program options
    if (vm.count("help")) printUsage(state->getAssetsPath());
    if (vm.count("dump"))  state->dump = true;
    if (!vm.count("params")) printUsage(state->getAssetsPath());

    // Handle Classifiers
    if (vm.count("knearestneighbor")) *classifier = new KNNClassifier();
    if (vm.count("localgmm") != 0 ) *classifier = new LocalGMMClassifier();
    if (vm.count("globalgmm") != 0)  *classifier = new GlobalGMMClassifier();
    if (vm.count("globalrandomforest") != 0) *classifier = new GlobalRFClassifier();
    if (vm.count("localrandomforest") != 0) *classifier = new LocalRFClassifier();
    if (vm.count("neuralnetwork") != 0) *classifier = new ANNClassifier();
    if (vm.count("boost") != 0) *classifier = new BoostClassifier();

    return vm["params"].as<vector<string> >();
}


// Main routine, initialize and test the classification accuracy of the
// specified classifier.
int main(int argc, char** argv)
{
    // Read manifests with train and test pairs - expect alternating train, 
    // test sets (in groups)
    ifstream is("./assets/normalized/dynamicManifestBal.txt");
    istream_iterator<string> start(is), end;
    vector<string> datasets(start, end);

    // Get train / test set pairs, perform classification, aggregate
    // over all iterations.
    arma::mat results(datasets.size()/2, 5);
    for (int i = 0; i < (int)datasets.size(); i=i+2)
    {
        // Load the program state
        MainState* state = new MainState();
        Classifier* classifier = nullptr;
        vector<string> posArg = parseCmdLine(argc, argv, state, &classifier);    
        if (classifier == nullptr || !state->checkState()) 
            printUsage(state->getAssetsPath());

        //vector<string> args = {datasets[i], datasets[i+1], "./output/"};
        posArg[posArg.size()-3] = datasets[i];
        posArg[posArg.size()-2] = datasets[i+1];

        // Boot up the classifier
        if(!classifier->bootstrap(posArg))
        {
            cout << "Classifier could not be bootstrapped at iteration " << i << endl;
            cout << datasets[i] << endl;
            cout << datasets[i+1] << endl;
            exit(1);
        }

        // Perform classification
        vector<double> res = classifier->computeStatistics();
        for (int j = 0; j < (int)res.size(); j++) results(i/2,j) = res[j];

        // Cleanup
        delete classifier;
        delete state;
    }

    cout << endl;
    results.raw_print(cout, "[Accuracy, F1 Score, Precision, Recall, Kappa] = ");
    cout << endl;
    arma::mat aggregated = mean(results,0);
    aggregated.raw_print(cout, "Aggregated results:");
    return 0;
}
