///////////////////////////////////////////////////////////////////////////////
// BoostClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#define ARMA_DONT_USE_CXX11

#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <omp.h>
#include "armadillo"
#include "BoostClassifier.h"
#include "../com/util.h"
#include <opencv2/opencv.hpp>

using namespace arma;
using namespace cv;
using namespace std;

// ____________________________________________________________________________
bool BoostClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;

    if (stoi(p[0]) < 0 || stoi(p[0]) > 1) return false;
    _trainBoosters = stoi(p[0]) == 0 ? false : true;

    if (stoi(p[1]) < 1) return false;
    _wkcount = stoi(p[1]);

    if (stoi(p[2]) < 1) return false;
    _depth = stoi(p[2]);

    if (stoi(p[3]) < 1) return false;
    _numerical = stoi(p[3]) == 1 ? true : false;

    if (stoi(p[4]) < 0) return false;
    _priors = stoi(p[4]);

    std::vector<std::string> v(p.begin()+5, p.end()); // 4 arguments belong here
    if(!Classifier::bootstrap(v)) return false;

    cout << "Bootstrapping BoostClassifier ..." << endl;
    cout << "Weak learner count = " << _wkcount << endl;
    cout << "Depth = " << _depth << endl;
    cout << "Priors: " << _priors << endl;

    // Get number of classes -> 1 boosted tree per class
    arma::mat uClasses = unique(_trainannot);
    _boosters = new vector<CvBoost>();
    _boosters->resize(uClasses.n_rows);


    // Compute and store fresh classifiers
    if (_trainBoosters)
    {
        // Define as classification problem
        cv::Mat types(1, _train.n_cols + 1, CV_8UC1);
        types.setTo(cv::Scalar(_numerical == 1 ? CV_VAR_NUMERICAL : 
                                             CV_VAR_CATEGORICAL));
        types.at<char>(0, _train.n_cols) = CV_VAR_CATEGORICAL;

        // Using set of class labels, build a boosted tree for each class,
        // using the corresponding feature vectors
        int counter = 0;
        int threads = 1; //omp_get_max_threads();
        #pragma omp parallel num_threads(threads)
        {
            #pragma omp for schedule(dynamic)
                for (int k = 0; k < (int)uClasses.n_rows; k++)
                {
                    counter++;
                    if (threads == 1) // Multithreading throws off progress meter
                    {
                        if (counter > 0) printf("\r                        \r");
                        printf("Computing Boosters. Progress: %.1lf%%", 
                              ((float)counter/(float)uClasses.n_rows)*100);
                        fflush(stdout);
                    }


                    // Build prior probabilities. Each iteration is a binary
                    // split of the data, therefore priors vector has length 2
                    vector<float> priors(2);

                    // Use equal priors
                    if (_priors == 1)
                        
                        std::for_each(priors.begin(), priors.end(), 
                            [&](float &n){ n = 0.5f; });

                    // Use adaptive priors - use the current binary class split
                    // to compute the priors
                    else if (_priors == 2)
                    {
                        arma::uvec classMatches =  
                        find(_trainannot == uClasses(k,0));

                        // What order are the priors supposed to be in???
                        // -> check confusion matrix ->  positive class in 
                        // index 0
                        priors[1] = _train.n_rows - classMatches.n_rows;
                        priors[0] = classMatches.n_rows;

                        // Under-represented classes get higher probability 
                        // to balance out bias
                        std::for_each(priors.begin(), priors.end(), 
                                [&](float &n){ n = 1.0f - 
                                (float)(n/(float)_train.n_rows); });                        
                    }

                    // Build params
                    float* priorPtr = _priors == 0 ? nullptr : priors.data();

                    CvBoostParams params(
                        CvBoost::REAL,      // Type of boosting algorithm
                        _wkcount,           // Number of weak classifiers
                        0.98,               // weight trim rate, used to
                                            // save computational time
                        _depth,             // max depth of tree
                        true,               // use surrogates
                        priorPtr            // priors - The array of a 
                                            // priori class probabilities, 
                                            // sorted by the class label value.
                                            // Use to tune against
                                            // skewed data
                    );

                    // Get feature vectors for current class - transform n-way
                    // classification problem into a series of binary 
                    // classification problems.
                    cv::Mat* data = new cv::Mat(_train.n_rows,
                                                _train.n_cols, CV_32F);
                    cv::Mat* resp = new cv::Mat(_train.n_rows, 1, CV_32SC1);
                    for (int i = 0; i < (int)_trainannot.size(); i++)
                    {
                        // Check if current annotation matches the class
                        // the classifier should be trained on.
                        resp->at<int>(i,0) = _trainannot[i] == uClasses(k,0) ? 1:0;
                        for (int j = 0; j < (int)_train.n_cols; j++)
                            data->at<float>(i,j) = _train(i,j);
                    }

                    // Train the boosted tree for the current class
                    _boosters->at(k).clear();
                    _boosters->at(k).train(*data, CV_ROW_SAMPLE, *resp, 
                                      cv::Mat(),cv::Mat(),types,
                                      cv::Mat(), params, false);

                    // write models
                    string save = "./output/boost" + std::to_string(k) + ".xml";
                    _boosters->at(k).save(save.c_str());
                    delete data;
                    delete resp;
                }
        }

        printf("\r                                    \r");
        printf("Computing Boosters. Progress: 100.0%%");
        cout << endl << flush;
    }

    // Load models
    for (int i = 0; i < (int)uClasses.n_rows; i++)
    {
        string name = "./output/boost" + std::to_string(i) + ".xml";
        _boosters->at(i).load(name.c_str());
    }
    return true;
}

// ____________________________________________________________________________
vector<int> BoostClassifier::classify()
{
    // Data to OpenCV
    cv::Mat sample(_test.n_rows, _test.n_cols, CV_32F);
    for (int j = 0; j < (int)_test.n_rows; j++)
        for (int k =0; k < (int)_test.n_cols; k++)
             sample.at<float>(j, k) = _test(j, k);


    // Classify each sample
    arma::mat uClasses = unique(_trainannot);
    vector<int> res;
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads)
    {
          #pragma omp for schedule(dynamic)
            for (int k = 0; k < (int)_test.n_rows; k++)
            {
                // Run the sample through the trees in the ensemble and return
                // the output class label based on the weighted voting.
                // Each model can "claim" the sample as it own -> pick the model
                // with the highest number of votes, which is the return value
                // of the predict function below with the returnSum parameter
                // set to true.
                vector<float> sums;
                for(size_t j = 0; j < _boosters->size(); j++)
                {
                    sums.push_back(_boosters->at(j).predict(sample.row(k), 
                                                         cv::Mat(), 
                                                         Range::all(), 
                                                         false, true));
                }
                int index = std::distance(sums.begin(), 
                    std::max_element(sums.begin(), sums.end()));

                // Booster at index #index has won the sample -> 
                // uClasses(#index) gives the winning class
                res.push_back(floor(uClasses(index,0)+0.5f));
            }
    }
    return res;
}