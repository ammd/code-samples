
///////////////////////////////////////////////////////////////////////////////
// Classifier.h
///////////////////////////////////////////////////////////////////////////////

#define ARMA_DONT_USE_CXX11

#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include <vector>
#include <string>
#include <unordered_map>
#include "armadillo"

using namespace arma;

// Comparison class for priority queue
class lesserpair
{
  public:
  lesserpair() {}
  bool operator()(const std::pair<double, int>& pair1, 
                  const std::pair<double, int>& pair2) const
  {
    return (pair1.first < pair2.first);
  }
};


// Represents an abstract classification strategy.
class Classifier
{
public:

    // Constructor, destructor
    Classifier() { }
    virtual ~Classifier() { }

    // Expect parameters [train file, test file, output dir]
    virtual bool bootstrap(std::vector<std::string> p);

    // Classify all samples in the test set. Returns a vector as long as the
    // test set with the integer class values that have been assigned to each
    // respective sample
    virtual std::vector<int> classify()=0;

    // Returns vector with: [Accuracy, Fscore, Precision, Recall, Kappa] stats
    // Currently only works for binary classification.
    std::vector<double> computeStatistics();

protected:

    // The list of arguments to this classifier
    std::vector<std::string> _arg;

    // Training data container, double precision. Let Armadillo decide
    // where to put the matrix, either on stack or heap
    arma::mat _train;

    // Training annotations
    arma::mat _trainannot;

    // Testing data container, double precision. Let Armadillo decide
    // where to put the matrix, either on stack or heap
    arma::mat _test;

    // Test annotations
    arma::mat _testannot;

    // output directory
    std::string output;

    // Number of expected global parameters
    int _hasParams;

private:

};

#endif  // CLASSIFIER_H

