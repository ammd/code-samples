.TH ClassifierMain 1 "4 October 2014" "MIT AgeLab" "Classifier"



.SH NAME
ClassifierMain \- classify a set of features into classes.



.SH SYNOPSIS
ClassifierMain [OPTIONS] <CLASSIFIER> <CLASSIFIER OPTS> <GLOBAL PARAMS>



.SH DESCRIPTION 
ClassifierMain allows the user to specify a train and test sets to train a model of choice with its own set of parameters and return / print the classification accuracy along with a set of post-classification analytics such as confusion matrices, precision & recall, etc.

This testing framework builds on the observation that training data is often merged from different sources (i.e. subjects) to build a conglomerate of individual subgroups that compose each class. Where meaningful, options are presented to build models in three ways: 1. build a model for each such subgroup 2. build a model for each class, aggregating over the individual subgroups that compose them. 3. Build a single monolithic model for all data, perform N-way classification, where N is the number of classes. These alternatives lead to fundamentally different semantic interpretations of each model (see descriptions below), and depending on the application context this could lead to different accuracy values.


.SH OPTIONS
.IP "\-d, \-\-dump"
Output classifications to file. This is a file with one entry per line, where each entry has the form <index of test sample> = <classification value>. If not declared, compute and print only classifiction accuracy to the standard output.

NOT IMPLEMENTED

.SH CLASSIFIER
.IP "\-n, \-\-knearestneighbor"
Compare features using k Nearest Neighbor and the L1 metric. Options:
.IP
#k - the number of nearest neighbors to consider.

.IP "\-l, \-\-localgmm"
Use features to build a GMM per class. This is the supervised approach to training GMMs. The components of the GMM will fit the complexity of the data within each class. More components will likely overfit, fewer components will underfit. The same number of components is taken for all classes, so skewed data might not be represented well. Options:
.IP
#trainmodels - 1 to compute and store models for each training element, otherwise 0 (assumes models have already been computed and are stored in @output).
.IP
#gaussians - the number of Gaussians used to model each subgroup.


.IP "\-g, \-\-globalgmm"
Use features to build a single GMM with as many clusters as there are classes in the training set. The components of the GMM correspond to the classes in the training set. Options:
.IP
#trainmodels - 1 to compute and store models for each genre, otherwise 0 (assumes models have already been computed and are stored in @output).
.IP
#metric - This controls how the mix components of the GMM are assigned to classes. 0 computes the percentage of each class in each component, normalized by how well the class is represented in the training set (this is the traditional variant). 1 finds the mapping by maximizing the prediction quality on the training set, considering all assignment possiblitites. This might not be suitable for classification problems with many classes.


.IP "\-r, \-\-globalrandomforest"
Train a single random forest, which works well for heterogenous features (with variables from separate semantic sources) to classify each sample. The random forest then discriminates between the N different classes present in the training set.
.IP
#forestsize - the number of trees to train. More trees usually increases classification accuracy, at the cost of more computation time. Good starting value: 100
.IP
#depth - the depth of each tree. Shallow trees will underfit, deep trees will overfit. Good starting value: 40
.IP
#trainForest - 1 to compute and store the random forest, otherwise 0 (assumes forest has already been computed and are stored in @output).
.IP
#datatype - 1 for numerical otherwise categorical input data
.IP
#priors - This instructs the classifier to aim for a better F score than overall accuracy. The idea is to work against a skewed dataset by assigning weights to classes. The default priors usually reflect the class distribution in the training dataset, so that classifying an under-represented class correctly will not be as important as classifying a more prevalent class. Possible values here are the following: 0 - do not use priors (weigh classes according to how well they are represented in the training set), 1 - use equal priors (equal weights for all classes, should work well with skewed input data), 2 - use proportional priors, so that under-represented classes will receive a higher weight than over-represented classes.


.IP "\-f, \-\-localrandomforest"
Build a set of random forests, one per class. A new label is assigned according the the class label of the random forest that offers the highest probability that the input sample belongs to the class it was trained on. Each random forest in the resulting set is a binary classifier that is optimized to work with either numerical (non categorical) or categorical variables, see below.
.IP
#trainForest - 1 to compute and store the random forest, otherwise 0 (assumes forest has already been computed and are stored in @output).
.IP
#forestsize - the number of trees to train. More trees usually increases classification accuracy, at the cost of more computation time. Good starting value: 100
.IP
#depth - the depth of each tree. Shallow trees will underfit, deep trees will overfit. Good starting value: 40
.IP
#datatype - 1 for numerical otherwise categorical input data
.IP
#priors - This instructs the classifier to aim for a better F score than overall accuracy. The idea is to work against a skewed dataset by assigning weights to classes. The default priors usually reflect the class distribution in the training dataset, so that classifying an under-represented class correctly will not be as important as classifying a more prevalent class. Possible values here are the following: 0 - do not use priors (weigh classes according to how well they are represented in the training set), 1 - use equal priors (equal weights for all classes, should work well with skewed input data), 2 - use proportional priors, so that under-represented classes will receive a higher weight than over-represented classes.

.IP "\-a, \-\-neuralnetwork"
Use features to train a feed-forward artificial neural network.
.IP
#trainnetwork - 1 to compute and store the neural network, otherwise 0 (assumes network has already been computed and stored in @output)
.IP
#priors - This instructs the classifier to aim for a better F score than overall accuracy. The idea is to work against a skewed dataset by assigning weights to classes. The default priors usually reflect the class distribution in the training dataset, so that classifying an under-represented class correctly will not be as important as classifying a more prevalent class. Possible values here are the following: 0 - weigh samples proportional to how well their class is represented in the training data, 1 - use equal priors (equal weights for all classes, should work well with skewed input data), 2 - use proportional priors, so that under-represented classes will receive a higher weight than over-represented classes.


.IP "\-b, \-\-boost"
Use features to train a boosted tree(s) of weak classifiers. Each tree is a binary classifier, with one tree per class. A new sample is assigned to the class of the boosted tree that claims the sample to its positive class with the hightest probability.
.IP
#trainbooster - 1 to compute and store the boosted trees, otherwise 0 (assumes trees have already been computed and stored in @output)
.IP
#wkcount - the number of weak classifiers to use for training.
.IP
#depth - the (max) depth of each tree.
.IP
#datatype - 1 for numerical otherwise categorical input data
.IP
#priors - This instructs the classifier to aim for a better F score than overall accuracy. The idea is to work against a skewed dataset by assigning weights to classes. The default priors usually reflect the class distribution in the training dataset, so that classifying an under-represented class correctly will not be as important as classifying a more prevalent class. Possible values here are the following: 0 - do not use priors (weigh classes according to how well they are represented in the training set), 1 - use equal priors (equal weights for all classes, should work well with skewed input data), 2 - use proportional priors, so that under-represented classes will receive a higher weight than over-represented classes.


.SH GLOBAL PARAMS
.IP "@trainfile"
A CSV file with the train annotations and features, with one feature per line in the following format: "<class integer>,<feat1,feat2,...featn>".

.IP "@testfile"
A CSV file with the test annotations and features, with one feature per line in the following format: "<class integer>,<feat1,feat2,...featn>".

.IP "@output"
Directory where the classification output should be placed, see option \-d, \-\-dump

.SH BUGS
ANN behaves non deterministically when training and testing on several sets in within a single execution.

.SH AUTHOR
Mauricio Munoz
