
///////////////////////////////////////////////////////////////////////////////
// GlobalRFClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef GLOBALRFCLASSIFIER_H
#define GLOBALRFCLASSIFIER_H

#include <vector>
#include <string>
#include "Classifier.h"
#include <opencv2/opencv.hpp>

using namespace cv; 


// Classify track features using a random forest
class GlobalRFClassifier : public Classifier
{
public:

    // Constructor, destructor.
    GlobalRFClassifier(): Classifier() { _hasParams = 8; }
    ~GlobalRFClassifier() { delete _rf; }

    // Validate given arguments, train and save a random forest for the given
    // training set.
    bool bootstrap(std::vector<std::string> params);

    // Push the sample down each tree of the random forest, and output a 
    // classification result based on the votes amongst all trees.
    vector<int> classify();

private:

    // The number of trees to train
    int _forestsize;

    // The depth of each tree
    int _depth;

    // Whether or not to train the classifier (or use an available one)
    bool _trainForest;

    // Whether to optimize training for numerical (true) or categorical 
    // (false) data
    bool _numerical;

    // How to set priors for the classifier. 0 = do not use priors,
    // 1 = use equal priors, 2 = use proportional priors
    int _priors;

    // The random forest
    CvRTrees* _rf;
};

#endif  // GLOBALRFCLASSIFIER_H

