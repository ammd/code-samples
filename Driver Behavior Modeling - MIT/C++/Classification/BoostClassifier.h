
///////////////////////////////////////////////////////////////////////////////
// BoostClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef BOOSTCLASSIFIER_H
#define BOOSTCLASSIFIER_H

#include <vector>
#include <string>
#include "Classifier.h"
#include <opencv2/opencv.hpp>

using namespace cv; 


// Classify track features using a random forest
class BoostClassifier : public Classifier
{
public:

    // Constructor, destructor.
    BoostClassifier(): Classifier() { _hasParams = 8; }
    ~BoostClassifier() { delete _boosters; }

    // Validate given arguments, train and save a set of boosted trees for the given
    // training set. 
    bool bootstrap(std::vector<std::string> params);

    // Assign label according the boosted tree (one per class) that claims each sample
    // with the highest probability. The output vector is as long as the number of
    // elements in the test set
    std::vector<int> classify();

private:

    // Whether or not to train the classifier (or use previously saved one)
    bool _trainBoosters;

    // The number of weak classifier to use during training
    int _wkcount;

    // The max depth of the trees
    int _depth;

    // The type of data we are working with, used for parameter optimization
    bool _numerical;

    // How to set priors for the classifier. 0 = do not use priors,
    // 1 = use equal priors, 2 = use proportional priors
    int _priors;

    // The weak classifier trees, one per genre (since only binary 
    // classification problems are supported)
    std::vector<CvBoost>* _boosters;
};

#endif  // BOOSTCLASSIFIER_H

