///////////////////////////////////////////////////////////////////////////////
// GlobalRFClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#define ARMA_DONT_USE_CXX11

#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <omp.h>
#include "armadillo"
#include "GlobalRFClassifier.h"
#include "../com/util.h"
#include <opencv2/opencv.hpp>

using namespace arma;
using namespace cv;
using namespace std;

// ____________________________________________________________________________
bool GlobalRFClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;

    if (stoi(p[0]) < 1) return false;
    _forestsize = stoi(p[0]);

    if (stoi(p[1]) < 1) return false;
    _depth = stoi(p[1]);

    if (stoi(p[2]) < 0 || stoi(p[2]) > 1) return false;
    _trainForest = stoi(p[2]) == 0 ? false : true;

    if (stoi(p[3]) < 0) return false;
    _numerical = stoi(p[3]) == 1 ? true : false;

    if (stoi(p[4]) < 0) return false;
    _priors = stoi(p[4]);

    std::vector<std::string> v(p.begin()+5, p.end());
    if(!Classifier::bootstrap(v)) return false;

    cout << "Bootstrapping RFClassifier ..." << endl;
    cout << "Forest size = " << _forestsize << endl;
    cout << "Depth = " << _depth << endl;
    cout << "Priors: " << _priors << endl;

    // Get number of classes
    arma::mat uClasses = unique(_trainannot); // classes in ascending order
    _rf = new CvRTrees();

    // Get stats on training data to build optimal model parameters
    // Priors: fixed prior class probabilities, see below
    vector<float> priors(uClasses.n_rows);

    // Use equal priors
    if (_priors == 1)
        
        std::for_each(priors.begin(), priors.end(), 
            [&](float &n){ n = 1.0f / (float)(uClasses.n_rows); });

    // Use adaptive priors.
    else if (_priors == 2)
    {
        for (int i = 0; i < (int)uClasses.n_rows; i++)
        {
            arma::uvec classMatches =  find(_trainannot == uClasses(i,0));
            priors[i] = (classMatches.n_rows);
        }

        // Under-represented classes get higher probability to balance out bias
        std::for_each(priors.begin(), priors.end(), 
                [&](float &n){ n = 1.0f - (float)(n/(float)_train.n_rows); });

        // What order are the priors supposed to be in???
        // -> check confusion matrix -> do not reverse
        // reverse(priors.begin(), priors.end());
    }

    // Build a fresh random forest classifier
    if (_trainForest)
    {
        // Training parameters
        float minsample = _train.n_rows * 0.01f;
        float* priorPtr = _priors == 0 ? nullptr : priors.data();

        CvRTParams rfparams(
            _depth,         // max depth
            minsample,      // # samples required for node split,
                            // i.e. 1% of data
            0.01f,          // regression accuracy
            true,           // use surrogates, needed for 
                            // variable importance
            10,             // max categories, not needed for
                            // binary/numerical classification
            priorPtr,       // priors
            true,           // calc var importance
            0,              // n active vars, number of features
                            // to select for each split, 
                            // set to sqrt(feat length)
            _forestsize,    // num trees
            0.01f,          // forest accuracy
            CV_TERMCRIT_ITER | CV_TERMCRIT_EPS // termcrit type
        );


        // Define as classification problem
        cv::Mat types(1, _train.n_cols + 1, CV_8UC1);
        types.setTo(cv::Scalar(_numerical ? CV_VAR_NUMERICAL : 
                                            CV_VAR_CATEGORICAL));
        types.at<char>(0, _train.n_cols) = CV_VAR_CATEGORICAL;

        // Data to OpenCV format
        cv::Mat* data = new cv::Mat(_train.n_rows, _train.n_cols, CV_32F);
        cv::Mat* resp = new cv::Mat(_trainannot.n_rows, 1, CV_32SC1);
        for (int i = 0; i < (int)_trainannot.size(); i++)
        {
            resp->at<int>(i,0) = _trainannot[i];
            for (int j = 0; j < (int)_train.n_cols; j++)
                data->at<float>(i,j) = _train(i,j);
        }

        // Train the random forest
        CvRTrees rf;
        rf.train(*data, CV_ROW_SAMPLE, *resp, cv::Mat(),cv::Mat(),types,
                  cv::Mat(), rfparams);

        cout << "Variable importance vector " << rf.getVarImportance() << endl;

        // write model
        rf.save("./output/randomforest.xml");
        delete data;
        delete resp;
    }

    // Load Random Forest
    _rf->load("./output/randomforest.xml");
    return true;
}

// ____________________________________________________________________________
vector<int> GlobalRFClassifier::classify()
{
    // Data to OpenCV
    cv::Mat sample(_test.n_rows, _test.n_cols, CV_32F);
    for (int j = 0; j < (int)_test.n_rows; j++)
        for (int k =0; k < (int)_test.n_cols; k++)
             sample.at<float>(j, k) = _test(j, k);

    // Classify each sample
    arma::mat uClasses = unique(_trainannot); // classes in ascending order
    vector<int> res;
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads)
    {
          #pragma omp for schedule(dynamic)
            for (int k = 0; k <  (int)_test.n_rows; k++)
            {
                // Push the sample down every tree, get class as a voted average
                // from all of the trees in the forest
                // Note: predict method returns index of class?
                int clz = floor(_rf->predict_prob(sample.row(k))+0.5f);
                res.push_back(uClasses(clz,0));            
            }
    }
    return res;
}