
///////////////////////////////////////////////////////////////////////////////
// KNNClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef KNNCLASSIFIER_H
#define KNNCLASSIFIER_H

#include <vector>
#include <string>
#include "Classifier.h"
#include <opencv2/opencv.hpp>


// Classify track features using a nearest neighbor classifier with SSD metrics
class KNNClassifier : public Classifier
{
public:

    // Constructor, destructor.
    KNNClassifier(): Classifier() { _hasParams = 4; } // 3 from parent
    ~KNNClassifier() { delete _model; }

    // Validate given arguments, do any required preprocessing
    bool bootstrap(std::vector<std::string> params);

    // Classifies tracks using SSD metrics (euclidean distance), 
    // prints output to output file
    std::vector<int> classify();

private:

	// The number of neighbors to check during classification
	int _k;

    // The classifier model
    CvKNearest* _model;
};

#endif  // KNNCLASSIFIER_H

