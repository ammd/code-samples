
///////////////////////////////////////////////////////////////////////////////
// LocalGMMClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef LocalGMMCLASSIFIER_H
#define LocalGMMCLASSIFIER_H

#include <vector>
#include <string>
#include <unordered_map>
#include "Classifier.h"
#include <opencv2/opencv.hpp>

using namespace cv;


// Classify track features using a nearest neighbor classifier with SSD metrics
class LocalGMMClassifier : public Classifier
{
public:

    // Constructor, destructor.
    LocalGMMClassifier(): Classifier() { _hasParams = 5; }
    ~LocalGMMClassifier() { delete _models; }

    // Validate given arguments, build and save GMM models for every class
    // in the training set. 
    bool bootstrap(std::vector<std::string> params);

    // Assign the label corresponding to the training model with the highest 
    // log likelihood
    std::vector<int>  classify();

private:

    // Train models for each track in the training set
    bool _trainModels;

    // The number of mixture components in the GMM
    int _gaussians;

    // The GMM models, one per class
    std::vector<cv::EM>* _models;
};

#endif  // LocalGMMCLASSIFIER_H

