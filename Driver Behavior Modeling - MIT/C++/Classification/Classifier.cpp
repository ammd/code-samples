
///////////////////////////////////////////////////////////////////////////////
// Classifier.cpp
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <time.h>
#include <omp.h>
#include <string>
#include <set>
#include <algorithm>
#include "Classifier.h"
#include "../com/util.h"

using namespace std;

// ____________________________________________________________________________
bool Classifier::bootstrap(vector<string> p)
{
    _arg = p;
    output = _arg[2]; // output dir

    // Read in train, test data, store features and annotations
    arma::mat traintemp, testtemp;
    traintemp.load(_arg[0],csv_ascii);
    testtemp.load(_arg[1],csv_ascii);

    _trainannot = traintemp.col(0);
    _testannot = testtemp.col(1);


    // ****************** CHOOSE FEATURES ******************

    _train = traintemp.cols(1, traintemp.n_cols-1);
    _test = testtemp.cols(2, testtemp.n_cols-1);

    //_train = traintemp.cols(1, traintemp.n_cols-3);
    //_test = testtemp.cols(2, testtemp.n_cols-3);


    // ****************** END CHOOSE FEATURES ***************


    // Class check
    arma::mat utrain = arma::unique(_trainannot);
    arma::mat utest = arma::unique(_testannot);
    if (utrain.size() < utest.size())
    {
        cout << "Error bootstrapping classifier: ";
        cout << "Test classes are not a subset of the training classes:"<<endl;
        cout << "Classes present in training set: " << endl;
        utrain.print();
        cout << "Classes present in test set: " << endl;
        utest.print();
        exit(1);
    }

    // Compute z scores and normalize
    /*
    _train.each_row() -= arma::mean(_train, 0);
    _train.each_row() /= arma::stddev(_train, 0);
    _test.each_row() -= arma::mean(_test, 0);
    _test.each_row() /= arma::stddev(_test, 0);
    */
    //_train = normalise(_train); // need new version of armadillo
    //_test = normalise(_test);
    
    return true;
}


// ____________________________________________________________________________
vector<double> Classifier::computeStatistics()
{
    // Holds: [Accuracy, Fscore, Precision, Recall, Kappa] stats
    vector<double> stats(5);

    // Classify
    time_t start, end;
    time(&start);
    cout << "Classifying ... ";
    vector<int> tclasses = classify();
    time(&end);
    cout << "done. Time: " << difftime(end, start)<<" s"<<endl;

    // Check data integrity
    if (tclasses.size() != _testannot.n_rows)
    {
        cout << "Classification size does not match test set." << endl;
        exit(1);
    }

    // Get accuracy
    int hits=0;
    for (int i = 0; i < (int)tclasses.size(); i++)
        hits = hits + ((tclasses[i] == floor(_testannot(i,0) + 0.5)) ? 1 : 0);

    cout << "Classification Accuracy: " << hits << "/" << _test.n_rows;
    cout << " = " << 100.0*(double)hits/_test.n_rows << "%" << endl;
    stats[0] = ((double)hits/_test.n_rows);

    // Data statistics
    arma::mat aClasses = unique(_testannot);
    arma::mat tClasses = unique(_trainannot);

    cout << endl << "Total train samples: " << _train.n_rows << endl;
    cout << "Total classes in train set: " << tClasses.n_rows << endl;
    for (int i = 0; i < (int)tClasses.n_rows; i++)
    {
        cout << "Samples in class " << tClasses(i,0) << ": ";
        arma::uvec samples = arma::find(_trainannot == tClasses(i,0));
        cout << samples.n_rows << endl;
    }

    cout << endl << "Total test samples: " << _test.n_rows << endl;
    cout << "Total classes in test set: " << aClasses.n_rows << endl;
    for (int i = 0; i < (int)aClasses.n_rows; i++)
    {
        cout << "Samples in class " << aClasses(i,0) << ": ";
        arma::uvec samples = arma::find(_testannot == aClasses(i,0));
        cout << samples.n_rows << endl;
    }
    
    // Confusion matrix
    arma::imat pClasses(tclasses);
    arma::imat uniquePred = unique(pClasses);
    arma::mat confusionMat(aClasses.n_rows, aClasses.n_rows);

    for (int i = 0; i < (int)aClasses.n_rows; i++)
    {
        // Indices of all actual classes that correspond to current index
        arma::uvec current = arma::find(_testannot == aClasses(i,0));

        // Find what the prediction results say at these indices
        arma::imat subrows = pClasses.rows(current);

        // How many elements are which class?
        for (int j = 0; j < (int)aClasses.n_rows; j++)
        {
            arma::uvec subelems = arma::find(subrows == aClasses(j,0));
            confusionMat(i,j) = subelems.n_rows;
        }
    }
    cout << endl << "Confusion matrix: " << endl;
    confusionMat.raw_print();
    cout << endl;

    // F-score, recall, precision, kappa currently only for binary
    // classification. Assumes "anomaly" class, which is the class with fewest
    // examples. This info is taken from the test set, so therefore we are
    // assuming that the training set has a similar distribution of classes.
    if (tClasses.n_rows != 2)
    {
        cout << "F-score, recall, precision, kappa currently only ";
        cout << "implemented for binary classification." << endl;
        return stats;
    }


    arma::rowvec csums = arma::sum(confusionMat,0);
    arma::colvec rsums = arma::sum(confusionMat,1);

    // Find index of anomaly class. Confusion mat at (minIdx, minIdx) gives
    // the number of true positives for this class.
    arma::uvec minIdx = arma::find(rsums == min(rsums)); 
    if (minIdx.n_rows != 1)
    {
        cout << "Warning! Could not detect anomaly class, ";
        cout << "using first class." << endl;
    }

    // Precision = true positives of rare class / 
    // (true positives + false positives)
    double precision = (double)confusionMat(minIdx(0), minIdx(0)) / 
                        (double)rsums(minIdx(0)); 
    // Recall: true positives / (true postives + false negatives)
    double recall = (double)confusionMat(minIdx(0), minIdx(0)) /
                    (double)csums(minIdx(0)); 

    // ... Or compute these values manually, but take a look at the
    // confusion matrix first to make sure                         
    //double precision = (double)confusionMat(0,0) / (confusionMat(0,0) + confusionMat(0,1));
    //double recall = (double)confusionMat(0,0) / (confusionMat(0,0) + confusionMat(1,0));


    // Recall, precision, F1-score
    double f1 = 2*precision*recall/(precision + recall);
    stats[1] = f1;
    stats[2] = precision;
    stats[3] = recall;
    cout << "Precision: " << precision << endl;
    cout << "Recall " << recall << endl;
    cout << "F1 Score: " << f1 << endl;


    // Kappa statistics

    /*
    double diagSum = 0.0;
    for (int i = 0; i < (int)confusionMat.n_rows; i++)
        diagSum += confusionMat(i,i);
    double pAgree = diagSum / (double)arma::accu(confusionMat);

    // classifier 1 says "yes"
    double a = (double)csums(minIdx(0)) / 
               (double)csums((minIdx(0)+1)%confusionMat.n_cols);

    // classifier 2 says "yes"
    double b = (double)rsums(minIdx(0)) / 
               (double)rsums((minIdx(0)+1)%confusionMat.n_rows);

    double posAgree =  a*b;                      
    double negAgree = (1.0 - a)*(1.0 - b);
    double cAgree =  posAgree + negAgree;
    double kappa = (pAgree - cAgree) / (1 - cAgree);
    */

    double matsum = arma::accu(confusionMat);
    double pAgree = (double)(confusionMat(0,0) + confusionMat(1,1))/ matsum;
    double cAgree = (double)rsums(0,0)/arma::accu(rsums) * (double)csums(0,0)/arma::accu(csums) + 
                    (double)rsums(1,0)/arma::accu(rsums) * (double)csums(0,1)/arma::accu(csums);
    double kappa = (pAgree - cAgree) / (1.0 - cAgree);

    stats[4] = kappa;
    cout << "Kappa: " << kappa << endl << endl;

    return stats;
}
