///////////////////////////////////////////////////////////////////////////////
// ANNClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#define ARMA_DONT_USE_CXX11

#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <omp.h>
#include <unordered_map>
#include "armadillo"
#include "ANNClassifier.h"
#include "../com/util.h"
#include <opencv2/opencv.hpp>

using namespace arma;
using namespace cv;
using namespace std;

// ____________________________________________________________________________
bool ANNClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;

    if (stoi(p[0]) < 0 || stoi(p[0]) > 1) return false;
    _trainNetwork = stoi(p[0]) == 0 ? false : true;

    if (stoi(p[1]) < 0 || stoi(p[1]) > 2) return false;
    _priors = stoi(p[1]);

    std::vector<std::string> v(p.begin()+2, p.end());
    if(!Classifier::bootstrap(v)) return false;

    cout << "Bootstrapping Neural Network ..." << endl;
    cout << "Priors: " << _priors << endl;

    // Get number of classes
    arma::mat uClasses = unique(_trainannot); // classes in ascending order
    _network = new CvANN_MLP();

    // Get stats on training data to build optimal model parameters
    // Priors: fixed prior class probabilities, see below
    vector<float> priors(uClasses.n_rows);

    // Use statistics of data in training set
    if (_priors == 0)
    {
       for (int i = 0; i < (int)uClasses.n_rows; i++)
        {
            arma::uvec classMatches =  find(_trainannot == uClasses(i,0));
            priors[i] = (classMatches.n_rows);
        }

        // Classes get weight according to how well they are represented
        // in the train set.
        std::for_each(priors.begin(), priors.end(), 
                [&](float &n){ n = (float)(n/(float)_train.n_rows); });
    }

    // Use equal priors
    if (_priors == 1)
        
        std::for_each(priors.begin(), priors.end(), 
            [&](float &n){ n = 1.0f / (float)(uClasses.n_rows); });

    // Use adaptive priors.
    else if (_priors == 2)
    {
        for (int i = 0; i < (int)uClasses.n_rows; i++)
        {
            arma::uvec classMatches =  find(_trainannot == uClasses(i,0));
            priors[i] = (classMatches.n_rows);
        }

        // Under-represented classes get higher probability to balance out bias
        std::for_each(priors.begin(), priors.end(), 
                [&](float &n){ n = 1.0f - (float)(n/(float)_train.n_rows); });
    }

	if (_trainNetwork)
    {

    	// Topology: input is ordered (not categorical), output is
        // categorical, so input layer has as many neurons as the length
        // of each feature vector, output layer has as many neurons as 
        // there are classes. 
        //
        // Good starting architecture: one hidden layer, which is twice as
        // large as the output layer, or if more than one hidden layer, use
        // same hidden layer size. Empirically determine the best fit
        // from there.
        //
        // Although the topology has great impact on performance, the real 
        // distinguishing characteristic of neural networks is the non linear
        // activation function at each node, so topology is less relevant.
        vector<int> layerSizes = {_train.n_cols, 
                                  2*uClasses.n_rows, 
                                  2*uClasses.n_rows, 
                                  2*uClasses.n_rows,
                                  uClasses.n_rows};
    	cv::Mat layers(1, layerSizes.size(), CV_32SC1);
    	for (size_t i = 0; i < layerSizes.size(); i++)
    		layers.at<int>(0, i) = layerSizes[i];
   
        // Set topology for the network, as well as the activation function
        // Last two params are alpha/beta activations, 0.0f is standard
        CvANN_MLP network;
    	network.create(layers, CvANN_MLP::SIGMOID_SYM);

        // Mapping from class identifier to index
        std::unordered_map<int,int> classMap;
        for (int i = 0; i < (int)uClasses.n_rows; i++)
            classMap.insert(make_pair<int,int>
                (floor(uClasses(i,0)+0.5f),int(i)));
        

        // Data to OpenCV. Note: responses are output vectors, indexing the class,
        // establishing which output node corresponds to which class.
        cv::Mat* data = new cv::Mat(_train.n_rows, _train.n_cols, CV_32F);
        cv::Mat* resp = new cv::Mat(_trainannot.n_rows, 
                                    uClasses.n_rows, CV_32F);
        cv::Mat* weights = new cv::Mat(_trainannot.n_rows, 1, CV_32F);
        for (int i = 0; i < (int)_trainannot.size(); i++)
        {
            // Weigh the sample according to its class and the specified priors
            float priorsVal = priors[classMap.at(floor(_trainannot(i,0)+0.5f))];
            weights->at<float>(i,0) = priorsVal;
            resp->at<float>(i,classMap[_trainannot[i]]) = 1.0f;
            for (int j = 0; j < (int)_train.n_cols; j++)
                data->at<float>(i,j) = _train(i,j);
        }

        // Build params (see opencv documentation for default values) and train
        // the neural network
        auto term_crit = cvTermCriteria(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 
                                        10000, 0.0001f);
        CvANN_MLP_TrainParams params(term_crit, CvANN_MLP_TrainParams::RPROP, 
                                     0.1, FLT_EPSILON);

        // Note - requires normalized inputs
        int a = network.train(*data, *resp, *weights, cv::Mat(), params, 
                               CvANN_MLP::NO_OUTPUT_SCALE);

        cout << "Iterations: " << a << endl;

        // write model
        network.save("./output/neuralnetwork.xml");
        delete data;
        delete resp;
        delete weights;
    }

    // Load Random Forest
    _network->load("./output/neuralnetwork.xml");
    return true;
}

// ____________________________________________________________________________
vector<int> ANNClassifier::classify()
{
    // Data to OpenCV
    cv::Mat samples(_test.n_rows, _test.n_cols, CV_32F);
    for (int j = 0; j < (int)_test.n_rows; j++)
        for (int k =0; k < (int)_test.n_cols; k++)
             samples.at<float>(j, k) = _test(j, k);

    arma::mat uClasses = unique(_trainannot); // classes in ascending order
     
    // Classify all samples
    cv::Mat response(_test.n_rows, uClasses.n_rows, CV_32F);
    _network->predict(samples, response);

    // Find max score in current row -> column refers to class
    vector<int> res;
    for (int i = 0; i < (int)_test.n_rows; i++)
    {
        vector<float> row(uClasses.n_rows);
        for (int j = 0; j < (int)uClasses.n_rows; j++) 
            row[j] = response.at<float>(i, j);

        auto it = max_element(row.begin(), row.end());
        auto maxindex = distance(row.begin(), it);
        res.push_back(floor(uClasses(maxindex,0)+0.5f));
    }
    return res;
}