﻿CREATE SCHEMA IF NOT EXISTS vtti;

-- Global tables, etc. --------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS vtti.dynamicmerged CASCADE;
CREATE TABLE vtti.dynamicmerged (
	trialid integer,
	epochid integer,
	epochtype integer,
	timestamp integer,
	eyeglancelocation integer,
	rawrotationx numeric,
	rawrotationy numeric,
	rawrotationz numeric,
	filteredrotationx numeric,
	filteredrotationy numeric,
	filteredrotationz numeric,
	timestampAccumulated integer,
	timestampConstant integer,
	hzAccumulated integer,
	hzConstant integer,
	cleanAccumulated integer,
	cleanConstant integer,
	cleanHzAccumulated integer,
	cleanHzConstant integer
);

-- Hold input static data
DROP TABLE IF EXISTS vtti.staticmerged CASCADE;
CREATE TABLE vtti.staticmerged (
	trialid integer,
	epochid integer,
	epochtype integer,
	timestamp integer,
	eyeglancelocation integer,
	rawrotationx numeric,
	rawrotationy numeric,
	rawrotationz numeric,
	filteredrotationx numeric,
	filteredrotationy numeric,
	filteredrotationz numeric,
	timestampAccumulated integer,
	timestampConstant integer,
	hzAccumulated integer,
	hzConstant integer,
	cleanAccumulated integer,
	cleanConstant integer,
	cleanHzAccumulated integer,
	cleanHzConstant integer
);

COPY vtti.dynamicmerged FROM E'C:\\Working\\Data\\VTTI\\VTTI-dynamic-sync-duration-merged.csv' DELIMITER ',' CSV;
COPY vtti.staticmerged FROM E'C:\\Working\\Data\\VTTI\\VTTI-static-sync-duration-merged.csv' DELIMITER ',' CSV;


-- Implement cantor pairing function
CREATE OR REPLACE FUNCTION vtti.pair2(x bigint, y bigint)
  RETURNS bigint AS
$func$
BEGIN
RETURN CAST((0.5 * (x + y)*(x + y + 1) + y) AS bigint);
END
$func$  LANGUAGE plpgsql IMMUTABLE;


-- Split function -------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------


-- Take input tables for dynamic and static datasets and split them into a training and test set each
CREATE OR REPLACE FUNCTION vtti.splitdata() RETURNS integer AS $$
BEGIN

DELETE FROM vtti.ordereddynamic;
DELETE FROM vtti.orderedstatic;
DELETE FROM vtti.dynamicsubs;
DELETE FROM vtti.staticsubs;

-- Dynamic data: 80% random training subjects, 20% test subjects
INSERT INTO vtti.ordereddynamic(
	SELECT * FROM vtti.dynamicmerged WHERE eyeglancelocation IN (2,4)
	ORDER BY trialid, epochid, timestamp);
	
WITH subs AS (SELECT DISTINCT trialid FROM vtti.ordereddynamic ORDER BY trialid)
INSERT INTO vtti.dynamicsubs (SELECT * FROM subs ORDER BY random() LIMIT 0.8*(SELECT COUNT(*) FROM subs));

-- Static data: 80% random training subjects, 20% test subjects
INSERT INTO vtti.orderedstatic(
	SELECT * FROM vtti.staticmerged WHERE eyeglancelocation IN (2,4)
	ORDER BY trialid, epochid, timestamp);
	
WITH subs AS (SELECT DISTINCT trialid FROM vtti.orderedstatic ORDER BY trialid)
INSERT INTO vtti.staticsubs (SELECT * FROM subs ORDER BY random() LIMIT 0.8*(SELECT COUNT(*) FROM subs));


RETURN 1;

END;
$$ LANGUAGE plpgsql;


-- Dynamic data: raw data table
DROP TABLE IF EXISTS vtti.ordereddynamic CASCADE;
CREATE TABLE vtti.ordereddynamic (
	trialid integer,
	epochid integer,
	epochtype integer,
	timestamp integer,
	eyeglancelocation integer,
	rawrotationx numeric,
	rawrotationy numeric,
	rawrotationz numeric,
	filteredrotationx numeric,
	filteredrotationy numeric,
	filteredrotationz numeric,
	timestampAccumulated integer,
	timestampConstant integer,
	hzAccumulated integer,
	hzConstant integer,
	cleanAccumulated integer,
	cleanConstant integer,
	cleanHzAccumulated integer,
	cleanHzConstant integer
);

-- Static data: raw data table
DROP TABLE IF EXISTS vtti.orderedstatic CASCADE;
CREATE TABLE vtti.orderedstatic (
	trialid integer,
	epochid integer,
	epochtype integer,
	timestamp integer,
	eyeglancelocation integer,
	rawrotationx numeric,
	rawrotationy numeric,
	rawrotationz numeric,
	filteredrotationx numeric,
	filteredrotationy numeric,
	filteredrotationz numeric,
	timestampAccumulated integer,
	timestampConstant integer,
	hzAccumulated integer,
	hzConstant integer,
	cleanAccumulated integer,
	cleanConstant integer,
	cleanHzAccumulated integer,
	cleanHzConstant integer
);

-- Store which subjects were used for the current split of train and test, for both dynamic and static datasets
DROP TABLE IF EXISTS vtti.dynamicsubs;
DROP TABLE IF EXISTS vtti.staticsubs;
CREATE TABLE vtti.dynamicsubs (subject integer);
CREATE TABLE vtti.staticsubs (subject integer);


-- Data views -----------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------


-- Training data
CREATE OR REPLACE VIEW vtti.dynamictrain AS(
	SELECT CASE WHEN eyeglancelocation = 7 THEN 0 
		    WHEN eyeglancelocation = 5 THEN 0
		    WHEN eyeglancelocation = 2 THEN 0
		    WHEN eyeglancelocation = 4 THEN 1
		    WHEN eyeglancelocation = 12 THEN 0
		    WHEN eyeglancelocation = 13 THEN 0
		    ELSE 3 END AS eyeglancelocation, 
		    filteredrotationx, filteredrotationy, filteredrotationz, cleanconstant, cleanAccumulated, trialid, epochid, timestamp
	FROM vtti.ordereddynamic WHERE trialid IN (SELECT * FROM vtti.dynamicsubs) 
	ORDER BY trialid, epochid, timestamp ASC
);

-- Long duration glances are all glances > 1.6 sec.
CREATE OR REPLACE VIEW vtti.dynamicdurationtrain AS (
	SELECT CASE WHEN timestampconstant > 1600 THEN 1 ELSE 0 END as longdurationglance,
	filteredrotationx, filteredrotationy, filteredrotationz, trialid, epochid, timestamp
	FROM vtti.dynamicmerged -- Take all glances from all tasks into account, not just the current split in ordereddynamic
	WHERE trialid IN (SELECT * FROM vtti.dynamicsubs) 
	ORDER BY trialid, epochid, timestamp ASC
);


-- Test data
-- Validation groups: one group per subject, epoch, eyeglance
CREATE OR REPLACE VIEW vtti.dynamicval AS (
	SELECT vtti.pair2(vtti.pair2(trialid,epochid),eyeglancelocation) AS seqid, 
	CASE WHEN eyeglancelocation = 7 THEN 0 
	     WHEN eyeglancelocation = 5 THEN 0
	     WHEN eyeglancelocation = 2 THEN 0
	     WHEN eyeglancelocation = 4 THEN 1
	     WHEN eyeglancelocation = 12 THEN 0
	     WHEN eyeglancelocation = 13 THEN 0
	     END AS eyeglancelocation,
	     filteredrotationx, filteredrotationy, filteredrotationz, cleanconstant, cleanAccumulated, trialid, epochid, timestamp
	FROM vtti.ordereddynamic WHERE trialid NOT IN (SELECT * FROM vtti.dynamicsubs) 
	AND vtti.pair2(vtti.pair2(trialid,epochid),eyeglancelocation) IN
		(SELECT vtti.pair2(vtti.pair2(trialid,epochid),eyeglancelocation)
		 FROM vtti.ordereddynamic GROUP BY trialid,epochid,eyeglancelocation HAVING COUNT(*) >= 10)
	ORDER BY trialid, epochid, timestamp ASC
);



-- Long duration glances are all glances > 1.6 sec.
-- Validation groups: one group per subject, epoch, eyeglance
CREATE OR REPLACE VIEW vtti.dynamicdurationval AS (
	SELECT vtti.pair2(vtti.pair2(trialid,epochid),eyeglancelocation) AS seqid, 
	CASE WHEN timestampconstant > 1600 THEN 1 ELSE 0 END AS longdurationglance, filteredrotationx, filteredrotationy, filteredrotationz, cleanconstant, cleanAccumulated, trialid, epochid, timestamp
	FROM vtti.dynamicmerged -- Take all glances from all tasks into account, not just the current split in ordereddynamic
	WHERE trialid NOT IN (SELECT * FROM vtti.dynamicsubs) 
	AND vtti.pair2(vtti.pair2(trialid,epochid),eyeglancelocation) IN
		(SELECT vtti.pair2(vtti.pair2(trialid,epochid),eyeglancelocation)
		 FROM vtti.dynamicmerged -- Take all glances from all tasks into account, not just the current split in ordereddynamic
		 GROUP BY trialid,epochid,eyeglancelocation HAVING COUNT(*) >= 10)
	ORDER BY trialid, epochid, timestamp ASC
);

--
-- Static data
-- 
--DELETE FROM vtti.statictrain;
--DELETE FROM vtti.staticval;

-- Training data
--WITH static AS (SELECT * FROM vtti.staticmerged WHERE (eyeglancelocation = 5 OR eyeglancelocation = 4)
--ORDER BY trialid, epochid, timestamp),
--trainstatic AS 
--(SELECT eyeglancelocation, filteredrotationx, filteredrotationy, filteredrotationz, cleanconstant, cleanAccumulated 
--FROM static WHERE trialid IN (SELECT * FROM vtti.staticsubs) ORDER BY trialid, epochid, timestamp ASC)
--INSERT INTO vtti.statictrain (SELECT * FROM trainstatic);

-- Test data
--WITH static AS (SELECT * FROM vtti.staticmerged WHERE (eyeglancelocation = 5 OR eyeglancelocation = 4)
--ORDER BY trialid, epochid, timestamp),
--staticvalidateunfiltered AS ( SELECT vtti.pair2(vtti.pair2(trialid,epochid),eyeglancelocation) AS seqid, eyeglancelocation, filteredrotationx, filteredrotationy, filteredrotationz, cleanconstant
--FROM static WHERE trialid NOT IN (SELECT * FROM vtti.staticsubs) ORDER BY trialid, epochid, timestamp ASC),
--validatestatic AS (SELECT * FROM staticvalidateunfiltered WHERE seqid IN (SELECT seqid FROM staticvalidateunfiltered GROUP BY seqid HAVING COUNT(*) >= 10))
--INSERT INTO vtti.staticval (SELECT * FROM validatestatic);








