﻿CREATE SCHEMA IF NOT EXISTS hmm;

-- 2012b tables ---------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------

-- 2012b study with exactly one coder per task per subject.
DROP TABLE IF EXISTS hmm.singlecoder2012b;
CREATE TABLE hmm.singlecoder2012b (
  subject integer,
  task text,
  coder text,
  session integer,
  look text,
  duration double precision,
  timestamp double precision,
  gender text,
  age integer,
  agegroup text
);

-- Filter: take only "good" cases, as laid out by 2012b_Log
DROP TABLE IF EXISTS hmm.goodcases2012b;
CREATE TABLE hmm.goodcases2012b ( subject integer );

-- Fill tables with data from file
COPY hmm.singlecoder2012b FROM E'C:\\Working\\Data\\2012b\\2012b-singlecoder-full.csv' DELIMITER ',' CSV;
COPY hmm.goodcases2012b FROM E'C:\\Working\\Data\\2012b\\2012b-Log-goodCases.csv' DELIMITER ',' CSV;

-- 2013a tables ---------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------

-- 2013a study with exactly one coder per task per subject.
DROP TABLE IF EXISTS hmm.singlecoder2013a;
CREATE TABLE hmm.singlecoder2013a (
  subject integer,
  task_code integer,
  task text,
  coder text,
  session_id integer,
  duration double precision,
  look text,
  timestamp double precision,
  media_part integer
);

-- Filter: take only "good" cases, as laid out by 2013a_Log
DROP TABLE IF EXISTS hmm.goodcases2013a;
CREATE TABLE hmm.goodcases2013a (
  subject integer,
  gender text,
  age integer,
  agegroup text,
  training text
);

-- Fill tables with data from file
COPY hmm.singlecoder2013a FROM E'C:\\Working\\Data\\2013a\\2013a-singlecoder.csv' DELIMITER ',' CSV;
COPY hmm.goodcases2013a FROM E'C:\\Working\\Data\\2013a\\2013a-Log-goodCases.csv' DELIMITER ',' CSV;


-- Global tables, etc. --------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------

-- Global encoding keys: Mapping of desired glance location / tasks to integer values. 
DROP TABLE IF EXISTS hmm.glanceencodingsmaster;
DROP TABLE IF EXISTS hmm.taskencodingsmaster;
CREATE TABLE hmm.glanceencodingsmaster (
  description text,
  code integer
);
CREATE TABLE hmm.taskencodingsmaster (
  description text,
  code integer
);

-- Fill tables
INSERT INTO hmm.glanceencodingsmaster(
  SELECT look, ROW_NUMBER() OVER() AS code FROM
  (SELECT DISTINCT look FROM hmm.singlecoder2012b 
  WHERE look NOT LIKE '%unknown%' AND look NOT LIKE '%other%' AND look NOT LIKE '%visible%'
  UNION 
  SELECT DISTINCT look FROM hmm.singlecoder2013a
  WHERE look NOT LIKE '%unknown%' AND look NOT LIKE '%other%' AND look NOT LIKE '%visible%') s
);
INSERT INTO hmm.taskencodingsmaster(
  SELECT DISTINCT task, ROW_NUMBER() OVER() AS code FROM
  (SELECT DISTINCT task FROM hmm.singlecoder2012b 
  UNION 
  SELECT DISTINCT task FROM hmm.singlecoder2013a) s
);

-- Cantor pairing function
DROP FUNCTION IF EXISTS hmm.pair2(x bigint, y bigint) CASCADE;
CREATE FUNCTION hmm.pair2(x bigint, y bigint)
  RETURNS bigint AS
$func$
BEGIN
RETURN CAST((0.5 * (x + y)*(x + y + 1) + y) AS bigint);
END
$func$  LANGUAGE plpgsql IMMUTABLE;


-- 2012b ----------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------

-- fix this ------------------------------------------
DROP FUNCTION IF EXISTS hmm.splitdata2012bdemographics();
CREATE FUNCTION hmm.splitdata2012bdemographics() RETURNS integer AS $$
BEGIN

PERFORM hmm.splitdata2012b();

RETURN 1;
END;
$$ LANGUAGE plpgsql;

-- Split data into training, test, and validation sets
DROP FUNCTION IF EXISTS hmm.splitdata2012b();
CREATE FUNCTION hmm.splitdata2012b() RETURNS integer AS $$
BEGIN

DELETE FROM hmm.ordereddemographics2012b;
DELETE FROM hmm.ordered2012b;
DELETE FROM hmm.trainsubs2012b;

-- RadioM vs. RadioV vs. Baseline class split
INSERT INTO hmm.ordered2012b(
  SELECT subject, t.code AS task, m.code AS look, duration, timestamp, ROW_NUMBER() OVER() AS rank,
         CASE WHEN s.task LIKE '%radiom_base%' OR s.task LIKE '%radiov_base%' THEN 0
              WHEN s.task LIKE '%radiom_1%' OR s.task LIKE '%radiom_2%' OR s.task LIKE '%radiom_4%' OR s.task LIKE '%radiom_6%' THEN 1
              WHEN s.task LIKE '%radiov_1%' OR s.task LIKE '%radiov_2%' OR s.task LIKE '%radiov_4%' OR s.task LIKE '%radiov_6%' THEN 2 END AS tasktype
  FROM hmm.singlecoder2012b s JOIN hmm.glanceencodingsmaster m ON s.look = m.description JOIN hmm.taskencodingsmaster t ON t.description = s.task
  WHERE s.subject IN (SELECT subject FROM hmm.goodcases2012b) AND
  (s.task LIKE '%radiom_base%' OR s.task LIKE '%radiov_base%' OR 
   s.task LIKE '%radiom_1%' OR s.task LIKE '%radiom_2%' OR s.task LIKE '%radiom_4%' OR s.task LIKE '%radiom_6%' OR 
   s.task LIKE '%radiov_1%' OR s.task LIKE '%radiov_2%' OR s.task LIKE '%radiov_4%' OR s.task LIKE '%radiov_6%')
  ORDER BY s.subject, s.task, s.timestamp
);

-- Age and gender class split
INSERT INTO hmm.ordereddemographics2012b(
  SELECT subject, t.code AS task, m.code AS look, duration, timestamp, ROW_NUMBER() OVER() AS rank,
	 CASE WHEN (s.gender LIKE '%Male%') THEN 0
	      WHEN (s.gender LIKE '%Female%') THEN 1
  END as demographic
  FROM hmm.singlecoder2012b s JOIN hmm.glanceencodingsmaster m ON s.look = m.description JOIN hmm.taskencodingsmaster t ON t.description = s.task
  WHERE s.subject IN (SELECT subject FROM hmm.goodcases2012b)
  ORDER BY s.subject, s.task, s.timestamp
);


INSERT INTO hmm.trainsubs2012b(
  SELECT * 
  FROM (SELECT DISTINCT subject FROM hmm.ordered2012b) s
  ORDER BY random() 
  LIMIT 0.8*(SELECT COUNT(DISTINCT subject) FROM hmm.ordered2012b)
);

RETURN 1;
END;
$$ LANGUAGE plpgsql;

-- Raw data table -> order by subject, task, timestamp, only take relevant subjects and tasks
DROP TABLE IF EXISTS hmm.ordered2012b CASCADE;
CREATE TABLE hmm.ordered2012b (
  subject integer,
  task integer,
  look integer,
  duration double precision,
  timestamp double precision,
  rank integer,
  tasktype integer
);

DROP TABLE IF EXISTS hmm.ordereddemographics2012b CASCADE;
CREATE TABLE hmm.ordereddemographics2012b (
  subject integer,
  task integer,
  look integer,
  duration double precision,
  timestamp double precision,
  rank integer,
  demographic integer
);

-- Training subjects table (80% of total number of subjects)
DROP TABLE IF EXISTS hmm.trainsubs2012b CASCADE;
CREATE TABLE hmm.trainsubs2012b (subject integer);

-- Training and validation data: manual vs. voice vs. baseline split
CREATE OR REPLACE VIEW hmm.trainglance2012b AS (
  SELECT o.tasktype, o.look
  FROM hmm.ordered2012b o 
  WHERE o.subject IN (SELECT * FROM hmm.trainsubs2012b)
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm.trainduration2012b AS (
  SELECT o.tasktype, o.duration
  FROM hmm.ordered2012b o 
  WHERE o.subject IN (SELECT * FROM hmm.trainsubs2012b)
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm.validateglance2012b AS (
  SELECT hmm.pair2(subject,task) AS seqid, tasktype, look
  FROM hmm.ordered2012b
  WHERE subject NOT IN (SELECT * FROM hmm.trainsubs2012b) AND hmm.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm.pair2(subject,task) AS seqid 
	 FROM hmm.ordered2012b GROUP BY subject,task HAVING COUNT(*) > 10) s)
  ORDER BY hmm.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm.validateduration2012b AS (
  SELECT hmm.pair2(subject,task) AS seqid, tasktype, duration
  FROM hmm.ordered2012b
  WHERE subject NOT IN (SELECT * FROM hmm.trainsubs2012b) AND hmm.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm.pair2(subject,task) AS seqid 
	 FROM hmm.ordered2012b GROUP BY subject,task HAVING COUNT(*) > 10) s)
  ORDER BY hmm.pair2(subject,task), rank
);


-- Training and validation data: demographics split
CREATE OR REPLACE VIEW hmm.trainglance2012bdemographics AS (
  SELECT o.demographic, o.look
  FROM hmm.ordereddemographics2012b o 
  WHERE o.subject IN (SELECT * FROM hmm.trainsubs2012b)
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm.trainduration2012bdemographics AS (
  SELECT o.demographic, o.duration
  FROM hmm.ordereddemographics2012b o 
  WHERE o.subject IN (SELECT * FROM hmm.trainsubs2012b)
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm.validateglance2012bdemographics AS (
  SELECT hmm.pair2(subject,task) AS seqid, demographic, look
  FROM hmm.ordereddemographics2012b
  WHERE subject NOT IN (SELECT * FROM hmm.trainsubs2012b) AND hmm.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm.pair2(subject,task) AS seqid 
	 FROM hmm.ordereddemographics2012b GROUP BY subject,task HAVING COUNT(*) > 10) s)
  ORDER BY hmm.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm.validateduration2012bdemographics AS (
  SELECT hmm.pair2(subject,task) AS seqid, demographic, duration
  FROM hmm.ordereddemographics2012b
  WHERE subject NOT IN (SELECT * FROM hmm.trainsubs2012b) AND hmm.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm.pair2(subject,task) AS seqid 
	 FROM hmm.ordereddemographics2012b GROUP BY subject,task HAVING COUNT(*) > 10) s)
  ORDER BY hmm.pair2(subject,task), rank
);


-- 2013a ----------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------

-- Split data into training, test, and validation sets
DROP FUNCTION IF EXISTS hmm.splitdata2013a();
CREATE FUNCTION hmm.splitdata2013a() RETURNS integer AS $$
BEGIN

DELETE FROM hmm.ordered2013a;
DELETE FROM hmm.trainsubs2013a;

INSERT INTO hmm.ordered2013a(
  SELECT subject, t.code AS task, m.code AS look, duration, timestamp, ROW_NUMBER() OVER() AS rank,
         CASE WHEN task LIKE '%radiom_base%' OR task LIKE '%radiov_base%' THEN 0
              WHEN task LIKE '%radiom_1%' OR task LIKE '%radiom_2%' OR task LIKE '%radiom_4%' OR task LIKE '%radiom_6%' THEN 1
              WHEN task LIKE '%radiov_1%' OR task LIKE '%radiov_2%' OR task LIKE '%radiov_4%' OR task LIKE '%radiov_6%' THEN 2 END AS tasktype
  FROM hmm.singlecoder2013a s JOIN hmm.glanceencodingsmaster m ON s.look = m.description JOIN hmm.taskencodingsmaster t ON t.description = s.task
  WHERE s.subject IN (SELECT subject FROM hmm.goodcases2013a) AND
  (task LIKE '%radiom_base%' OR task LIKE '%radiov_base%' OR 
   task LIKE '%radiom_1%' OR task LIKE '%radiom_2%' OR task LIKE '%radiom_4%' OR task LIKE '%radiom_6%' OR 
   task LIKE '%radiov_1%' OR task LIKE '%radiov_2%' OR task LIKE '%radiov_4%' OR task LIKE '%radiov_6%')
  ORDER BY s.subject, s.task, s.timestamp
);


INSERT INTO hmm.trainsubs2013a(
  SELECT * 
  FROM (SELECT DISTINCT subject FROM hmm.ordered2013a) s
  ORDER BY random() 
  LIMIT 0.8*(SELECT COUNT(DISTINCT subject) FROM hmm.ordered2013a)
);

RETURN 1;
END;
$$ LANGUAGE plpgsql;


-- Raw data table -> order by subject, task, timestamp, only take relevant subjects and tasks
DROP TABLE IF EXISTS hmm.ordered2013a CASCADE;
CREATE TABLE hmm.ordered2013a (
  subject integer,
  task integer,
  look integer,
  duration double precision,
  timestamp double precision,
  rank integer,
  tasktype integer
);

-- Training subjects table (80% of total number of subjects)
DROP TABLE IF EXISTS hmm.trainsubs2013a;
CREATE TABLE hmm.trainsubs2013a (subject integer);

-- Training and validation data
CREATE OR REPLACE VIEW hmm.trainglance2013a AS(
  SELECT o.tasktype, o.look
  FROM hmm.ordered2013a o
  WHERE o.subject IN (SELECT * FROM hmm.trainsubs2013a)
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm.trainduration2013a AS(
  SELECT o.tasktype, o.duration
  FROM hmm.ordered2013a o
  WHERE o.subject IN (SELECT * FROM hmm.trainsubs2013a)
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm.trainglanceduration2013a AS(
  SELECT o.tasktype, o.look, o.duration
  FROM hmm.ordered2013a o
  WHERE o.subject IN (SELECT * FROM hmm.trainsubs2013a)
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm.validateglance2013a AS (
  SELECT hmm.pair2(subject,task) AS seqid, tasktype, look
  FROM hmm.ordered2013a
  WHERE subject NOT IN (SELECT * FROM hmm.trainsubs2013a) AND hmm.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm.pair2(subject,task) AS seqid 
	 FROM hmm.ordered2013a GROUP BY subject,task HAVING COUNT(*) > 10) s)
  ORDER BY hmm.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm.validateduration2013a AS (
  SELECT hmm.pair2(subject,task) AS seqid, tasktype, duration
  FROM hmm.ordered2013a
  WHERE subject NOT IN (SELECT * FROM hmm.trainsubs2013a) AND hmm.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm.pair2(subject,task) AS seqid 
	 FROM hmm.ordered2013a GROUP BY subject,task HAVING COUNT(*) > 10) s)
  ORDER BY hmm.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm.validateglanceduration2013a AS (
  SELECT hmm.pair2(subject,task) AS seqid, tasktype, look, duration
  FROM hmm.ordered2013a
  WHERE subject NOT IN (SELECT * FROM hmm.trainsubs2013a) AND hmm.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm.pair2(subject,task) AS seqid 
	 FROM hmm.ordered2013a GROUP BY subject,task HAVING COUNT(*) > 10) s)
  ORDER BY hmm.pair2(subject,task), rank
);




-- 2012b + 2013a --------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------

-- Split data into training, test, and validation sets
DROP FUNCTION IF EXISTS hmm.splitdata2012b2013a();
CREATE FUNCTION hmm.splitdata2012b2013a() RETURNS integer AS $$
BEGIN

-- Split 2012b, 2013a datasets
PERFORM hmm.splitdata2012b();
PERFORM hmm.splitdata2013a();

RETURN 1;
END;
$$ LANGUAGE plpgsql;

-- Training and validation data
CREATE OR REPLACE VIEW hmm.trainglance2012b2013a AS (
  SELECT tasktype, look FROM (
  
  (SELECT '2012b' AS study, tasktype, look, rank
  FROM hmm.ordered2012b
  WHERE subject IN (SELECT * FROM hmm.trainsubs2012b)
  ORDER BY rank)

  UNION ALL 

  (SELECT '2013a' AS study, tasktype, look, rank
  FROM hmm.ordered2013a
  WHERE subject IN (SELECT * FROM hmm.trainsubs2013a)
  ORDER BY rank)
  ) s
  ORDER BY study, rank
);
CREATE OR REPLACE VIEW hmm.trainduration2012b2013a AS (
  SELECT tasktype, duration FROM (
  
  (SELECT '2012b' AS study, tasktype, duration, rank
  FROM hmm.ordered2012b
  WHERE subject IN (SELECT * FROM hmm.trainsubs2012b)
  ORDER BY rank)

  UNION ALL 

  (SELECT '2013a' AS study, tasktype, duration, rank
  FROM hmm.ordered2013a
  WHERE subject IN (SELECT * FROM hmm.trainsubs2013a)
  ORDER BY rank)
  ) s
  ORDER BY study, rank
);
CREATE OR REPLACE VIEW hmm.validateglance2012b2013a AS (

  WITH filtered AS (
  SELECT seqid, tasktype, look, rank FROM (
  
  (SELECT hmm.pair2(1,hmm.pair2(subject,task)) AS seqid, tasktype, look, rank
  FROM hmm.ordered2012b
  WHERE subject NOT IN (SELECT * FROM hmm.trainsubs2012b)
  ORDER BY rank)

  UNION ALL 

  (SELECT hmm.pair2(2,hmm.pair2(subject,task)) AS seqid, tasktype, look, rank
  FROM hmm.ordered2013a
  WHERE subject NOT IN (SELECT * FROM hmm.trainsubs2013a)
  ORDER BY rank)
  ) s
  ORDER BY seqid, rank)
  
  SELECT seqid, tasktype, look
  FROM filtered 
  WHERE seqid IN (SELECT seqid FROM filtered GROUP BY seqid HAVING COUNT(*) > 10)
  ORDER BY seqid, rank
);
CREATE OR REPLACE VIEW hmm.validateduration2012b2013a AS (

  WITH filtered AS (
  SELECT seqid, tasktype, duration, rank FROM (
  
  (SELECT hmm.pair2(1,hmm.pair2(subject,task)) AS seqid, tasktype, duration, rank
  FROM hmm.ordered2012b
  WHERE subject NOT IN (SELECT * FROM hmm.trainsubs2012b)
  ORDER BY rank)

  UNION ALL 

  (SELECT hmm.pair2(2,hmm.pair2(subject,task)) AS seqid, tasktype, duration, rank
  FROM hmm.ordered2013a
  WHERE subject NOT IN (SELECT * FROM hmm.trainsubs2013a)
  ORDER BY rank)
  ) s
  ORDER BY seqid, rank)
  
  SELECT seqid, tasktype, duration 
  FROM filtered 
  WHERE seqid IN (SELECT seqid FROM filtered GROUP BY seqid HAVING COUNT(*) > 10)
  ORDER BY seqid, rank
);
