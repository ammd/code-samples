﻿DROP EXTENSION IF EXISTS file_fdw CASCADE;
CREATE EXTENSION file_fdw;
CREATE SERVER pgserver FOREIGN DATA WRAPPER file_fdw;

-- Dynamic Data
CREATE FOREIGN TABLE clipDynamicEpochsV1 (
trialID integer,
epochID integer,
epochType integer,
timeOfDay integer,
baseballCap integer,
glasses integer,
epochBeginTimestamp integer,
epochEndTimestamp integer
)SERVER pgserver
OPTIONS (filename E'C:\\Working\\Data\\VTTI\\clipDynamicEpochsV1.txt', format 'csv');

CREATE FOREIGN TABLE clipDynamicEyeglanceV1 (
trialID integer,
epochID integer,
timestamp integer,
eyeglanceLocation integer
)SERVER pgserver
OPTIONS (filename E'C:\\Working\\Data\\VTTI\\clipDynamicEyeglanceV1.txt', format 'csv');

CREATE FOREIGN TABLE clipDynamicFacialAnnotationV1 (
trialID integer,
epochID integer,
analystID integer,
timestamp integer,
facialLandmark integer,
x double precision,
y double precision
)SERVER pgserver
OPTIONS (filename E'C:\\Working\\Data\\VTTI\\clipDynamicFacialAnnotationV1.txt', format 'csv');

CREATE FOREIGN TABLE clipDynamicGMRotationEstimatesV1 (
trialID integer,
epochID integer,
timestamp integer,
rawRotationX double precision,
rawRotationY double precision,
rawRotationZ double precision,
filteredRotationX double precision,
filteredRotationY double precision,
filteredRotationZ double precision
)SERVER pgserver
OPTIONS (filename E'C:\\Working\\Data\\VTTI\\clipDynamicGMRotationEstimatesV1.txt', format 'csv');

CREATE FOREIGN TABLE clipDynamicMaskV1 (
trialID integer,
epochID integer,
timestamp integer,
headRotationX double precision,
headRotationXBaseline double precision,
headRotationY double precision,
headRotationYBaseline double precision,
headRotationZ double precision,
headRotationZBaseline double precision,
confidence integer
)SERVER pgserver
OPTIONS (filename E'C:\\Working\\Data\\VTTI\\clipDynamicMaskV1.txt', format 'csv');


-- Static Data
CREATE FOREIGN TABLE clipStaticEpochsV1 (
trialID integer,
epochID integer,
epochType integer,
timeOfDay integer,
baseballCap integer,
glasses integer,
epochBeginTimestamp integer,
epochEndTimestamp integer
)SERVER pgserver
OPTIONS (filename E'C:\\Working\\Data\\VTTI\\clipStaticEpochsV1.txt', format 'csv');

CREATE FOREIGN TABLE clipStaticEyeglanceV1 (
trialID integer,
epochID integer,
timestamp integer,
eyeglanceLocation integer
)SERVER pgserver
OPTIONS (filename E'C:\\Working\\Data\\VTTI\\clipStaticEyeglanceV1.txt', format 'csv');

CREATE FOREIGN TABLE clipStaticFacialAnnotationV1 (
trialID integer,
epochID integer,
analystID integer,
timestamp integer,
facialLandmark integer,
x double precision,
y double precision
)SERVER pgserver
OPTIONS (filename E'C:\\Working\\Data\\VTTI\\clipStaticFacialAnnotationV1.txt', format 'csv');

CREATE FOREIGN TABLE clipStaticGMRotationEstimatesV1 (
trialID integer,
epochID integer,
timestamp integer,
rawRotationX double precision,
rawRotationY double precision,
rawRotationZ double precision,
filteredRotationX double precision,
filteredRotationY double precision,
filteredRotationZ double precision
)SERVER pgserver
OPTIONS (filename E'C:\\Working\\Data\\VTTI\\clipStaticGMRotationEstimatesV1.txt', format 'csv');

CREATE FOREIGN TABLE clipStaticMaskV1 (
trialID integer,
epochID integer,
timestamp integer,
headRotationX double precision,
headRotationXBaseline double precision,
headRotationY double precision,
headRotationYBaseline double precision,
headRotationZ double precision,
headRotationZBaseline double precision,
confidence integer
)SERVER pgserver
OPTIONS (filename E'C:\\Working\\Data\\VTTI\\clipStaticMaskV1.txt', format 'csv');




-- Query -------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

WITH 

-- Dynamic Data -------------------------------------

-- 222 rows
dynamicEpochs AS (SELECT * FROM clipDynamicEpochsV1 ORDER BY trialid, epochid),

-- 19165 rows
dynamicEyeglance AS (SELECT * FROM clipDynamicEyeglanceV1 ORDER BY trialid, epochid, timestamp),

-- 18037 rows
dynamicRotation AS (SELECT * FROM clipDynamicGMRotationEstimatesV1 ORDER BY trialid, epochid, timestamp),

-- 16772 rows. Join rotation and eyeglance by subject, task, and timestamp
dynamiceyeglancerotation AS (SELECT * FROM dynamiceyeglance join dynamicrotation USING (trialid,epochid,timestamp) ORDER BY trialid, epochid, timestamp),

-- 19165 rows. As above, but will have gaps wherever rotation data is missing.
dynamiceyeglancerotationfull AS (SELECT * FROM dynamiceyeglance left outer join dynamicrotation using (trialid,epochid,timestamp) ORDER BY trialid, epochid, timestamp),

-- statistics on known data (no gaps present) for each subject and glance location
dynamicsubjectstats AS (SELECT trialid, eyeglancelocation, avg(rawrotationx) as avgrawrotationx, avg(rawrotationy) as avgrawrotationy, avg(rawrotationz) as avgrawrotationz, avg(filteredrotationx) as avgfilteredrotationx, avg(filteredrotationy) as avgfilteredrotationy, avg(filteredrotationz) as avgfilteredrotationz 
FROM dynamiceyeglancerotation GROUP BY trialid, eyeglancelocation),

-- merge averages with original data set -> should have no more gaps
dynamicmergedstats AS (SELECT * FROM dynamiceyeglancerotationfull LEFT OUTER JOIN dynamicsubjectstats USING (trialid,eyeglancelocation) ORDER BY trialid, epochid, timestamp),

-- Merge with epochs info using subject (trial) id and epoch id. 
dynamicrenamed AS (
SELECT trialid,epochid,epochType,timestamp,eyeglancelocation, 
CASE WHEN rawrotationx IS NULL THEN avgrawrotationx ELSE rawrotationx END AS rawrotationx,  
CASE WHEN rawrotationy IS NULL THEN avgrawrotationy ELSE rawrotationy END AS rawrotationy,
CASE WHEN rawrotationz IS NULL THEN avgrawrotationz ELSE rawrotationz END AS rawrotationz,
CASE WHEN filteredrotationx IS NULL THEN avgfilteredrotationx ELSE filteredrotationx END AS filteredrotationx,  
CASE WHEN filteredrotationy IS NULL THEN avgfilteredrotationy ELSE filteredrotationy END AS filteredrotationy,
CASE WHEN filteredrotationz IS NULL THEN avgfilteredrotationz ELSE filteredrotationz END AS filteredrotationz
FROM dynamicmergedstats JOIN dynamicEpochs USING(trialid,epochid) ORDER BY trialid, epochid, timestamp),

-- Only keep groups which, after adding in mean information for each subject and eyeglance, are still empty (i.e. no original information was provided)
dynamicoutput AS (
SELECT * FROM dynamicrenamed WHERE (rawrotationx IS NOT NULL OR rawrotationy IS NOT NULL OR rawrotationz IS NOT NULL OR
filteredrotationx IS NOT NULL OR filteredrotationy IS NOT NULL OR filteredrotationz IS NOT NULL)),



-- Static Data -------------------------------------

-- 1053 rows
staticEpochs AS (SELECT * FROM clipStaticEpochsV1 ORDER BY trialid, epochid),

-- 49789 rows
staticEyeglance AS (SELECT * FROM clipStaticEyeglanceV1 ORDER BY trialid, epochid, timestamp),

-- 40776 rows
staticRotation AS (SELECT * FROM clipStaticGMRotationEstimatesV1 ORDER BY trialid, epochid, timestamp),

-- Join rotation and eyeglance by subject, task, and timestamp
staticeyeglancerotation AS (SELECT * FROM staticeyeglance join staticrotation using (trialid,epochid,timestamp) ORDER BY trialid,epochid,timestamp),

-- As above, but will have "holes" wherever rotation data is missing.
staticeyeglancerotationfull AS (SELECT * FROM staticeyeglance left outer join staticrotation using (trialid,epochid,timestamp) ORDER BY trialid,epochid,timestamp),

-- statistics on known data (no gaps present) for each subject and glance location
staticsubjectstats AS (SELECT trialid, eyeglancelocation, avg(rawrotationx) as avgrawrotationx, avg(rawrotationy) as avgrawrotationy, avg(rawrotationz) as avgrawrotationz, avg(filteredrotationx) as avgfilteredrotationx, avg(filteredrotationy) as avgfilteredrotationy, avg(filteredrotationz) as avgfilteredrotationz 
FROM staticeyeglancerotation GROUP BY trialid, eyeglancelocation),

-- merge averages with original data set
staticmergedstats AS (SELECT * FROM staticeyeglancerotationfull LEFT OUTER JOIN staticsubjectstats USING (trialid,eyeglancelocation) ORDER BY trialid,epochid,timestamp),

-- Merge with epochs info using subject (trial) id and epoch id.
staticrenamed AS (
SELECT trialid,epochid,epochType,timestamp,eyeglancelocation, 
CASE WHEN rawrotationx IS NULL THEN avgrawrotationx ELSE rawrotationx END AS rawrotationx,  
CASE WHEN rawrotationy IS NULL THEN avgrawrotationy ELSE rawrotationy END AS rawrotationy,
CASE WHEN rawrotationz IS NULL THEN avgrawrotationz ELSE rawrotationz END AS rawrotationz,
CASE WHEN filteredrotationx IS NULL THEN avgfilteredrotationx ELSE filteredrotationx END AS filteredrotationx,  
CASE WHEN filteredrotationy IS NULL THEN avgfilteredrotationy ELSE filteredrotationy END AS filteredrotationy,
CASE WHEN filteredrotationz IS NULL THEN avgfilteredrotationz ELSE filteredrotationz END AS filteredrotationz
FROM staticmergedstats JOIN staticEpochs USING(trialid,epochid) ORDER BY trialid, epochid, timestamp),

-- Only keep groups which, after adding in mean information for each subject and eyeglance, are still empty (i.e. no original information was provided)
staticoutput AS (
SELECT * FROM staticrenamed WHERE (rawrotationx IS NOT NULL OR rawrotationy IS NOT NULL OR rawrotationz IS NOT NULL OR
filteredrotationx IS NOT NULL OR filteredrotationy IS NOT NULL OR filteredrotationz IS NOT NULL))

--------------------------------------------------------------

select eyeglancelocation, epochtype, count(*) from dynamicoutput group by eyeglancelocation, epochtype order by eyeglancelocation, epochtype

