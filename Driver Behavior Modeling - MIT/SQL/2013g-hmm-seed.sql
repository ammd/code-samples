﻿CREATE SCHEMA IF NOT EXISTS hmm2013g;

-- 2013g tables ---------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------

-- 2013g study with exactly one coder per task per subject.
DROP TABLE IF EXISTS hmm2013g.singlecoder2013g;
CREATE TABLE hmm2013g.singlecoder2013g (
  subject integer,
  task text,
  task_code integer,
  coder text,
  session integer,
  duration double precision,
  look text,
  time double precision,
  media_part integer,
  gender text,
  age integer,
  age_group text,
  car text
);

-- Filter: take only "good" cases, as laid out by 2013g_Log
DROP TABLE IF EXISTS hmm2013g.goodcases2013g;
CREATE TABLE hmm2013g.goodcases2013g ( subject integer );

-- Fill tables with data from file
COPY hmm2013g.singlecoder2013g FROM E'C:\\Working\\Data\\2013g\\2013g-singlecoder-2015-02-04.csv' DELIMITER ',' CSV;
COPY hmm2013g.goodcases2013g FROM E'C:\\Working\\Data\\2013g\\2013g-Log-goodCases.csv' DELIMITER ',' CSV;


-- Global tables, etc. --------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------

-- Global encoding keys: Mapping of desired glance location / tasks to integer values. 
DROP TABLE IF EXISTS hmm2013g.glanceencodingsmaster;
DROP TABLE IF EXISTS hmm2013g.taskencodingsmaster;
CREATE TABLE hmm2013g.glanceencodingsmaster (
  description text,
  code integer
);
CREATE TABLE hmm2013g.taskencodingsmaster (
  description text,
  code integer
);

-- Fill tables
INSERT INTO hmm2013g.glanceencodingsmaster(
  SELECT look, ROW_NUMBER() OVER() AS code FROM
  (SELECT DISTINCT look FROM hmm2013g.singlecoder2013g 
  WHERE look NOT LIKE '%unknown%' AND look NOT LIKE '%other%' AND look NOT LIKE '%visible%') s
);
INSERT INTO hmm2013g.taskencodingsmaster(
  SELECT task, ROW_NUMBER() OVER() AS code FROM
  (SELECT DISTINCT task FROM hmm2013g.singlecoder2013g) s
);

-- Cantor pairing function
DROP FUNCTION IF EXISTS hmm2013g.pair2(x bigint, y bigint) CASCADE;
CREATE FUNCTION hmm2013g.pair2(x bigint, y bigint)
  RETURNS bigint AS
$func$
BEGIN
RETURN CAST((0.5 * (x + y)*(x + y + 1) + y) AS bigint);
END
$func$  LANGUAGE plpgsql IMMUTABLE;


-- 2013g ----------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------

-- Split data into training, test, and validation sets
DROP FUNCTION IF EXISTS hmm2013g.splitdata2013g();
CREATE FUNCTION hmm2013g.splitdata2013g() RETURNS integer AS $$
BEGIN


DELETE FROM hmm2013g.orderedvoicemanual2013g;
DELETE FROM hmm2013g.orderedvoicemanualphone2013g;
DELETE FROM hmm2013g.orderedvoicemanualsmartphone2013g;
DELETE FROM hmm2013g.orderedvoicemanualembedded2013g;

DELETE FROM hmm2013g.trainsubs2013g;

-- Map tasks in 2013g to baseline tasks (0), manual tasks (1), or voice tasks (2)
INSERT INTO hmm2013g.orderedvoicemanual2013g(
  SELECT subject, m.code AS look, t.code AS task, duration, time, gender, age_group, car, ROW_NUMBER() OVER() AS rank,
         CASE WHEN task LIKE '%base%' THEN 0
	      WHEN task LIKE '%m_c_%' OR task LIKE '%m_p_%' OR task LIKE '%radio_m%' THEN 1 
              WHEN task LIKE '%nav_%' OR task LIKE '%v_c_%' OR task LIKE '%v_p_%' THEN 2 
              END AS tasktype
  FROM hmm2013g.singlecoder2013g s JOIN hmm2013g.glanceencodingsmaster m ON s.look = m.description
  JOIN hmm2013g.taskencodingsmaster t ON t.description = s.task
  WHERE s.subject IN (SELECT subject FROM hmm2013g.goodcases2013g) AND
  (task LIKE '%base%' OR task LIKE '%m_c_%' OR task LIKE '%m_p_%' OR task LIKE '%radio_m%' OR
   task LIKE '%nav_%' OR task LIKE '%v_c_%' OR task LIKE '%v_p_%')
  ORDER BY s.subject, s.task, s.time
);

-- Map tasks in 2013g to baseline phone tasks (0), manual phone tasks (1), or voice phone tasks (2)
INSERT INTO hmm2013g.orderedvoicemanualphone2013g(
  SELECT subject, m.code AS look, t.code AS task, duration, time, gender, age_group, car, ROW_NUMBER() OVER() AS rank,
         CASE WHEN (task LIKE 'm_c_base' OR task LIKE 'm_p_base' OR task LIKE 'v_c_base' OR task LIKE 'v_p_base') THEN 0
	      WHEN (task LIKE 'm_c_1' OR task LIKE 'm_c_2' OR task LIKE 'm_c_3' OR task LIKE 'm_c_4' OR task LIKE 'm_p_1' OR task LIKE 'm_p_2' OR task LIKE 'm_p_3' OR task LIKE 'm_p_4') THEN 1 
              WHEN (task LIKE 'v_c_1' OR task LIKE 'v_c_2' OR task LIKE 'v_c_3' OR task LIKE 'v_c_4' OR task LIKE 'v_p_1' OR task LIKE 'v_p_2' OR task LIKE 'v_p_3' OR task LIKE 'v_p_4') THEN 2
              END AS tasktype
  FROM hmm2013g.singlecoder2013g s JOIN hmm2013g.glanceencodingsmaster m ON s.look = m.description
  JOIN hmm2013g.taskencodingsmaster t ON t.description = s.task
  WHERE s.subject IN (SELECT subject FROM hmm2013g.goodcases2013g) AND
  (task LIKE 'm_c_base' OR task LIKE 'm_p_base' OR task LIKE 'v_c_base' OR task LIKE 'v_p_base' OR 
   task LIKE 'm_c_1' OR task LIKE 'm_c_2' OR task LIKE 'm_c_3' OR task LIKE 'm_c_4' OR task LIKE 'm_p_1' OR task LIKE 'm_p_2' OR task LIKE 'm_p_3' OR task LIKE 'm_p_4' OR 
   task LIKE 'v_c_1' OR task LIKE 'v_c_2' OR task LIKE 'v_c_3' OR task LIKE 'v_c_4' OR task LIKE 'v_p_1' OR task LIKE 'v_p_2' OR task LIKE 'v_p_3' OR task LIKE 'v_p_4')
  ORDER BY s.subject, s.task, s.time
);

-- Map tasks in 2013g to baseline smartphone tasks (0), manual smartphone tasks (1), or voice smartphone tasks (2)
INSERT INTO hmm2013g.orderedvoicemanualsmartphone2013g(
  SELECT subject, m.code AS look, t.code AS task, duration, time, gender, age_group, car, ROW_NUMBER() OVER() AS rank,
         CASE WHEN (task LIKE 'm_p_base' OR task LIKE 'v_p_base') THEN 0
	      WHEN (task LIKE 'm_p_1' OR task LIKE 'm_p_2' OR task LIKE 'm_p_3' OR task LIKE 'm_p_4') THEN 1 
              WHEN (task LIKE 'v_p_1' OR task LIKE 'v_p_2' OR task LIKE 'v_p_3' OR task LIKE 'v_p_4') THEN 2
              END AS tasktype
  FROM hmm2013g.singlecoder2013g s JOIN hmm2013g.glanceencodingsmaster m ON s.look = m.description
  JOIN hmm2013g.taskencodingsmaster t ON t.description = s.task
  WHERE s.subject IN (SELECT subject FROM hmm2013g.goodcases2013g) AND
  (task LIKE 'm_p_base' OR task LIKE 'v_p_base' OR 
   task LIKE 'm_p_1' OR task LIKE 'm_p_2' OR task LIKE 'm_p_3' OR task LIKE 'm_p_4' OR 
   task LIKE 'v_p_1' OR task LIKE 'v_p_2' OR task LIKE 'v_p_3' OR task LIKE 'v_p_4')
  ORDER BY s.subject, s.task, s.time
);

-- Map tasks in 2013g to baseline embedded tasks (0), manual embedded tasks (1), or voice embedded tasks (2)
INSERT INTO hmm2013g.orderedvoicemanualembedded2013g(
  SELECT subject, m.code AS look, t.code AS task, duration, time, gender, age_group, car, ROW_NUMBER() OVER() AS rank,
         CASE WHEN (task LIKE 'm_c_base' OR task LIKE 'v_c_base') THEN 0
	      WHEN (task LIKE 'm_c_1' OR task LIKE 'm_c_2' OR task LIKE 'm_c_3' OR task LIKE 'm_c_4') THEN 1 
              WHEN (task LIKE 'v_c_1' OR task LIKE 'v_c_2' OR task LIKE 'v_c_3' OR task LIKE 'v_c_4') THEN 2
              END AS tasktype
  FROM hmm2013g.singlecoder2013g s JOIN hmm2013g.glanceencodingsmaster m ON s.look = m.description
  JOIN hmm2013g.taskencodingsmaster t ON t.description = s.task
  WHERE s.subject IN (SELECT subject FROM hmm2013g.goodcases2013g) AND
  (task LIKE 'm_c_base' OR task LIKE 'v_c_base' OR 
   task LIKE 'm_c_1' OR task LIKE 'm_c_2' OR task LIKE 'm_c_3' OR task LIKE 'm_c_4' OR 
   task LIKE 'v_c_1' OR task LIKE 'v_c_2' OR task LIKE 'v_c_3' OR task LIKE 'v_c_4')
  ORDER BY s.subject, s.task, s.time
);



-- Subject split based on total good subjects, not on particular class split =>
-- Use single subject split for multiple classification instances
INSERT INTO hmm2013g.trainsubs2013g(
  SELECT * 
  FROM (SELECT DISTINCT subject FROM hmm2013g.goodcases2013g) s
  ORDER BY random() 
  LIMIT 0.8*(SELECT COUNT(DISTINCT subject) FROM hmm2013g.goodcases2013g)
);

RETURN 1;
END;
$$ LANGUAGE plpgsql;




-- fix this ------------------------------------------
DROP FUNCTION IF EXISTS hmm2013g.splitdata2013gvolvo();
CREATE FUNCTION hmm2013g.splitdata2013gvolvo() RETURNS integer AS $$
BEGIN

PERFORM hmm2013g.splitdata2013g();

RETURN 1;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS hmm2013g.splitdata2013gchevy();
CREATE FUNCTION hmm2013g.splitdata2013gchevy() RETURNS integer AS $$
BEGIN

PERFORM hmm2013g.splitdata2013g();

RETURN 1;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS hmm2013g.splitdata2013gvolvophone();
CREATE FUNCTION hmm2013g.splitdata2013gvolvophone() RETURNS integer AS $$
BEGIN

PERFORM hmm2013g.splitdata2013g();

RETURN 1;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS hmm2013g.splitdata2013gchevyphone();
CREATE FUNCTION hmm2013g.splitdata2013gchevyphone() RETURNS integer AS $$
BEGIN

PERFORM hmm2013g.splitdata2013g();

RETURN 1;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS hmm2013g.splitdata2013gvolvosmartphone();
CREATE FUNCTION hmm2013g.splitdata2013gvolvosmartphone() RETURNS integer AS $$
BEGIN

PERFORM hmm2013g.splitdata2013g();

RETURN 1;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS hmm2013g.splitdata2013gchevysmartphone();
CREATE FUNCTION hmm2013g.splitdata2013gchevysmartphone() RETURNS integer AS $$
BEGIN

PERFORM hmm2013g.splitdata2013g();

RETURN 1;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS hmm2013g.splitdata2013gvolvoembedded();
CREATE FUNCTION hmm2013g.splitdata2013gvolvoembedded() RETURNS integer AS $$
BEGIN

PERFORM hmm2013g.splitdata2013g();

RETURN 1;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS hmm2013g.splitdata2013gchevyembedded();
CREATE FUNCTION hmm2013g.splitdata2013gchevyembedded() RETURNS integer AS $$
BEGIN

PERFORM hmm2013g.splitdata2013g();

RETURN 1;
END;
$$ LANGUAGE plpgsql;

------------------------------------------------------

-- Raw data table -> order by subject, task, timestamp, only take relevant subjects and tasks
-- Class split: all voice tasks vs. all baselines vs. all manual tasks
DROP TABLE IF EXISTS hmm2013g.orderedvoicemanual2013g CASCADE;
CREATE TABLE hmm2013g.orderedvoicemanual2013g (
  subject integer,
  look integer,
  task integer,
  duration double precision,
  time double precision,
  gender text,
  age_group text,
  car text,
  rank integer,
  tasktype integer
);

-- Raw data table -> order by subject, task, timestamp, only take relevant subjects and tasks
-- Class split: all phone voice tasks vs. all phone baselines vs. all phone manual tasks
DROP TABLE IF EXISTS hmm2013g.orderedvoicemanualphone2013g CASCADE;
CREATE TABLE hmm2013g.orderedvoicemanualphone2013g (
  subject integer,
  look integer,
  task integer,
  duration double precision,
  time double precision,
  gender text,
  age_group text,
  car text,
  rank integer,
  tasktype integer
);

-- Raw data table -> order by subject, task, timestamp, only take relevant subjects and tasks
-- Class split: all smartphone voice tasks vs. all smartphone baselines vs. all smartphone manual tasks
DROP TABLE IF EXISTS hmm2013g.orderedvoicemanualsmartphone2013g CASCADE;
CREATE TABLE hmm2013g.orderedvoicemanualsmartphone2013g (
  subject integer,
  look integer,
  task integer,
  duration double precision,
  time double precision,
  gender text,
  age_group text,
  car text,
  rank integer,
  tasktype integer
);


-- Raw data table -> order by subject, task, timestamp, only take relevant subjects and tasks
-- Class split: all smartphone voice tasks vs. all smartphone baselines vs. all smartphone manual tasks
DROP TABLE IF EXISTS hmm2013g.orderedvoicemanualembedded2013g CASCADE;
CREATE TABLE hmm2013g.orderedvoicemanualembedded2013g (
  subject integer,
  look integer,
  task integer,
  duration double precision,
  time double precision,
  gender text,
  age_group text,
  car text,
  rank integer,
  tasktype integer
);

-- Training subjects table (80% of total number of subjects)
DROP TABLE IF EXISTS hmm2013g.trainsubs2013g CASCADE;
CREATE TABLE hmm2013g.trainsubs2013g (subject integer);

-- Training & validation data, visual / manual class split
CREATE OR REPLACE VIEW hmm2013g.trainglance2013g AS (
  SELECT o.tasktype, o.look
  FROM hmm2013g.orderedvoicemanual2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainduration2013g AS (
  SELECT o.tasktype, o.duration
  FROM hmm2013g.orderedvoicemanual2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.validateglance2013g AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, look
  FROM hmm2013g.orderedvoicemanual2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanual2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateduration2013g AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, duration
  FROM hmm2013g.orderedvoicemanual2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanual2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  ORDER BY hmm2013g.pair2(subject,task), rank
);

-- Training & validation data, visual / manual class split per vehicle
CREATE OR REPLACE VIEW hmm2013g.trainglance2013gvolvo AS (
  SELECT o.tasktype, o.look
  FROM hmm2013g.orderedvoicemanual2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Volvo%' 
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainglance2013gchevy AS (
  SELECT o.tasktype, o.look
  FROM hmm2013g.orderedvoicemanual2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Chevy%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainduration2013gvolvo AS (
  SELECT o.tasktype, o.duration
  FROM hmm2013g.orderedvoicemanual2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Volvo%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainduration2013gchevy AS (
  SELECT o.tasktype, o.duration
  FROM hmm2013g.orderedvoicemanual2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Chevy%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.validateglance2013gvolvo AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, look
  FROM hmm2013g.orderedvoicemanual2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanual2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Volvo%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateglance2013gchevy AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, look
  FROM hmm2013g.orderedvoicemanual2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanual2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Chevy%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateduration2013gvolvo AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, duration
  FROM hmm2013g.orderedvoicemanual2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanual2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Volvo%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateduration2013gchevy AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, duration
  FROM hmm2013g.orderedvoicemanual2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanual2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Chevy%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);


-- Training & validation data, visual / manual phone tasks split per vehicle
CREATE OR REPLACE VIEW hmm2013g.trainglance2013gvolvophone AS (
  SELECT o.tasktype, o.look
  FROM hmm2013g.orderedvoicemanualphone2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Volvo%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainglance2013gchevyphone AS (
  SELECT o.tasktype, o.look
  FROM hmm2013g.orderedvoicemanualphone2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Chevy%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainduration2013gvolvophone AS (
  SELECT o.tasktype, o.duration
  FROM hmm2013g.orderedvoicemanualphone2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Volvo%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainduration2013gchevyphone AS (
  SELECT o.tasktype, o.duration
  FROM hmm2013g.orderedvoicemanualphone2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Chevy%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.validateglance2013gvolvophone AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, look
  FROM hmm2013g.orderedvoicemanualphone2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanualphone2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Volvo%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateglance2013gchevyphone AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, look
  FROM hmm2013g.orderedvoicemanualphone2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanualphone2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Chevy%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateduration2013gvolvophone AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, duration
  FROM hmm2013g.orderedvoicemanualphone2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanualphone2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Volvo%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateduration2013gchevyphone AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, duration
  FROM hmm2013g.orderedvoicemanualphone2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanualphone2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Chevy%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);

-- Training & validation data, visual / manual smartphone tasks split per vehicle
CREATE OR REPLACE VIEW hmm2013g.trainglance2013gvolvosmartphone AS (
  SELECT o.tasktype, o.look
  FROM hmm2013g.orderedvoicemanualsmartphone2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Volvo%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainglance2013gchevysmartphone AS (
  SELECT o.tasktype, o.look
  FROM hmm2013g.orderedvoicemanualsmartphone2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Chevy%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainduration2013gvolvosmartphone AS (
  SELECT o.tasktype, o.duration
  FROM hmm2013g.orderedvoicemanualsmartphone2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Volvo%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainduration2013gchevysmartphone AS (
  SELECT o.tasktype, o.duration
  FROM hmm2013g.orderedvoicemanualsmartphone2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Chevy%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.validateglance2013gvolvosmartphone AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, look
  FROM hmm2013g.orderedvoicemanualsmartphone2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanualsmartphone2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Volvo%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateglance2013gchevysmartphone AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, look
  FROM hmm2013g.orderedvoicemanualsmartphone2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanualsmartphone2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Chevy%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateduration2013gvolvosmartphone AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, duration
  FROM hmm2013g.orderedvoicemanualsmartphone2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanualsmartphone2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Volvo%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateduration2013gchevysmartphone AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, duration
  FROM hmm2013g.orderedvoicemanualsmartphone2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanualsmartphone2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Chevy%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);


-- Training & validation data, visual / manual embedded tasks split per vehicle
CREATE OR REPLACE VIEW hmm2013g.trainglance2013gvolvoembedded AS (
  SELECT o.tasktype, o.look
  FROM hmm2013g.orderedvoicemanualembedded2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Volvo%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainglance2013gchevyembedded AS (
  SELECT o.tasktype, o.look
  FROM hmm2013g.orderedvoicemanualembedded2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Chevy%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainduration2013gvolvoembedded AS (
  SELECT o.tasktype, o.duration
  FROM hmm2013g.orderedvoicemanualembedded2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Volvo%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.trainduration2013gchevyembedded AS (
  SELECT o.tasktype, o.duration
  FROM hmm2013g.orderedvoicemanualembedded2013g o 
  WHERE o.subject IN (SELECT * FROM hmm2013g.trainsubs2013g)
  AND car LIKE '%Chevy%'
  ORDER BY o.rank
);
CREATE OR REPLACE VIEW hmm2013g.validateglance2013gvolvoembedded AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, look
  FROM hmm2013g.orderedvoicemanualembedded2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanualembedded2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Volvo%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateglance2013gchevyembedded AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, look
  FROM hmm2013g.orderedvoicemanualembedded2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanualembedded2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Chevy%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateduration2013gvolvoembedded AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, duration
  FROM hmm2013g.orderedvoicemanualembedded2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanualembedded2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Volvo%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);
CREATE OR REPLACE VIEW hmm2013g.validateduration2013gchevyembedded AS (
  SELECT hmm2013g.pair2(subject,task) AS seqid, tasktype, duration
  FROM hmm2013g.orderedvoicemanualembedded2013g
  WHERE subject NOT IN (SELECT * FROM hmm2013g.trainsubs2013g) AND hmm2013g.pair2(subject,task) IN 
	(SELECT seqid FROM 
	(SELECT hmm2013g.pair2(subject,task) AS seqid 
	 FROM hmm2013g.orderedvoicemanualembedded2013g GROUP BY subject,task HAVING COUNT(*) > 10) s)
  AND car LIKE '%Chevy%'
  ORDER BY hmm2013g.pair2(subject,task), rank
);