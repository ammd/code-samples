﻿-- Function: "2013-onrd-g".fillsubtaskdata()

-- DROP FUNCTION "2013-onrd-g".fillsubtaskdata();

CREATE OR REPLACE FUNCTION "2013-onrd-g".fillsubtaskdata()
  RETURNS void AS
$BODY$

BEGIN

CREATE TEMPORARY TABLE HR 
AS (
  WITH
    max_session(subject_id, session_id) AS (
      SELECT subject_id, MAX("session")
      FROM "2013-onrd-g".qrs
      GROUP BY subject_id
    ),
    
    -- Mean and SD of HR for baseline per subject
    baseline_HR(subject_id, hr_mean, hr_sd) AS (
      SELECT
        q.subject_id,
        avg(hr),
        stddev_samp(hr)

      FROM "2013-onrd-g".qrs q
        INNER JOIN max_session ON (
          max_session.subject_id = q.subject_id
          and max_session.session_id = q.session
          AND NOT EXISTS (
            select 1
            FROM "2013-onrd-g".invalid_data id
            WHERE id.tablename = 'gsrd'
            AND id.columnname = 'EKG'
            AND id.subject_id = q.subject_id
            AND q.unique_unix_timestamp BETWEEN id.start_unix_timestamp AND id.end_unix_timestamp
          )
        )

        INNER JOIN "2013-onrd-g".protocol_subject ps ON (
          ps.annotation = 'Nav_C_base' AND
          q.subject_id = ps.subject_id AND
          q.unique_unix_timestamp between ps.start_timestamp AND ps.end_timestamp
        )

      --WHERE q.subject_id in (SELECT id FROM analysis_subjects)
      GROUP BY q.subject_id
    )

 SELECT q.subject_id AS subject, 
    ps.annotation AS task, 
    q.unique_unix_timestamp AS "timestamp",
    q.hr AS "HR",
    (q.hr - bl.HR_mean) / bl.HR_sd as "HR_score"

  FROM "2013-onrd-g".qrs q INNER JOIN max_session ON (
    max_session.subject_id = q.subject_id
    AND max_session.session_id = q.session
    AND NOT EXISTS (
      select 1
      FROM "2013-onrd-g".invalid_data id
      WHERE id.tablename = 'gsrd'
      AND id.columnname = 'EKG'
      AND id.subject_id = q.subject_id
      AND q.unique_unix_timestamp BETWEEN id.start_unix_timestamp AND id.end_unix_timestamp
    )
  )
  INNER JOIN "2013-onrd-g".protocol_subject ps ON (
    q.subject_id = ps.subject_id
    AND q.unique_unix_timestamp BETWEEN ps.start_timestamp-3 AND ps.end_timestamp+3
  )
  INNER JOIN baseline_HR bl ON (q.subject_id = bl.subject_id)

  --WHERE q.subject_id IN ( SELECT id FROM analysis_subjects)
  --AND ps.annotation LIKE '_-back1' OR ps.annotation = 'n-back1_base'

  ORDER BY q.subject_id, ps.annotation, q.unique_unix_timestamp
);
CREATE UNIQUE INDEX HR_subject_timestamp ON HR("subject", "timestamp");


--
-- Complete SCL data with change scores
--
CREATE TEMPORARY TABLE SCL
AS (
  WITH
    -- Mean and SD of edited SCL for 2-min-baseline per subject
    baseline_SCL(subject_id, SCL_mean, SCL_sd) AS (
      SELECT
        q.subject_id,
        avg(signal_edit),
        stddev_samp(signal_edit)
    
      FROM "2013-onrd-g".daq_edit q
      INNER JOIN "2013-onrd-g".protocol_subject ps ON (
        ps.annotation = 'Nav_C_base' AND
        q.subject_id = ps.subject_id AND
        q.unique_unix_timestamp between ps.start_timestamp AND ps.end_timestamp    
      )

      --WHERE q.subject_id in (SELECT id FROM analysis_subjects)
      GROUP BY q.subject_id
    )

  SELECT q.subject_id AS subject, 
    at.task_id AS task, 
    q.unique_unix_timestamp AS "timestamp",
    q.unique_unix_timestamp - (min(q.unique_unix_timestamp) over(PARTITION BY ps.subject_id, at.task_id)) as "offset",
    q.signal_edit AS "SCL",
    (q.signal_edit - bl.SCL_mean) / bl.SCL_sd as "SCL_score"
    
  FROM "2013-onrd-g".daq_edit q
  INNER JOIN "2013-onrd-g".protocol_subject ps ON (
    q.subject_id = ps.subject_id
    AND q.unique_unix_timestamp BETWEEN ps.start_timestamp AND ps.end_timestamp
  )
  INNER JOIN baseline_SCL bl ON (q.subject_id = bl.subject_id)
  JOIN "2013-onrd-g".annotated_task at ON ps.annotation = at.description

  --WHERE q.subject_id IN ( SELECT id FROM analysis_subjects)
  --AND ps.annotation LIKE '_-back1' OR ps.annotation = 'n-back1_base'

  ORDER BY q.subject_id, at.task_id, q.unique_unix_timestamp
);
CREATE UNIQUE INDEX SCL_subject_timestamp ON SCL("subject", "timestamp");


DELETE FROM "2013-onrd-g".subtask_data;
INSERT INTO "2013-onrd-g".subtask_data (subject, task, subtask, code, unique_unix_timestamp, SCL, SCL_score, HR, HR_score, media_part, annotated_type, duration, description)
    (



WITH
-- Mapping of individual QRS times to continuous 10Hz timestamps
  times("subject", "scl_timestamp", "hr_timestamp") as (
    SELECT
      s.subject as "subject",
      s.timestamp AS "scl_timestamp",
      max(h.timestamp) as "hr_timestamp"
    FROM SCL s INNER JOIN HR h ON (s.subject = h.subject AND h.timestamp BETWEEN s.timestamp-2 AND s.timestamp)
    GROUP BY s.subject, s.timestamp
    ORDER BY s.subject, s.timestamp
  ),

  scores AS (

	SELECT s.*, h."HR", h."HR_score"
	FROM SCL s
	INNER JOIN HR h ON (s.subject = h.subject)
	INNER JOIN times t ON (t.hr_timestamp = h.timestamp AND t.scl_timestamp = s.timestamp)
	ORDER BY s.subject, s.timestamp
  ),


 subtask_codings as 
(Select * from "2013-onrd-g".annotated_subtask_duration ad NATURAL JOIN "2013-onrd-g".annotated_subtask_data asd
WHERE asd.unique_unix_timestamp BETWEEN ad.start_timestamp AND ad.end_timestamp),

subtask_codings_descriptions as(
select sc.subject_id, t.task_id AS task, s.description AS subtask, c.description as code, sc.unique_unix_timestamp from subtask_codings sc 
	    LEFT OUTER JOIN "2013-onrd-g".annotated_task t ON sc.task_id = t.task_id
            LEFT OUTER JOIN "2013-onrd-g".annotated_subtask s ON sc.subtask_id = s.subtask_id
            LEFT OUTER JOIN "2013-onrd-g".annotated_type_code c ON sc.code = c.code_id
            order by subject_id, unique_unix_timestamp),

-- Finds next timestamp in time for each subject and task
boundaries AS (

select t1.*, 
(select t.unique_unix_timestamp from subtask_codings_descriptions t where t1.subject_id = t.subject_id and t1.task = t.task and t.unique_unix_timestamp > t1.unique_unix_timestamp order by t.unique_unix_timestamp ASC limit 1) AS boundaryEnd
from subtask_codings_descriptions t1
),

-- Last entry in task -> look at when the entire task periods ended
fixedboundaries AS (
 select b.subject_id AS subject, b.task, b.subtask, b.code, b.unique_unix_timestamp, case when boundaryEnd ISNULL 
 THEN (select max(end_timestamp) from "2013-onrd-g".annotated_subtask_duration d where d.subject_id = b.subject_id ) 
 ELSE boundaryEnd END AS boundaryEnd
 FROM boundaries b
),

-- scl values are sampled at a higher rate than hr, so use them as a "global clock", that is, find, for every scl value,
-- the last hr value did not take place after the scl value.
withphysio AS (
select b1.subject, b1.task, b1.subtask, b1.code, s.timestamp, s."SCL", s."SCL_score", s."HR", s."HR_score" 
from fixedboundaries b1 JOIN scores s using (subject, task)
where s.timestamp between b1.unique_unix_timestamp and b1.boundaryend 
order by b1.subject, b1.task, b1.unique_unix_timestamp, s.timestamp ASC),

withglance AS (
select p.*, g.media_part, g.annotated_type, g.duration, g.description
from withphysio p join "2013-onrd-g".glance_annotations g ON p.subject = g.subject_id and p.task = g.task_id
and p.timestamp between g.unique_unix_timestamp and g.unique_unix_timestamp + g.duration)

select * from withglance

    );
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION "2013-onrd-g".fillsubtaskdata()
  OWNER TO mauricio;