%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HMM_Main
%
% Sample usage:
%
% [valAcc, valErrorCost,valRes,numClasses, numTrails] = ...
% HMMMain('-verbose','-global','-discrete','logfile',...
% 'C:\Users\Mauricio\Desktop\allclasseslog.txt', ...
% 'trainfile','C:\Users\Mauricio\Desktop\2013a-hmmTrain-VM.csv', ...
% 'valfile','C:\Users\Mauricio\Desktop\2013a-hmmVal-VM.csv', ...
% 'testfile','C:\Users\Mauricio\Desktop\2013a-hmmTestAll-VM.csv')
%
% Offline classification framework for Hidden Markov Models, including 
% standard routines such as training, validation, and testing. In this
% context, validation measures the performance of the trained model /
% framework by comparing predictions to annotated class labels. Testing
% allows the model to predict class labels for data with no annotations.
%
% The framework differentiates two fundamental building blocks: 
% classification framework models and HMM business logic models. Data is 
% always processed firstly by the former, which shape the general shape and
% architecture of the data. An HMM business logic model is then used via 
% dependency injection to handle the concrete business logic of the HMM 
% model. While the framework models handle the high level structure of the 
% data (provide an answer to the question HOW? the testing procedure is 
% carried out), the HMM business logic models work on the concrete data 
% type level (provide an answer to question WHAT? exactly is being tested).
%
% This classification framework is meant to scale in both directions,
% allowing the user to build his or her own business logic and/or framework
% models, which must provide functionality for the preprocess, train, 
% validate, test, and postprocess procedures.
% 
%% Options: ---------------------------------------------------------------
%
% -debug:    
%           Print program internal state at certain control junctures,
%           for development within MATLAB.
% -verbose: 
%           print low level classifications in addition to accuracy
% -silent:
%           do not print out classification progress on standard output
% -global: 
%           Classification framework model. Build a single HMM instead of 
%           one per class, use posterior prob. to choose the winning class.
% -synthetic: 
%           Classification framework model. Build an HMM for each class, 
%           use maximum likelihood of input sequence to choose the winning 
%           model/class.
% -discrete: 
%           HMM business model. Input emissions are single discrete 
%           observations
% -discreteCluster: 
%           HMM business model. Input emissions are single discrete 
%           observations corresponding to their cluster centroids wrt to a
%           specified clustering algorithm
% -continuous: 
%           HMM business model. Input emissions are not single observations
%           but a sequence, i.e. different continuous features. 
% -kmeans: 
%           Use kmeans clustering preprocessing to get discrete emissions.
% -gmm:    
%           Use a Gaussian Mixture Model (as preprocessing step) to get
%           discrete emissions. Requires data with some variance, which
%           should be the case for continuous features. Should this not be
%           the case, an error will be thrown.
% -balanceTS:    
%           Balance the training set so that all classes are equally
%           represented. The relative ordering of the features within each
%           class is respected.
%
%% Optional Inputs --------------------------------------------------------
% Flags in [] mark with which options they must be provided:
%
% trainfile [required]
%           CSV source file with 2 columns containing 
%           1. the class to which the particular emission belongs (numbers)
%           2. the observed emissions from which to learn from (time
%           series). Emissions belonging to a class do not have to
%           be grouped together, but emissions within a class must be  
%           ordered as a time series.
% logfile []
%           output file to append to, log results and calls.
% valfile []: 
%           CSV validation file, used to validate the model 
%           created. Expect table with int columns: sequenceID, class, and 
%           observations. Because HMMs perform blockwise classification,
%           the sequenceID is provided to mark the blocks to be
%           classified. In the standard case, there should only be a single
%           class represented within each such block.
% testfile []: 
%           CSV test file. Classify data with no ground truth. 
%           Matrix containing sequenceID, classID and observations columns. 
%           Observations within each sequence are treated as a time series. 
%           Because HMMs perform blockwise classification,
%           the sequenceID is provided to mark the blocks to be classified.
%           In the standard case, there should only be a single
%           class represented within each such block.
% classMapFile []: 
%           CSV file containing 2 columns, the first
%           of which is a string containing a class name, and the second 
%           containing a unique integer encoding for that class, which
%           corresponds to the classID in the testfile. This file is used
%           to print out meaningful output during testing. If not file is 
%           given, just print out the corresponding classID instead.
% states [-synthetic]: 
%           number of synthetic states used to model each HMM in the 
%           synthetic scheme.
%%
function [valAcc, valErrorCost, valRes, numClasses, numTrails] = ...
          HMMMain(varargin)
  
  % The "signature" with which this program was run.
  signed = varargin;
  parser = inputParser;
  
  % Handle input flags. Anonymous function corresponds to validation
  % function, returns true iff the use of the flag is valid.
  vF = {};

  [verbose, varargin, vF{end+1}] = parseFlag(varargin,...
      '-verbose',@()true);
  
  [silent, varargin, vF{end+1}] = parseFlag(varargin,...
      '-silent',@()true);
  
  [globalModel,varargin,vF{end+1}] = parseFlag(varargin,...
      '-global',@()true);
  
  [synthModel,varargin,vF{end+1}]=parseFlag(varargin,...
      '-synthetic',@()parser.Results.states>0);
  
  [continuous, varargin, vF{end+1}] = parseFlag(varargin,...
      '-continuous',@()~isempty(parser.Results.trainfile));
  
  [discrete, varargin, vF{end+1}] = parseFlag(varargin,...
      '-discrete',@()~isempty(parser.Results.trainfile));
  
  [discreteCluster,varargin,vF{end+1}] = parseFlag(varargin, ...
      '-discreteCluster',@()~isempty(parser.Results.trainfile));
  
  [kmeans, varargin, vF{end+1}] = parseFlag(varargin,...
      '-kmeans',@()true);
  
  [gmm, varargin, vF{end+1}] = parseFlag(varargin,...
      '-gmm',@()true);
  
  [debug, varargin, vF{end+1}] = parseFlag(varargin,...
      '-debug',@()true);
  
  [balanceTS, varargin, vF{end+1}] = parseFlag(varargin,...
      '-balanceTS',@()true);
  
  % Global validations
  vF{end+1} = @() globalModel || synthModel;
  vF{end+1} = @() discrete || continuous || discreteCluster;
  vF{end+1} = @() (gmm + kmeans == 1) || ~discreteCluster;
  
  % Input arguments and parameters  
  addParameter(parser,'logfile','',@(x)true);
  addParameter(parser,'trainfile','',@(x)true);
  addParameter(parser,'valfile','',@(x)true);
  addParameter(parser,'testfile','',@(x)true);
  addParameter(parser,'classMapFile','',@(x)true);
  addParameter(parser,'states',-1,@(states) synthModel && states >= 1);
  
  % Validate inputs
  parser.parse(varargin{:});
  for i=1:length(vF)
      if (~vF{i}()) 
          error('HMMMain:flagCheck', ...
          strcat('The following condition is not met: ',func2str(vF{i})));
      end
  end
  
  % Set control switches
  log = ~isempty(parser.Results.logfile);
  validate = ~isempty(parser.Results.valfile);
  test = ~isempty(parser.Results.testfile);
  
  % If debugging, print the current control parameters
  if (debug)
      log
      validate
      test
      verbose
      silent
      globalModel
      synthModel
      continuous
      discrete
      discreteCluster
      kmeans
      gmm
      balanceTS
      debug
      parser.Results
  end
 
  % Get program state variables
  if log output = fopen(parser.Results.logfile, 'a+'); end
  [emissions] = parseMatrix(parser.Results.trainfile);
  [valData] = parseMatrix(parser.Results.valfile);
  [testData] = parseMatrix(parser.Results.testfile);
  [classMap] = parseNonNumericCSV(parser.Results.classMapFile, '%s %s');
      
  % Print program state variables if debugging
  if debug
    fprintf('Trainfile size: %d %d\n',size(emissions));
    fprintf('Top 1%% rows: \n');
    emissions(1:int8(size(emissions,1)*0.01),:)
    
    fprintf('Valfile size: %d %d\n',size(valData));
    fprintf('Top 1%% rows: \n');
    valData(1:int8(size(valData,1)*0.01),:)
    
    fprintf('Testfile size: %d %d\n',size(testData));
    fprintf('Top 1%% rows: \n');
    testData(1:int8(size(testData,1)*0.01),:)
    
    classMap
  end
  
  % Get function handles: output
  if log [header, classAcc, classPairs] = printOutput(output); end
  
  % Get function handles: experiment model / framework
  if synthModel
    [preModel,trainModel,valModel,testModel,postModel] = ...
        HMMSynthetic(debug, parser.Results.states, balanceTS);
  elseif globalModel
    [preModel,trainModel,valModel,testModel,postModel] = ...
        HMMGlobal(debug);
  end
 
  % Get function handles: HMM extension
  if continuous
    [pExt,trExt,vExt,tsExt,psExt] = HMMContinuous(debug);
  elseif discrete
    [pExt,trExt,vExt,tsExt,psExt] = HMMDiscrete(debug);
   elseif discreteCluster
    [pExt,trExt,vExt,tsExt,psExt] = HMMDiscreteCluster(debug,gmm,kmeans);
  end
  
  % Preprocessing
  if ~silent disp('Preprocessing ....');  end
  [emissions,valData,testData,numClasses] = ...
      preModel(pExt,emissions,valData,testData);
  
  % Training
  if ~silent disp('Training ....'); end
  [models] = trainModel(trExt, emissions);
  
  % Validating
  valRes = [];
  [valAcc, valErrorCost] = deal(0);
  if validate
      if ~silent disp('Validating ....'); end
      [valRes, valAcc, valErrorCost] = valModel(vExt, valData, models);
      numTrials = length(valRes);
  end
  
  % Testing
  testRes = [];
  if test
      if ~silent disp('Testing ....'); end
      [testRes] = testModel(tsExt, testData, models);
  end
  
  % Postprocessing
  if ~silent disp('Postprocessing ....'); end
  [valRes, testRes] = postModel(psExt, valRes, testRes);
  numTrails = length(valRes);
  if (~isempty(classMap))
    mTestRes = cell(size(testRes));
    for i=1:size(testRes,1)
        mapIndex = ismember(classMap{2}, num2str(testRes(i, 1)));
        mTestRes{i,1}=classMap{1}{mapIndex};
        mTestRes{i,2}=testRes(i,2);
    end
    testRes=mTestRes;
  end
  
  % Print output
  if log
      header(signed);
      if validate classAcc(valAcc, numTrials, valErrorCost); end
      if validate && verbose classPairs(valRes,'%d,'); end
      if test && verbose classPairs(testRes,'%d,%d\n'); end
      fclose(output);
  end
  disp('done.');
  
  
  %% Functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % Returns true for boolRes iff the flag in found in args and the given
  % condition for its existence is fulfilled. Output function is set to
  % input function iff boolRes is met.
  function [boolRes, args, func] = parseFlag(args, flag, valFunc)
    boolRes = false;
    func = @()true;
    if find(strcmp(flag, args)) boolRes = true; end
    args = args(~strcmp(args,flag));
    if boolRes func = valFunc; end
  end

  % Wrapper utility for reading numeric matrices. If empty input return
  % empty matrix. Skips header if present.
  function [res] = parseMatrix(file)
      if isempty(file) res = [];
      else
          try
              res = csvread(file,0,0); 
          catch
              try
                  res = csvread(file,1,0);
              catch
                  res = [];
              end
          end
      end
  end

  % Wrapper utility for reading non numeric CSVs. If emtpy input return
  % empty matrix.
  function [res] = parseNonNumericCSV(file, format)
       if isempty(file) res = []; else
       fileId = fopen(file);
       res = textscan(fileId, format, 'delimiter', ',');
       fclose(fileId);
       end
  end

  % Print a matrix-like data structure to file with descriptor fileid.
  % typeSep is a string specifying the format of the result, including a
  % cell to cell separator (e.g. ',' for a csv output file)
  function printMat(fileid, mat, typeSep)
      if iscell(mat)
          for row = 1:size(mat,1)
              fprintf(fileid,typeSep,mat{row,:});
          end
      else
          fprintf(fileid, [repmat(typeSep, 1, size(mat, 2)) '\n'], mat');    
      end
  end

  % Emulate a printer class, takes fileId of where to write the output.
  function [header, classAcc, classPairs] = printOutput(out)
      
      % Function handles
      header = @printHeader;
      classAcc = @printClassAcc;
      classPairs = @printClassPairs;
      
      % Print header information (program title, parameters, etc.)
      function printHeader(head)
       fprintf(out, '\n\n%d-%d-%d %d:%d:%d ', clock);
       fprintf(out, '----------------------------------------------\n');
       fprintf(out, 'Ran HMM with parameters ');
       for i=1:length(head) fprintf(out, '%s ', num2str(head{i}));end
      end
      
      % Classification accuracy, with number of sequences tested and
      % a cost sum over all misclassifications
      function printClassAcc(acc, trials, cost)
        fprintf(out, strcat('\nClassification accuracy is:', ...
             ' %3.2f%% (%d trials), error cost: %3.3f%\n'), ...
             100*acc,trials,cost);
      end
       
       % Print stored results
      function printClassPairs(pairs, format)
         fprintf(out, '\nClassifications (input,assigned class):\n');
         printMat(out, pairs, format);
      end
  end

end
