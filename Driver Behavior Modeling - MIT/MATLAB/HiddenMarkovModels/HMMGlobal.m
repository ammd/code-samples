%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HMM_Global
%
% Function repository for a global HMM classification framework. In this
% framework, one HMM is trained based on the entire annotated training
% data. The states of the HMM correspond to the class labels of the data.
%
% The given functions process data within the standard preprocessing,
% training, validating, testing, and postprocessing steps in a standard
% classification framework in terms of their general structure, not their
% contents, i.e. this repository makes no assumptions as to whether the
% data are discrete or continuous, or as to their dimensionality.
%
% In this respect, each respective method takes (as the first parameter)
% an input function which computes the actual business logic of the model.
%%
function [p, tr, v, ts, ps] = HMMGlobal(debug)

% Function handles
p = @preprocess;
tr = @train;
v = @validate;
ts = @test;
ps = @postprocess;

% Preprocess the data. Check if the general structure of the data is
% consistent, then pass further handling to the concrete extension type.
function [trainData, valData, testData, nClasses] = ...
          preprocess(preFunc, tr, val, ts)
    
    if debug fprintf('HMMGlobal:preprocess...\n'); end
    
    % Classes present in validation and test sets must be a subset of the
    % classes present in the training set.
    trainClasses = unique(tr(:,1));
    nClasses = length(trainClasses);
    if (~isempty(val) && ~all(ismember(unique(val(:,2)), trainClasses)))
    
        error('HMMGlobal:preprocess', ...
              'Validation set classes not subsets of training classes.\n');
    end
    [trainData, valData, testData] = preFunc(tr, val, ts);
end

% Train a global HMM with the given emission training data.
% Assumes emissions is a time series matrix, with time progressing with
% each row. Format should be as follows: [class, emissions].
% Returns the global HMM model specific to the HMM extension train
% method given by trainFunc
function [model] = train(trainFunc, emissions)
    
    if debug fprintf('HMMGlobal:train...\n'); end
    model = {trainFunc(emissions,0)};
end


% Instead of testing multiple sequences, test a single sequence
% containing states according to the trained model.
% Use posterior state probabilities to guess the class at each
% emission for the input sequence. The accuracy is then given as the
% fraction of correctly labeled emissions within the sequence.
%
% Expects validation data with format [sequence ID, class ID, emissions]
% as a time series, where time progresses with each row. models is an array
% of models specific to the HMM extension represented by valFunc. sequence
% ID is not important here.
%
% Returns the classification results, the (validation) accuracy, as well
% as an estimate for the error costs under the assumption that the class
% labels, although being categorical, also belong to some scale. This
% value therefore weights the final error measure by penalizing large
% misclassifications based on the class scale.
function [valRes, valAcc, valErrorCost] = validate(valFunc,valData,models)
    
    if debug fprintf('HMMGlobal:validate...\n'); end
    
    if (~iscell(models) || length(models)~=1)
        error('HMMGlobal:validate', 'Global model is not valid.');
    end
    [~,classes] = valFunc(valData(:,3:end), models);
    valRes = [valData(:,2), classes];
    valAcc = mean(double(valRes(:,1)==valRes(:,2)));
    valErrorCost = sum((valRes(:,1)-valRes(:,2)).^2) / (length(valRes)*...
                  ((max(max(valRes))-min(min(valRes))).^2));
end


% Expects test data with format [sequenceID, sampleID, emissions]
% as a time series, where time progresses with each row. models is an array
% of models specific to the HMM extension represented by testFunc.
% sequenceID is ignored here; all input sequences are combined into a
% single sample.
function [testOutput] = test(testFunc, testData, models)
    
    if debug fprintf('HMMGlobal:test...\n'); end
    
    if (~iscell(models) || length(models)~=1)
        error('HMMGlobal:test', 'Global model is not valid.');
    end

    % Get per-emission classification of the input sequence using the
    % given model
    [~,classes] = testFunc(testData(:,3:end), models);
    testRes = [testData(:,2) classes];
    
    % Group by sampleID, calculate the winning class for each group.
    testOutput = grpstats(testRes, testRes(:,1),@(x)mode(x)');
end

% Postprocess the data. From an architectural point of view, there is
% nothing to be done here, so simply send it to the HMM extension for
% postprocessing.
function [valRes, testRes] = postprocess(postFunc, valRes, testRes)
    
    if debug fprintf('HMMGlobal:postprocess...\n'); end
    [valRes, testRes] = postFunc(valRes, testRes);
end

%% Private Functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



end