% Splits sequences given by seqid in half repeatedly until minlength is
% reached. Each new split (on all sequences) produces a new output file
% Expect valfile as matrix with [seqid, class, feature1, feature2 ... ]
% Order of seqid matters; only continguous blocks with the same seqid are
% recognized as sequences (?)
% output is an output directory where the new validation files are to be
% placed. minlength refers to the lowest resolution (sequence length) to
% which to decompose to.
function [res] = HMMValidationPreprocessor(valfile,output,minlength,iter,balance)

valname = nameFromPath(valfile);
[data] = parseMatrix(valfile);

% For debugging purposes - gives the sequence nr and the number of
% observations in each sequence, plus the average sequence size
% [attribute, gdata]=groupData1D(data,1,[2:size(data,2)]);
% sequencesizes = [cell2mat(attribute)' ...
%                  cell2mat(cellfun(@(x)length(x),gdata,'un',0))']
% avgseqlength = mean(sequencesizes(:,2))

% Build resolutions
[res] = getResolutions(data,minlength,iter);

% if balance
%     res = cellfun(@(x)balanceDataset(x,2),res,'un',0);
% end

% % Print data sets
% for i=1:length(res)
%     out = strcat(output,'\',valname,'-res',num2str(i),'.csv');
%     format longG;
%     dlmwrite(out,res{i},'precision',10);
% end
end

function [res] = getResolutions(data,minlength,iterations)
    
    res = cell(1,iterations);
    res{1} = data;
    for j=2:iterations
        res{j} = [];
        
        % group data: seqid as the grouping attribute, ALL data including
        % the sequence id as the remainder
        [~, groupData] = groupData1D(res{j-1},1,[1:size(res{j-1},2)]);
        splitdata = cell(1,length(groupData)*2);
        for i=1:length(groupData)
            block = groupData{i};
            pointer = round(size(block,1)/2);
            if pointer < minlength
                splitdata{2*i-1} = block;
                continue;
            end
            splitdata{2*i-1} = block(1:pointer,:);
            splitdata{2*i} = block(pointer+1:end,:);
        end
        splitdata(cellfun(@isempty,splitdata)) = []; % Take out empty sections
        
        % Each cell of splitdata has a new split -> rename the seqid with
        % its index in splitdata
        for i=1:length(splitdata)
            block = splitdata{i};
            renamed = [repmat(i,size(block,1),1) block(:,2:end)];
            res{j} = [res{j}; renamed];
        end
    end    
end

% Read in data matrix
function [res] = parseMatrix(file)
    try
        res = csvread(file,0,0); 
    catch
        try
            res = csvread(file,1,0);
        catch
            res = [];
        end
    end
end

% Extract last name from path
function [fname] = nameFromPath(path)
    delims = strfind(path, '\');
    last = delims(length(delims));
    fname = path(last+1:end);
end


% Group the data in the given data matrix along the values in the 
% #groupIndex column. dataRange is an array specifying the indices from
% which the rest of the data should be collected.
% Method returns two cell arrays, one with the grouped values and one with
% the corresponding data.
function [groupedAtt, groupedData] = groupData1D(data,groupIndex,dataRange)
    [groupedAtt, groupedData] = deal({});
    if isempty(data) return; end
    [C,~,~] = unique(data(:,groupIndex));
    groupedAtt = cell(1, length(C));
    groupedData = cell(1, length(C));
    for i=1:length(C)
        filtered = data(data(:,groupIndex)==C(i),:);
        groupedAtt{i} = unique(filtered(:,groupIndex));
        groupedData{i} = filtered(:,dataRange);
    end
end
