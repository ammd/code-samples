%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HMMUtil
%
% Generic utility function repository for Hidden Markov Models
%%
function [m,bts] = HMMUtil(varargin)

% Function handles
m = @mapMat;
bts = @balanceTrainingSetPCA;

%% Public functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Apply a mapping function to each column of inputMat. funcs is a cell 
% array containing as many functions as inputMat has columns.
function [mapped] = mapMat(inputMat, funcs)
    
    if isempty(inputMat) mapped = []; return; end
    if size(inputMat,2) ~= length(funcs) 
        error('HMMUtil:mapMat', 'Dimensions mismatch.');
    end
    mapped = zeros(size(inputMat,1), size(inputMat,2));
    for i=1:length(funcs)
        mapped(:,i) = cell2mat(cellfun(@(x)funcs{i}(x), ...
                      num2cell(inputMat(:,i)),'un',0));
    end
end



%     classes = unique(data(:,classIdx));
%     [~, groupData] = groupData1D(data,classIdx,[1:size(data,2)]);
%     blockClass = cell2mat(cellfun(@(x)unique(x(:,2)),groupData,'un',0))
%     [~,idx] = min(histc(blockClass,classes));
%     mincases = {groupData{1, blockClass == classes(idx)}};
%     res = [];
%     for i=1:length(classes)
%         if i == idx continue; end
%         
%     end



% TODO: use PCA to reduce dimensionality of more heavily represented
% classes instead of throwing away information.

% Data is a training dataset, so a collection of features together
% with a class annotation, which can be found at the 1st column.
% This function balances the dataset so that each class is equally
% represented in the output. The relative ordering of the features per 
% class is respected in the output.
function [balanced] = balanceTrainingSet(data)
    classes = unique(data(:,1));
    [~,groupData] = groupData1D(data,1,[1:size(data,2)]);
    [minclass,~] = min(cell2mat(cellfun(@(x)length(x),groupData,'un',0)));
    parts = cellfun(@(x)x(1:minclass,:),groupData,'un',0);
    balanced = zeros(minclass*length(classes), size(data,2));
    for i=1:length(parts)
       balanced(((i-1)*minclass+1):(i*minclass),:) = parts{i}; 
    end
    
    [groupAtt,groupData] = groupData1D(balanced,1,[1:size(balanced,2)]);
    for i=1:length(groupAtt)
        length(groupData{i})
    end
end


% Note: PCA is performed on the transposed blocks of data, i.e. not to
% remove variables but to decrease the number of samples
function [balanced] = balanceTrainingSetPCA(data)
    
    classes = unique(data(:,1));
    flength = size(data,2)-1;
    [groupAtt,groupData] = groupData1D(data,1,[1:size(data,2)]);
    [classSizes] = cellfun(@(x)length(x),groupData,'un',0);
    [minclass,~] = min(cell2mat(classSizes)); % use this number of comp.

% windowed approach
     % Indices of all classes which should not be touched
%    [minclassid] = cellfun(@(x)length(x)==minclass,groupData,'un',0);

%     % Goal: all classes should have #minclass amount of entries
%     % 1. Compute required compression rates X:1 for all other classes
%     [compRates] = cellfun(@(x)round(length(x)/minclass),groupData,'un',0);
% 
%     % 2. Find power of compression rate so that the number of corresponding
%     % entries from the smaller data source is largest but does not exceed
%     % p, where p is the feature vector length
%     % 3. Double this number to get the final window size. This is to 
%     % accomodate Window overlap is set to 50%
%     princomps = nextpow2(flength)/2; % take this many components per window
%     [winSize] = cellfun(@(x)2*x.^(princomps),compRates,'un',0);

    balanced = zeros(minclass*length(classes), size(data,2));
    [rawData] = cellfun(@(x)x(:,2:end)'-repmat(mean(x(:,2:end)'),size(x,1),1),groupData,'un',0);
    for i=1:length(rawData)
        sigma = cov(rawData{i});
        [U,~,~] = svd(sigma);
        Ureduce = U(:,1:minclass);
        start = ((i-1)*minclass+1);
        finish = (i*minclass);
        balanced(start:finish,1) = unique(groupAtt{i});
        newData = (Ureduce'*rawData{i}');
        balanced(start:finish,2:end) = newData;
    end
end



%% Private functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Group the data in the given data matrix along the values in the 
% #groupIndex column. dataRange is an array specifying the indices from
% which the rest of the data should be collected.
% Method returns two cell arrays, one with the grouped values and one with
% the corresponding data.
function [groupedAtt, groupedData] = groupData1D(data,groupIndex,dataRange)
    [groupedAtt, groupedData] = deal({});
    if isempty(data) return; end
    [C,~,~] = unique(data(:,groupIndex));
    groupedAtt = cell(1, length(C));
    groupedData = cell(1, length(C));
    for i=1:length(C)
        filtered = data(data(:,groupIndex)==C(i),:);
        groupedAtt{i} = unique(filtered(:,groupIndex));
        groupedData{i} = filtered(:,dataRange);
    end
end



end

