%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HMM_Continuous
%
% Function repository for Hidden Markov Models designed to work with
% multiple continuous emission outputs. Returns customized procedures for
% preprocessing, training, validation, testing, and interpreting results.
%%
function [p, tr, v, ts, ps] = HMMContinuous(debug)

% Function handles
p = @preprocess;
tr = @train;
v = @validate;
ts = @test;
ps = @postprocess;

% State variables of this HMM type.
[mapMat,~] = HMMUtil();
clzMapRev = 0;


%% Public functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Preprocess the data by mapping the raw classes to application internal
% values, as well as normalizing all feature vectors (zscore) across
% all features.
function [mEm, mVal, mTest] = preprocess(em, val, test)
    
    if debug fprintf('HMMContinuous:preprocess...\n'); end
    
    % Check inputs
    if(size(em,2) <  2) 
        error('HMMContinuous:preprocess','Emission data format error.');
    elseif(~isempty(val) && size(val,2) < 3)
        error('HMMContinuous:preprocess','Validation data format error.');
    elseif(~isempty(test) && size(test,2) < 3)
        error('HMMContinuous:preprocess','Test data format error.');
    end
          
    % Build mappings
    [uniqueClass,~,~] = unique(em(:,1)); % valData classes = subset
    clzMap = containers.Map(uniqueClass, [1:size(uniqueClass, 1)]');
    clzMapRev = containers.Map([1:size(uniqueClass, 1)]', uniqueClass);
    [mVal, mTest] = deal([]);
   
    % Map data to usable internal format
    mEm  = [mapMat(em(:,1),   {@(x)clzMap(x)}) zscore(em(:,2:end))];
    
    if ~isempty(val)
    mVal = [mapMat(val(:,1:2),{@(x)x,@(x)clzMap(x)}) zscore(val(:,3:end))]; 
    end
    
    if ~isempty(test)
    mTest= [mapMat(test(:,1:2),{@(x)x,@(x)x}) zscore(test(:,3:end))];
    end
end

% Train HMM model(s) from continuous input data. 
% Expect annotated sequence in index form (starting with index 1), 
% number of possible classifications

% Train a continuous HMM model using the specified number of states.
% If states is a positive number, then explicitly use that many states.
% Otherwise use the input emissions sequence to infer 
% the number of states to train with, i.e. states = groups in data
% Expects emissionSeq as [class, emission], states as integer, dataStats as
% struct containing information mined during preprocessing that might be
% useful during training.
%
% Returns an application specific continuous HMM model
function [model] = train(emissionSeq, states, dataStats)
    
    if debug fprintf('HMMContinuous:train...\n'); end
      
    if states > 0    
        
        %  TODO: insert priors plus starting distributions
        [model,~]= hmmFit(emissionSeq(:,2:end)',states,'gauss',...
                      'maxIter',1000,'convTol', 1e-7,'nRandomRestarts',5);
    else
        % Get an approximation for the transition matrix based on the data,
        % use as a starting point for training.
        trMat = inferTransMat(emissionSeq(:,1));
        [model,~]= hmmFit(emissionSeq(:,2:end)',size(trMat,1),...
                      'gauss','trans0',trMat);
    end
    
    % Set the model's class. If emissionSeq has multiple classes, return
    % the set of all represented classes.
    model.mClz = unique(emissionSeq(:,1));
end

% TODO: move to paradigm, but override testSequence here?

% Validate a cell array of continuous HMM models against a given sequence.
% Expects seq as a 2 column matrix with [class, emission], and models
% as a cell array of discrete HMM models, which are arrays of the form
% {transition matrix, emission matrix, model classes}.
% Returns, for the given seq, the class of the model it is best represented
% by according to the log likelihood, as well as a matrix with dimensions 
% [length(seq) x % length(models)], where the entry at (i,j) gives the 
% index of the most likely state for model j.
function [class, statePerModel] = validate(seq, models)
    
    if debug fprintf('HMMContinuous:validate...\n'); end
    
    % PSTATES are the posterior state probabilities. For each trained
    % model, PSTATES has a matrix with dimensions [numStates x length(seq)]
    % giving the probability, for i=1:length(seq), that the respective 
    % model is in that state.
    %
    % score is a cell array with dimensions [1 x size(models)] giving the
    % log likelihood that the input sequence was produced by that model.
    [PSTATES, score] = cellfun(@(x) testSequence(x, seq),models,'un',0);
    
    % Get the index of the winning model based on log likelihood 
    [~, class_index] = max([score{:}]);
    
    % For each matrix in PSTATES (each matrix corresponds to a model),
    % compute the max across all states (columns) to find the winning state
    % (id) at each column, i.e. the state that the model is most likely in.
    % Return a matrix with dimensions [length(seq) x 
    % length(models)], where the entry at (i,j) gives the index of the 
    % winning state for model j.
    [~, idx] = cellfun(@(x) max(x), PSTATES,'un',0);
    statePerModel = cell2mat(cellfun(@(x)x',idx,'un',0));
    
    % Assign the class of the winning log likelihood model to the current
    % sample, and return it.
    class = models{class_index}.mClz;
end


% Assign a class label to the given sequence using the specified model.
% Expects seq as a 2 column matrix with [class,
% emission], and models as a cell array containing trained models
% i.e. as returned by the HMMContinuous::train method.
function [class, classPerState] = test(seq, models)
    if debug fprintf('HMMContinuous:test...\n'); end
    [class, classPerState] = validate(seq, models);
end

% Postprocess the validation and test output data. This involves un-mapping
% the individual classes to match their original values in the input file
% Expects val and test to be 2 column matrices mapping sampleIDs (in case
% of val, classIDs) to their assigned classes.
function [val, test] = postprocess(val, test)
    if debug fprintf('HMMContinuous:postprocess...\n'); end
    [val] = mapMat(val, {@(x)clzMapRev(x), @(x)clzMapRev(x)});
    [test] = mapMat(test, {@(x)x, @(x)clzMapRev(x)});
end



%% Private functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compute the transition matrix based on the given data. Expect time series
% given as a column vector (discrete values). Expects states / values to
% be in range [1, # of distinct states], outputs matrix with row / col
% indices corresponding to states, with rows summing to one.
function [p] = inferTransMat(M)
    
    numStates = length(unique(M(:,1)));
    p = zeros(numStates, numStates);
    
    % How many times does state i transition to state j
    for t = 1:size(M, 1)
        if t+1 <= size(M, 1)
            p(M(t, 1), M(t+1, 1)) = p(M(t, 1), M(t+1, 1)) + 1;
        end
    end

    % Vector of row sums
    rowSum = sum(p, 2);

    % Divide by total number of transitions from state (row) i
    for i = 1:size(p, 1)
        for j = 1:size(p, 2)
            if rowSum(i) == 0
                p(i, j) = 0;
            else p(i, j) = double(p(i, j)) / double(rowSum(i));
            end;
        end
    end
end

% model = PMTK model
% PSTATES = array of probs, with length as number of states
% logpseq = single value, score the sequence
function [gamma, loglik] = testSequence(model, seq)
    [gamma,loglik,~,~,~] = hmmInferNodes(model, seq');
end


end

