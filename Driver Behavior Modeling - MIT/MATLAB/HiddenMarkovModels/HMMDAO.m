%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HMMDAO
% Data access abstraction over database layer.
%
% HMMDAO('-display','-discrete','iter',100,'schema','hmm','train',...
%        '2012b','test','2012b','out','C:\Users\ammd\Desktop\out\')
%
%% Options ---------------------------------------------------------------
% -display:    
%           Display output on standard output
% -discrete:    
%           Input data is discrete.
% -continuous: 
%           Input data is continuous
%
%% Inputs ----------------------------------------------------------------
% Flags in [] mark with which options they must be provided:
%
% iter [required]:
%           Number of iterations over which to average the computation of
%           classification accuracy.
% schema [required]:
%           Name of schema to pull from (see database specification)
% train [required]:
%           Name of study to use for training, i.e. 2012b,2013a,2012b2013a.
%           See scheme specifications.
% test [required]:
%           Name of study case to use for testing, i.e. 2012b,2013a,etc.
%           See schema specifications.
% out [required]:
%           Temp output folder, must not previously exist (gets deleted
%           on exit).
%%
function [accuAcc, accuError, accuRes, accuClasses, accuTrails] = ...
          HMMDAO(varargin)
      
format longG;
parser = inputParser;

% Handle input flags. Anonymous function corresponds to validation
% function, returns true iff the use of the flag is valid.
vF = {};
[display, varargin,vF{end+1}] =parseFlag(varargin,'-display',@()true);
[discrete, varargin,vF{end+1}] =parseFlag(varargin,'-discrete',@()true);
[continuous, varargin,vF{end+1}]=parseFlag(varargin,'-continuous',@()true);

% Value parameters
addParameter(parser,'iter','',@(x)x>0);
addParameter(parser,'train','',@(x)~isempty(x));
addParameter(parser,'schema','',@(x)~isempty(x));
addParameter(parser,'test','',@(x)~isempty(x));
addParameter(parser,'out','',@(x)~isempty(x));

% Global validations
vF{end+1} = @() (discrete + continuous == 1);

% Validate inputs
parser.parse(varargin{:})
for i=1:length(vF)
    if (~vF{i}()) 
        error('HMMDAO:flagCheck', ...
        strcat('The following condition is not met: ',func2str(vF{i})));
    end
end

% Get data - state variables
iter = parser.Results.iter;
train = parser.Results.train;
test = parser.Results.test;
out = parser.Results.out;
schema = parser.Results.schema;
mkdir(out);

%Set preferences with setdbprefs.
setdbprefs('DataReturnFormat', 'cellarray');
setdbprefs('NullNumberRead', 'NaN');
setdbprefs('NullStringRead', 'null');

% Make connection to database.  Note that the password has been omitted.
% Using ODBC driver.
conn = database('PostgreSQL30', 'postgres', '');

statsmat = [];
trainsplitquery = ['SELECT * FROM ' schema '.splitdata' train '();'];
testsplitquery = ['SELECT * FROM ' schema '.splitdata' test '();'];
trainquery = ['SELECT * FROM ' schema '.trainglance' train];
valquery = ['SELECT * FROM ' schema '.validateglance' test];

% Accumulated results over all iterations
[accuAcc, accuError, accuRes, accuClasses, accuTrails] = deal({});

counter = 1;
while counter <= iter
    
    % Get fresh splits
    curs = exec(conn, [trainsplitquery]);
    curs = fetch(curs);
    curs = exec(conn, [testsplitquery]);
    curs = fetch(curs);
    close(curs);
    
    % Training file
    curs = exec(conn, [trainquery]);
    curs = fetch(curs);
    train = strcat(out,'\hmmtrain-',int2str(counter),'.csv');
    dlmwrite(train,curs.Data,'precision',10);
    close(curs);
    
    % Validation file
    curs = exec(conn, [valquery]);
    curs = fetch(curs);
    valfile = strcat(out,'\hmmval-',int2str(counter),'.csv');
    dlmwrite(valfile,curs.Data,'precision',10);
    close(curs);
    
    % Preprocessing val file, pick resolution of validation sequences
    [split] = HMMValidationPreprocessor(valfile,out,10,10,false);
    temp = strcat(out,'\temp.csv');
    dlmwrite(temp,split{1},'precision',10);
    
    % Accumulate results
    try
        % If dealing with continuous values, the gaussian fit might not 
        % work due to non positive definite matrices in the cholensky 
        % decomposition
        if continuous
        [accuAcc{end+1}, accuError{end+1}, accuRes{end+1}, ...
         accuClasses{end+1}, accuTrails{end+1}] = ...
         HMMMain('-silent','-synthetic','-continuous','trainfile',train,...
                 'valfile',temp,'states',5);
             
        elseif discrete
         [accuAcc{end+1}, accuError{end+1}, accuRes{end+1}, ...
         accuClasses{end+1}, accuTrails{end+1}] = ...    
         HMMMain('-silent','-synthetic','-discrete','trainfile',train,...
                'valfile',temp,'states',5);
        end

        % Results to display
        statsmat = [statsmat; accuAcc{end}, accuError{end},...
                    accuClasses{end},accuTrails{end}];
        counter = counter +1;
    catch exception
        disp(exception);
    end
end

%Close database connection.
close(conn);

% Cleanup temp file
rmdir(out,'s');

% Display numeric results
if display
    statsmat
    disp('Aggregated (Accuracy, Error, Classes, Trials): ');
    res = mean(statsmat(~any(isnan(statsmat),2),:),1) % take non NAN rows
    disp('Standard deviation: ');
    std(statsmat(~any(isnan(statsmat),2),:)) % take non NAN rows
end
end

% Returns true for boolRes iff the flag in found in args and the given
% condition for its existence is fulfilled. Output function is set to
% input function iff boolRes is met.
function [boolRes, args, func] = parseFlag(args, flag, valFunc)
    boolRes = false;
    func = @()true;
    if find(strcmp(flag, args)) boolRes = true; end
    args = args(~strcmp(args,flag));
    if boolRes func = valFunc; end
end
