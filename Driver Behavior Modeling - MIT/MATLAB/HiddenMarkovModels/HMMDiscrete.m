%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HMM_Discrete
%
% Function repository / business logic for Hidden Markov Models designed to
% work with single discrete emission outputs. Returns customized procedures
% for preprocessing, training, validation, testing, and postprocessing
% results.
%%
function [p, tr, v, ts, ps] = HMMDiscrete(debug)

% Function handles
p = @preprocess;
tr = @train;
v = @validate;
ts = @test;
ps = @postprocess;

% State variables of this HMM type.
[mapMat,~] = HMMUtil();
maxEmissions = 0;
emissionDist = [];
clzMapRev = 0;


%% Public functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Preprocess the data by mapping classes and emissions to application
% internal values, i.e. to the range [1, #number of distinct possible 
% emissions]. Expect emissions as two column matrix with [class, emission]
% Expect valData as 3 column matrix with [seqID, class, emission]
% Expect testData as 3 column matrix with [seqID, sampleID, emission]
function [mEm, mVal, mTest] = preprocess(emissions, valData, testData)
    
    if debug fprintf('HMMDiscrete:preprocess...\n'); end
     
    % Check inputs
    if(size(emissions,2) ~= 2) 
        error('HMMDiscrete:preprocess','Emission data format error.');
    elseif(~isempty(valData) && size(valData,2) ~= 3)
        error('HMMDiscrete:preprocess','Validation data format error.');
    elseif(~isempty(testData) && size(testData,2) ~= 3)
        error('HMMDiscrete:preprocess','Test data format error.');
    end
    
    % Build mappings - use data files directly to determine which emissions
    % are possible. Note: this will create a model that is tailored to the
    % data at hand, yet with no dependency on an external encodings file
    allemissions = [emissions(:,2); valData(:,3:end); testData(:,3:end)];
    uniqueEn = unique(allemissions(:,1));
    maxEmissions = size(uniqueEn(:,1),1);
    
    % Map emission integers to range [1, maxEmissions]
    emMap = containers.Map(uniqueEn,[1:size(uniqueEn, 1)]');
    [uniqueClass,~,~] = unique(emissions(:,1)); % valData classes = subset
    clzMap = containers.Map(uniqueClass, [1:size(uniqueClass, 1)]');
    clzMapRev = containers.Map([1:size(uniqueClass, 1)]', uniqueClass);
    
    % Map data to usable internal format
    mEm =   mapMat(emissions, {@(x)clzMap(x), @(x)emMap(x)});
    mVal =  mapMat(valData,   {@(x)x,@(x)clzMap(x),@(x)emMap(x)});
    mTest = mapMat(testData,  {@(x)x, @(x)x, @(x)emMap(x)});
    
    % Compute some global statistics over all data, which may be levied
    % during training. Note: it is important that this step come after
    % the data mapping step above.
    allData = [mEm(:,1:end); mVal(:,2:end)];
    allDataGroups = unique(allData(:,1),'sorted'); % range: [1, max class]
    emissionDist = zeros(length(allDataGroups), maxEmissions);
    
    % Compute emissions count - how many times does each possible emission
    % appear within each group?
    for i=1:length(allDataGroups)
        filtered = allData(allData(:,1)==i,2);
        for k=1:maxEmissions
            emissionDist(i,k) = size(filtered(filtered(:,1)==k,:),1);
        end
    end
end


% Train a discrete HMM model using the specified number of states.
% If states is a positive number, then explicitly use that many states.
% Otherwise use the input emissions sequence to infer 
% the number of states to train with, i.e. states = groups in data
% Expects emissionSeq as [class, emission], states as integer, dataStats as
% struct containing information mined during preprocessing that might be
% useful during training.
%
% Returns a discrete HMM model, which is a cell array containing the 
% trained transition probability matrix, the emissions matrix, and the
% distinct classes represented in emissionSeq
function [model] = train(emissionSeq, states, dataStats)
    
    if debug fprintf('HMMDiscrete:train...\n'); end
        
    if states > 0
                
        % Compute pseudoemissions - take the average of each emission count
        % across all groups. The idea is that this emission distribution
        % will remain relatively unchanged even when using a user defined
        % number of states. Result is normalized (shows best classification
        % accuracy).
        pseudoEm = normr(repmat(mean(emissionDist), states,1));
        
        % Compute pseudotransitions - corresponds to a Laplace smoother, 
        % says we have no prior info on the distribution of transitions, 
        % which is the case here because states are set by user and thus
        % separate from the raw data
        pseudoTrans = ones(states,states); 
        fillFactor = max(1,round(dataStats.avgSeqLength / (states^2)));
        %pseudoTrans = normr(repmat(fillFactor,states,states));
       
        % Starting guess parameters, assume uniform distributions
        trans = ones(states) / states;
        emMat = ones(states, maxEmissions) / maxEmissions;
        
        % Leaving out pseudoemissions and pseudotransitions may
        % improve performance accuracy, although leaving them out is not
        % really an alternative, because the emission sequence might not
        % contain all possible emissions or transitions, so the trained HMM
        % will be unable to handle a validation or test sequence that 
        % contains emissions or transitions not present during training.
        [trans_est, em_est] = hmmtrain(emissionSeq(:,2)',trans,emMat,...
                              'Algorithm','BaumWelch',...
                              'Maxiterations',1000,...
                              'Pseudoemissions',pseudoEm,...
                              'Pseudotransitions',pseudoTrans);
%         TODO: PMTK3
%         'emissionPrior',struct('alpha',pseudoEm)
%         'transPrior',trans
%         [m,~]= hmmFit(emissionSeq(:,2:end)',states,'discrete',...
%                       'maxIter',1000,'convTol',1e-7,...
%                       'nRandomRestarts',5,'emission0',x,...
%                       tabularCpdCreate(emMat));
    else
   
        maxClz = length(unique(emissionSeq(:,1)));
        pseudoEm = normr(repmat(mean(emissionDist),maxClz,1)); % see above
        pseudoTrans = ones(maxClz,maxClz); % Laplace smoother, see above
        %pseudoTrans = normr(repmat(fillFactor,states,states));
        
        % Because the states are inferred (taken from the data group
        % identifiers in the data), the HMM can be trained with the
        % knowledge of which emissions correspond to which state. This
        % leads to the trivial (and best) computation of the transition
        % matrix, for which hmmestimate is used. 
        % Whether the assignment of an emission sequence to
        % particular class label makes sense / contains discriminative
        % potential is another question.
        [trans_est, em_est] = hmmestimate(emissionSeq(:,2)',...
                              emissionSeq(:,1)',...    
                              'Pseudoemissions',pseudoEm,...
                              'Pseudotransitions',pseudoTrans);
    end
    
    % Set the model's class. If several classes are present in the
    % training data, return the set of all classes that are represented.
    mClz = unique(emissionSeq(:,1));
    model = {trans_est, em_est, mClz};
    %model = {m, mClz}; % PMTK3
end

% TODO: move to paradigm, but override testSequence here?

% Validate a cell array of discrete HMM models against a given sequence.
% Expects seq as a 2 column matrix with [class, emission], and models
% as a cell array of discrete HMM models, which are arrays of the form
% {transition matrix, emission matrix, model classes}.
% Returns, for the given seq, the class of the model it is best represented
% by according to the log likelihood, as well as a matrix with dimensions 
% [length(seq) x % length(models)], where the entry at (i,j) gives the 
% index of the most likely state for model j.
function [class, statePerModel] = validate(seq, models)
    
    if debug fprintf('HMMDiscrete:validate...\n'); end
        
    % PSTATES are the posterior state probabilities. For each trained
    % model, PSTATES has a matrix with dimensions [numStates x length(seq)]
    % giving the probability, for i=1:length(seq), that the respective 
    % model is in that state.
    %
    % score is a cell array with dimensions [1 x size(models)] giving the
    % log likelihood that the input sequence was produced by that model.
   [PSTATES, score] = cellfun(@(x) testSequence(x, seq),models,'un',0);

   % Get the index of the winning model based on log likelihood 
   [~, class_index] = max([score{:}]);
   
   % For each matrix in PSTATES (each matrix corresponds to a model),
   % compute the max across all states (columns) to find the winning state
   % (id) at each column, i.e. the state that the model is most likely in.
   % Return a matrix with dimensions [length(seq) x 
   % length(models)], where the entry at (i,j) gives the index of the 
   % winning state for model j.
   [~, idx] = cellfun(@(x) max(x), PSTATES,'un',0);
   statePerModel = cell2mat(cellfun(@(x)x',idx,'un',0));
   
   % Assign the class of the winning log likelihood model to the current
   % sample, and return it.
   class = models{class_index}{3};
   % class = models{class_index}{2}; % PMKT3
end


% Assign a class label to the given sequence using the specified models (
% one class label per model). Expects seq as a 2 column matrix with [class,
% emission], and models as a cell array of trained models i.e. as
% returned by the HMMDiscrete::train method.
function [classes, classPerState] = test(seq, models)
    if debug fprintf('HMMDiscrete:test...\n'); end
    [classes,classPerState] = validate(seq,models);
end

% Postprocess the validation and test output data. This involves un-mapping
% the individual classes to match their original values in the input file
% Expects val and test to be 2 column matrices mapping sampleIDs (in case
% of val, classIDs) to their assigned classes.
function [val, test] = postprocess(val, test)
    if debug fprintf('HMMDiscrete:postprocess...\n'); end
    [val] = mapMat(val, {@(x)clzMapRev(x), @(x)clzMapRev(x)});
    [test] = mapMat(test, {@(x)x, @(x)clzMapRev(x)});
end





%% Private functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Test how well a given model ({transition mat, emission mat, class})
% explains a given input sequence (col vector containing emissions).
% Expect sequence in index form (starting with index 1)
function [PSTATES, logpseq] = testSequence(model, seq)
    [PSTATES, logpseq,~,~,~] = hmmdecode(seq(:,1)', model{1}, model{2}); 
    % [PSTATES,logpseq,~,~,~] = hmmInferNodes(model{1}, seq'); % PMTK3
end
end

