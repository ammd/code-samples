%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HMMVisualizer
% Visualize HMM classification data
%
% HMMVisualizer('-accuracy','-confusion','iter',100,...
%        'temp','C:\Users\ammd\Desktop\temp\',...
%        'out','C:\Users\ammd\Desktop\out\')
%
%% Options ---------------------------------------------------------------
% -accuracy:    
%           Display output on standard output
% -confusion:    
%           Input data is discrete.
%
%% Inputs ----------------------------------------------------------------
% Flags in [] mark with which options they must be provided:
%
% iter [required]:
%           Number of iterations over which to average the computation of
%           classification accuracy.
% temp [required]:
%           Temp output folder, must not previously exist (gets deleted
%           on exit).
% out [required]:
%           Directory where final results are to be placed.
%%
function HMMVisualizer(varargin)

format longG;
parser = inputParser;

% Handle input flags. Anonymous function corresponds to validation
% function, returns true iff the use of the flag is valid.
vF = {};
[vizAccMat, varargin,vF{end+1}] =parseFlag(varargin,'-accuracy',@()true);
[vizConfMat, varargin,vF{end+1}] =parseFlag(varargin,'-confusion',@()true);

% Value parameters
addParameter(parser,'iter','',@(x)x>0);
addParameter(parser,'temp','',@(x)~isempty(x));
addParameter(parser,'out','',@(x)~isempty(x));

% Validate inputs
parser.parse(varargin{:})
for i=1:length(vF)
    if (~vF{i}()) 
        error('HMMDAO:flagCheck', ...
        strcat('The following condition is not met: ',func2str(vF{i})));
    end
end

% Get state variables
iter = parser.Results.iter;
temp = parser.Results.temp;
out = parser.Results.out;

% Get data ----------------------------------------------------------------
% Modify accordingly 

%2012b-2012b
[accuAcc2012b2012b, accuError2012b2012b, accuRes2012b2012b, accuClasses2012b2012b, ...
accuTrials2012b2012b] = HMMDAO('-discrete','iter',iter,'train','2012bdemographics',...
                               'test','2012bdemographics','schema','hmm','out',temp);

%2013a-2013a
% [accuAcc2013a2013a, accuError2013a2013a, accuRes2013a2013a, accuClasses2013a2013a, ...
%  accuTrials2013a2013a] = HMMDAO('-discrete','iter',iter,'train','2013a',...
%                                'test','2013a','schema','hmm','out',temp);
% %2012b-2013a
% [accuAcc2012b2013a, accuError2012b2013a, accuRes2012b2013a, accuClasses2012b2013a, ...
% accuTrials2012b2013a] = HMMDAO('-continuous','iter',iter,'train','2012b',...
%                               'test','2013a','schema','hmm','out',temp);
% 
% %2013a-2012b
% [accuAcc2013a2012b, accuError2013a2012b, accuRes2013a2012b, accuClasses2013a2012b, ...
% accuTrials2013a2012b] = HMMDAO('-continuous','iter',iter,'train','2013a',...
%                               'test','2012b','schema','hmm','out',temp);
% 
% %2012b2013a-2012b2013a
% [accuAcc2012bU2013a, accuError2012bU2013a, accuRes2012bU2013a, ...
% accuClasses2012bU2013a, accuTrials2012bU2013a] = ...
%                           HMMDAO('-continuous','iter',iter,'train',...
%                                  '2012b2013a','test','2012b2013a',...
%                                  'schema','hmm','out',temp);


% 2013g stuff
%
% [accuAcc2013g, accuError2013g, accuRes2013g, accuClasses2013g, ...
% accuTrials2013g] = HMMDAO('-discrete','iter',iter,'train','2013g',...
%                           'test','2013g','schema','hmm2013g','out',temp);
%                       
% [accuAcc2013gvolvo, accuError2013gvolvo, accuRes2013gvolvo, accuClasses2013gvolvo, ...
% accuTrials2013gvolvo] = HMMDAO('-continuous','iter',iter,'train','2013gvolvo',...
%                           'test','2013gvolvo','schema','hmm2013g','out',temp);
%                       
% [accuAcc2013gchevy, accuError2013gchevy, accuRes2013gchevy, accuClasses2013gchevy, ...
% accuTrials2013gchevy] = HMMDAO('-continuous','iter',iter,'train','2013gchevy',...
%                           'test','2013gchevy','schema','hmm2013g','out',temp);

% [accuAcc2013gvolvosmartphone, accuError2013gvolvosmartphone, accuRes2013gvolvosmartphone, accuClasses2013gvolvosmartphone, ...
% accuTrials2013gvolvosmartphone] = HMMDAO('-discrete','iter',iter,'train','2013gvolvosmartphone',...
%                           'test','2013gvolvosmartphone','schema','hmm2013g','out',temp);
%                       
% [accuAcc2013gchevysmartphone, accuError2013gchevysmartphone, accuRes2013gchevysmartphone, accuClasses2013gchevysmartphone, ...
% accuTrials2013gchevysmartphone] = HMMDAO('-discrete','iter',iter,'train','2013gchevysmartphone',...
%                           'test','2013gchevysmartphone','schema','hmm2013g','out',temp);
%                       
% [accuAcc2013gvolvoembedded, accuError2013gvolvoembedded, accuRes2013gvolvoembedded, accuClasses2013gvolvoembedded, ...
% accuTrials2013gvolvoembedded] = HMMDAO('-discrete','iter',iter,'train','2013gvolvoembedded',...
%                           'test','2013gvolvoembedded','schema','hmm2013g','out',temp);
%                       
% [accuAcc2013gchevyembedded, accuError2013gchevyembedded, accuRes2013gchevyembedded, accuClasses2013gchevyembedded, ...
% accuTrials2013gchevyembedded] = HMMDAO('-discrete','iter',iter,'train','2013gchevyembedded',...
%                           'test','2013gchevyembedded','schema','hmm2013g','out',temp);




% Visualize confusion matrices
if vizConfMat
    disp('Visualizing confusion matrices ... ');

    dataLabels{1} = [0;1];
    dataLabels{2} = {'Males','Females'};


% Manual, voice, baseline split
%     dataLabels{1} = [0;1;2];
%     dataLabels{2} = {'Baseline','PhoneM','PhoneV'};
    
    % 2012b
    labels = {};
    numIter = length(accuRes2012b2012b);
    for i=1:numIter labels{i} = strcat('2012b-',int2str(i)); end
    visualizeConfusion('2012b',accuRes2012b2012b,labels,dataLabels,out,true);
    
    % 2013a
%     labels = {};
%     numIter = length(accuRes2013a2013a);
%     for i=1:numIter labels{i} = strcat('2013a-',int2str(i)); end
%     visualizeConfusion('2013a',accuRes2013a2013a,labels,dataLabels,out,true);
    
    % 2012b + 2013a
%     labels = {};
%     numIter = length(accuRes2012bU2013a);
%     for i=1:numIter labels{i} = strcat('2012b2013a-',int2str(i)); end
%     visualizeConfusion('2012b2013a',accuRes2012bU2013a,labels,dataLabels,out,true);
%     disp('done.');
% 
%     dataLabels{1} = [0;1;2];
%     dataLabels{2} = {'Baseline','M Phone Dial','V Phone Dial'};
%   
%     % 2013g - all 
%     labels = {};
%     numIter = length(accuRes2013g);
%     for i=1:numIter labels{i} = strcat('2013g-',int2str(i)); end
%     visualizeConfusion('2013g',accuRes2013g,labels,dataLabels,out,true);
%     
%     % 2013g - volvo
%     labels = {};
%     numIter = length(accuRes2013gvolvo);
%     for i=1:numIter labels{i} = strcat('2013gvolvo-',int2str(i)); end
%     visualizeConfusion('2013gvolvo',accuRes2013gvolvo,labels,dataLabels,out,true);
%     
%     % 2013g - chevy
%     labels = {};
%     numIter = length(accuRes2013gchevy);
%     for i=1:numIter labels{i} = strcat('2013gchevy-',int2str(i)); end
%     visualizeConfusion('2013gchevy',accuRes2013gchevy,labels,dataLabels,out,true);

%     % 2013g - volvo phone
%     labels = {};
%     numIter = length(accuRes2013gvolvophone);
%     for i=1:numIter labels{i} = strcat('2013gvolvophone-',int2str(i)); end
%     visualizeConfusion('2013gvolvophone',accuRes2013gvolvophone,labels,dataLabels,out,true);
%     
%     % 2013g - chevy phone
%     labels = {};
%     numIter = length(accuRes2013gchevyphone);
%     for i=1:numIter labels{i} = strcat('2013gchevyphone-',int2str(i)); end
%     visualizeConfusion('2013gchevyphone',accuRes2013gchevyphone,labels,dataLabels,out,true);

%     % 2013g - volvo smartphone
%     labels = {};
%     numIter = length(accuRes2013gvolvosmartphone);
%     for i=1:numIter labels{i} = strcat('2013gvolvosmartphone-',int2str(i)); end
%     visualizeConfusion('2013gvolvosmartphone',accuRes2013gvolvosmartphone,labels,dataLabels,out,true);
%     
%     % 2013g - chevy phone
%     labels = {};
%     numIter = length(accuRes2013gchevysmartphone);
%     for i=1:numIter labels{i} = strcat('2013gchevysmartphone-',int2str(i)); end
%     visualizeConfusion('2013gchevysmartphone',accuRes2013gchevysmartphone,labels,dataLabels,out,true);
%     
%     % 2013g - volvo embedded
%     labels = {};
%     numIter = length(accuRes2013gvolvoembedded);
%     for i=1:numIter labels{i} = strcat('2013gvolvoembedded-',int2str(i)); end
%     visualizeConfusion('2013gvolvoembedded',accuRes2013gvolvoembedded,labels,dataLabels,out,true);
%     
%     % 2013g - chevy embedded
%     labels = {};
%     numIter = length(accuRes2013gchevyembedded);
%     for i=1:numIter labels{i} = strcat('2013gchevyembedded-',int2str(i)); end
%     visualizeConfusion('2013gchevyembedded',accuRes2013gchevyembedded,labels,dataLabels,out,true);

end

% Visualize accuracy
if vizAccMat
    disp('Visualizing accuracy ... ');
    fig = figure('visible','on');
    set(fig, 'units', 'inches', 'position', [5 5 14 10]);
    
        
%     h=notBoxPlot([...
%                 cell2mat(accuAcc2012b2012b), ...
%                 cell2mat(accuAcc2013a2013a), ...
%                 cell2mat(accuAcc2012b2013a),...
%                 cell2mat(accuAcc2013a2012b),...
%                 cell2mat(accuAcc2012bU2013a)], ...
%                 [...  
%                  repmat(1,1,length(accuAcc2012b2012b)), ...
%                  repmat(2,1,length(accuAcc2013a2013a)), ...
%                  repmat(3,1,length(accuAcc2012b2013a)), ...
%                  repmat(4,1,length(accuAcc2013a2012b)), ...
%                  repmat(5,1,length(accuAcc2012bU2013a)) ...
%                 ]);
%     d=[h.data];
%     set(d(1:end),'markerfacecolor',[0.4,1,0.4],'color',[0,0.4,0]);
%     
%     % X axis labels
%     labels ={...
%              strcat(num2str(mean(cell2mat(accuTrials2012b2012b)))),...
%              strcat(num2str(mean(cell2mat(accuTrials2013a2013a)))),...
%              strcat(num2str(mean(cell2mat(accuTrials2012b2013a)))),...    
%              strcat(num2str(mean(cell2mat(accuTrials2013a2012b)))),...    
%              strcat(num2str(mean(cell2mat(accuTrials2012bU2013a))))...
%              };


%     h=notBoxPlot([...
%                 cell2mat(accuAcc2013g'), ...
%                 cell2mat(accuAcc2013gvolvo'), ...
%                 cell2mat(accuAcc2013gchevy'), ...
%                 cell2mat(accuAcc2013gvolvophone'), ...
%                 cell2mat(accuAcc2013gchevyphone')],...
%                 [...  
%                  repmat(1,1,length(accuAcc2013g)), ...
%                  repmat(2,1,length(accuAcc2013gvolvo)), ...
%                  repmat(3,1,length(accuAcc2013gchevy)), ...
%                  repmat(4,1,length(accuAcc2013gvolvophone)), ...
%                  repmat(5,1,length(accuAcc2013gchevyphone))...
%                 ]);

%  h=notBoxPlot([...
%                 cell2mat(accuAcc2013g'), ...
%                 cell2mat(accuAcc2013gvolvosmartphone'), ...
%                 cell2mat(accuAcc2013gchevysmartphone'), ...
%                 cell2mat(accuAcc2013gvolvoembedded'), ...
%                 cell2mat(accuAcc2013gchevyembedded')]);
%     d=[h.data];
%     set(d(1:end),'markerfacecolor',[0.4,1,0.4],'color',[0,0.4,0]);
%     
%     % X axis labels
%     labels ={...
%                  num2str(mean(cell2mat(accuTrials2013g))),...
%                  num2str(mean(cell2mat(accuTrials2013gvolvosmartphone))),...
%                  num2str(mean(cell2mat(accuTrials2013gchevysmartphone))),...  
%                  num2str(mean(cell2mat(accuTrials2013gvolvoembedded))),...
%                  num2str(mean(cell2mat(accuTrials2013gchevyembedded)))  
%                  
%              };


    h=notBoxPlot([cell2mat(accuAcc2012b2012b')]);
    d=[h.data];
    set(d(1:end),'markerfacecolor',[0.4,1,0.4],'color',[0,0.4,0]);
    
    % X axis labels
    labels ={num2str(mean(cell2mat(accuTrials2012b2012b)))};

    set(gca, 'XTickLabel', labels);
    set(gca, 'FontSize', 20);
    set(gcf, 'Color', 'w');
    xlabel('Datasets', 'FontSize',22);
    ylabel('Accuracy (%)','FontSize',22);
    xticklabel_rotate([1:length(labels)],30,labels,'interpreter','none');
    export_fig(strcat(out,'\acc.png'),'-q101','-a4',fig);
    disp('done.');
end
end



% Produce a confusion matrix for the given classifications in classes,
% which is a cell array with n elements, with n = number of datasets, where
% each element is a 2 element matrix, the first element of each 
% containing the annotated data (labels) and the second the predicted 
% class labels. labels is a cell array of the same length as classes, 
% providing a descriptive string for each dataset. dataLabels is a 2
% element cell array mapping integer output values as present in classes to
% actual string descriptors. These values are placed on the x and y axis of
% the final plot. output describes the directory where the results should
% be placed. avg is true iff a single confusion matrix is to be created
% over all trials, taking the average across all individual confusion
% matrices.
function visualizeConfusion(label,classes,labels,dataLabels,output,avg)

    disp('Visualizing confusion matrix ....');
    
    % Data integrity check
    if length(classes) ~= length(labels)
        error('HMMVisualizer:visualizeConfusion',...
              'Data and labels length mismatch.');
    end
    
    accmat = zeros(length(dataLabels{1}),length(dataLabels{1}));
    order = [];
    
    % Produce one confusion matrix per trial
    output = strcat(output,'\');
    for i=1:length(classes)
        [cmat,order] = confusionmat(classes{i}(:,1),classes{i}(:,2), 'order', dataLabels{1});
        accmat = accmat + cmat;
        visualizeMatrix(cmat,order,strcat(label,'-',int2str(i)));
    end
    
    % Confusion matrix average over all trials
    if avg 
        visualizeMatrix(accmat ./ length(classes),order,strcat(label,'-avg')); 
    end
    
    % Render a matrix
    function visualizeMatrix(cmat,order,outname)
        s = sum(cmat, 2);
        p = bsxfun(@rdivide, cmat',s')';
        [~,loc] = ismember(order, dataLabels{1});
        xlabels = {dataLabels{2}{loc}};
        ylabels = xlabels;
        r = abs(p - eye(size(p))) .^0.3;
        fig = figure('visible','off');
        imagesc(r); % Display gamma corrected colors
        caxis([0 1]); % color range to [0,1], this allows for comparisons
        colormap(gray);
        textS = num2str(cmat(:),'%0.0f'); % original values on top
        textS = strtrim(cellstr(textS));
        [x,y] = meshgrid(1:size(r,1));
        hStrings = text(x(:),y(:),textS(:),'HorizontalAlignment','center');
        textColors = true(1,3);
        set(hStrings,{'Color'},num2cell(textColors,2));
        set(gca,'YTick',[1:length(ylabels)]);
        set(gca,'XTick',[1:length(xlabels)]);
        set(gca, 'XTickLabel', xlabels);
        set(gca, 'YTickLabel', ylabels);
        set(gcf, 'Color', 'w');
        set(gca, 'FontSize', 24);
        set(hStrings,'FontSize',getTextSize(max(size(p)),2));
        set(fig, 'PaperPositionMode', 'auto');
        set(fig, 'units', 'inches', 'position', [5 5 15 10]);
        ylabel('Actual Class','FontSize',26);
        xlabel('Predicted Class','FontSize',26);
        export_fig(strcat(output,outname),'-q101','-a4',fig);
    end
    
    % Set the font size of in-graphic text. The mapping from input to
    % output size is essentially a gamma correction with a bottom of 8
    function [newsize] = getTextSize(size, gamma)
        val = (25-size) / 25;
        if (val < 0) newsize = 8;
        else newsize = (val.^gamma)*50;
        end
    end

    disp('done.');
end


% DEPRECATED
% Use the given accuracy measurements and labels to produce an accuracy
% bar chart. acc and labels are equally sized cell arrays containing the
% individual accuracy values and data set labels respectively. numClasses
% is also an equally sized cell array specifying how many classes each
% dataset was comprised of; a valid input has equal number of classes for
% all datasets. Finally, the graph is written to the directory specified by
% output with a filename that results in the concatenation of all labels.
function visualizeAccuracyBoxPlot(acc,labels,nTrials,numClasses,output)

    disp('Visualizing accuracy ....');
    
    % Data integrity check
    if length(acc) ~= length(labels)
        error('HMMVisualizer:visualizeAccuracy',...
              'Data,labels,trials length mismatch.');
    elseif min([numClasses{:}]) ~= max([numClasses{:}])
        error('HMMVisualizer:visualizeAccuracy',...
              'Datasets have different number of classes.');
    end
    
    % Classification accuracy values
    y = [acc{:}];
    numClasses = min([numClasses{:}]);
    
    % Positions for value labels
    ylabelpos = y';
    xlabelpos = [1:length(acc)];
    
    % X axis labels
    xLabels = cell(1,length(nTrials));
    for i=1:length(nTrials)
        xLabels{i} = strcat(labels{i},' (',int2str(nTrials{i}),' trials)');
    end
    
    % Build bar chart
    fig = figure('visible','off');
    set(fig, 'units', 'inches', 'position', [5 5 14 10]);
    bar(y,0.3);
    
    % Draw baseline
    hold on
    baseline = double(1) / double(numClasses);
    plot(xlim,[baseline baseline], 'r','LineWidth',2)
    base = text([length(acc)+0.6],[baseline+0.15],'Baseline');
    set(base, 'rotation', -90);
    set(base, 'FontSize', 14);
    
    % Lay in accuracy texts and labels
    acctext = text(xlabelpos,ylabelpos,num2str(ylabelpos,'%0.2f'),...
        'HorizontalAlignment','center',...
        'VerticalAlignment','bottom');
    set(acctext, 'FontSize', 16);
    set(gca, 'XTickLabel', xLabels);
    set(gca, 'FontSize', 13);
    set(gcf, 'Color', 'w');
    xlabel('Datasets', 'FontSize',16);
    ylabel('Accuracy (%)','FontSize',16);
    output = strcat(output,'\');
    xticklabel_rotate([1:length(xLabels)],30,xLabels,'interpreter','none');
    export_fig(strcat(output,strjoin(labels,'-')),'-q101','-a4',fig);
    disp('done.');
end

% Returns true for boolRes iff the flag in found in args and the given
% condition for its existence is fulfilled. Output function is set to
% input function iff boolRes is met.
function [boolRes, args, func] = parseFlag(args, flag, valFunc)
    boolRes = false;
    func = @()true;
    if find(strcmp(flag, args)) boolRes = true; end
    args = args(~strcmp(args,flag));
    if boolRes func = valFunc; end
end
