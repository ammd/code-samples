%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HMM_Synthetic
%
% Function repository for a synthetic HMM classification framework. In this
% framework, the training data is split across the annotated class, and one
% HMM is trained per class. For previously unseen data, the max log
% likelihood is used to determine which models interprets it best, thus
% assigning it to its class. This corresponds to the standard approach in
% the literature.
%
% The given functions process data within the standard preprocessing,
% training, validating, testing, and postprocessing steps in a standard
% classification framework in terms of their general structure, not their
% contents, i.e. this repository makes no assumptions as to whether the
% data are discrete or continuous, or as to their dimensionality.
%
% In this respect, each respective method takes (as the first parameter)
% an input function which computes the actual business logic of the model.
%%
function [p,tr,v,ts,ps] = HMMSynthetic(debug, states, balanceTS)

% Function handles
p = @preprocess;
tr = @train;
v = @validate;
ts = @test;
ps = @postprocess;

% Global variables
dataStats = struct;
[~,balanceTraining] = HMMUtil();

% Preprocess the data. Check if the general structure of the data is
% consistent, then pass further handling to the concrete extension type.
function [trainData, valData, testData, nClasses] = ...
          preprocess(preFunc, tr, val, ts)
    
    if debug fprintf('HMMSynthetic:preprocess...\n'); end
    
    % Classes present in validation set must be a subset of the
    % classes present in the training set.
    trainClasses = unique(tr(:,1));
    nClasses = length(trainClasses);
    if (~isempty(val) && ~all(ismember(unique(val(:,2)), trainClasses)))
    
        error('HMMSynthetic:preprocess', ...
              'Validation set classes not subsets of training classes.');
    end
    
    % Each sequence in the validation and test sets should contain
    % not more than one class / sampleID
    [~,valClasses] = groupData1D(val,1,[2:2]);
    for i=1:length(valClasses)
        if length(unique(valClasses{i})) ~= 1
            error('HMMSynthetic:preprocess', ...
              'Validation set does not have single class per sequence');
        end
    end
    
    [~,testClasses] = groupData1D(ts,1,[2:2]);
    for i=1:length(testClasses)
        if length(unique(testClasses{i})) ~= 1
            error('HMMSynthetic:preprocess', ...
              'Test set does not have single class per sequence');
        end
    end
    
    % Balance the training set if required
    if balanceTS [tr] = balanceTraining(tr); end
    
    [trainData, valData, testData] = preFunc(tr, val, ts);
    
    % Compute global stats over all data pertaining to its structure:
    % Compute transition count - within each group, use the total sequence
    % length to deduce an estimate (average) of the number of transitions
    % from each state.
    allData = [trainData(:,1:end); valData(:,2:end)];
    allDataGroups = unique(allData(:,1));
    dataStats=struct('avgSeqLength',length(allData)/length(allDataGroups));
end

% Train a synthetic HMM with the given emission training data.
% Assumes emissions is a time series matrix, with time progressing with
% each row. Format should be as follows: [class, emissions].
% Returns an array of the synthetic HMM models (one per class) specific 
% to the HMM extension train method given by trainFunc
function [models] = train(trainFunc, emissions)
    
    if debug fprintf('HMMSynthetic:train...\n'); end
    
    % Group the training data according to its class. Result is a cell
    % array of matrices, each as [class, emissions]
    [C,~,~] = unique(emissions(:,1));
    E = cell(1, length(C));
    for i=1:length(C) E{i} = emissions(emissions(:,1)==C(i),:); end
    [models] = cellfun(@(x) trainFunc(x,states,dataStats),E,'un',0);
end


% Validate a set of models against a time series given by valData.
% models is a cell array containing HMM extension specific data structures,
% as given by the HMM extension used for the training step.
% valData is assumed to be a matrix with the following format:
% [sequence ID, classID, emissions], organized as a time series where
% time progresses with the row count. The classID must also be represented
% in the training set, although this is checked on preprocessing. Each
% sequence specified by a sequenceID must have at most one class, this 
% is also checked during preprocessing.
%
% Returns the actual classifications comparisons, the classification
% accuracy, and the total error cost of the classification. This is a
% useful metric where the classes themselves are not entirely categorical,
% but have some sort of internal correspondence to each other. The error
% cost penalizes classifications that are farther away from the ground
% truth more harshly.
function [valRes,valAcc,valErrorCost] = validate(valFunc, valData, models)
    
    if debug fprintf('HMMSynthetic:validate...\n'); end

    % Group validation file by the seqID. As a prerequisite, there should
    % only be one class represented within each sequence (checked during
    % preprocessing)
    [~,data] = groupData1D(valData,1,[2:size(valData,2)]);
    [annot,V]=cellfun(@(x)groupData1D(x,1,[2:size(x,2)]),data,'un',0);

    % Collect the individual sequences, run the business logic validation
    % function on each, returns a classification label in a cell array
    [classes,~] = arrayfun(@(x)valFunc(x{:}, models),[V{:}],'un',0);
    
    % Compare the annotations on file with the predicted labels, compute
    % classification accuracy and error cost.
    valRes = [ cell2mat([annot{:}]'), cell2mat(classes)'];
    valAcc = mean(double(valRes(:,1)==valRes(:,2)));
    valErrorCost = sum((valRes(:,1)-valRes(:,2)).^2) / (length(valRes)*...
                  ((max(max(valRes))-min(min(valRes))).^2));
end



% Test a set of models against a time series given by testData.
% models is a cell array containing extension specific data structures, as
% given by the HMM extension used during the training step.
% testData is assumed to be a matrix with the following format:
% [sequence ID, sampleID, emissions], organized as a time series where
% time progresses with the row count. The sequence ID is used to generate
% test sequences. There should be exactly one class present within each
% test sequence (this is checked during preprocessing). The sampleID refers
% to the ID of the sample that is to be classified. Note: a sample ID may
% appear under multiple sequence IDs. Therefore, in order to classify the
% sample it is necessary to first group testData along the seqID, classify
% the sample within, and then find the most common classification for that
% sample across all sequences. The function returns a mapping (as a matrix
% with two columns) from sampleID to a class, as defined within each model.
function [testOutput] = test(testFunc, testData, models)
    
    if debug fprintf('HMMSynthetic:test...\n'); end           
             
    % Group test file by the seqID. As a prerequisite, there should
    % only be one sampleID represented within each sequence (checked during
    % preprocessing).
    [~,data] = groupData1D(testData,1,[2:size(testData,2)]);
    [sampleID,T]=cellfun(@(x)groupData1D(x,1,[2:size(x,2)]),data,'un',0);

    % Collect the individual sequences, run the business logic test
    % function on each, returns a classification label in a cell array
    [classes,~] = arrayfun(@(x)testFunc(x{:}, models),[T{:}],'un',0);
    
    % For each sampleID, compute the most common assigned class, take this
    % as the final class assigned to that sample.
    testRes = [ cell2mat([sampleID{:}]') cell2mat(classes)'];
    testOutput = grpstats(testRes, testRes(:,1),@(x)mode(x)');
end

% Postprocess the data. From an architectural point of view, there is
% nothing to be done here, so simply send it to the HMM extension for
% postprocessing.
function [valRes, testRes] = postprocess(postFunc, valRes, testRes)
    
    if debug fprintf('HMMSynthetic:postprocess...\n'); end
    [valRes, testRes] = postFunc(valRes, testRes);
end


%% Private Functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Group the data in the given data matrix along the values in the 
% #groupIndex column. dataRange is an array specifying the indices from
% which the rest of the data should be collected.
% Method returns two cell arrays, one with the grouped values and one with
% the corresponding data.
function [groupedAtt, groupedData] = groupData1D(data,groupIndex,dataRange)
    [groupedAtt, groupedData] = deal({});
    if isempty(data) return; end
    [C,~,~] = unique(data(:,groupIndex));
    groupedAtt = cell(1, length(C));
    groupedData = cell(1, length(C));
    for i=1:length(C)
        filtered = data(data(:,groupIndex)==C(i),:);
        groupedAtt{i} = unique(filtered(:,groupIndex));
        groupedData{i} = filtered(:,dataRange);
    end
end

end