%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HMM_DiscreteCluster
%
%%
function [p, tr, v, ts, ps] = HMMDiscreteCluster(debug,gmm,km)

% This is a direct extension of the basic HMM discrete business logic model
[basePre, baseTrain, baseVal, baseTest, basePost] = HMMDiscrete(debug);

% Function handles
p = @preprocess;
tr = baseTrain;
v = baseVal;
ts = baseTest;
ps = basePost;

%% Public functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Preprocess the data according to the discrete scheme, after clustering
% the input features and taking the cluster centroid ids as the new
% discrete feature. The input data sets may contain numeric data of any
% length or type, see HMMDiscrete for the general format.
function [mEm, mVal, mTest] = preprocess(emissions, valData, testData)
  
  % Intuition: cluster the input data into however many classes the
  % training data says there are total
  nClz = length(unique(emissions(:,1)));
  ds = {emissions(:,[2:end]), ...
        valData(:,  [3:end]), ...
        testData(:, [3:end])};
    
  % Clustering option: Replace emissions vector with single emission in
  % each dataset. Derive number of clusters from emission data directly
  if (km)
      [ds] = cellfun(@(x)kMeansCluster(nClz,5,x),ds,'un',0); 
  elseif (gmm) 
      [ds] = gmmCluster(nClz,ds{1}, ds); 
  end
  
  emissions = [emissions(:,1)             ds{1}];
  valData =   [valData(:,1:min(end, 2))   ds{2}];
  testData =  [testData(:,1:min(end, 2))  ds{3}];
    
  [mEm, mVal, mTest] = basePre(emissions,valData,testData);
end


%% Private functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
% Cluster the given dataset in data (matrix, where each row
% is interpreted as a feature vector) using the given number of clusters,
% for the given number of iterations. For the given dataset return
% a column vector containing the cluster ID of every feature vector.
function [clusters] = kMeansCluster(numClusters, iterations, data)
    if isempty(data) clusters = [];
    else clusters = getKMeansID(numClusters,iterations,data); end
end

% Use fitData to build a Gaussian Mixture Model with the specified number
% of clusters. Return, for each dataset in data (cell array of matrices
% where each row is interpreted as a feature vector), a column vector
% containing the cluster ID of every feature vector (in a cell array)
function [clusters] = gmmCluster(numClusters, fitData, data)
    gmmObj = fitgmdist(fitData,numClusters,'Regularize',0.01);
    [clusters] = cellfun(@(x)clusterGMM(gmmObj, x),data,'un',0);
end

% K means clustering is sensitive to the order of the input data,
% so randomize the order, compute the cluster centers, then run
% again using the computed centers as input.
function [IDX] = getKMeansID(numClusters, iterations, data)

    parpool;
    opts = statset('UseParallel', true); % requires Parallelization toolkit
    EM_CLUSTERS = ones(numClusters,size(data,2),iterations);
    for i=1:iterations
        
        % Shuffle input rows, cluster to exactly #numClusters many clusters
        % keeping any stray or underpopulated clusters
        shuffled = data(randperm(size(data,1)),:);
        [~,C]= kmeans(shuffled,numClusters,'emptyaction','singleton',...
                      'options',opts,'start','sample');
        EM_CLUSTERS(:,:,i) = C;
    end
           
    % Take cluster centroid means across all iterations
    [IDX]= kmeans(data, numClusters,'emptyaction','singleton',...
              'options',opts,'start',mean(EM_CLUSTERS,3));                  
    delete(gcp);
end

% For a given GMM object (as returned to e.g. by fitgmdist), cluster
% the data given in the data matrix, where each row is considered a feature
% vector. Returns a column vector with the cluster id of each input vector.
function [GMMcluster] = clusterGMM(gmmObj, data)
    if isempty(data) GMMcluster = [];
    else GMMcluster = cluster(gmmObj, data); end
end

end

