%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HMMTestSuite
%
% Sample usage:
%
%
%% Options: ---------------------------------------------------------------
%
% -confusionmat: 
%           For every training/validation dataset pairs, print
%           a confusion matrix of the output classification. This is an nxn
%           square matrix, where n is the number of classes, giving the
%           relationship between actual class and predicted class in
%           number of classified samples.
% -accuracy: 
%           Produce a bar chart of classification accuracies across all
%           training/validation dataset pairs.
% -debug:    
%           Print program internal state at certain control junctures,
%           for development within MATLAB.
%
%% HMM Options : ----------------------------------------------------------
%
% -global: 
%           Classification framework model. Build a single HMM instead of 
%           one per class, use posterior prob. to choose the winning class.
% -synthetic: 
%           Classification framework model. Build an HMM for each class, 
%           use maximum likelihood of input sequence to choose the winning 
%           model/class.
% -discrete: 
%           HMM business model. Input emissions are single discrete 
%           observations
% -discreteCluster: 
%           HMM business model. Input emissions are single discrete 
%           observations corresponding to their cluster centroids wrt to a
%           specified clustering algorithm
% -continuous: 
%           HMM business model. Input emissions are not single observations
%           but a sequence, i.e. different continuous features. 
% -kmeans: 
%           Use kmeans clustering preprocessing to get discrete emissions.
% -gmm:    
%           Use a Gaussian Mixture Model (as preprocessing step) to get
%           discrete emissions. Requires data with some variance, which
%           should be the case for continuous features. Should this not be
%           the case, an error will be thrown.
% -balanceTS:    
%           Balance the training set so that all classes are equally
%           represented. The relative ordering of the features within each
%           class is respected.
%
%% Inputs -----------------------------------------------------------------
% Flags in [] mark with which options they must be provided:
%
% testSuite [] :
%           An input data source (csv file), composed of a series of 
%           training and validation pairs, each preceded by an arbitrary
%           label identifying that test case. Test cases are each separated
%           by a delimiter (#).
% output []: 
%           An output data sink, points to a directory on disk.
% encodings [-confusionmat]:
%           A two column csv file mapping an integer output class 
%           (see HMMMain) to a class name.
%
%% HMM Inputs -------------------------------------------------------------
% states [-synthetic]: 
%           number of synthetic states used to model each HMM in the 
%           synthetic scheme.
% logfile []
%           output file to append to, log results and calls.
%%
function HMMTestSuite(varargin)

    vargs = varargin;
    parser = inputParser;

    % Handle input flags
    vF = {};
    flags = {};
    [confusionmat,vargs,vF{end+1},~]= parseFlag(vargs,...
        '-confusionmat',@()~isempty(parser.Results.encodings));
    [accuracy,vargs,vF{end+1},~]= parseFlag(vargs,...
        '-accuracy',@()true);
    
    % Subtask flags
    [debug,vargs,vF{end+1},flags{end+1}]= parseFlag(vargs,...
        '-debug',@()true);
    [globalModel,vargs,vF{end+1},flags{end+1}] = parseFlag(vargs,...
        '-global',@()true);
    [synthModel,vargs,vF{end+1},flags{end+1}]=parseFlag(vargs,...
        '-synthetic',@()true);
    [continuous,vargs, vF{end+1},flags{end+1}] = parseFlag(vargs,...
        '-continuous',@()true);
    [discrete,vargs, vF{end+1},flags{end+1}] = parseFlag(vargs,...
        '-discrete',@()true);
    [discreteCluster,vargs,vF{end+1},flags{end+1}] = parseFlag(vargs, ...
        '-discreteCluster',@()true);
    [kmeans,vargs, vF{end+1},flags{end+1}] = parseFlag(vargs,...
        '-kmeans',@()true);
    [gmm,vargs, vF{end+1},flags{end+1}] = parseFlag(vargs,...
        '-gmm',@()true);
    [balanceTS,vargs, vF{end+1},flags{end+1}] = parseFlag(vargs,...
        '-balanceTS',@()true);
    
    % Remove "empty" flags
    flags(cellfun(@isempty,flags)) = [];
    
    % Specify optional arguments
    addParameter(parser,'testSuite','',@(x)true);
    addParameter(parser,'output','./',@(x)true);
    addParameter(parser,'encodings','',@(x)confusionmat);
    
    % Subtask options
    subtaskArg = {'logfile', 'states'};
    cellfun(@(x)addParameter(parser,x,'',@(x)true),subtaskArg,'un',0);
    
    % Validate arguments specific only to the visualizer. The latter has
    % no responsibility to check the consistency of everything else.
    parse(parser,vargs{:});
    for i=1:length(vF)
      if (~vF{i}()) 
          error('HMMVisualizer:flagCheck', ...
          strcat('The following condition is not met: ',func2str(vF{i})));
      end
    end
    if (debug) parser.Results
    end
    
    % Get state variables
    [enc] = parseNonNumericCSV(parser.Results.encodings, '%d %s');
    [vizConf,vizAcc] = HMMVisualizer();
    
    % Get data
    [acc,classes,nClasses,nTrials] = deal({});
    input = parseNonNumericCSV(parser.Results.testSuite,'%s');
    [groups,cases] = splitStringCell(input{1},'#');
    sizes = cellfun(@(x)length(x),groups,'un',0);
    if sum([sizes{:}] ~= 3) > 0
        error('HMMVisualizer:processInput',...
        strcat('One or more input test cases are not valid'));
    end
    
    % Run training + validation for each group
    for i=1:length(groups)
        
       % Build function argument list
       opts = cell(1,2*length(subtaskArg));
       for j=1:length(subtaskArg)
           opts{2*j-1} = subtaskArg{j};
           opts{2*j} = parser.Results.(subtaskArg{j});
       end
       
       % Get current testcase
       testcase = {'trainfile',cases{2}{i},'valfile',cases{3}{i}};
       args =  {flags{:},opts{:},testcase{:}};
       
       % Run training and validation
       [acc{end+1},~,classes{end+1},nClasses{end+1},nTrials{end+1}] = ...    
       HMMMain(args{:});
    end

    % Produce visualizations
    if accuracy
        vizAcc(acc,cases{1},nTrials,nClasses,parser.Results.output); 
    end
    if confusionmat 
        vizConf(classes,cases{1},enc,parser.Results.output);
    end 
end

% Returns true for boolRes iff the flag in found in args and the given
% condition for its existence is fulfilled. Output function is set to
% input function iff boolRes is met, same for oFlag.
function [boolRes, args, func, oFlag] = parseFlag(args, flag, valFunc)
    boolRes = false;
    func = @()true;
    if find(strcmp(flag, args)) boolRes = true; end
    args = args(~strcmp(args,flag));
    oFlag = '';
    if boolRes 
       func = valFunc; 
       oFlag = flag;
    end
end

% Wrapper utility for reading non numeric CSVs. If empty input return
% empty matrix.
function [res] = parseNonNumericCSV(file, format)
     if isempty(file) res = []; else
     fileId = fopen(file);
     res = textscan(fileId, format, 'delimiter', ',');
     fclose(fileId);
     end
end

% Split a cell array of strings along the given delimiter, returns a cell
% array containing each block (cell array) split both along the delimiter
% (perGroup) or across each group according to the delimiter (perField).
function [perGroup, perField] = splitStringCell(inputCell,delim)

    % Switch to rolled out char array, find how many times the delimiter
    % appears. This determines how many blocks of information there are
    aschar = char(inputCell);
    nblocks = sum(aschar(:) == delim) + 1;
    
    % Split according to group and according to individual entries across
    % all groups
    perGroup = cell(1,nblocks);
    perField = cell(1, length(inputCell)); % worst case
    currentBlock = 1;
    fieldCounter = 1;
    for i=1:length(inputCell)
        if inputCell{i} ~= delim
            perGroup{currentBlock}{end+1}=inputCell{i};
            perField{fieldCounter}{end+1}=inputCell{i};
            fieldCounter = fieldCounter +1;
        else
            currentBlock = currentBlock+1;
            fieldCounter = 1;
        end
    end
    
    % Remove empty cells
    perField(cellfun(@isempty,perField)) = [];
end

