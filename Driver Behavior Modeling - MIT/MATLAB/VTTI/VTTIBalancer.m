%% VTTIBalancer
% Takes an input training or test data set and balances the classes found
% within, so that all classes are equally represented. Samples from classes
% that are over-represesnted are randomly discarded until the class
% distribution is equal.
%
% Returns the balanced data set as a matrix.
%
% VTTIBalancer('-debug','-train','dataSource',...
%              'C:\Users\ammd\Desktop\test.csv')
%
%
%% Options ---------------------------------------------------------------
% -train:    
%           Input data is in training format
% -test:    
%           Input data is in test format
% -debug: 
%           Print debugging messages
%
%% Inputs ----------------------------------------------------------------
% Flags in [] mark with which options they must be provided:
%
% dataSource [required]:
%           Path to input file, or matrix with data
%
function [outData] = VTTIBalancer(varargin)

format longG;
parser = inputParser;

% Handle input flags. Anonymous function corresponds to validation
% function, returns true iff the use of the flag is valid.
vF = {};
[debug, varargin, vF{end+1}] = parseFlag(varargin,'-debug',@()true);
[train, varargin, vF{end+1}] = parseFlag(varargin,'-train',@()true);
[test, varargin, vF{end+1}] = parseFlag(varargin,'-test',@()true);

% Global validations
vF{end+1} = @() train || test;
vF{end+1} = @() (train & ~test) || (test & ~train);

% Value parameters
addParameter(parser,'dataSource','',@(x)true);

% Validate inputs
parser.parse(varargin{:})
for i=1:length(vF)
    if (~vF{i}()) 
        error('VTTIBalancer:flagCheck', ...
        strcat('The following condition is not met: ',func2str(vF{i})));
    end
end

% Get data
dataSource = parser.Results.dataSource;
if ischar(dataSource) [data] = parseMatrix(dataSource);
else data = dataSource;
end

% Choose data format
if train [clz, gData]=groupData1D(data,1,[2:size(data,2)]);
elseif test [clz, gData] = groupData1D(data,2,[1, 3:size(data,2)]);
end 

% Balance data
sizes = cellfun(@(x)size(x,1),gData,'un',0);
[minVal, minIdx] = min(cell2mat(sizes));
outData = [];
for i=1:length(gData)
    if i~=minIdx
        k = randperm(size(gData{i},1));
        gData{i} = gData{i}(k(1:minVal),:);
    end
end      

% Choose output format
for i=1:length(gData)
    if train 
        outData = [outData;repmat(clz{i},size(gData{i},1),1), gData{i}];  
    
    elseif test 
        outData = [outData; gData{i}(:,1), ...
                       repmat(clz{i},size(gData{i},1),1), ...
                       gData{i}(:,2:end)];
    end
end

if debug
    disp('New block sizes: ');
    gData
end
end

% Read in data matrix
function [res] = parseMatrix(file)
    try
        res = csvread(file,0,0); 
    catch
        try
            res = csvread(file,1,0);
        catch
            res = [];
        end
    end
end

% Group matrix by column value
function [groupedAtt, groupedData] = groupData1D(data,groupIndex,dataRange)
    [groupedAtt, groupedData] = deal({});
    if isempty(data) return; end
    [C,~,~] = unique(data(:,groupIndex));
    groupedAtt = cell(1, length(C));
    groupedData = cell(1, length(C));
    for i=1:length(C)
        filtered = data(data(:,groupIndex)==C(i),:);
        groupedAtt{i} = unique(filtered(:,groupIndex));
        groupedData{i} = filtered(:,dataRange);
    end
end

% Returns true for boolRes iff the flag in found in args and the given
% condition for its existence is fulfilled. Output function is set to
% input function iff boolRes is met.
function [boolRes, args, func] = parseFlag(args, flag, valFunc)
    boolRes = false;
    func = @()true;
    if find(strcmp(flag, args)) boolRes = true; end
    args = args(~strcmp(args,flag));
    if boolRes func = valFunc; end
end
