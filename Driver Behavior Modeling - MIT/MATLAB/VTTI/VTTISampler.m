% varargin{1} - path to input file, training format
% varargin{2} - number of iterations
% varargin{3} - output directory for test / train files
% varargin{4} - name of validation file that pairs with original train file
function [] = VTTISampler(varargin)

% state variables
[data] = parseMatrix(varargin{1});
maxres = varargin{2};
out = varargin{3};
val = varargin{4};

% output train / test manifest + files
manifest = fopen(strcat(out,'\samplerManifest.txt'), 'a+');

% For each class in the training data, pick random examples to leave out.
% How many examples are left out is controlled by the ratio res / maxres
[clz, gData] = groupData1D(data,1,[2:size(data,2)]);

for res=0:maxres
    outdata = [];
    for i=1:length(clz)
        block = gData{i};

        % 10% of data should always be left
        if res / maxres < 0.9 
            numRemove = round((res / maxres) * size(block,1));
        else
            numRemove = round(0.9*size(block,1));
        end
        numRemove
        k = randperm(size(block,1));
        block(k(1:numRemove),:) = [];
        outdata = [outdata; repmat(clz{i},size(block,1),1), block];
    end
    
    format longG;
    samplefile = strcat(out,'\sample-',int2str(res),'.csv');
    dlmwrite(samplefile,outdata,'precision',10);
    fprintf(manifest, strcat('sample-',int2str(res),'.csv\n'));
    fprintf(manifest, val);
end


end

function [groupedAtt, groupedData] = groupData1D(data,groupIndex,dataRange)
    [groupedAtt, groupedData] = deal({});
    if isempty(data) return; end
    [C,~,~] = unique(data(:,groupIndex));
    groupedAtt = cell(1, length(C));
    groupedData = cell(1, length(C));
    for i=1:length(C)
        filtered = data(data(:,groupIndex)==C(i),:);
        groupedAtt{i} = unique(filtered(:,groupIndex));
        groupedData{i} = filtered(:,dataRange);
    end
end


% Read in data matrix
function [res] = parseMatrix(file)
    try
        res = csvread(file,0,0); 
    catch
        try
            res = csvread(file,1,0);
        catch
            res = [];
        end
    end
end