%% VTTIDAO
% Iterate over randomized partitions across the VTTI subject pool and 
% collect train and test data set pairs, as well as compute certain 
% statistics over each iteration.
%
% Returns:
% t - function that returns the training data set for a subject split
% m - function that performs monte carlo estimation on principal component
%     loading and variance matrices by iterating through random subject
%     splits for a specified number of iterations.
%
function [d, m] = VTTIDAO(varargin)

% Function handles
d = @getdata;
m = @montecarlo;

end

% Returns true for boolRes iff the flag in found in args and the given
% condition for its existence is fulfilled. Output function is set to
% input function iff boolRes is met.
function [boolRes, args, func] = parseFlag(args, flag, valFunc)
    boolRes = false;
    func = @()true;
    if find(strcmp(flag, args)) boolRes = true; end
    args = args(~strcmp(args,flag));
    if boolRes func = valFunc; end
end

function [conn] = odbcconnect()
    
    %Set preferences with setdbprefs.
    setdbprefs('DataReturnFormat', 'cellarray');
    setdbprefs('NullNumberRead', 'NaN');
    setdbprefs('NullStringRead', 'null');

    %Make connection to database.  Note that the password has been omitted.
    %Using ODBC driver.
    conn = database('PostgreSQL30', 'postgres', '');
    format longG;
end

% Data retrieval and preprocessing. Get a fresh data split, along with balanced version
function [traindata, testdata, alldata, trainbal, testbal, subs] = getdata(usepca, normalize)
    
    conn = odbcconnect();
    
    % Get fresh splits
    [alldata, allbal] = deal([]);
    curs = exec(conn, ['SELECT vtti.splitdata();']);
    curs = fetch(curs);
    close(curs);
    
    % Dynamic training subjects
    curs = exec(conn, ['SELECT * FROM vtti.dynamicsubs']);
    curs = fetch(curs);
    subs = curs.Data;
    close(curs);
    
    % Dynamic training set
    curs = exec(conn, ['SELECT trialid, eyeglancelocation, filteredrotationx, filteredrotationy, filteredrotationz FROM vtti.dynamictrain ORDER BY trialid, epochid, timestamp ASC']);
    curs = fetch(curs);
    retMat = cell2mat(curs.Data);
    
    % Preprocessing dynamic training set: normalize -> pca
    traindata = retMat;
    if normalize traindata = normalizeByGroup(traindata,1,[3:size(traindata,2)]); end
    if usepca traindata = [traindata(:,1:2) pca(traindata(:,3:end))]; end
    traindata = traindata(:,2:end);
    
    % All data - training
    alldata = [alldata; traindata];
    close(curs);
    
    % Dynamic training set - balanced
    trainbal = VTTIBalancer('-train','dataSource',traindata);
    
    % Dynamic test set
    curs = exec(conn, ['SELECT trialid, seqid, eyeglancelocation, filteredrotationx, filteredrotationy, filteredrotationz FROM vtti.dynamicval ORDER BY trialid, epochid, timestamp ASC']);
    curs = fetch(curs);
    retMat = cell2mat(curs.Data);
    
    % Preprocessing dynamic test set: normalize -> pca
    testdata = retMat;
    if normalize testdata = normalizeByGroup(testdata,1,[4:size(testdata,2)]); end
    if usepca testdata = [testdata(:,1:3) pca(testdata(:,4:end))]; end
    testdata = testdata(:,2:end);
    
    % All data - testing
    alldata = [alldata; testdata(:,2:end)];
    close(curs);

    % Dynamic test set - balanced
    testbal = VTTIBalancer('-test','dataSource',testdata);
    
    % Postprocess all data
    if usepca alldata = pcascores(alldata,1); end
    close(conn);
end

% Iterate through random subject splits, and print train/test pairs to file
% and/or compute statistics.
% iter - number of iterations
% usepca - use pca to get reduce data to scores
% subnorm - normalize using zscore per subject / trial
% print - write resulting train and test sets to file
% out - folder to put results in (must exist)
function [loadings, expl] = montecarlo(iter, usepca, subnorm, print, out)

    format longG;
        
    % Accumulated results over all iterations
    % Principal components loadings average, principal components variance avg.
    [loadings, expl] = deal([]);

    % Manifest outputs
    dynamicManifest = fopen(strcat(out,'\dynamicManifest.txt'), 'a+');
    dynamicManifestBal = fopen(strcat(out,'\dynamicManifestBal.txt'),'a+');


    for i=1:iter
    
       [train, test, ~, trainbal, testbal, subs] = getdata(usepca, subnorm);
        
        dynamicsubsfile = strcat(out,'\dynamicsubs-',int2str(i),'.csv');
        dynamictrainfile = strcat(out,'\dynamictrain-',int2str(i),'.csv');
        dynTrainBalanced = strcat(baseFromPath(dynamictrainfile),'-balanced.csv');
        dynamictestfile = strcat(out,'\dynamictest-',int2str(i),'.csv');
        dynTestBalanced = strcat(baseFromPath(dynamictestfile),'-balanced.csv');

        % Print intermediate files
        if print
            dlmwrite(dynamicsubsfile,subs,'precision',10);
            
            dlmwrite(dynamictrainfile,train,'precision',10);
            fprintf(dynamicManifest, strcat('dynamictrain-',...
                                     int2str(i),'.csv\n'));
            dlmwrite(dynTrainBalanced,trainbal,'precision',10);
            fprintf(dynamicManifestBal, strcat('dynamictrain-',...
                                     int2str(i),'-balanced.csv\n'));
            dlmwrite(dynamictestfile,test,'precision',10);
            fprintf(dynamicManifest, strcat('dynamictest-',...
                                     int2str(i),'.csv\n'));
            dlmwrite(dynTestBalanced,testbal,'precision',10);
            fprintf(dynamicManifestBal, strcat('dynamictest-',...
                                     int2str(i),'-balanced.csv\n'));
        end
    
    
        % Use training set to compute principal component loadings for this
        % iteration, then average over all other iterations.
        [coeff,~,~,~,explained] = pca(train(:,2:end));
  
        % Alternate way of computing loadings
        %covMat=cov(matdata(:,2:size(matdata,2)));
        %[V,D]=eigs(covMat,size(covMat,2),'lm');
       
        if isempty(loadings) 
            loadings = zeros(size(coeff));
            expl = zeros(size(explained));
        end
        
        loadings = loadings + coeff;
        expl = expl + explained;
    
    end % End iterations

    % Compute averaged stats over all non NAN rows
    loadings = loadings ./ iter;
    expl = expl ./ iter;
end

% For the input matrix, uses the col column to build groups and normalize
% values for the specified range within each group. Returns identical 
% matrix, but with normalized values in place of the input ones.
function [out] = normalizeByGroup(data, col, range)
    [gCol, gData] = groupData1D(data, col, range);
    [normalized] = cellfun(@(x)zscore(x),gData,'un',0);
    nMat = zeros(size(data,1), length(range));
    insertCounter = 1;
    for i=1:length(normalized)
        ndata = normalized{i};
        nMat(insertCounter:insertCounter+size(ndata,1)-1,:) = ndata;
        insertCounter = insertCounter + size(ndata,1);
    end
    out = data;
    out(:,range) = nMat;
end

% Group data by an attribute (variable)
function [groupedAtt, groupedData] = groupData1D(data,groupIndex,dataRange)
    [groupedAtt, groupedData] = deal({});
    if isempty(data) return; end
    [C,~,~] = unique(data(:,groupIndex));
    groupedAtt = cell(1, length(C));
    groupedData = cell(1, length(C));
    for i=1:length(C)
        filtered = data(data(:,groupIndex)==C(i),:);
        groupedAtt{i} = unique(filtered(:,groupIndex));
        groupedData{i} = filtered(:,dataRange);
    end
end

% Extract last name from path, excluding file type
function [fname] = baseFromPath(path)
    delims = strfind(path, '.');
    last = delims(length(delims));
    fname = path(1:last-1);
end
