%% VTTIVisualizer
% Visualizes VTTI training data by pulling sample splits from VTTIDAO.
%
% VTTIVisualizer('-zscore','-pcascores','-princomp','iter',100,...
%                'varnames',{'X Rotation', 'Y Rotation', 'Z Rotation'})
%
%
%% Options ---------------------------------------------------------------
% -zscore:    
%           Normalize current split using zscore.
% -subnorm:    
%           Normalize data per subject (trial).
% -pcascores:    
%           Use PCA to decompose and re-align current split to most 
%           salient patterns.
% -princomp: 
%           For a specified number of iterations, compute new data split,
%           compute principal component loadings, and then average over
%           all iterations.
%
%% Inputs ----------------------------------------------------------------
% Flags in [] mark with which options they must be provided:
%
% iter [-princomp]:
%           Number of iterations over which to average the computation of
%           principal component loadings.
% varnames [-princomp]:
%           Names of variables as a cell array of chars.
%%
function [] = VTTIVisualizer(varargin)

format longG;
parser = inputParser;

% Handle input flags. Anonymous function corresponds to validation
% function, returns true iff the use of the flag is valid.
vF = {};
[zscores, varargin, vF{end+1}] =parseFlag(varargin,'-zscore',@()true);
[subnorm, varargin, vF{end+1}] =parseFlag(varargin,'-subnorm',@()true);
[pcascores, varargin, vF{end+1}] =parseFlag(varargin,'-pcascores',@()true);
[princomp, varargin, vF{end+1}] =parseFlag(varargin,'-princomp',@()true);

% Value parameters
addParameter(parser,'iter','',@(x)x>0);
addParameter(parser,'varnames','',@(x)~isempty(x));

% Validate inputs
parser.parse(varargin{:})
for i=1:length(vF)
    if (~vF{i}()) 
        error('VTTIVisualizer:flagCheck', ...
        strcat('The following condition is not met: ',func2str(vF{i})));
    end
end

% Get data
iter = parser.Results.iter;
header = parser.Results.varnames;
[getdata,~] = VTTIDAO();
[data,~,alldata,~,~,~] = getdata(pcascores, subnorm); % Sample subject split

% Normalize features
if zscores 
    data(:,2:end) = zscore(data(:,2:end)); 
    alldata(:,2:end) = zscore(alldata(:,2:end)); 
end

% Average principal component loadings over a set number of iterations
if princomp pcloadings(iter, header); end

% Visualize data
fig = figure;
if pcascores alldata = alldata(:,1:3); end % visualize in 2 dimensions
[clz, gData] = groupData1D(alldata,1,[2:size(alldata,2)]);
set(fig, 'units', 'inches', 'position', [0 0 15 15]);

% add options for visualizing 3D data e.g. number of classes
if size(gData{1},2) == 3 
    scatter3(gData{1}(:,1), gData{1}(:,2), gData{1}(:,3),25,'r','filled','MarkerEdgeColor','k');
    %scatter3(gData{1}(:,1), gData{1}(:,2), gData{1}(:,3),3,'+','r','LineWidth',0.1);
    hold on;
    scatter3(gData{2}(:,1), gData{2}(:,2), gData{2}(:,3),3,'+','b','LineWidth',0.1);
    hold off;
    
% add options for visualizing 2D data e.g. number of classes
elseif size(gData{1},2) == 2
    scatter(gData{1}(:,1), gData{1}(:,2),25,'r','filled','MarkerEdgeColor','k');
    hold on;
    scatter(gData{2}(:,1), gData{2}(:,2),3,'+','b','LineWidth',0.1);
    hold off;
end
set(gca, 'FontSize', 28);

if pcascores
    xlabel('PC1');
    ylabel('PC2');
    zlabel('PC3');
else 
    xlabel(header{1});
    ylabel(header{2});
    zlabel(header{3});
end
end

% Group data across a column
function [groupedAtt, groupedData] = groupData1D(data,groupIndex,dataRange)
    [groupedAtt, groupedData] = deal({});
    if isempty(data) return; end
    [C,~,~] = unique(data(:,groupIndex));
    groupedAtt = cell(1, length(C));
    groupedData = cell(1, length(C));
    for i=1:length(C)
        filtered = data(data(:,groupIndex)==C(i),:);
        groupedAtt{i} = unique(filtered(:,groupIndex));
        groupedData{i} = filtered(:,dataRange);
    end
end

% Visualize average principal component loadings for the specified
% number of iterations. Data retrieved from VTTIDAO
function [] = pcloadings(iter, header)
    fig = figure;
    colormap(parula);
    
    % Get averaged results from VTTIDAO
    [~, montecarlo] = VTTIDAO();
    [V,D] = montecarlo(iter,0,0,0,'');
    
    imagesc(V,[-1,1]);
    textS = num2str(V(:)); % original values on top
    textS = strtrim(cellstr(textS));
    [x,y] = meshgrid(1:size(V,1));
    hStrings = text(x(:),y(:),textS(:),'HorizontalAlignment','center');
    textColors = repmat(V(:) < 0.6,1,3);
    set(hStrings,{'Color'},num2cell(textColors,2));
    set(hStrings,'FontSize',12);
    set(gca,'YTick',[1:size(V,2)]);
    set(gca,'XTick',[1:size(V,2)]);
    set(gca, 'XTickLabel', {[1:size(V,2)]});
    set(gca, 'YTickLabel', header);
    set(gcf, 'Color', 'w');
    set(gca, 'FontSize', 14);
    set(fig, 'PaperPositionMode', 'auto');
    ylabel('Variable');
    xlabel('Principal component');
    colorbar;
end

    
% Set the font size of in-graphic text. The mapping from input to
% output size is essentially a gamma correction with a bottom of 8
function [newsize] = getTextSize(size, gamma)
    val = (25-size) / 25;
    if (val < 0) newsize = 8;
    else newsize = (val.^gamma)*40;
    end
end

% Returns true for boolRes iff the flag in found in args and the given
% condition for its existence is fulfilled. Output function is set to
% input function iff boolRes is met.
function [boolRes, args, func] = parseFlag(args, flag, valFunc)
    boolRes = false;
    func = @()true;
    if find(strcmp(flag, args)) boolRes = true; end
    args = args(~strcmp(args,flag));
    if boolRes func = valFunc; end
end


% % Computes and returns PCA scores and data reconstruction for ndim
% % dimensions for training data
% function [scores, rec] = pcascores(data, ndim)
%     
%     disp('Using PCA ...')
%     % automatically centers data, returns components in decreasing order
%     % of variance
%     [coeff,score,~] = pca(data(:,2:end)); 
%     
%     % recreated data to ndim dimensions
%     rec = score(:,1:ndim)*coeff(1:ndim,1:ndim)'; 
%     
%     % keep PCA decomposition or recreate to fewer dimensions
%     scores = [data(:,1) score(:,:)]; 
% end