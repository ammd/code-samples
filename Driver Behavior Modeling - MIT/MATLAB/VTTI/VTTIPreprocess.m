

% varargin{1} - Input matrix to parse (VTTI-[dynamic|static]-sync.csv)
% varargin{2} - Output dir
% varargin{3} - true for debug, false otherwise
function [data] = VTTIPreprocess(varargin)

format longG;
[data] = parseMatrix(varargin{1});
[canonicalAcc, canonicalConst, hzAcc, hzConst] = glanceDuration(data);
[data,replace] = preprocess(data, 200);
[cleanAcc,cleanConst,cleanHzAcc,cleanHzConst] = glanceDuration(data);

if varargin{3}
[[1:length(hzAcc)]', canonicalAcc(1:end,1), canonicalConst(1:end,1), ...
           hzAcc(1:end,1), hzConst(1:end,1)]
end


% build final result
data = [data, canonicalAcc, canonicalConst, hzAcc, hzConst, cleanAcc, ...
        cleanConst, cleanHzAcc, cleanHzConst];
dlmwrite(strcat(varargin{2},'\',nameFromPath(varargin{1}),...
         '-duration-merged.csv'),data,'precision',10);
dlmwrite(strcat(varargin{2},'\',nameFromPath(varargin{1}),...
         '-duration-replaced.csv'),replace);
end

% Extract last name from path, excluding file type
function [fname] = nameFromPath(path)
    delims = strfind(path, '.');
    delims1 = strfind(path, '\');
    last = delims(length(delims));
    last1 = delims1(length(delims1));
    fname = path(last1+1:last-1);
end

% Read in data matrix
function [res] = parseMatrix(file)
    try
        res = csvread(file,0,0); 
    catch
        try
            res = csvread(file,1,0);
        catch
            res = [];
        end
    end
end

% Compute glance duration statistics. Assumes input with schema
% [trialid, epochid, epochtype, timestamp, eyeglancelocation,
% rawrotationx, rawrotationy, rawrotationz, filteredrotationx,
% filteredrotationy, filteredrotationz]
function [glanceDurationAcc, glanceDurationConst,...
          glanceDurationAccHz, glanceDurationConstHz]= glanceDuration(data)

% New data to add
glanceDurationAcc = zeros(size(data,1),1);
glanceDurationConst = zeros(size(data,1),1);
glanceDurationAccHz = zeros(size(data,1),1);
glanceDurationConstHz = zeros(size(data,1),1);

% ACCUMULATING GLANCE DURATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

change = false; % subjects switches glances within a task
for i=1:length(data)-1
    
    % if the next tupel is in the same TRIAL and EPOCH group accumulate
    % result, otherwise nothing to do (epoch endings are repaired in a very 
    % last step postprocessing step, see below)
    if (data(i,1) == data(i+1,1) && data(i,2) == data(i+1,2))
        
        % accumulated result until now, gets set only if we are i > 1
        offset = 0;
        offsetHz = 0;
        
        % 5 is index for GLANCE location. If the glance location does not
        % change in this next step, then get the accumulated result and add
        % the new timestamp difference
        if data(i,5) == data(i+1,5) 
            
            % Get accumulated total. note this might be the last group's
            % accumulated total
            if (i > 1) 
                offset = glanceDurationAcc(i-1,1); 
                offsetHz = glanceDurationAccHz(i-1,1);
            end
            
            % if we recently changed into a new group, reset the group
            % accumulator and the new group flag.
            if change
                offset = 0;
                offsetHz = 0;
                change = false;
            end
            
            % Compute accumulated total again, 4 is index for TIMESTAMP
            glanceDurationAcc(i,1) =  offset + data(i+1,4)-data(i,4);
            glanceDurationAccHz(i,1) =  offsetHz + 67; % 67 ms = 15 Hz
            
            
        % subject moves within an epoch to a different eyeglance in the
        % next step
        else
            
            % if this change was preceded directly by another, reset the
            % accumulator
            if change
                offset = 0;
                offsetHz = 0;
                
            % otherwise the next annotation is a different eyeglance,
            % however for this annotation we still need to compute the
            % accumulated total
            elseif (i > 1) 
                offset = glanceDurationAcc(i-1,1);
                offsetHz = glanceDurationAccHz(i-1,1);
            end
            
            % get accumulated total
            glanceDurationAcc(i,1) =  offset + data(i+1,4)-data(i,4);
            glanceDurationAccHz(i,1) =  offsetHz + 67;
            
            % eyeglances were just switched
            change = true;
        end
    end
end

% Postprocessing: Fix epoch endings with artificial durations of 15Hz
% all glances other than epoch endings have a duration
epochEnd = glanceDurationAcc==0; 
ind = find(epochEnd); % gets indices of non null elements
glanceDurationAcc(ind,1) = glanceDurationAcc(ind-1,1) + 67;
glanceDurationAccHz(ind,1) = glanceDurationAccHz(ind-1,1) + 67;




% BLOCKWISE GLANCE DURATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

start = 1;  % moving marker, marks the beginning of each (trialid, epochid, 
            % eyeglance) block
finish = 1; % moving marker, marks the end of each (trialid, epochid, 
            %eyeglance) block
            
for i=1:length(data)-1
    
     % only advance end marker if we are in the same trial, the same epoch,
     % and the same eyeglance
     if (data(i,1) == data(i+1,1) && data(i,2) == data(i+1,2) && ...
         data(i,5) == data(i+1,5))
         finish = i+1;
     else
         
         glanceDurationConstHz(start:finish,1) = (finish-start+1)*67;
         
         % otherwise, if only the eyeglance has changed, use the next
         % available timestamp as an end point to the current data streak
         if (data(i,1) == data(i+1,1) && data(i,2) == data(i+1,2))
             
                glanceDurationConst(start:finish,1) = ...
                data(finish+1,4) - data(start,4);

         % otherwise the subject or epoch has changed, so just add an
         % arbitrary 15 Hz = 67ms at the epoch boundary
         else
            glanceDurationConst(start:finish,1) = ...
            data(finish,4) - data(start,4) + 67;
         end
         
         % the starting block pointer now becomes the next available point,
         start = finish+1;
         finish = start;
     end
end

% Handle final sequence
glanceDurationConst(start:finish,1) = data(finish,4) - data(start,4) + 67;
glanceDurationConstHz(start:finish,1) = (finish-start+1)*67;
end





% Quality control: Replace annotations blocks with total duration < limit 
% with the annotations in its context.
% Note: replacement only takes place if the block in question is surrounded
% by two other blocks that have the same eyeglance.
function [data, replace] = preprocess(data, limit)

    replace = [];

    start = 1;  % moving marker, marks the beginning of each (trialid,
                % epochid, eyeglance) block
    finish = 1; % moving marker, marks the end of each (trialid, epochid, 
                % eyeglance) block

    for i=1:length(data)-1

         % only advance end marker if we are in the same trial, the same
         % epoch, and the same eyeglance
         
         if (data(i,1) == data(i+1,1) && data(i,2) == data(i+1,2) && ...
             data(i,5) == data(i+1,5))
             finish = i+1; 
         end
         
         if (~(data(i,1) == data(i+1,1) && data(i,2) == data(i+1,2) && ...
             data(i,5) == data(i+1,5)) || finish == length(data))

             % otherwise either the eyeglance has changed, or we are at the
             % EOF. In the former case, use the next available timestamp as
             % an end to the current data streak. In the latter case, add
             % an additional 15 Hz to the group and then decide.
             if (data(i,1) == data(i+1,1) && data(i,2) == data(i+1,2))

                if finish+1 > length(data)
                    block = data(finish,4) - data(start,4) + 67;
                else
                    block = data(finish+1,4) - data(start,4);
                end
               
                if block < limit 
                     
                    % only take info from context if the context has the
                    % same epoch and subject on both sides
                    if start > 1 && ...
                       data(start-1,1) == data(start,1) && ...
                       data(start-1,2) == data(start,2) && ...
                       finish+1 <= length(data) && ...
                       data(finish+1,1) == data(finish,1) && ...
                       data(finish+1,2) == data(finish,2) && ...
                       data(start-1,5) == data(finish+1,5)
                  
                       replace = ...
                       [replace; start, finish, ...
                       unique(data(start:finish,5)), data(start-1,5)];
                   
                       data(start:finish, 5) = data(start-1,5);
                       
                    % otherwise throw away block
                    elseif(start > 1 && finish+1 <= length(data))
                        data(start:finish, :) = [];     
                    end
                end
             end

             % the starting block pointer now becomes the next point
             start = finish+1;
             finish = start;
         end
    end
    data( all(cellfun(@isempty,data),2), : ) = [];
end
