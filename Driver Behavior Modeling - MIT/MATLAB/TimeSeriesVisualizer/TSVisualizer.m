%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TimeSeriesVisualizer
%
% Visualize a time series of discrete data.
%
% Sample usage:
%
% TSVisualizer('-debug','-single','-transitions','input', ...
% 'C:\Users\Mauricio\Desktop\2013a-radiov-1-2-at-250ms-SA.csv', ...
% 'output','C:\Users\Mauricio\Desktop\', ...
% 'obsEnc','C:\Users\Mauricio\Desktop\2013a-encodings-master-SA.csv')
%
%% Options: ---------------------------------------------------------------
%
% -transitions: 
%           Analyze the time series according to the time-adjacent
%           transistions from state (i.e. discrete observation) to state.
%           Produces a color coded transition matrix.
%           
%           output a matrix with
%           entries corresponding to the probability of going from one
%           state to every other state.
%
%           output a matrix with
%           entries corresponding to the number of jumps from each state to
%           every other state.
%
%           normalize previous output
%           to build an "importance mask", which is then applied to the 
%           output of -probabilities. The result is a matrix which encodes
%           (a) how likely a given transition is in addition to (b) how
%           well represented that transition is in the raw data, showing
%           the overall relative signficance of each transition.
% -pairwiseDiv: 
%           Analyze divergence patterns in two transition matrices by 
%           subtracting one from the other and color coding the resulting
%           absolute intensities.
% -distribution: 
%           Analyze distribution of the discrete data points with respect
%           to a given grouping index. Results in a color coded matrix.
% -single: 
%           Perform analysis on a single input data source.
% -batch: 
%           Perform analysis on all suitable data sources in a given
%           location / directory.
% -debug:    
%           Print program internal state at certain control junctures,
%           for development within MATLAB.
% -printMat:    
%           Write raw matrix output values to file.
%
%% Inputs -----------------------------------------------------------------
% Flags in [] mark with which options they must be provided:
%
% input [-single, -batch]: 
%           Full system path to data source. If -single, then this points
%           to a specific CSV file with format [groupID, discrete data
%           point]. If -batch, then this points to a folder with a
%           collection of these files.
% output [-single, -batch]: 
%           An output data sink. If -single, then this points to a specific
%           file (e.g. .png). If -batch, then this points to a folder where
%           the results should be placed.
% obsEnc [-single, -batch]: 
%           CSV file with two columns, [discrete label, discrete label 
%           code], where discrete label is the string label of the discrete
%           label code which appears on the input file (data point).
% taskEnc [-distribution]: 
%           CSV file with two columns, [discrete label, discrete label 
%           code], where discrete label is the string label of the discrete
%           label code which appears on the input file (groupID).
% title []: 
%           Optional title to add to -single output figure 
% comp [-pairwiseDiv, -single]: 
%           Full path to data source file used for comparison with input
%%
function TSVisualizer(varargin)

    vargs = varargin;
    parser = inputParser;

    % Handle input flags
    vF = {};
    [transitions,vargs,vF{end+1}]= parseFlag(vargs,'-transitions',...
        @()~isempty(parser.Results.input) && ...
           ~isempty(parser.Results.obsEnc) && ...
           ~isempty(parser.Results.output));
    [pairwiseDiv,vargs,vF{end+1}]= parseFlag(vargs,'-pairwiseDiv',...
        @()~isempty(parser.Results.input) && ...
           ~isempty(parser.Results.obsEnc) && ...
           ~isempty(parser.Results.output));
    [distribution,vargs,vF{end+1}]=parseFlag(vargs,'-distribution',...
        @()~isempty(parser.Results.input) && ...
           ~isempty(parser.Results.obsEnc) && ...
           ~isempty(parser.Results.output) && ...
           ~isempty(parser.Results.taskEnc));    
    [batch,vargs,vF{end+1}] = parseFlag(vargs,  '-batch', ...
        @()~isempty(parser.Results.input));
    [single,vargs,vF{end+1}] = parseFlag(vargs, '-single', ...
        @()~isempty(parser.Results.input));
    [debug,vargs,vF{end+1}] = parseFlag(vargs,  '-debug', ...
        @()true);
    [printMatCSV,vargs,vF{end+1}] = parseFlag(vargs,'-printMat',...
        @()true);

    % Specify optional arguments
    addParameter(parser,'input','',@(x)true);
    addParameter(parser,'output','./',@(x)true);
    addParameter(parser,'obsEnc','',@(x)true);
    addParameter(parser,'taskEnc','',@(x)true);
    addParameter(parser,'title','',@(x)single);
    addParameter(parser,'comp','', @(x)pairwiseDiv && single);
    
    % Global validations
    vF{end+1} = @() ~(batch && single);
    
    % Validate arguments
    parse(parser,vargs{:});
    for i=1:length(vF)
      if (~vF{i}()) 
          error('TimeSeriesVisualizer:flagCheck', ...
          strcat('The following condition is not met: ',func2str(vF{i})));
      end
    end
    if (debug) parser.Results
    end
    
    % Get program state variables
    [obsEnc] = parseNonNumericCSV(parser.Results.obsEnc, '%s %s', {{},{}});    
    [taskEnc] = parseNonNumericCSV(parser.Results.taskEnc,'%s %s',{{},{}});    
    [singles, batchers] = deal({});
    
    % Initialize preprocessor
    fprintf('Initializing ...\n');
    [preprocessor] = DAOPreprocessor(obsEnc, taskEnc); 
    
    % Initialize visualizers
    if transitions
    [singles{end+1},batchers{end+1}] = TransitionVisualizer(preprocessor);
    inputData = {parser.Results.input};
    end
    
    if pairwiseDiv
    [singles{end+1},batchers{end+1}] = PairDivVisualizer(preprocessor); 
    inputData = {parser.Results.input, parser.Results.comp};
    end
     
    if distribution
    [singles{end+1},batchers{end+1}] = DistrVisualizer(preprocessor);
    inputData = {parser.Results.input};
    end
    
    % Produce results - singles
    if single
        for i=1:length(singles) 
          singles{i}(inputData,parser.Results.title,printMatCSV,...
                     parser.Results.output);
        end
    end
    
    % Produce results - batch
    if batch
        for i=1:length(batchers) 
          batchers{i}(parser.Results.input,printMatCSV,...
                      parser.Results.output); 
        end
    end
end

% Wrapper utility for reading non numeric CSVs. If the input is empty then
% return the default value given.
function [res] = parseNonNumericCSV(file, format, default)
    if isempty(file) res = default; else
    fileId = fopen(file);
    res = textscan(fileId, format, 'delimiter', ',');
    fclose(fileId);
    end
end

% Returns true for boolRes iff the flag in found in args and the given
% condition for its existence is fulfilled. Output function is set to
% input function iff boolRes is met.
function [boolRes, args, func] = parseFlag(args, flag, f)
    boolRes = false;
    func = @()true;
    if find(strcmp(flag, args)) boolRes = true; end
    args = args(~strcmp(args,flag));
    if boolRes func = f; end
end

