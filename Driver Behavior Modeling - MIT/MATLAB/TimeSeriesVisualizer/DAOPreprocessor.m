%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DAOPreprocessor
%
% Function repository for manipulating time and data series. This function
% serves as an abstraction layer between the data representation (i.e. 
% transition matrix) and the low level data access/preprocessing steps
% (i.e. buffering to maximize performance when multiple visualizations
% are requested from the same data.)
%%
function [constructor] = DAOPreprocessor(obsEnc, groupEnc)

    constructor = @DAOPreprocessorConstructor;

    function [g, b, n, d, m] = DAOPreprocessorConstructor()
    
    % Function handles
    g = @getTransMat;
    b = @getBatchedTransMat;
    n = @getNumFiles;
    d = @getDistrMat;
    m = @getBatchedDistrMat;
    
    % Global variables
    data = [];
    [fnames, xlabels, ylabels] = deal({});
    
    
    % Public functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Return the transition matrix of the time series given under the
    % specified path. Result is an empty matrix if path invalid or data
    % under path does not meet formatting requirements.
    function [tprob,tcount,tsig,xlab,ylab,defaultname] = getTransMat(file)
        [tprob,tcount,tsig,xlab,ylab] = ...
        computeTransMat(parseMatrix(file), obsEnc);
        defaultname = nameFromPath(file);
    end
    
    % Return the distribution matrix of the data sereis given under the
    % specifiec path. Result is an empty matrix if path invalid or data
    % under path does not meet formatting requirements.
    function [mat,xlab,ylab,defaultname] = getDistrMat(file)
        [mat,xlab,ylab]=computeDistrMat(parseMatrix(file),obsEnc,groupEnc);
        defaultname = nameFromPath(file);
    end
    
    % Returns the idx-th transition matrix from cache.
    % If no information is cached, then build the cache and return the
    % specified segment
    function [mat,xL,yL,defaultname]=getBatchedTransMat(idx,batchPath)
        [mat,xL,yL,defaultname]=getBatchedMat(idx,batchPath,@getTransMat);
    end
    
    % Returns the idx-th distribution matrix from cache.
    % If no information is cached, then build the cache and return the
    % specified segment
    function [mat,xL,yL,defaultname]=getBatchedDistrMat(idx,batchPath)
        [mat,xL,yL,defaultname]=getBatchedMat(idx,batchPath,@getDistrMat);
    end
    
        
    % Return the number of potential data source candidates within a
    % specified directory.
    function [numFiles] = getNumFiles(path)
         numFiles = length(dir(strcat(path,'\*.csv')));
    end

    % Private functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Returns the idx-th distribution matrix from the collection stored 
    % in cache. If no information is cached, then build the cache and 
    % return the specified segment. func refers to a function that
    % transforms a given input file to a matrix (see collectData)
    function [mat,xL,yL,defaultname]=getBatchedMat(idx,batchPath,func)
                
        % Retrieve cached data if possible, otherwise compute it
        if isempty(data)
        [data,xlabels,ylabels,fnames]=collectData(batchPath,func);
        end
        
        % Default return values
        defaultname = '';
        mat = [];
        [xL,yL] = deal({});
        
        % Check if indices out of bounds and whether matrix is invalid 
        % -> return empty matrix
        unitHeight = length(ylabels);
        s = unitHeight*(idx-1)+1;
        f = s+unitHeight-1;
        if ~(s<1 || s>size(data,1) || f<1 || f>size(data,1))
            if max(any(data(s:f,1:end))) mat = data(s:f,1:end); end
            defaultname = fnames{idx};
            xL = xlabels;
            yL = ylabels;
        end
    end

    % Given a system path, find all numeric .csv with precisely two columns
    % and compute and concactenate (vertically) the resulting transition
    % matrices
    function [data,xlabels,ylabels,fName] = collectData(path, func)

        % Set file names
        fnames = dir(strcat(path,'\*.csv'));
        numfids = length(fnames);
        fName = cell(numfids,1);

        %  Data contains all transition matrices concatenated vertically
        [dataR, dataC] = deal(0);
        maxdim = max(length(obsEnc{2}),length(groupEnc{2}));
        data = zeros(maxdim*numfids, maxdim); % worst case scenario
        for K = 1:numfids
            [mat,xlabels,ylabels,fName{K}] = func(strcat(path,...
                                             '\',fnames(K).name));
            if isempty(mat) continue; end
            dataR = size(mat,1);
            dataC = size(mat,2);
            start = dataR*(K-1)+1;
            finish = start+dataR-1;
            data(start:finish,1:dataC) = mat(:,:);
        end
        
        % Refit data
        data = data(1:numfids*dataR, 1:dataC);
    end
    
    % Use the data series given in data, along with a mapping of discrete
    % labels to integers to compute a transition matrix for the series.
    % Expect matrix with two columns, the first a grouping index column, 
    % and the second and observation column
    function [trans,counts,sig,xlab,ylab] = computeTransMat(data,encodings)

        % Default output values
        trans = [];
        [xlab, ylab] = deal({});
        
        if (isempty(data) || size(data,2)~=2) return; end
        
        % Map discrete data to range [1, #of discrete data points]
        dataMap = containers.Map(cellfun(@str2num,encodings{2}),...
                                 [1:length(encodings{2})]'); 
        [M] = mapMat(data, {@(x)x, @(x)dataMap(x)});
        
        % Handle labels
        [xlab,ylab] = deal(encodings{1});

        % Matlab implements stable sort
        M = sortrows(M,1);

        % Compute mat
        trans = zeros(length(encodings{2}),length(encodings{2}));

        % How many times does state i transition to state j
        for t = 1:size(M, 1)
            if t+1 <= size(M, 1) && M(t, 1) == M(t+1,1)
               trans(M(t,2), M(t+1,2)) = trans(M(t,2), M(t+1,2)) + 1;
            end
        end

        % Sum across all columns
        s = sum(trans, 2);
        
        % Importance mask
        mask = trans./max(max(trans));
        
        % Counts matrix
        counts = trans;

        % Divide by total number of transitions from state (row) i
        trans = bsxfun(@rdivide, trans',s')';
        trans(isnan(trans)) = 0;
        
        sig = trans.*mask;
    end
    
    % Use the data series given in data, along with a mapping of discrete
    % labels to integers to compute a distribution matrix for the series.
    % Expect matrix with two columns, the first a grouping index column, 
    % and the second and observation column
    function [p, xlab, ylab] = computeDistrMat(data,obslabels,grouplabels)

        if (isempty(data) || size(data,2)~=2) 
            p = [];
            [xlab, ylab] = deal({});
            return; 
        end

        % Map discrete data to range [1, #of discrete data points]
        dataMap = containers.Map(cellfun(@str2num,obslabels{2}),...
                                 [1:length(obslabels{2})]');
                             
        % Map discrete groups to range [1, #of groups]
        groupMap = containers.Map(cellfun(@str2num,grouplabels{2}),...
                                 [1:length(grouplabels{2})]');
                             
        [M] = mapMat(data, {@(x)groupMap(x), @(x)dataMap(x)});
        [idx, groupedData] = groupData1D(M,1,[2:size(M,2)]);
        
        % Handle labels
        yidx =  sort([idx{:}]);
        ylab = cell(1,length(yidx));
        for i=1:length(yidx) ylab{i} = grouplabels{1}{yidx(i)}; end
        
        xidx = sort([unique(vertcat(groupedData{:}))]);
        xlab = cell(1,length(xidx));
        for i=1:length(xidx) xlab{i} = obslabels{1}{xidx(i)}; end
        
        % Fill matrix with sums of observations
        p = zeros(length(idx), length(obslabels{2}));
        for i=1:length(idx)
            uniqueobs = unique(groupedData{i});
            for j=1:length(uniqueobs)
                p(idx{i},j) = sum(groupedData{i}==uniqueobs(j));
            end
        end
        
        % Normalize to get probabilities
        s = sum(p, 2);
        p = bsxfun(@rdivide, p',s')';
        p(isnan(p)) = 0;
    end
    
    % Wrapper utility for reading matrices. If empty input return empty
    % matrix, or if matrix does not have the correct dimensions as
    % specified
    function [res] = parseMatrix(file)
        if isempty(file) res = [];
        else
            try
                res = csvread(file,0,0); 
            catch
                try
                    res = csvread(file,1,0);
                catch
                    res = [];
                end
            end
        end
    end

    % Apply a mapping function to each column of inputMat. funcs is a cell 
    % array containing as many functions as inputMat has columns.
    function [mapped] = mapMat(inputMat, funcs)

       if isempty(inputMat) mapped = []; return; end
       if size(inputMat,2) ~= length(funcs) 
           error('TimeSeriesVisualizer:mapMat', 'Dimensions mismatch.');
       end
       mapped = zeros(size(inputMat,1), size(inputMat,2));
       for i=1:length(funcs)
           mapped(:,i) = cell2mat(cellfun(@(x)funcs{i}(x), ...
                         num2cell(inputMat(:,i)),'un',0));
       end
    end

    % Extract last name from path
    function [fname] = nameFromPath(path)
        delims = strfind(path, '\');
        last = delims(length(delims));
        fname = path(last+1:end);
    end
    
    
    % Group the data in the given data matrix along the values in the 
    % #groupIndex column. dataRange is an array specifying the indices from
    % which the rest of the data should be collected.
    % Method returns two cell arrays, one with the grouped values and one 
    % with the corresponding data.
    function [groupAtt,groupedData]=groupData1D(data,groupIndex,dataRange)
        [groupAtt, groupedData] = deal({});
        if isempty(data) return; end
        [~,~,idx]=unique(data(:,1));
        groupedData = cell(1, max(idx));
        groupAtt = cell(1, max(idx));
        for i=1:max(idx)
            groupAtt{i} = unique(data(idx==i,groupIndex));
            groupedData{i} = data(idx==i,dataRange);
        end
    end
end
end