%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TransitionVisualizer
%
% Visualize a time series of discrete data via the time adjacent
% transitions present in the time series. The result is a transition matrix
% with dimensions dim x dim, where dim is the number of possible distinct
% data points. This information is given via an encoding file, which maps
% a discrete data label (string) to the corresponding integer for which
% it stands in the input file. The latter is a two column matrix with the
% format [groupID, discrete data point (integer)].
%%
function [s, b] = TransitionVisualizer(preprocessor)

% Function handles
s = @produceSingle;
b = @batch;

% Global variables: DAOPreprocessor
[getSingle, getBatch, getNumFiles,~,~] = preprocessor();


%% Public functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Produce visualization for a single data source. Filepath is the complete
% system path to the source (csv file), which is assumed to be a 2 col
% matrix with format [groupID, discrete data point (integer)]. The result
% is stored in output, which is also a complete system path to a file.
function [] = produceSingle(filepath, title, printCSV, output)
    if isempty(filepath) return; end
    [tprobs,tcounts,tsig,xlabel,ylabel,defaultname] = getSingle(filepath{1});
    if isempty(tprobs) return; end
    fprintf('Processing %s\n', filepath{1});
    if isempty(title) title = defaultname; end
    
    
    visualize1Scale(tprobs,title,xlabel,ylabel,printCSV,...
        strcat(output,'.probabilities.png'));
    visualize1Scale(tcounts,title,xlabel,ylabel,printCSV,...
        strcat(output,'.counts.png'));
    visualize1Scale(tsig,title,xlabel,ylabel,printCSV,...
        strcat(output,'.significance.png'));
end

% Produce visualizations for all suitable data sources in dirpath. These
% are csv files with exactly two colums with format [group ID, discrete
% data point]. The results are stored in output, which is a complete
% system path to a directory
function [] = batch(dirpath, printCSV, output)
   
    for i=1:getNumFiles(dirpath)
        [mat, xlabels, ylabels, name] = getBatch(i,dirpath);
        if isempty(mat) continue; end
        fprintf('Processing %s\\%s\n', dirpath,name);
        visualize1Scale(mat,name,xlabels,ylabels,printCSV,...
                        strcat(output,'\',name));
    end
end


%% Private functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Use a figure to visualize the matrix given in p. Visualization
% is done using a single color scale from the lowest to the highest
% Result is printed directly to the file specified by output.
% t - title of figure
% range - color range, specified as a two dimensional row vector. Use []
% for automatic range detection
% xlabels - labels on x axis, string array
% ylabels - labels on y axis, string array
% printCSV - true to print additional csv file
% output - output folder
function visualize1Scale(p, t, xlabels, ylabels, printCSV, output)

    if isempty(p) return; end
    r = p .^0.5; % Increase color contrast, gamma correction
    fig = figure('visible','off');
    imagesc(r); % Display gamma corrected colors
    if max(max(p)) <= 1 caxis([0 1]); end % Set color range
    colormap(jet(256));
    textS = num2str(p(:),'%0.2f'); % original values on top
    textS = strtrim(cellstr(textS));
    [x,y] = meshgrid(1:size(r,1));
    hStrings = text(x(:),y(:),textS(:),'HorizontalAlignment','center');
    maxVal = max(max(p));
    textColors = repmat(p(:) < 0.08*maxVal | p(:) > 0.92*maxVal,1,3);% 8% margin is white
    set(hStrings,{'Color'},num2cell(textColors,2));
    set(gca,'YTick',[1:length(ylabels)]);
    set(gca,'XTick',[1:length(xlabels)]);
    set(gca, 'XTickLabel', xlabels);
    set(gca, 'YTickLabel', ylabels);
    set(gcf, 'Color', 'w');
    %set(gca, 'FontSize', 12);
    %set(hStrings,'FontSize',getTextSize(max(size(p)),2));
    set(gca, 'FontSize', 16);
    set(hStrings,'FontSize',16);
    set(fig, 'PaperPositionMode', 'auto');
    set(fig, 'units', 'inches', 'position', [5 5 15 10]);
    ylabel('FROM');
    xlabel('TO');
    %title(t);
    xticklabel_rotate([1:length(xlabels)],45,xlabels,'interpreter','none');
    export_fig(output,'-q101','-a4',fig);
    if printCSV csvwrite(output,p); end
end

% Set the font size of in-graphic text. The mapping from input to output
% size is essentially a gamma correction with a bottom limit of 8
function [newsize] = getTextSize(size, gamma)
    val = (25-size) / 25;
    if (val < 0) newsize = 8;
    else newsize = (val.^gamma)*40;
    end
end
end

