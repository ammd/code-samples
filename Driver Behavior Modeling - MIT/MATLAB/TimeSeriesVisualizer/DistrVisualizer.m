%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DistrVisualizer
%
% Visualize data distributions in a discrete data series according to 
% groups. The result is a distribution matrix with size 
% [groups observations] where groups is the total number of groups in the
% original dataset and observations is the number of distinct discrete
% observations recorded.
%%
function [s, b] = DistrVisualizer(preprocessor)

% Function handles
s = @produceSingle;
b = @batch;

% Global variables: DAOPreprocessor
[~,~,getNumFiles,getSingle,getBatch] = preprocessor();


%% Public functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [] = produceSingle(filepath, title, printCSV, output)
    if isempty(filepath) return; end
    [mat,xlabel,ylabel,defaultname] = getSingle(filepath{1});
    if isempty(mat) return; end
    fprintf('Processing %s\n', filepath{1});
    if isempty(title) title = defaultname; end
    visualize1Scale(mat,title,xlabel,ylabel,printCSV,output);
end

function [] = batch(dirpath, output)
    
    for i=1:getNumFiles(dirpath)
        [mat, xlabels, ylabels, name] = getBatch(i,dirpath);
        if isempty(mat) continue; end
        fprintf('Processing %s\\%s\n', dirpath,name);
        visualize1Scale(mat,name,xlabels,ylabels,printCSV,...
                        strcat(output,'\',name));
    end
end


%% Private functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Use a figure to visualize the matrix given in p. Visualization
% is done using a single color scale from the lowest to the highest
% Result is printed directly to the file specified by output.
function visualize1Scale(p, t, xlabels, ylabels, printCSV, output)
    
    if isempty(p) return; end
    r = p .^0.5;
    fig = figure('visible','off');
    imagesc(r); % Display gamma corrected colors
    caxis([0 1]); % Set color range to [0,1], this allows for comparisons
    colormap(jet(256));
    textS = num2str(p(:),'%0.2f'); % original values on top
    textS = strtrim(cellstr(textS));
    [x,y] = meshgrid(1:size(r,2), 1:size(r,1));    
    hStrings = text(x(:),y(:),textS(:),'HorizontalAlignment','center');
    textColors = repmat(p(:) < 0.08 | p(:) > 0.92,1,3);% 8% margin is white
    set(hStrings,{'Color'},num2cell(textColors,2));
    set(gca,'YTick',[1:length(ylabels)]);
    set(gca,'XTick',[1:length(xlabels)]);
    set(gca, 'XTickLabel', xlabels);
    set(gca, 'YTickLabel', ylabels);
    set(gcf, 'Color', 'w');
    set(gca, 'FontSize', 12);
    set(hStrings,'FontSize',getTextSize(max(size(p)),2));
    set(fig, 'PaperPositionMode', 'auto');
    set(fig, 'units', 'inches', 'position', [5 5 15 10]);
    ylabel('TASK');
    xlabel('STATE');
    title(t);
    xticklabel_rotate([1:length(xlabels)],45,xlabels,'interpreter','none');
    export_fig(output,'-q101','-a4',fig);
    if printCSV csvwrite(output,p); end
end

% Set the font size of in-graphic text. The mapping from input to output
% size is essentially a gamma correction with a bottom limit of 8
function [newsize] = getTextSize(size, gamma)
    val = (25-size) / 25;
    newsize = max(9,(val.^gamma)*40);
end
end

