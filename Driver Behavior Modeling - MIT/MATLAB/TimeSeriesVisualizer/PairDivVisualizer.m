%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PairDivVisualizer
%
% Visualize the divergence between two transition patterns of two distinct
% time series with the same set of possible discrete data points. 
% The result is a matrix with dimensions dim x dim, where dim is the number
% of possible distinct data points. This information is given via an 
% encoding file, which maps a discrete data label (string) to the 
% corresponding integer for which it stands in the input files. The latter 
% are two column matrices with the format [groupID, discrete data point 
% (integer)].
%%
function [s, b] = PairDivVisualizer(preprocessor)

% Function handles
s = @produceSingle;
b = @batch;

% Global variables: DAOPreprocessor
[getSingle, getBatch, getNumFiles,~,~] = preprocessor();


%% Public functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Produce visualization for a single data source. Filepath is the complete
% system path to the source (csv file), which is assumed to be a 2 col
% matrix with format [groupID, discrete data point (integer)]. The result
% is stored in output, which is also a complete system path to a file.
function [] = produceSingle(filepaths, title, printCSV, output)
    if isempty(filepaths) return; end
    [tprob1, tcount1, tsig1, xlab1, ylab1, name1] = getSingle(filepaths{1});
    [tprob2, tcount2, tsig2, xlab2, ylab2, name2] = getSingle(filepaths{2});
    if isempty(tprob1) || isempty(tprob2) return; end
    fprintf('Processing %s -- %s\n', filepaths{1}, filepaths{2});
    if isempty(title) title = strcat(name1,'--',name2); end
    if ~isequal(xlab1, xlab2) || ~isequal(ylab1, ylab2)
        error('PairDivVisualizer:produceSingle', 'Labels mismatch.');
    end
    visualize2Scale(tprob1-tprob2,title,xlab1,ylab1,printCSV,...
        strcat(output,'.probabilities.png'));
    visualize2Scale(tcount1-tcount2,title,xlab1,ylab1,printCSV,...
        strcat(output,'.counts.png'));
    visualize2Scale(tsig1-tsig2,title,xlab1,ylab1,printCSV,...
        strcat(output,'.significance.png'));
end

% Produce visualizations for all suitable data sources in dirpath. These
% are csv files with exactly two colums with format [group ID, discrete
% data point]. The results are stored in output, which is a complete
% system path to a directory
function [] = batch(dirpath, printCSV, output)
  
    for i=1:getNumFiles(dirpath)
        for j=i+1:getNumFiles(dirpath)
            [mat1, xlab1, ylab1, name1] = getBatch(i,dirpath);
            [mat2, xlab2, ylab2, name2] = getBatch(j,dirpath);
            if isempty(mat1) || isempty(mat2) continue; end
            fprintf('Processing %s: %s -- %s\n', dirpath,name1,name2);
            name = strcat(name1,'--',name2);
            if ~isequal(xlab1, xlab2) || ~isequal(ylab1, ylab2)
                error('PairDivVisualizer:batch', 'Labels mismatch.');
            end
            visualize2Scale(mat1-mat2,name,xlab1,xlab2,printCSV,...
                            strcat(output,'\',name));
        end
    end
end


%% Private functions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Use a figure to visualize the matrix given in p. Visualization
% is done using a double color scale from the lowest to the highest
% Result is printed directly to the file specified by output.
function visualize2Scale(p, t, xlabels, ylabels, printCSV, output)
    if isempty(p) return; end
    w = p;
    w(w<0)=-1;
    w(w>0)= 1;
    r = (abs(p) .^0.3).*w;
    fig = figure('visible','off');
    set(fig, 'units', 'inches', 'position', [5 5 15 10]);
    imagesc(r);
    rgbslide(r,'lowval',-1,'highval',1);
    textS = num2str(p(:),'%0.2f'); % original values on top
    textS = strtrim(cellstr(textS));
    [x,y] = meshgrid(1:size(r,1));
    hStrings = text(x(:),y(:),textS(:),'HorizontalAlignment','center');
    textColors = true(1,3);% text is white
    set(hStrings,{'Color'},num2cell(textColors,2));
    set(gcf, 'Color', 'w');
    set(gca, 'XTickLabel', xlabels);
    set(gca, 'YTickLabel', ylabels);
    
    set(gca, 'FontSize', 16);
    set(hStrings,'FontSize',16);
    
    %set(gca, 'FontSize', 12);
    %set(hStrings,'FontSize',getTextSize(max(size(p)),2));
    set(fig, 'PaperPositionMode', 'auto');
    set(fig, 'units', 'inches', 'position', [5 5 15 10]);
    ylabel('FROM');
    xlabel('TO');
    title(t);
    xticklabel_rotate([1:length(xlabels)],45,xlabels,'interpreter','none');
    export_fig(output,'-q101','-a4',fig);
    if printCSV csvwrite(output,p); end
end

% Set the font size of in-graphic text. The mapping from input to output
% size is essentially a gamma correction with a bottom limit of 8
function [newsize] = getTextSize(size, gamma)
    val = (25-size) / 25;
    if (val < 0) newsize = 8;
    else newsize = (val.^gamma)*40;
    end
end
end

