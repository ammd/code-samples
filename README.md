This is a repository containing code samples from a variety of old and current projects.

Copyright (c) 2015, Mauricio Muñoz.
All rights reserved.

Source materials are intended for illustrative purposes only. Redistribution and use in source and binary forms, with or without modification, are not permitted without written consent from the author(s). 

### Bundestag Elections 
Languages: Java, SQL. Notable technologies: Google GWT, Apache Tomcat. This semester project at the Technical University of Munich used the freely-available 2008 German Bundestag elections voter data in a tiered application architecture to visualize and compute election results and statistics according to German law.

### Database Implementation
Languages: C++. In this semester project at the Technical University of Munich, a database management system was implemented from the ground up. The code provided here consists of the lower data management levels.

### Documed 
Languages: Java, SQL. Notable technologies: Google Cloud Messaging, Android development toolkit, Spring, Apache Tomcat. In this semester project at the Technical University of Munich, a distributed healthcare application was designed to improve communication between patients and physicians, with a particular focus on security, scalability, and performance.

### Driver Behavior Modeling 
Languages: C++, MATLAB, SQL. This project encompasses the efforts made at the MIT AgeLab during my master's thesis to visualize and model driver visual behavior using established statistical tools and machine learning methods, serving as the foundation for various publications in the field of human factors.

### Efficient Route Planning
Languages: C++, Javascript. Notable technologies: Google Maps API. This mini-project is an excerpt from a larger set of coursework for a route planning course at the University of Freiburg. This present distributed application takes OSM map data and interactively visualizes shortest path queries in a Google Maps based front end.

### Google Contacts Chrome Extension
Languages: Javascript. Notable Technologies: Angular JS. This project illustrates the type of productivity tools developed at the MIT AgeLab over the course of my master's thesis. This extension for Google Chrome is designed to enable contacts filtering, searching, etc. 

###  Rhyve 
Languages: C++. Notable technologies: Yaafe. This project implements a set of core features in a larger distributed, music-based social interaction application developed at the Technical University of Munich. The project performs classification of audio files in musical genres, and matches users of the software with other users according to musical taste.