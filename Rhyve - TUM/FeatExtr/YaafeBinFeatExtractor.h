
///////////////////////////////////////////////////////////////////////////////
// YaafeBinFeatExtractor.h
///////////////////////////////////////////////////////////////////////////////

#ifndef YAAFEBINFEATEXTRACTOR_H
#define YAAFEBINFEATEXTRACTOR_H

#include <vector>
#include <iostream>
#include <functional>
#include "FeatureExtractor.h"

// Extract features using Yaafe's compiled 'yaafe-engine' program
class YaafeBinFeatExtractor : public FeatureExtractor
{

public:

    // Constructor, destructor. Requires the script ./assets/setup.sh.
    YaafeBinFeatExtractor(): FeatureExtractor() 
    { 
        _hasParams = 3; 
        _groupSize = 12;
        _step = _groupSize / 2;
    }
    ~YaafeBinFeatExtractor() { }

    // Expects params in the form [dataflow file]
    bool bootstrap(std::vector<std::string> params, 
                   std::vector<std::string> const* tracksptr,
                   ResRequester req);
                   
    // Assemble feature file
    void postprocess(bool rowf);   

    // Features to be extracted (as well as their order) are predefined
    // in the dataflow file provided as a parameter.
    void extractFeatures(bool clipoffset);

    // The length of the per frame feature vector
    int getFeatureLength();

                  
private:

    // The number of features that should be grouped together during PCA.
    // Represents a trade off: the more features grouped, the higher the
    // compression, however more information is potentially lost.
    int _groupSize;

    // Determines the overlap for neighboring groups during PCA
    int _step;

    std::string _assetpath;
    std::string _avconvpath;
};

#endif  // YAAFEBINFEATEXTRACTOR_H

