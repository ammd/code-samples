
///////////////////////////////////////////////////////////////////////////////
// MainState.h
///////////////////////////////////////////////////////////////////////////////

#ifndef MAINSTATE_H
#define MAINSTATE_H

#include <iostream>
#include <stdlib.h>
#include <vector>
#include <string>
#include <unordered_map>
#include <utility>
#include "../com/util.h"


// Available feature extractors:
// YAAFEBIN:    YAAFE features using a provided binary, yaafe-engine
enum class extrType { extrType_begin, YAAFEBIN = extrType_begin, extrType_end };

class MainState
{
public:
    MainState()
    {
        rowf = false;
        offset = false;
        ASSETSPATH = "ASSETS_PATH";
        AVCONVPATH = "AVCONV_PATH";
        env.insert(make_pair(ASSETSPATH,""));
        env.insert(make_pair(AVCONVPATH,""));
        loadEnvironment();
    }
    bool checkState()
    {
        for (auto it = env.begin(); it != env.end(); ++it)
            if (it->second.compare("") == 0) return false;
        if (extr.size() == 0) return false;
        return true;
    }
    std::string getAssetsPath() const { return env.find(ASSETSPATH)->second; }
    std::string getAvconvPath() const { return env.find(AVCONVPATH)->second; }
    std::vector<extrType> extr;
    bool offset;
    bool rowf;

private:
    std::string ASSETSPATH, AVCONVPATH;
    std::unordered_map<std::string,std::string> env;
    void loadEnvironment()
    {
        std::vector<char> delim;
        delim.push_back('=');    
        std::vector<std::string> rows;
        util::parseFile<std::string>(rows,[&](std::string& s){return s;},"./config.cfg");
        for (unsigned int i = 0; i < rows.size(); i++)
        {
            auto cur = rows[i];
            if (cur.size() == 0 || cur.substr(0, 1).compare("#") == 0) continue;
            auto stringVec = util::splitString(cur, delim);
            if (stringVec.size() != 2)
            {
                std::cout << "Config file malformed. " << std::endl;
                exit(1);
            }
            auto it = env.find(stringVec[0]);
            if (it != env.end()) it->second = stringVec[1];
        }
    }
};

#endif  // MAINSTATE_H