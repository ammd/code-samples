
///////////////////////////////////////////////////////////////////////////////
// FeatureExtractor.h
///////////////////////////////////////////////////////////////////////////////

#ifndef FEATUREEXTRACTOR_H
#define FEATUREEXTRACTOR_H

#include <vector>
#include <string>
#include <math.h>
#include <stdlib.h>

// Available extra resources (i.e. information about environment, assets, etc.)
// In contrast to arguments which are given directly via the 'bootstrap'
// interface, these resources are available on request only basis, as they might
// not all be needed by every extractor.
//
// ASSETS:    Relative path to assets directory
// AVCONV:    Relative path to avconv executable
enum class resType {resType_begin, ASSETS=resType_begin, AVCONV, resType_end};

// Resource callback to be executed in FeatureAssembler instance when
// a resource is requested.
typedef std::function<void (std::string& s)> ResCallback;

// Dispatcher (callback wrapper) function for resource requests
typedef std::function<void (ResCallback)> ResDispatcher;

// Communication interface between a FeatureAssembler and a FeatureExtractor.
// Based on a desired resource type, returns an appropriate dispatcher,
// which ultimately fires the callback within.
typedef std::function<ResDispatcher (resType res)> ResRequester;

// Abstract class, represents a feature extraction strategy
class FeatureExtractor
{
    friend class FeatureAssembler;

public:

    // Constructor, destructor
    FeatureExtractor(){ }
    virtual ~FeatureExtractor() { }
    
    // Perform any prepocessing required by this extractor, update
    // parameters if necessary.
    // Tracksptr: pointer to a vector<string> containing the tracklist
    // ResRequester: allows resources to be dynamically requested from
    // the associated FeatureAssembler instance.
    virtual bool bootstrap(std::vector<std::string> p,
                           std::vector<std::string> const* tracksptr,
                           ResRequester req)
    {
        if ((int)p.size() != _hasParams) return false;

        for(auto &pm : p) 
            if(pm.compare("")==0 || pm.substr(0,1).compare(" ")==0) 
                return false;

        _arg = p;
        _tracks = tracksptr;
        return true;
    }

    // Analyzes the given tracklist and extracts features from each
    // individual track. The results are stored as a single file, 
    // consisting of the appended results over all features extracted
    // by this extractor, with one value per line.
    virtual void extractFeatures(bool clip)=0;

    // Perform any postprocessing steps are required by this extractor
    virtual void postprocess(bool rowf)=0;
                  
protected:

    // A container for the arguments used by this extractor
    std::vector<std::string> _arg;

    // A handle to the listing of tracks to be analyzed
    std::vector<std::string> const* _tracks;

    // Specifies how many paramters (including global) are expected or 
    // required by this extractor.
    int _hasParams;
};

#endif  // FEATUREEXTRACTOR_H

