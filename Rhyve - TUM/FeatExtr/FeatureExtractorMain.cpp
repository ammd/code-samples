
///////////////////////////////////////////////////////////////////////////////
// FeatureExtractorMain.cpp
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <stdlib.h>
#include <vector>
#include <string>
#include <unordered_map>
#include <utility>
#include <boost/program_options.hpp>
#include "../com/util.h"
#include "MainState.hpp"
#include "FeatureAssembler.h"
#include "FeatureExtractor.h"

using namespace std;
namespace bpo = boost::program_options;

// Prints usage (parameters + options) of the program, and then exits
void printUsage(string assetsPath)
{	
    string printcmd = "groff -Tascii -man "+assetsPath+"/usage.1";
    if (system(printcmd.c_str()) < 0) {}
    exit(1);
}

// Parse command line arguments using Boost
vector<string> parseCmdLine(int argc, char** argv, MainState& state)
{
    bpo::options_description optDes("");
	optDes.add_options()
        ("help,h",          "")
		("clipoffset,c",    "")
		("rowformat,r",     "")
		("yaafeBin,y",      "")
        ("params",  bpo::value< vector<string> >(), "");

    bpo::positional_options_description posDes;
    posDes.add("params", -1);

    bpo::variables_map vm;
    try{
	    bpo::store(bpo::command_line_parser(argc, argv).
            options(optDes).positional(posDes).run(), vm);
	    bpo::notify(vm);
    } catch(std::exception& ex){
        cout << ex.what() << endl;   
    }
    
    // General program options
    if (vm.count("help")) printUsage(state.getAssetsPath());
    if (vm.count("clipoffset"))  state.offset = true;
    if (vm.count("rowformat"))  state.rowf = true;

    // Extractors
    if (vm.count("yaafeBin"))  state.extr.push_back(extrType::YAAFEBIN);
    return vm["params"].as<vector<string> >();
}

// Main routine, binds the specified extractors and produces '.feature' files
// for each specified input element.
int main(int argc, char** argv)
{   
    MainState state;
    vector<string> posArg = parseCmdLine(argc, argv, state);
    if (!state.checkState()) printUsage(state.getAssetsPath());

    // Create feature assembler and validate arguments
    FeatureAssembler* assembler = new FeatureAssembler(state);
    
    // Boot up and hook extractors to assembler
    if(!assembler->bindExtractors(posArg)) 
        printUsage(state.getAssetsPath());

    // Extract features
    assembler->extractFeatures();

    // Cleanup
    delete assembler;
    return 0;
}
