
///////////////////////////////////////////////////////////////////////////////
// FeatureAssembler.cpp
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <functional>
#include <utility>
#include "../com/util.h"
#include "FeatureAssembler.h"
#include "YaafeBinFeatExtractor.h"

using namespace std;

// ____________________________________________________________________________
FeatureAssembler::FeatureAssembler(MainState& state) : _state(state)
{
    _extrFactory.insert(std::make_pair(extrType::YAAFEBIN, 
        [](){ return new YaafeBinFeatExtractor(); }));
    
    _reqFactory.insert(std::make_pair(resType::ASSETS, 
        [&](std::function<void (string& s)> f){ f(_state.getAssetsPath()); }));

    _reqFactory.insert(std::make_pair(resType::AVCONV, 
        [&](std::function<void (string& s)> f){ f(_state.getAvconvPath()); }));
}

// ____________________________________________________________________________
FeatureAssembler::~FeatureAssembler()
{
    for (auto &extr : _extractors) delete extr;
}

// ____________________________________________________________________________
bool FeatureAssembler::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;
    for(auto &pm : p) 
        if(pm.compare("")==0 || pm.substr(0,1).compare(" ")==0) return false;

    _arg = p;
    auto func = [](string& s) { return s; };
    util::parseFile<string>(_tracks, func, _arg[0]);
    return true;
}

// ____________________________________________________________________________
bool FeatureAssembler::bindExtractors(vector<string> p)
{
    size_t cur = 0;
    size_t paramcount = 0;

    // Handle extractors and their arguments
    for (auto &type : _state.extr)
    {
        auto ex = getExtractor(type)();
        _extractors.push_back(ex);
        paramcount += (ex->_hasParams-_hasParams);
    }

    if (paramcount+_hasParams != p.size()) return false;
    vector<string> global(p.end()-_hasParams, p.end());
    for (auto &extr : _extractors)
    {
        vector<string> params(p.begin()+cur, 
                              p.begin()+cur+extr->_hasParams-_hasParams);
        params.insert(params.end(), global.begin(), global.end());
        vector<string>* const tracksptr = &_tracks;
        ResRequester func = [&](resType res){ return getDispatcher(res); };
        if(!extr->bootstrap(params, tracksptr, func)) return false;
        cur += extr->_hasParams;
    }

    // Boot up the assembler
    return bootstrap(global);
}

// ____________________________________________________________________________
void FeatureAssembler::extractFeatures()
{
    for (auto &extractor : _extractors)
    {
        extractor->extractFeatures(_state.offset);
        extractor->postprocess(_state.rowf);
    }
}

// ____________________________________________________________________________
void FeatureAssembler::assembleFeatures()
{
    return;
}