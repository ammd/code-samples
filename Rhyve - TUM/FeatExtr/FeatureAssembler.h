
///////////////////////////////////////////////////////////////////////////////
// FeatureAssembler.h
///////////////////////////////////////////////////////////////////////////////

#ifndef FEATUREASSEMBLER_H
#define FEATUREASSEMBLER_H

#include <vector>
#include <string>
#include <functional>
#include <unordered_map>
#include "MainState.hpp"
#include "FeatureExtractor.h"

// Invoker function for all feature extractors
typedef std::function<FeatureExtractor* ()> extrBuilder;

// Component in charge of organizing and assembling the I/O of the
// different feature extractors. Represents the presentation layer between
// these lower level extractors and the final output of the application.
class FeatureAssembler
{
public:

    // Constructor, binds the extractor enum ids to their constructors.
    FeatureAssembler(MainState& state);

    // Destructor. Free memory taken up by extractors.
    ~FeatureAssembler();

    // Bind a set of extractors to this assembler. Should the given parameters
    // not match the required for any bound extractor,
    // free up memory and return false, otherwise return true.
    bool bindExtractors(std::vector<std::string> p);

    // Use all extractors to produce, for each track, a feature file with
    // exactly one feature per line. All feature files should have the same
    // length,and be composed in the same manner (i.e. extractor order).
    void extractFeatures();

    // Piece extractor outputs together, produce the final output for 
    // each input element.
    void assembleFeatures();


private:

    // Parse global parameters and perform any global preprocessing, 
    // i.e. parsing the tracklist. Return false iff parameters are malformed
    bool bootstrap(std::vector<std::string> p);

    // Returns an extractor's constructor based on its enum value
    extrBuilder getExtractor(extrType type) { return _extrFactory[type]; }

    // Returns a resource's request function based on its enum value
    ResDispatcher getDispatcher(resType type) { return _reqFactory[type]; }

    // Container holding pointers to all currently bound extractors
    std::vector<FeatureExtractor*> _extractors;

    // Container holding the paths to each element to be processed
    std::vector<std::string> _tracks;

    // Dictionary mapping extractor enum to constructor
    std::unordered_map<extrType, extrBuilder> _extrFactory;

    // Dictionary mapping resource enum to request function
    std::unordered_map<resType, ResDispatcher > _reqFactory;

    // Container stores the global parameters: [tracklist, outputdir]
    std::vector<std::string> _arg;

    // The environment in which this assembler is to work
    const MainState _state;

    // Number of expected global parameters
    const static int _hasParams = 2;
};

#endif  // FEATUREASSEMBLER_H

