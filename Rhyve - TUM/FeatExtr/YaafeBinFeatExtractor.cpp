
///////////////////////////////////////////////////////////////////////////////
// YaafeBinFeatExtractor.cpp
///////////////////////////////////////////////////////////////////////////////

#include <omp.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include "armadillo"
#include "../com/util.h"
#include "YaafeBinFeatExtractor.h"
#include <boost/network/protocol/http/client.hpp>
#include <boost/asio.hpp>

using namespace arma;
using namespace std;
using boost::asio::ip::tcp;
using boost::system::error_code;

namespace http = boost::network::http;


// ____________________________________________________________________________
bool YaafeBinFeatExtractor::bootstrap(vector<string> p,
                                      vector<string> const* tracksptr,
                                      ResRequester req)
{   
    if(!FeatureExtractor::bootstrap(p, tracksptr, req)) return false;
    (req(resType::ASSETS))([&](string& s){ _assetpath = s;});
    (req(resType::AVCONV))([&](string& s){ _avconvpath = s;});
    return true;
}

// ____________________________________________________________________________
void YaafeBinFeatExtractor::extractFeatures(bool offset)
{  
    // Each thread is dynamically assigned an iteration of the inner loop.
    time_t start, end;
    time(&start);
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads)
    {
        #pragma omp for schedule(dynamic)
            for (int k = 0; k < (int)_tracks->size(); k++) 
            {
                string t = _tracks->at(k);
                util::parsePath(t);

                // automatically derive the audio file format
                string clipn = t + ".clip." + 
                               t.substr(t.rfind(".")+1, string::npos);
                string name = _arg[2]+t.substr(t.rfind("/"),string::npos);
                
                
                //string cmd = 

                // Setup YAAFE path variables
                //string("") + ". ./assets/setup.bash && ";

                // Clip input track
                string clipcmd = offset ? 

                (_avconvpath+ " -i " + t + " -ss 00:01:00.000 -t 10 " +
                "-acodec copy " + clipn + "") :

                (_avconvpath+ " -i " + t + " -ss 00:00:00.000 -t 10 " +
                "-acodec copy " + clipn + "");

                cout << clipcmd << endl;

                /*
                cmd += clipcmd;

                // Run the engine on clipped track
                cmd += "./assets/yaafe-engine -c "+_arg[0]+
                " -b "+ name + " " + clipn;

                // Cleanup - remove clipped track
                cmd += " && rm " + clipn;*/
            
                if (system(clipcmd.c_str()) < 0)
                {
                    cout << "YAAFE: Error extracting features " << endl;
                    exit(1);
                }
            }
    }

    
	/*boost::asio::io_service io_service;
    tcp::socket socket(io_service);
	std::ostringstream request;
	request << "GET HTTP/1.0\r\n"
	        << "Host: rhyve \r\n"
			<< "Accept: \r\n"
			<< "Connection: close\r\n";	
	boost::system::error_code write_error;
	boost::asio::write(socket, boost::asio::buffer(request.str()),
	boost::asio::transfer_all(), write_error);*/

    string serverip = "127.0.0.1";
    string serverport = "8888";
    try {
        http::client client;
        std::ostringstream url;
        url << "http://" << serverip << ":" << serverport << "/";
        http::client::request request(url.str());
        http::client::response response = client.get(request);
        std::cout << body(response) << std::endl;
    } catch (std::exception & e) {
        std::cerr << e.what() << std::endl;
    }

    time(&end);
    cout<<endl<<"YAAFE computation time: "<<difftime(end, start)<<" sec"<<endl;
    exit(1);
}

// ____________________________________________________________________________
void YaafeBinFeatExtractor::postprocess(bool rowf)
{
    // Each thread is dynamically assigned an iteration of the inner loop.
    time_t start, end;
    time(&start);
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads)
    {
        #pragma omp for schedule(dynamic)
            for (int k = 0; k < (int)_tracks->size(); k++) 
            {
                string o = _arg[2];
                string t = _tracks->at(k);
                string tid = t.substr(t.rfind("/"),string::npos);
                string ptid(tid);
                string featpath = o + tid + ".feature";
                string pfeatpath(featpath);
                util::parsePath(ptid);
                util::parsePath(pfeatpath);

                // Define command
                string cmd = string("") +

                // Find all .csv parts for the current track
                "find " + o + ptid + " -iname \"*.csv\" | " +

                // Set them in a specified order, and parse the filenames
                // TODO: void duplicate parsing logic, see util module
                "sort | sed " +

                "-e 's/ /\\\\ /g' " +
                "-e 's/\&/\\\\&/g' " +
                "-e \"s/'/\\\\\\'/g\" | " +

                // Concatenate the contents
                // "xargs cat | " +
                "xargs paste -d ',' | " +

                // One value per line
                //"sed 's/,/\\n/g' | " +

                // Filter out comments (lines starting with  '%')
                "grep ^[^%] > " + pfeatpath + " && " +

                // Truncate features
                //"sed -i '1294,$ d' " + pfeatpath + " && "+

                // Cleanup other files
                "rm -r " + o + ptid;

                // Execute command
                if (system(cmd.c_str()) < 0)
                {
                    cout << "YAAFE: Error formatting features" << endl;
                    exit(1);
                }

                // Use PCA to reduce dimensionality and decorrelate features:
                mat fileMat;
                fileMat.load(featpath);

                // Adjust data by subtracting mean (per variable)
                for (unsigned int i = 0; i < fileMat.n_cols; i++)
                {
                    double mean = 0.0;
                    for (unsigned int k = 0; k <fileMat.n_rows; k++) 
                        mean+=fileMat(k, i);
                    mean /= fileMat.n_rows;
                    for (unsigned int k = 0; k <fileMat.n_rows; k++) 
                        fileMat(k, i)-=mean;
                }
        
                // Subdivide feature set into groups, build and output final feat vec
                ofstream file(featpath, ofstream::out|ofstream::trunc);
                for (unsigned int i = 0; i+_groupSize<=fileMat.n_rows; i+=_step)
                {
                    // Perform PCA for each group
                    auto group = fileMat.rows(i, i+_groupSize-1);
                    vec latent;
                    mat coeff, score;
                    princomp(coeff, score, latent, group);
                    vector<double> featureVector;
                    auto featVec = coeff.col(0);
                    double n = norm(featVec, 2);
                    featVec.transform( [&](double val) { return (val/n); });
                    for(size_t j = 0; j < featVec.n_elem; j++) 
                        file << featVec(j) << (rowf ? " " : "\n");
                    if(rowf) file << "\n";
                }
                file.close();
            }   // end tracks loop
    }
    time(&end);
    cout<<"YAAFE postprocessing time: "<<difftime(end, start)<<" s"<<endl;
    exit(1);
}