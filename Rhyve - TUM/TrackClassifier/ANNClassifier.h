
///////////////////////////////////////////////////////////////////////////////
// ANNClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef ANNCLASSIFIER_H
#define ANNCLASSIFIER_H

#include <vector>
#include <string>
#include <unordered_map>
#include "TrackClassifier.h"
#include <opencv2/opencv.hpp>

using namespace cv; 


// Classify track features using a random forest
class ANNClassifier : public TrackClassifier
{
public:

    // Constructor, destructor.
    ANNClassifier(): TrackClassifier() { _hasParams = 8; }
    ~ANNClassifier() { delete _network; }

    // Validate given arguments, train and save a random forest for the given
    // training set. A training sample consists of a conglomerate feature vector
    // for a frame
    bool bootstrap(std::vector<std::string> params);

    // Using the individual frame features for the input track, assign the
    // label corresponding to the genre to which the majority of frames have
    // been classified
    std::unordered_map<int, int> classify(std::string input);

private:

    // The neural network
    CvANN_MLP* _network;

    // Whether to train or load a trained network
    bool _trainNetwork;

    // The size of each feature vector
    int _fvecsize;

    // The total number of features
    int _numFeat;
};

#endif  // ANNCLASSIFIER_H

