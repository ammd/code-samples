
///////////////////////////////////////////////////////////////////////////////
// KNNClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <queue>
#include <math.h>
#include <time.h>
#include <omp.h>
#include <unordered_map>
#include "TrackClassifier.h"
#include "KNNClassifier.h"
#include "../com/util.h"

using namespace std;

// ____________________________________________________________________________
bool KNNClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;
    if (stoi(p[0]) < 1) return false;
    _k = stoi(p[0]);

    std::vector<std::string> v(p.begin()+1, p.end());
    return TrackClassifier::bootstrap(v);
}

// ____________________________________________________________________________
unordered_map<int, int> KNNClassifier::classify(string path)
{
    // Store sum of squared differences in a
    // priority queue, delivers largest element, uses vector container
    priority_queue< pair<double,int>, vector<pair<double,int> >, lesserpair> q;

    // Get the feature vector for the input
    vector<double> fvec;
    string tname = path.substr(path.rfind("/")+1, string::npos);
    auto func = [](const string& s){ return stod(s); };
    util::parseFile<double>(fvec, func, _arg[1]+"/"+tname+".feature");

    // Compute the euclidean distance to every track in the annotated db
    int threads = omp_get_max_threads();
    #pragma omp parallel num_threads(threads) shared(q)
    {
        #pragma omp for schedule(dynamic)
            for (int k = 0; k < (int)_train.size(); k++) 
            {
                // Get feature vector
                vector<double> cmpvec;
                string t = _train[k];
                string name = t.substr(t.rfind("/")+1, string::npos);
                string featpath = _arg[3]+"/"+name+".feature";
                util::parseFile<double>(cmpvec, func, featpath);

                double sum = 0.0;
                for(size_t i = 0; i < fvec.size(); i++) 
                    sum+=pow(fvec[i]-cmpvec[i], 2.0);
                q.push(make_pair<double, int>(double(sqrt(sum)), int(k)));
            }
    }

    // Find nearest k neighbors, assign label of majority. If no clear
    // majority, label is chosen non deterministically from majority leaders
    unordered_map<int, int> hist;
    for (int i = 0; i < _k; i++)
    {
        auto top = q.top();
        int annot = _trainannot[top.second];
        auto index = hist.find(annot);
        if (index == hist.end()) 
            hist.insert(make_pair<int,int>(int(annot),int(0)));
        else
            hist.at(annot)++;
        q.pop();
    }
    return hist;
}