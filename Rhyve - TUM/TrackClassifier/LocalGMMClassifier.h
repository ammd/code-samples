
///////////////////////////////////////////////////////////////////////////////
// LocalGMMClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef LOCALGMMCLASSIFIER_H
#define LOCALGMMCLASSIFIER_H

#include <vector>
#include <string>
#include "TrackClassifier.h"


// Classify track features using gaussian mixture models for each track
class LocalGMMClassifier : public TrackClassifier
{
public:

    // Constructor, destructor.
    LocalGMMClassifier(): TrackClassifier() { _hasParams = 9; }
    ~LocalGMMClassifier() { }

    // Validate given arguments, build and save GMM models for every track
    // in the training database. 
    bool bootstrap(std::vector<std::string> params);

    // Using the features for the input track, assign the label corresponding
    // to the training model with the highest log likelihood
    std::unordered_map<int, int> classify(std::string input);

private:

	// The length of an atomic feature vector
	int _sampleLength;

    // Train models for each track in the training set
    bool _trainModels;

    // Number of nearest neighbors to consider
    int _k;

    // The number of gaussian components in the GMM
    int _gaussians;
};

#endif  // LOCALGMMCLASSIFIER_H

