
///////////////////////////////////////////////////////////////////////////////
// KNNClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef KNNCLASSIFIER_H
#define KNNCLASSIFIER_H

#include <vector>
#include <string>
#include "TrackClassifier.h"


// Classify track features using a nearest neighbor classifier with SSD metrics
class KNNClassifier : public TrackClassifier
{
public:

    // Constructor, destructor.
    KNNClassifier(): TrackClassifier() { _hasParams = 6; }
    ~KNNClassifier() { }

    // Validate given arguments, do any required preprocessing
    bool bootstrap(std::vector<std::string> params);

    // Classifies tracks using SSD metrics (euclidean distance), 
    // prints output to output file
    std::unordered_map<int, int> classify(std::string input);

private:

	// The number of neighbors to check during classification
	int _k;
};

#endif  // KNNCLASSIFIER_H

