
///////////////////////////////////////////////////////////////////////////////
// TrackClassifierMain.cpp
///////////////////////////////////////////////////////////////////////////////

#include "TrackClassifier.h"
#include "KNNClassifier.h"
#include "LocalGMMClassifier.h"
#include "GlobalGMMClassifier.h"
#include "GlobalRFClassifier.h"
#include "LocalRFClassifier.h"
#include "ANNClassifier.h"
#include "BoostClassifier.h"
#include "../com/util.h"
#include "MainState.h"
#include <boost/program_options.hpp>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <getopt.h>
#include <vector>
#include <string>

using namespace std;
namespace bpo = boost::program_options;

// Prints usage (parameters + options) of the program, and then exits
void printUsage(string assetsPath)
{   
    string printcmd = "groff -Tascii -man "+assetsPath+"/usage.1";
    if (system(printcmd.c_str()) < 0) {}
    exit(1);
}

// Parse command line arguments using Boost
vector<string> parseCmdLine(int argc, char** argv, MainState* state, 
                            TrackClassifier* classifier)
{
    bpo::options_description optDes("");
    optDes.add_options()
        ("help,h",                 "")
        ("dump,d",                 "")
        ("knearestneighbor,n",     "")
        ("localgmm,l",             "")
        ("globalgmm,g",            "")
        ("globalrandomforest,r",   "")
        ("localrandomforest,f",    "")
        ("neuralnetwork,a",        "")
        ("boost,b",                "")
        ("aggregate,t",            "")
        ("params",    bpo::value< vector<string> >(), "");

    bpo::positional_options_description posDes;
    posDes.add("params", -1);

    bpo::variables_map vm;
    try{
        bpo::store(bpo::command_line_parser(argc, argv).
            options(optDes).positional(posDes).run(), vm);
        bpo::notify(vm);
    } catch(std::exception& ex){
        cout << ex.what() << endl;   
    }
    
    // General program options
    if (vm.count("help")) printUsage(state->getAssetsPath());
    if (vm.count("dump"))  state->dump = true;
    if (vm.count("aggregate"))  state->aggregate = true;
    if (!vm.count("params")) printUsage(state->getAssetsPath());

    // Classifiers
    if (vm.count("knearestneighbor")) classifier = new KNNClassifier();
    if (vm.count("localgmm")) classifier = new LocalGMMClassifier();
    if (vm.count("globalgmm"))  classifier = new GlobalGMMClassifier();
    if (vm.count("globalrandomforest")) classifier = new GlobalRFClassifier();
    if (vm.count("localrandomforest")) classifier = new LocalRFClassifier();
    if (vm.count("neuralnetwork")) classifier = new ANNClassifier();
    if (vm.count("boost")) classifier = new BoostClassifier();

    return vm["params"].as<vector<string> >();
}

// command line options
struct option options[] =
{
    {"help",                 0, NULL, 'h'},
    {"dump",                 0, NULL, 'd'},
    {"knearestneighbor",     0, NULL, 'n'},
    {"localgmm",             0, NULL, 'l'},
    {"globalgmm",            0, NULL, 'g'},
    {"globalrandomforest",   0, NULL, 'r'},
    {"localrandomforest",    0, NULL, 'f'},
    {"neuralnetwork",        0, NULL, 'a'},
    {"boost",                0, NULL, 'b'},
    {"aggregate",            0, NULL, 't'},
    {NULL,                   0, NULL,  0 }
};


// Main routine, initialize and test the classification accuracy of the
// specified classifier.
int main(int argc, char** argv)
{
    // Load the program state
    MainState* state = new MainState();
    TrackClassifier* classifier = nullptr;
    vector<string> posArg = parseCmdLine(argc, argv, state, classifier);
    if (classifier == nullptr || !state->checkState()) 
        printUsage(state->getAssetsPath());

    // Boot up the classifier
    if(!classifier->bootstrap(posArg)) printUsage(state->getAssetsPath());

    // Perform classification
    if(!state->dump) classifier->getAccuracy();
    else classifier->classify(state->aggregate);

    // Cleanup
    delete classifier;
    return 0;
}
