///////////////////////////////////////////////////////////////////////////////
// GlobalRFClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <queue>
#include <time.h>
#include <omp.h>
#include <unordered_map>
#include <set>
#include "armadillo"
#include "TrackClassifier.h"
#include "GlobalRFClassifier.h"
#include "../com/util.h"
#include <opencv2/opencv.hpp>

using namespace arma;
using namespace cv;
using namespace std;

// ____________________________________________________________________________
bool GlobalRFClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;

    if (stoi(p[0]) < 1) return false;
    _forestsize = stoi(p[0]);

    if (stoi(p[1]) < 1) return false;
    _depth = stoi(p[1]);

    if (stoi(p[2]) < 0 || stoi(p[2]) > 1) return false;
    _trainForest = stoi(p[2]) == 0 ? false : true;

    if (stoi(p[3]) < 1) return false;
    _fvecsize = stoi(p[3]);

    if (stoi(p[4]) < 1) return false;
    _numFeat = stoi(p[4]);

    std::vector<std::string> v(p.begin()+5, p.end());
    if(!TrackClassifier::bootstrap(v)) return false;

    cout << "Bootstrapping RFClassifier ..." << endl;
    if (_trainForest)
    {
        // Build params
        set<int> unique(_trainannot.begin(), _trainannot.end());
        float minsample = _numFeat * _train.size() * 0.01f;
        
        CvRTParams rfparams(
            _depth,         // max depth
            minsample,      // min sample count
            0,              // regression accuracy
            true,           // use surrogates
            unique.size(),  // max categories
            0,              // priors
            false,          // calc var importance
            0,              // n active vars
            _forestsize,    // num trees
            0.01f,          // forest accuracy
            CV_TERMCRIT_ITER | CV_TERMCRIT_EPS // termcrit type
        );

        // Define as classification problem
        cv::Mat types(_fvecsize + 1, 1, CV_8UC1);
        types.setTo(cv::Scalar(CV_VAR_NUMERICAL));
        types.at<char>(_fvecsize, 0) = CV_VAR_CATEGORICAL;

        // Get training feature vectors and responses (class labels). 
        // Assumes frame features are stored in row major form.
        cv::Mat* data = new cv::Mat(_train.size()*_numFeat, _fvecsize, CV_32F);
        cv::Mat* resp = new cv::Mat(0, 1, CV_32SC1);
        for (size_t i = 0; i < _train.size(); i++)
        {
            // Get responses for all frames of current track
            cv::Mat curResp(_numFeat, 1, CV_32SC1, _trainannot[i]);
            resp->push_back(curResp);
            
            // Get training data
            string t = _train[i];
            string name = t.substr(t.rfind("/")+1, string::npos);
            string featpath = _arg[3]+"/"+name+".feature";
            
            arma::fmat fileMat;
            fileMat.load(featpath);
            for (int j = 0; j < (int)fileMat.n_rows; j++)
                for (int k =0; k < (int)fileMat.n_cols; k++)
                    data->at<float>(_numFeat*i+j, k) = fileMat(j, k);

            // TODO: manual vs. automatic matrix loading?
            /*vector<string> sfvec;
            auto func = [](const string& s){ return s; };
            vector<char> delim = {' '};
            util::parseFile<string>(sfvec, func, featpath);
            for (size_t k = 0; k < sfvec.size(); k++)
            {
                vector<string> temp = util::splitString(sfvec[k], delim);
                auto f = [](const string& s){ return stof(s); };
                vector<float> fvec;

                util::moveVector<string, float>(temp, fvec, f);
                for (int l = 0; l < (int)fvec.size(); l++)
                    curData.at<float>(k, l) = fvec[l]; 
            }*/            
        }

        // Train the random forest
        _rf.clear();
        _rf.train(*data, CV_ROW_SAMPLE, *resp, cv::Mat(),cv::Mat(),types,
                  cv::Mat(), rfparams);

        // write model
        _rf.save("./output/randomforest.xml");
        delete data;
        delete resp;
    }

    // Load Random Forest
    _rf.load("./output/randomforest.xml");
    return true;
}

// ____________________________________________________________________________
unordered_map<int, int> GlobalRFClassifier::classify(string path)
{
    // Get the feature vector for the input
    // Assumes frame features are stored in row major form.
    vector<string> sfvec;
    string tname = path.substr(path.rfind("/")+1, string::npos);
    string featpath = _arg[1]+"/"+tname+".feature";
    arma::fmat fileMat;
    fileMat.load(featpath);
    cv::Mat sample(_numFeat, _fvecsize, CV_32F);
    for (int j = 0; j < (int)fileMat.n_rows; j++)
        for (int k =0; k < (int)fileMat.n_cols; k++)
             sample.at<float>(j, k) = fileMat(j, k);
    

    // Classify the feature at each frame, build a histogram of votes
    unordered_map<int, int> hist;
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads) shared(hist)
    {
          #pragma omp for schedule(dynamic)
            for (int k = 0; k < sample.rows; k++)
            {
                // Histogram classification for current frame
                int clz = (_rf.predict(sample.row(k)));
                auto index = hist.find(clz);
                if (index == hist.end()) 
                    hist.insert(make_pair<int,int>(int(clz),int(1)));
                else
                    hist.at(clz)++;
            }
    }
    return hist;
}