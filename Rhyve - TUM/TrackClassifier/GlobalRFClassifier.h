
///////////////////////////////////////////////////////////////////////////////
// GlobalRFClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef GLOBALRFCLASSIFIER_H
#define GLOBALRFCLASSIFIER_H

#include <vector>
#include <string>
#include "TrackClassifier.h"
#include <opencv2/opencv.hpp>

using namespace cv; 


// Classify track features using a random forest
class GlobalRFClassifier : public TrackClassifier
{
public:

    // Constructor, destructor.
    GlobalRFClassifier(): TrackClassifier() { _hasParams = 10; }
    ~GlobalRFClassifier() { }

    // Validate given arguments, train and save a random forest for the given
    // training set. A training sample consists of a conglomerate feature vector
    // for a frame
    bool bootstrap(std::vector<std::string> params);

    // Using the individual frame features for the input track, assign the
    // label corresponding to the genre to which the majority of frames have
    // been classified
    std::unordered_map<int, int> classify(std::string input);

private:

    // The number of trees to train
    int _forestsize;

    // The depth of each tree
    int _depth;

    // Whether or not to train the classifier (or use an available one)
    bool _trainForest;

    // The size of each feature vector
    int _fvecsize;

    // The total number of features
    int _numFeat;

    // The random forest
    CvRTrees _rf;
};

#endif  // GLOBALRFCLASSIFIER_H

