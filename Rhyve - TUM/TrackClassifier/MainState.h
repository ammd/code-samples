
///////////////////////////////////////////////////////////////////////////////
// MainState.h
///////////////////////////////////////////////////////////////////////////////

#ifndef MAINSTATE_H
#define MAINSTATE_H

#include <iostream>
#include <stdlib.h>
#include <vector>
#include <string>
#include <unordered_map>
#include <utility>
#include "../com/util.h"

class MainState
{
public:

    // Set default values, read config.cfg file in executable directory
    MainState()
    {
        dump = false;
        aggregate = false;
        loadEnvironment();
    }

    // Returns true iff the current value of internals represents
    // a valid system configuration.
    bool checkState()
    {
        for (auto it = env.begin(); it != env.end(); ++it)
            if (it->second.compare("") == 0) return false;
        return true;
    }

    // Resource request API
    std::string getAssetsPath() const { return getVar("ASSETS_PATH"); }
    
    // Internal state
    bool dump, aggregate;

private:

    // The application environment
    std::unordered_map<std::string,std::string> env;

    // Returns the mapped string corresponding to the given value for the
    // environment specified via config.cfg
    const std::string getVar(std::string varName) const
    {
        auto it = env.find(varName);
        if (it == env.end())
        {
            std::cout << "Nonexistent resource: " << varName << std::endl;
            exit(1);
        }
        return it->second;
    }

    // Read config.cfg, load environment into memory
    void loadEnvironment()
    {
        std::vector<char> delim;
        delim.push_back('=');    
        std::vector<std::string> rows;
        util::parseFile<std::string>(rows,[&](std::string& s){return s;},"./config.cfg");
        for (unsigned int i = 0; i < rows.size(); i++)
        {
            auto cur = rows[i];
            if (cur.size() == 0 || cur.substr(0, 1).compare("#") == 0) continue;
            auto stringVec = util::splitString(cur, delim);
            if (stringVec.size() != 2)
            {
                std::cout << "Config file malformed: " << std::endl;
                for (unsigned int j = 0; j < stringVec.size(); j++)
                    std::cout << stringVec[j] << "|";
                std::cout << std::endl; 
                exit(1);
            }
            auto it = env.find(stringVec[0]);
            if (it == env.end()) 
                env.insert(make_pair(stringVec[0], stringVec[1]));
        }
    }
};

#endif  // MAINSTATE_H