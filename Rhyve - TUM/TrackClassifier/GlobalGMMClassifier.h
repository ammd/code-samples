
///////////////////////////////////////////////////////////////////////////////
// GlobalGMMClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef GLOBALGMMCLASSIFIER_H
#define GLOBALGMMCLASSIFIER_H

#include <vector>
#include <string>
#include <unordered_map>
#include "TrackClassifier.h"
#include <opencv2/opencv.hpp>

using namespace cv;


// Classify track features using a nearest neighbor classifier with SSD metrics
class GlobalGMMClassifier : public TrackClassifier
{
public:

    // Constructor, destructor.
    GlobalGMMClassifier(): TrackClassifier() { _hasParams = 8; }
    ~GlobalGMMClassifier() { }

    // Validate given arguments, build and save GMM models for every track
    // in the training database. 
    bool bootstrap(std::vector<std::string> params);

    // Using the features for the input track, assign the label corresponding
    // to the training model with the highest log likelihood
    std::unordered_map<int, int>  classify(std::string input);

private:

	// The length of a full feature vector (for entire track)
	int _sampleLength;

    // The number of mixture components in the GMM
    int _gaussians;

    // Train models for each track in the training set
    bool _trainModels;

    // The GMM models, one per genre
    std::vector<cv::EM> _models;
};

#endif  // GLOBALGMMCLASSIFIER_H

