///////////////////////////////////////////////////////////////////////////////
// ANNClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <queue>
#include <time.h>
#include <omp.h>
#include <unordered_map>
#include <set>
#include "armadillo"
#include "TrackClassifier.h"
#include "ANNClassifier.h"
#include "../com/util.h"
#include <opencv2/opencv.hpp>

using namespace arma;
using namespace cv;
using namespace std;

// ____________________________________________________________________________
bool ANNClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;

    if (stoi(p[0]) < 0 || stoi(p[0]) > 1) return false;
    _trainNetwork = stoi(p[0]) == 0 ? false : true;

    if (stoi(p[1]) < 1) return false;
    _fvecsize = stoi(p[1]);

    if (stoi(p[2]) < 1) return false;
    _numFeat = stoi(p[2]);

    std::vector<std::string> v(p.begin()+3, p.end());
    if(!TrackClassifier::bootstrap(v)) return false;

    cout << "Bootstrapping Neural Network ..." << endl;
    _network = new CvANN_MLP();

	if (_trainNetwork)
    {
        set<int> unique(_trainannot.begin(), _trainannot.end());
        vector<int> genres(unique.begin(), unique.end());

    	// Topology: input is ordered (not categorical), output is
        // categorical, so input layer has as many neurons as the length
        // of each feature vector, output layer has as many neurons as 
        // there are classes
        vector<int> layerSizes = {_fvecsize, 
                                  _fvecsize*3, 
                                  int(_fvecsize*1.618), 
                                  int(_fvecsize/1.618), 
                                  _fvecsize/3, 
                                  int(unique.size())};
        //vector<int> layerSizes =  { _fvecsize, (_fvecsize+int(unique.size()))/2, int(unique.size()) };
    	/*for (int layer = _fvecsize; layer > int(genres.size()); layer /= 1.618) 
    		layerSizes.push_back(layer);
        layerSizes.push_back(genres.size());*/

    	cv::Mat layers(1, layerSizes.size(), CV_32SC1);
    	for (size_t i = 0; i < layerSizes.size(); i++)
    		layers.at<int>(0, i) = layerSizes[i];
   
        // Set topology for the network, as well as the activation function
    	_network->create(layers, CvANN_MLP::SIGMOID_SYM, 1.0f, 1.0f);

        // Get training feature vectors and responses (class labels enconded
        // in vectors where the ith element is 1 iff the training sample
        // belongs to the ith class). 
        // Assumes frame features are stored in row major form.
        cv::Mat* data = new cv::Mat(_train.size()*_numFeat, _fvecsize, CV_32F);
        cv::Mat* resp = new cv::Mat(0, genres.size(), CV_32F);
        for (size_t i = 0; i < _train.size(); i++)
        {
            // Get responses for all frames of current track
            cv::Mat curResp(_numFeat, genres.size(), CV_32F, 0.0f);
            int index = -1;
            for (size_t j = 0; j < genres.size(); j++)
            {
                if (genres[j] == _trainannot[i])
                {
                    index = j;
                    break;
                }
            }
            curResp.col(index) += 1.0f;

            /*
            for (int j = 0; j < _numFeat; j++)
            {
                for (int k = 0; k < (int)unique.size(); k++)
                    cout << curResp.at<float>(j, k);
                cout << endl;
            }*/
            
            

            resp->push_back(curResp);
            
            // Get training data
            string t = _train[i];
            string name = t.substr(t.rfind("/")+1, string::npos);
            string featpath = _arg[3]+"/"+name+".feature";
            
            arma::fmat fileMat;
            fileMat.load(featpath);
            for (int j = 0; j < (int)fileMat.n_rows; j++)
                for (int k =0; k < (int)fileMat.n_cols; k++)
                    data->at<float>(_numFeat*i+j, k) = fileMat(j, k);

            // TODO: manual vs. automatic matrix loading?
            /*auto func = [](const string& s){ return s; };
            vector<char> delim = {' '};
            util::parseFile<string>(sfvec, func, featpath);
            for (size_t k = 0; k < sfvec.size(); k++)
            {
                vector<string> temp = util::splitString(sfvec[k], delim);
                auto f = [](const string& s){ return stof(s); };
                vector<float> fvec;

                util::moveVector<string, float>(temp, fvec, f);
                for (int l = 0; l < (int)fvec.size(); l++)
                    curData.at<float>(k, l) = fvec[l]; 
            }*/            
        }

        // Build params (see opencv documentation for default values) and train
        // the neural network
       	//int a = _network->train(*data, *resp, cv::Mat(), cv::Mat(), 
        //	            CvANN_MLP_TrainParams(), CvANN_MLP::NO_OUTPUT_SCALE);
        
        auto term_crit = cvTermCriteria( CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 
                           5000, 0.00000001f);
        CvANN_MLP_TrainParams params(term_crit, CvANN_MLP_TrainParams::RPROP, 
                                     0.1, FLT_EPSILON);
        int a = _network->train(*data, *resp, cv::Mat(), cv::Mat(), params, 
                                CvANN_MLP::NO_OUTPUT_SCALE);

        cout << "Iterations: " << a << endl;
        

        // write model
        _network->save("./output/neuralnetwork.xml");
        delete data;
        delete resp;
    }

    // Load Random Forest
    _network->load("./output/neuralnetwork.xml");
    return true;
}

// ____________________________________________________________________________
unordered_map<int, int> ANNClassifier::classify(string path)
{
    // Get the feature vector for the input
    // Assumes frame features are stored in row major form.
    vector<string> sfvec;
    string tname = path.substr(path.rfind("/")+1, string::npos);
    string featpath = _arg[1]+"/"+tname+".feature";
    arma::fmat fileMat;
    fileMat.load(featpath);
    cv::Mat sample(_numFeat, _fvecsize, CV_32F);
    for (int j = 0; j < (int)fileMat.n_rows; j++)
        for (int k =0; k < (int)fileMat.n_cols; k++)
             sample.at<float>(j, k) = fileMat(j, k);

    set<int> unique(_trainannot.begin(), _trainannot.end());
    vector<int> genres(unique.begin(), unique.end());
     
    // Classify the feature at each frame, build a histogram of votes
    unordered_map<int, int> hist;
    cv::Mat response(_numFeat, genres.size(), CV_32F);
    _network->predict(sample, response);

    /*
    for (int j = 0; j < _numFeat; j++)
    {
        for (int k = 0; k < (int)genres.size(); k++)
            cout << response.at<float>(j, k) << " ";
        cout << endl;
    }
    exit(1);
    */

    for (int i = 0; i < _numFeat; i++)
    {
        // Find max probability value in current row
        vector<float> row(genres.size());
        for (int j = 0; j < (int)genres.size(); j++) 
            row[j] = response.at<float>(i, j);

        auto it = max_element(row.begin(), row.end());
        auto maxindex = distance(row.begin(), it);
        int clz = genres[maxindex];
        auto index = hist.find(clz);
        if (index == hist.end()) 
            hist.insert(make_pair<int,int>(int(clz),int(1)));
        else
            hist.at(clz)++;
    }
    return hist;
}