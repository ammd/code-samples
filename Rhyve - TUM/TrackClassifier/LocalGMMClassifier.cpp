
///////////////////////////////////////////////////////////////////////////////
// LocalGMMClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <queue>
#include <time.h>
#include <omp.h>
#include <unordered_map>
#include "armadillo"
#include "TrackClassifier.h"
#include "LocalGMMClassifier.h"
#include "../com/util.h"
#include <opencv2/opencv.hpp>

using namespace arma;
using namespace cv;
using namespace std;

// ____________________________________________________________________________
bool LocalGMMClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;

    if (stoi(p[0]) < 1) return false;
    _sampleLength = stoi(p[0]);

    if (stoi(p[1]) < 0 || stoi(p[1]) > 1) return false;
    _trainModels = stoi(p[1]) == 0 ? false : true;

    if (stoi(p[2]) < 1) return false;
    _k = stoi(p[2]);

    if (stoi(p[3]) < 1) return false;
    _gaussians = stoi(p[3]);

    std::vector<std::string> v(p.begin()+4, p.end());
    if(!TrackClassifier::bootstrap(v)) return false;

    cout << "Bootstrapping LocalGMMClassifier ..." << endl;
    if (!_trainModels) return true;

    // Train and save a GMM for each track in the training set
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads)
    {
        #pragma omp for schedule(dynamic)
            for (int k = 0; k < (int)_train.size(); k++) 
            {
                if (threads == 1)
                {
                    if (k > 0) printf("\r                                 \r");
                    printf("Computing GMMs. Progress: %.1lf%%", 
                          ((float)k/(float)_train.size())*100);
                    fflush(stdout);
                }
                
                // Get feature vector, convert to samples matrix
                vector<double> fvec;
                string t = _train[k];
                string name = t.substr(t.rfind("/")+1, string::npos);
                string featpath = _arg[3]+"/"+name+".feature";
                
                // Convert to samples matrix
                arma::fmat mat;
                mat.load(featpath);
                cv::Mat* data = new cv::Mat(mat.n_rows, mat.n_cols, CV_32F);
                for (int j = 0; j < (int)mat.n_rows; j++)
                    for (int k =0; k < (int)mat.n_cols; k++)
                        data->at<float>(j, k) = mat(j, k);

                /*auto func = [](const string& s){ return stod(s); };
                util::parseFile<double>(fvec, func, featpath);
                cv::Mat m((int)fvec.size()/_sampleLength,_sampleLength,CV_64F);
                for (int i = 0; i < m.rows; i++)
                    for (int j = 0; j < m.cols; j++)
                        m.at<double>(i, j) = fvec[i*_sampleLength+j];*/

                // Train GMM
                cv::EM em(_gaussians);
                em.train(*data);

                // write model
                // TODO: figure out why only first level dir possible
                cv::FileStorage fs("./output/"+name+".model.xml",
                                   cv::FileStorage::WRITE);
                em.write(fs);
                delete data;
            }
    }
    printf("\r                                    \r");
    printf("Computing GMMs. Progress: 100.0%%");
    cout << endl << flush;
    return true;
}

// ____________________________________________________________________________
unordered_map<int, int> LocalGMMClassifier::classify(string path)
{
    // Store sum of squared differences in a
    // priority queue, delivers largest element, uses vector container
    priority_queue< pair<double,int>, vector<pair<double,int> >, lesserpair> q;

    // Get the feature vector for the input
    vector<double> fvec;
    string tname = path.substr(path.rfind("/")+1, string::npos);
    //auto func = [](const string& s){ return stod(s); };
    string featpath = _arg[1]+"/"+tname+".feature";

    // Convert to samples matrix
    arma::fmat mat;
    mat.load(featpath);
    cv::Mat* data = new cv::Mat(mat.n_rows, mat.n_cols, CV_32F);
    for (int j = 0; j < (int)mat.n_rows; j++)
        for (int k =0; k < (int)mat.n_cols; k++)
                data->at<float>(j, k) = mat(j, k);

    /*util::parseFile<double>(fvec, func, _arg[1]+"/"+tname+".feature");
    cv::Mat m((int)fvec.size()/_sampleLength,_sampleLength,CV_64F);
    for (int i = 0; i < m.rows; i++)
        for (int j = 0; j < m.cols; j++)
            m.at<double>(i, j) = fvec[i*_sampleLength+j];*/

    // Loop over all models (one per training element)
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads) shared(q)
    {
          #pragma omp for schedule(dynamic)
            for (int k = 0; k < (int)_train.size(); k++) 
            {
                /*
                if (threads == 1)
                {
                    if (k > 0) printf("\r                                 \r");
                    printf("Scanning GMMs. Progress: %.1lf%%", 
                          ((float)k/(float)_train.size())*100);
                    fflush(stdout);
                }*/

                // Get stored GMM
                string t = _train[k];
                string name = t.substr(t.rfind("/")+1, string::npos);
                string outpath = "./output/"+name+".model.xml";
                const cv::FileStorage fs(outpath, cv::FileStorage::READ);
                cv::EM model;
                if (fs.isOpened()) {
                    const FileNode& fn = fs[outpath];
                    model.read(fn);
                } 
                if (!model.isTrained()) 
                {
                    cout << "Model not trained! " << k << endl;
                    exit(1);
                }

                // Store prediction score
                double sum = 0.0;
                for (int i = 0; i < data->rows; i++)
                    sum += (model.predict(data->row(i)))[0];
                q.push(make_pair<double, int>(double(sum), int(k)));
            }
    }
    /*printf("\r                                    \r");
    printf("Scanning GMMs. Progress: 100.0%%");
    cout << endl << flush;*/

    // Find nearest k neighbors, assign label of majority. If no clear
    // majority, label is chosen non deterministically from majority leaders
    unordered_map<int, int> knn, hist;
    for (int i = 0; i < _k; i++)
    {
        auto top = q.top();
        int annot = _trainannot[top.second];
        auto index = knn.find(annot);
        if (index == knn.end()) 
            knn.insert(make_pair<int,int>(int(annot),int(1)));
        else
            knn.at(annot)++;
        q.pop();
    }

    auto it = max_element(knn.begin(), knn.end(),
      [](const pair<int, int>& p1, const pair<int, int>& p2) {
        return p1.second < p2.second; });

    // This local gmm classifier does not support classification on a frame
    // granularity, but rather on the entire track.
    hist.insert(make_pair<int,int>(int(it->first),int(data->rows)));
    delete data;
    return hist;
}