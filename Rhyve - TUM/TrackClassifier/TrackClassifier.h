
///////////////////////////////////////////////////////////////////////////////
// TrackClassifier.h
///////////////////////////////////////////////////////////////////////////////

#ifndef TRACKCLASSIFIER_H
#define TRACKCLASSIFIER_H

#include <vector>
#include <string>
#include <unordered_map>

// Comparison class for priority queue
class lesserpair
{
  public:
  lesserpair() {}
  bool operator()(const std::pair<double, int>& pair1, 
                  const std::pair<double, int>& pair2) const
  {
    return (pair1.first < pair2.first);
  }
};


// Represents an abstract classification strategy for track features.
class TrackClassifier
{
public:

    // Constructor, destructor
    TrackClassifier() { }
    virtual ~TrackClassifier() { }

    // Expect parameters [test manifest, test features dir, train manifest, 
    // train features dir, output]
    virtual bool bootstrap(std::vector<std::string> p);

    // Classifies the track under the given path, returns distribution of 
    // class labels, (label for track is i.e. most common label). 
    // Assumes track is part of test set, and so features have already been
    // computed and stored in the corresponding location.
    virtual std::unordered_map<int, int> classify(std::string input)=0;

    // Classifies all tracks in the test set, output a file with 
    // "<classification> = <file>" in output directory. The format of 
    // "<classification>" depends on the value of aggregate, see usage.1
    void classify(bool aggregate);

    // Test classification accuracy, uses classify
    void getAccuracy();

protected:

    // The list of arguments to this classifier
    std::vector<std::string> _arg;

    // Container holding the paths to each element in the training set
    std::vector<std::string> _train;

    // Container holding the paths to each element in the test set
    std::vector<std::string> _test;

    // Container holding the annotations for each track in _train
    std::vector<int> _trainannot;

    // Container holding the annotations for each track in _test
    std::vector<int> _testannot;

    // Number of expected global parameters
    int _hasParams;

private:

};

#endif  // TRACKCLASSIFIER_H

