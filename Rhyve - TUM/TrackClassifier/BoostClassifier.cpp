///////////////////////////////////////////////////////////////////////////////
// BoostClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <queue>
#include <time.h>
#include <omp.h>
#include <unordered_map>
#include <set>
#include "armadillo"
#include "TrackClassifier.h"
#include "BoostClassifier.h"
#include "../com/util.h"
#include <opencv2/opencv.hpp>

using namespace arma;
using namespace cv;
using namespace std;

// ____________________________________________________________________________
bool BoostClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;

    if (stoi(p[0]) < 0 || stoi(p[0]) > 1) return false;
    _trainBoosters = stoi(p[0]) == 0 ? false : true;

    if (stoi(p[1]) < 1) return false;
    _fvecsize = stoi(p[1]);

    if (stoi(p[2]) < 1) return false;
    _numFeat = stoi(p[2]);

    if (stoi(p[3]) < 1) return false;
    _wkcount = stoi(p[3]);

    if (stoi(p[4]) < 1) return false;
    _depth = stoi(p[4]);

    std::vector<std::string> v(p.begin()+5, p.end());
    if(!TrackClassifier::bootstrap(v)) return false;

    cout << "Bootstrapping BoostClassifier ..." << endl;
    cout << "WKcount = " << _wkcount << endl;
    cout << "Depth = " << _depth << endl;
    set<int> unique(_trainannot.begin(), _trainannot.end());
    vector<int> genres(unique.begin(), unique.end());
    _boosters = new vector<CvBoost>();
    _boosters->resize(genres.size());


    if (_trainBoosters)
    {
        // Using set of class labels, build a random forest for each class,
        // using the corresponding feature vectors
        int counter = 0;
        int threads = 1; //omp_get_max_threads();
        #pragma omp parallel num_threads(threads)
        {
            #pragma omp for schedule(dynamic)
                for (int k = 0; k < (int)genres.size(); k++)
                {
                    counter++;
                    if (threads == 1)
                    {
                        if (counter > 0) printf("\r                        \r");
                        printf("Computing Boosters. Progress: %.1lf%%", 
                              ((float)counter/(float)genres.size())*100);
                        fflush(stdout);
                    }

                    // Build params
                    CvBoostParams params(
                        CvBoost::REAL,      // Type of boosting algorithm
                        _wkcount,           // Number of weak classifiers
                        0.98,               // weight trim rate, used to
                                            // save computational time
                        _depth,             // max depth of tree
                        true,               // use surrogates
                        0                   // priors
                    );

                    // Define as classification problem
                    cv::Mat types(1, _fvecsize + 1, CV_8UC1);
                    types.setTo(cv::Scalar(CV_VAR_NUMERICAL));
                    types.at<char>(0, _fvecsize) = CV_VAR_CATEGORICAL;

                    // Get feature vectors for current genre
                    cv::Mat* data = new cv::Mat(0, _fvecsize, CV_32F);
                    cv::Mat* resp = new cv::Mat(0, 1, CV_32SC1);
                    for (size_t i = 0; i < _trainannot.size(); i++)
                    {
                        // Check if current annotation matches the genre
                        // the classifier should be trained on.
                        bool match = true;
                        if (_trainannot[i] != genres[k]) match = false;

                        // Get features, parse to matrix
                        string t = _train[i];
                        string name = t.substr(t.rfind("/")+1, string::npos);
                        string featpath = _arg[3]+"/"+name+".feature";
        
                        arma::fmat fileMat;
                        fileMat.load(featpath);
                        cv::Mat m(fileMat.n_rows, fileMat.n_cols, CV_32F);
                        for (int j = 0; j < (int)fileMat.n_rows; j++)
                            for (int k =0; k < (int)fileMat.n_cols; k++)
                                m.at<float>(j, k) = fileMat(j, k);

                        int annot = match ? 1 : 0;
                        cv::Mat curResp(_numFeat, 1, CV_32SC1, annot);
                        resp->push_back(curResp);
                        data->push_back(m);
                    }

                    // Train the random forest for the current genre
                    _boosters->at(k).train(*data, CV_ROW_SAMPLE, *resp, 
                                      cv::Mat(),cv::Mat(),types,
                                      cv::Mat(), params, false);

                    // write models
                    string save = "./output/boost" + std::to_string(k) + ".xml";
                    _boosters->at(k).save(save.c_str());
                    delete data;
                    delete resp;
                }
        }

        printf("\r                                    \r");
        printf("Computing Boosters. Progress: 100.0%%");
        cout << endl << flush;
    }

    // Load random forests
    for (size_t i = 0; i < genres.size(); i++)
    {
        string rfname = "./output/boost" + std::to_string(i) + ".xml";
        _boosters->at(i).load(rfname.c_str());
    }
    return true;
}

// ____________________________________________________________________________
unordered_map<int, int> BoostClassifier::classify(string path)
{
    // Get the feature vector for the input
    // Assumes frame features are stored in row major form.
    vector<string> sfvec;
    string tname = path.substr(path.rfind("/")+1, string::npos);
    string featpath = _arg[1]+"/"+tname+".feature";
    arma::fmat fileMat;
    fileMat.load(featpath);
    cv::Mat sample(_numFeat, _fvecsize, CV_32F);
    for (int j = 0; j < (int)fileMat.n_rows; j++)
        for (int k =0; k < (int)fileMat.n_cols; k++)
             sample.at<float>(j, k) = fileMat(j, k);
    

    // Classify the feature at each frame, build a histogram of votes
    unordered_map<int, int> hist;
    set<int> unique(_trainannot.begin(), _trainannot.end());
    vector<int> genres(unique.begin(), unique.end());

    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads) shared(hist)
    {
          #pragma omp for schedule(dynamic)
            for (int k = 0; k < sample.rows; k++)
            {
                // Each booster can "claim" the frame as its own
                // If no booster claims the frame, ignore it
                // If more than than booster claims the frame, add
                // them all to the final histogram
                for(size_t j = 0; j < _boosters->size(); j++)
                {
                    float clz = _boosters->at(j).predict(sample.row(k), 
                                                         cv::Mat(), 
                                                         Range::all(), 
                                                         false, false);
                    if (int(clz + 0.5f) == 1)
                    {
                        // Histogram classification for current frame
                        auto index = hist.find(genres[j]);
                        if (index == hist.end()) 
                            hist.insert(make_pair<int,int>
                                (int(genres[j]),int(1)));
                        else
                            hist.at(genres[j])++;
                    }
                        
                }
                

                
            }
    }
    return hist;
}