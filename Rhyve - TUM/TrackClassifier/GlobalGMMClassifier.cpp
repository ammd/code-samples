///////////////////////////////////////////////////////////////////////////////
// GlobalGMMClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <string>
#include <queue>
#include <time.h>
#include <omp.h>
#include <unordered_map>
#include <set>
#include "armadillo"
#include "TrackClassifier.h"
#include "GlobalGMMClassifier.h"
#include "../com/util.h"
#include <opencv2/opencv.hpp>

using namespace arma;
using namespace cv;
using namespace std;

// ____________________________________________________________________________
bool GlobalGMMClassifier::bootstrap(std::vector<std::string> p)
{
    if ((int)p.size() != _hasParams) return false;

    if (stoi(p[0]) < 1) return false;
    _sampleLength = stoi(p[0]);

    if (stoi(p[1]) < 0 || stoi(p[1]) > 1) return false;
    _trainModels = stoi(p[1]) == 0 ? false : true;

    if (stoi(p[2]) < 1) return false;
    _gaussians = stoi(p[2]);

    std::vector<std::string> v(p.begin()+3, p.end());
    if(!TrackClassifier::bootstrap(v)) return false;

    set<int> unique(_trainannot.begin(), _trainannot.end());
    vector<int> genres(unique.begin(), unique.end());

    cout << "Bootstrapping GlobalGMMClassifier ..." << endl;
    if (_trainModels)
    {
        int counter = 0;
        int threads = 1; //omp_get_max_threads();
        #pragma omp parallel num_threads(threads)
        {
            #pragma omp for schedule(dynamic)
                for (int k = 0; k < (int)genres.size(); k++)
                {
            	    counter++;
                    if (threads == 1)
                    {
                        if (counter > 0) printf("\r                        \r");
                        printf("Computing GMMs. Progress: %.1lf%%", 
                              ((float)counter/(float)genres.size())*100);
                        fflush(stdout);
                    }
    
                    // Get feature vectors for current genre
                    //vector<double> gvec;
                    cv::Mat* data = new cv::Mat(0, _sampleLength, CV_32F);
                    for (size_t i = 0; i < _trainannot.size(); i++)
                    {
                        if (_trainannot[i] != genres[k]) continue;
                        
                        // Convert to samples matrix
                        arma::fmat mat;
                        string t = _train[i];
                        string name = t.substr(t.rfind("/")+1, string::npos);
                        string featpath = _arg[3]+"/"+name+".feature";
                        mat.load(featpath);
                        cv::Mat cur(mat.n_rows, mat.n_cols, CV_32F);
                        for (int j = 0; j < (int)mat.n_rows; j++)
                            for (int l =0; l < (int)mat.n_cols; l++)
                                cur.at<float>(j, l) = mat(j, l);

                        data->push_back(cur);

                        /*vector<double> fvec;
                        auto func = [](const string& s){ return stod(s); };
                        util::parseFile<double>(fvec, func, featpath);
                        gvec.insert(gvec.end(), fvec.begin(), fvec.end());*/
                    }

                    /*// Convert to samples matrix
                    int rows = (int)gvec.size()/_sampleLength; 
                    cv::Mat m(rows,_sampleLength,CV_64F);
                    for (int i = 0; i < m.rows; i++)
                        for (int j = 0; j < m.cols; j++)
                            m.at<double>(i, j) = gvec[i*_sampleLength+j];*/
    
                    // Train GMM
                    cv::EM em(_gaussians);
                    em.train(*data);
    
                    // write model
                    // TODO: figure out why only first level dir possible
                    cv::FileStorage fs("./output/"+to_string(genres[k])+
                                       ".model.xml", cv::FileStorage::WRITE);
                    em.write(fs);
                    delete data;
                }
        }
        printf("\r                                    \r");
        printf("Computing GMMs. Progress: 100.0%%");
        cout << endl << flush;
    }

    // Load GMM models. This should be done here, as there will most likely
    // not be that many distinct genres, so the memory cost does not scale
    // linearly with the size of the database.
    for (int k = 0; k < (int)genres.size(); k++) 
    {
        // Get stored GMM
        int g = genres[k];
        string outpath = "./output/"+to_string(g)+".model.xml";
        const cv::FileStorage fs(outpath, cv::FileStorage::READ);
        cv::EM model;
        if (fs.isOpened()) {
            const FileNode& fn = fs[outpath];
            model.read(fn);
        } 
        if (!model.isTrained()) 
        {
            cout << "Model not trained! " << to_string(g) << endl;
            exit(1);
        }
        _models.push_back(model);
    }
    return true;
}

// ____________________________________________________________________________
unordered_map<int, int> GlobalGMMClassifier::classify(string path)
{
    // Store prediction scores for all genres
    set<int> unique(_trainannot.begin(), _trainannot.end());
    vector<int> genres(unique.begin(), unique.end());
    vector<double> scores(genres.size(), 0.0);

    // Get the feature vector for the input
    vector<double> fvec;
    string tname = path.substr(path.rfind("/")+1, string::npos);
    string featpath = _arg[1]+"/"+tname+".feature";
    //auto func = [](const string& s){ return stod(s); };
    //util::parseFile<double>(fvec, func, _arg[1]+"/"+tname+".feature");

    // Convert to samples matrix
    arma::fmat mat;
    mat.load(featpath);
    cv::Mat data(mat.n_rows, mat.n_cols, CV_32F);
    for (int j = 0; j < (int)mat.n_rows; j++)
        for (int l =0; l < (int)mat.n_cols; l++)
            data.at<float>(j, l) = mat(j, l);

    /*// Convert to samples matrix
    cv::Mat m((int)fvec.size()/_sampleLength,_sampleLength,CV_64F);
    for (int i = 0; i < m.rows; i++)
        for (int j = 0; j < m.cols; j++)
            m.at<double>(i, j) = fvec[i*_sampleLength+j];
    */

    
    // Loop over all genres
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads) shared(scores)
    {
          #pragma omp for schedule(dynamic)
            for (int k = 0; k < (int)genres.size(); k++)
            {
                // Store prediction score
                double sum = 0.0;
                for (int i = 0; i < data.rows; i++)
                    sum += (_models[k].predict(data.row(i)))[0];
                scores[k] = sum;
            }
    }

    auto it = max_element(scores.begin(), scores.end());
    auto index = distance(scores.begin(), it);

    // This local gmm classifier does not support classification on a frame
    // granularity, but rather on the entire track.
    unordered_map<int, int> hist;
    hist.insert(make_pair<int,int>(int(genres[index]),int(data.rows)));
    return hist;

    /*
    // Loop over all frames
    unordered_map<int, int> hist;
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads) shared(hist)
    {
          #pragma omp for schedule(dynamic)
            for (int k = 0; k < m.rows; k++)
            {
                vector<double> scores(genres.size(), 0.0);
                for (size_t i = 0; i < genres.size(); i++)
                    scores[k] = (_models[i].predict(m.row(k)))[0];

                // Get genre with highest score
                auto max = max_element(scores.begin(), scores.end());
                auto clz = genres[distance(scores.begin(), max)];
                auto index = hist.find(clz);
                if (index == hist.end()) 
                    hist.insert(make_pair<int,int>(int(clz),int(1)));
                else
                    hist.at(clz)++;
            }
    }
    return hist;*/
}