
///////////////////////////////////////////////////////////////////////////////
// FANNClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef FANNCLASSIFIER_H
#define FANNCLASSIFIER_H

#include <vector>
#include <string>
#include "TrackClassifier.h"

// Classify track features using a random forest
class FANNClassifier : public TrackClassifier
{
public:

    // Constructor, destructor.
    FANNClassifier(): TrackClassifier() { _hasParams = 8; }
    ~FANNClassifier() {  }

    // Validate given arguments, train and save a random forest for the given
    // training set. A training sample consists of a conglomerate feature vector
    // for a frame
    bool bootstrap(std::vector<std::string> params);

    // Using the individual frame features for the input track, assign the
    // label corresponding to the genre to which the majority of frames have
    // been classified
    std::unordered_map<int, int> classify(std::string input);

private:

    bool _train;

    // The size of each feature vector
    int _fvecsize;

    // The total number of features
    int _numFeat;
};

#endif  // FANNCLASSIFIER_H

