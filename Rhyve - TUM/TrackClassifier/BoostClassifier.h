
///////////////////////////////////////////////////////////////////////////////
// BoostClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef BOOSTCLASSIFIER_H
#define BOOSTCLASSIFIER_H

#include <vector>
#include <string>
#include "TrackClassifier.h"
#include <opencv2/opencv.hpp>

using namespace cv; 


// Classify track features using a random forest
class BoostClassifier : public TrackClassifier
{
public:

    // Constructor, destructor.
    BoostClassifier(): TrackClassifier() { _hasParams = 10; }
    ~BoostClassifier() { delete _boosters; }

    // Validate given arguments, train and save a random forest for the given
    // training set. A training sample consists of a conglomerate feature vector
    // for a frame
    bool bootstrap(std::vector<std::string> params);

    // Using the individual frame features for the input track, assign the
    // label corresponding to the genre to which the majority of frames have
    // been classified
    std::unordered_map<int, int> classify(std::string input);

private:

    // Whether or not to train the classifier (or use an available one)
    bool _trainBoosters;

    // The number of weak classifier to use during training
    int _wkcount;

    // The max depth of the trees
    int _depth;

    // The size of each feature vector
    int _fvecsize;

    // The total number of features
    int _numFeat;

    // The weak classifier trees, one per genre (since only binary 
    // classification problems are supported)
    std::vector<CvBoost>* _boosters;
};

#endif  // BOOSTCLASSIFIER_H

