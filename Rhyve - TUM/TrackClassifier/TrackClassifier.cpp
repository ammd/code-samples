
///////////////////////////////////////////////////////////////////////////////
// TrackClassifier.cpp
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <time.h>
#include <omp.h>
#include <string>
#include <set>
#include <algorithm>
#include "TrackClassifier.h"
#include "../com/util.h"

using namespace std;

// ____________________________________________________________________________
bool TrackClassifier::bootstrap(vector<string> p)
{
    _arg = p;
    vector<string> trainannot, testannot;
    vector<vector<string>* > tracks = { &trainannot, &_train};
    vector<vector<string>* > test = { &testannot, &_test};
    auto func = [](string s){ return atoi(s.c_str()); };

    util::parseFile("=", tracks, _arg[2]);
    util::moveVector<string, int>(trainannot, _trainannot, func);

    util::parseFile("=", test, _arg[0]);
    util::moveVector<string, int>(testannot, _testannot, func);
    return true;
}

// ____________________________________________________________________________
void TrackClassifier::getAccuracy()
{
    int hits = 0;
    time_t start, end;
    time(&start);
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads)
    {
        #pragma omp for schedule(dynamic)
            for (int k = 0; k < (int)_test.size(); k++)
            {
                if (threads == 1)
                {
                    if (k > 0) printf("\r                                 \r");
                    printf("Classification in progress: %.1lf%%", 
                          ((float)k/(float)_test.size())*100.0f);
                    fflush(stdout);
                }

                auto hist = classify(_test[k]);

                // Get class label with most votes
                auto it = max_element(hist.begin(), hist.end(),
                [](const pair<int, int>& p1, const pair<int, int>& p2) {
                return p1.second < p2.second; });

                if (it->first == _testannot[k]) hits++;
            }        
    }
    time(&end);
    printf("\r                                    \r");
    printf("Classification in progress: 100.0%%");
    cout << endl << flush;

    set<int> s;
    for(unsigned i = 0; i < _trainannot.size(); ++i ) s.insert(_trainannot[i]);
    
    cout << "Classification time: "<<difftime(end, start)<<" s"<<endl;
    cout << "Accuracy: " << hits << "/" << _test.size();
    cout << " = " << 100.0*(double)hits/_test.size() << "%" << endl;
    cout << "Baseline: 1/"<< s.size();
    cout << " = " << 100.0/s.size() << "%" << endl;
}

// ____________________________________________________________________________
void TrackClassifier::classify(bool aggregate)
{    
    // Store histogram responses
    vector<vector<float> > resp(_test.size());
    vector<vector<int> > clz(_test.size());

    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads) shared(clz)
    {
        #pragma omp for schedule(dynamic)
            for (int k = 0; k < (int)_test.size(); k++)
            {
                if (threads == 1)
                {
                    if (k > 0) printf("\r                                 \r");
                    printf("Classification in progress: %.1lf%%", 
                          ((float)k/(float)_test.size())*100.0f);
                    fflush(stdout);
                }

                // Classify the test input, returns a class distribution
                auto hist = classify(_test[k]);

                // If the result is to be aggregated, compute the class
                // with the highest frequency in the histogram
                if (aggregate)
                {
                    // Get class label with most votes
                    auto it = max_element(hist.begin(), hist.end(),
                    [](const pair<int, int>& p1, const pair<int, int>& p2) {
                    return p1.second < p2.second; });
                    vector<int> c = { it->first };
                    vector<float> r = { 1.0f };
                    clz[k] = c;
                    resp[k] = r;
                }

                else
                {
                    // Find norm, normalize output response
                    float sum = 0.0f;
                    for (auto it = hist.begin(); it != hist.end(); it++)
                        sum += it->second;

                    vector<float> curResp;
                    vector<int> curClz;
                    for (auto it = hist.begin(); it != hist.end(); it++)
                    {
                        curClz.push_back(it->first);
                        curResp.push_back((it->second)/sum);
                    }

                    clz[k] = curClz;
                    resp[k] = curResp;
                }
            }
    }
    printf("\r                                    \r");
    printf("Classification in progress: 100.0%%");
    cout << endl << flush;

    ofstream file(_arg[4], ofstream::out|ofstream::trunc);
    for (size_t i = 0; i < clz.size(); i++)
    {
        for (size_t j = 0; j < clz[i].size(); j++)
            file << clz[i][j] << ":" << resp[i][j] << " ";
        file << "= " << _test[i] << "\n";
    }
    file.close();
}