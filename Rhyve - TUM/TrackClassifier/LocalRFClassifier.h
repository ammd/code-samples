
///////////////////////////////////////////////////////////////////////////////
// LocalRFClassifier.h
//////////////////////////////////////////////////////////////////////////////

#ifndef LOCALRFCLASSIFIER_H
#define LOCALRFCLASSIFIER_H

#include <vector>
#include <string>
#include "TrackClassifier.h"
#include <opencv2/opencv.hpp>

using namespace cv; 


// Classify track features using a random forest
class LocalRFClassifier : public TrackClassifier
{
public:

    // Constructor, destructor.
    LocalRFClassifier(): TrackClassifier() { _hasParams = 10; }
    ~LocalRFClassifier() { delete _rfs; }

    // Validate given arguments, train and save a random forest for the given
    // training set. A training sample consists of a conglomerate feature vector
    // for a frame
    bool bootstrap(std::vector<std::string> params);

    // Using the individual frame features for the input track, assign the
    // label corresponding to the genre to which the majority of frames have
    // been classified
    std::unordered_map<int, int> classify(std::string input);

private:

    // The number of trees to train
    int _forestsize;

    // The depth of each tree
    int _depth;

    // Whether or not to train the classifier (or use an available one)
    bool _trainForest;

    // The size of each feature vector
    int _fvecsize;

    // The total number of features
    int _numFeat;

    // The random forests, one per class. Each random forest therefore
    // acts as a binary classifier.
    std::vector<CvRTrees>* _rfs;
};

#endif  // LOCALRFCLASSIFIER_H

