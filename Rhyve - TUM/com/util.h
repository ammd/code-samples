
///////////////////////////////////////////////////////////////////////////////
// util.h
//////////////////////////////////////////////////////////////////////////////

#ifndef UTIL_H
#define UTIL_H

#include <vector>
#include <string>
#include <functional>
#include <fstream>
#include <iostream>
#include "util.tcc"

namespace util
{
    // Stores a column in the file in each vector provided.
    // Assumes columns are separated by sep, expects an empty placeholder
    void parseFile(std::string sep, std::vector<std::vector<std::string>* >&
                   vecs, std::string path);

    // Parses an input unix path into a console executable version,
    // changes are made in place
    void parsePath(std::string& path);

    // Split the string on the given delimiters
    std::vector<std::string> splitString(std::string& s,
                                         std::vector<char>& delim);

    // Parses the contents of the file into the given vec, with one line
    // per entry, applies func as an adapter from string to T, i.e. atoi
    template<typename T>
    void parseFile(std::vector<T>& vec, std::function<T (std::string&)> 
                   func, std::string path);

    // Append contents of a vector to another, applying a given function 
    // to each element. Precondition: to is empty, Postcondition: from is empty
    template<typename T, typename S>
    void moveVector(std::vector<T>& from, std::vector<S>& to, 
                    std::function<S (T)> func);
}

#endif  // UTIL_H

