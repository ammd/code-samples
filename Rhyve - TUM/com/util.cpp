
///////////////////////////////////////////////////////////////////////////////
// util.cpp
///////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include "util.h"

using namespace std;

namespace util
{
    //__________________________________________________________________________
    vector<string> splitString(string& s, vector<char>& delim)
    {
        string delims = "";
        vector<string> strs;
        for(unsigned int i = 0; i < delim.size(); i++) delims += delim[i];
        boost::split(strs, s, boost::is_any_of(delims));
        for(unsigned int i = 0; i < strs.size(); i++) boost::trim(strs[i]);
        return strs;
    }


    //__________________________________________________________________________
    void parseFile(string sep, vector<vector<string>* >& vecs, string path)
    {
        /*
        if (vecs.size() > 0)
        {
            cout << "Error parsing file: input vector is not empty";
            exit(1);
        }*/
        vector<string> buffer;
        util::parseFile<string>(buffer, [&](const string& s) {return s;}, path);

        // If vecs is empty, then construct vectors here. Otherwise,
        // user has already provided containers of his own
        if (vecs.size() == 0) vecs.resize(buffer.size(), new vector<string>());
        reverse(buffer.begin(), buffer.end());
        while (buffer.size() > 0)
        {
            string line = buffer.back();
            buffer.pop_back();
            size_t colCount = 0;
            size_t start = 0;
            size_t cur = line.find(sep);
            while(cur != string::npos)
            {
                auto subline = line.substr(start, cur);
                subline.erase(0, subline.find_first_not_of(' '));
                subline.erase(subline.find_last_not_of(' ')+1);
                vecs[colCount++]->push_back(subline);
                start = cur+1;
                cur = line.find(sep, start);
            }
            auto subline = line.substr(start, cur);
            subline.erase(0, subline.find_first_not_of(' '));
            subline.erase(subline.find_last_not_of(' ')+1);
            vecs[colCount++]->push_back(subline);
        }
    }

    // ________________________________________________________________________
    void parsePath(string& path)
    {
        // Replace empty spaces in path with '\ '
        size_t index = path.find(" ");
        while (index != string::npos)
        {
            if (path.at(index-1)=='\\') continue;
            path.replace(index, 1, "\\ ");
            index = path.find(" ", index+2);
        }

        // Replace '(' with '\(', same with ')'
        index = path.find("(");
        while (index != string::npos)
        {
            if (path.at(index-1)=='\\') continue;
            path.replace(index, 1, "\\(");
            index = path.find("(", index+2);
        }

        index = path.find(")");
        while (index != string::npos)
        {
            if (path.at(index-1)=='\\') continue;
            path.replace(index, 1, "\\)");
            index = path.find(")", index+2);
        }

        // Replace '[' with '\[', same with ']'
        index = path.find("[");
        while (index != string::npos)
        {
            if (path.at(index-1)=='\\') continue;
            path.replace(index, 1, "\\[");
            index = path.find("[", index+2);
        }

        index = path.find("]");
        while (index != string::npos)
        {
            if (path.at(index-1)=='\\') continue;
            path.replace(index, 1, "\\]");
            index = path.find("]", index+2);
        }

        // Add escape sequence to single quotes
        index = path.find("'");
        while (index != string::npos)
        {
            if (path.at(index-1)=='\\') continue;
            path.replace(index, 1, "\\'");
            index = path.find("'", index+2);
        }

        // Escape '&'
        index = path.find("&");
        while (index != string::npos)
        {
            if (path.at(index-1)=='\\') continue;
            path.replace(index, 1, "\\&");
            index = path.find("&", index+2);
        }
    }
}
