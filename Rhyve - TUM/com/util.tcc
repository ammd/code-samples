
///////////////////////////////////////////////////////////////////////////////
// util.tcc
///////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>
#include <functional>
#include <fstream>
#include <iostream>
#include "util.h"

using namespace std;

namespace util
{
    // ________________________________________________________________________
    template<typename T>
    void parseFile(vector<T>& vec, function<T (string&)> func,string path)
    {
        ifstream file(path.c_str());
        if (file.is_open())
        {
            string line;
            while (file.good())
            {
                getline(file, line); 
                if (line.size()>0) vec.push_back(func(line));
            }
            file.close();
        } else {
            cout << "Util: Unable to open file: " << path << endl;
            exit(1);
        }
    }

    // ________________________________________________________________________
    template<typename T, typename S>
    void moveVector(vector<T>& from, vector<S>& to, function<S (T)> func)
    {
        reverse(from.begin(), from.end());
        while (from.size() > 0)
        {
            to.push_back(func(from.back()));
            from.pop_back();
        }
    }
}
