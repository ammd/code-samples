
///////////////////////////////////////////////////////////////////////////////
// HistUserModel.cpp
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include <iostream>
#include <set>
#include <math.h>
#include "HistUserModel.h"

using namespace std;

// ____________________________________________________________________________
bool HistUserModel::bootstrap(vector<string>* p)
{
	if ((int)p->size() != _hasParams) return false;
    return true;
}

// ____________________________________________________________________________
float HistUserModel::compare(UserModel* mod)
{
	HistUserModel* cmp = dynamic_cast<HistUserModel*>(mod);
	if (cmp == nullptr)
	{
		cout << "Invalid argument to HistUserModel::compare" << endl;
		exit(1);
	}

	// Get the set of keys
	set<string> keys;
	float localsum = 0.0f;
	float cmpsum = 0.0f;
	for (auto it = _hist.begin(); it != _hist.end(); it++)
	{
		keys.insert(it->first);
		localsum += it->second;
	}
	for (auto it = cmp->_hist.begin(); it != cmp->_hist.end(); it++)
	{
		keys.insert(it->first);
		cmpsum += it->second;
	}

	// Compare histograms using the euclidean distance on the normalized
	// histogram values
	float ssd = 0.0f;
	for (auto it = keys.begin(); it != keys.end(); it++)
	{
		if (_hist.find(*it) == _hist.end()) 
			ssd += pow(cmp->_hist.at(*it)/cmpsum, 2.0f);
		else if (cmp->_hist.find(*it) == cmp->_hist.end()) 
			ssd+=pow(_hist.at(*it)/localsum, 2.0f);
		else
			ssd+=pow(_hist.at(*it)/localsum - cmp->_hist.at(*it)/cmpsum, 2.0f);
	}
	return 100.0f * (1.0f- sqrt(ssd));
}

// ____________________________________________________________________________
void HistUserModel::updateModel(string obs, float weight)
{
    if (_hist.find(obs) == _hist.end()) 
        _hist.insert(make_pair<string,float>(string(obs),float(weight)));
    else
        _hist.at(obs) += weight;
}

// ____________________________________________________________________________
void HistUserModel::reset()
{
	_hist.clear();
}