
///////////////////////////////////////////////////////////////////////////////
// UserMatcher.cpp
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include "UserMatcher.h"
#include "HistUserModel.h"
#include "../com/util.h"

using namespace std;

// ____________________________________________________________________________
UserMatcher::UserMatcher()
{
	_modelFactory.insert(std::make_pair<modelType, modelBuilder>
        (HIST, [](){ return new HistUserModel(); }) );
}

// ____________________________________________________________________________
bool UserMatcher::bind(modelType type, vector<string> p)
{
    // Create dummy model, check if arguments are ok
    _mtype = type;
    auto dummy = getModel(_mtype)();
    if ((int)p.size() != dummy->_hasParams + _hasParams) return false;
    _marg.assign(p.begin(), p.begin()+ dummy->_hasParams);
    if (!dummy->bootstrap(&_marg)) return false;
    delete dummy;

    // Boot up the matcher
    vector<string> v(p.end()-_hasParams, p.end());
    return bootstrap(v);
}


// ____________________________________________________________________________
bool UserMatcher::bootstrap(vector<string> p)
{
    if ((int)p.size() != _hasParams) return false;
    for(auto &pm : p) 
        if(pm.compare("")==0 || pm.substr(0,1).compare(" ")==0) return false;

    _arg = p;


    // Parse input user
    //util::parseFile<string>(_inputUser, [](const string& s) { return s; }, _arg[0]);
    _usrClz.resize(1);
    vector<vector<string> > usertrackpath;
    usertrackpath.resize(1);
    vector<vector<string>* > userEntries = { &_usrClz[0], &usertrackpath[0]};
    util::parseFile("=", userEntries, _arg[0]);

    
    // Parse user directory
    util::parseFile<string>(_dir, [](const string& s) { return s; }, _arg[1]);
    _clz.resize(_dir.size());
    vector<vector<string> > trackpath;
    trackpath.resize(_dir.size());
    for (size_t i = 0; i < _dir.size(); i++)
    {
    	// Parse playlist, add classifications for each track on each list
    	vector<vector<string>* > entries = { &_clz[i], &trackpath[i]};
    	util::parseFile("=", entries, _dir[i]);
    }
    return true;
}