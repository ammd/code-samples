
///////////////////////////////////////////////////////////////////////////////
// LinearUserMatcher.cpp
///////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>
#include <fstream>
#include <omp.h>
#include "../com/util.h"
#include "UserModel.h"
#include "LinearUserMatcher.h"

using namespace std;

// Parse classification output, incorporate results into a model
void parseClassification(vector<string>& classOutput, UserModel* mod)
{
    vector<char> delim = {' '};
    for (size_t i = 0; i < classOutput.size(); i++)
    {
        vector<string> split =util::splitString(classOutput[i], delim);
        for (size_t l = 0; l < split.size(); l++)
        {
            string token = split[l];
            if (token.compare("=") == 0) break;
            auto found = token.find(":");
            if (found == string::npos) mod->updateModel(token, 1.0f);
            else mod->updateModel(token.substr(0, found), 
                                  stof(token.substr(found+1, string::npos)));
        }
    }
}

// ____________________________________________________________________________
bool LinearUserMatcher::bootstrap(vector<string> p)
{
	return UserMatcher::bootstrap(p);
}

// ____________________________________________________________________________
void LinearUserMatcher::getMatches(string inputUser)
{
    auto current = _usrClz[0];
    vector<float> matches;

    for (size_t j = 0; j < _clz.size(); j++)
    {
        auto base = getModel(_mtype)();
        base->bootstrap(&_marg);
        parseClassification(current, base);

        auto cmp = getModel (_mtype)();
        cmp->bootstrap(&_marg);
        parseClassification(_clz[j], cmp);

        matches.push_back(base->compare(cmp));
        delete base;
        delete cmp;
    }

    // Output scores
    ofstream file(_arg[2], ofstream::out|ofstream::trunc);
    for (size_t k = 0; k < matches.size(); k++)
        file << matches[k] << "\n";
    file.close();

}

// ____________________________________________________________________________
void LinearUserMatcher::getMatches()
{
	// Get comparison scores
	vector< vector<float> > adjMat;
	adjMat.resize(_clz.size());
    int threads = 1; //omp_get_max_threads();
    #pragma omp parallel num_threads(threads) shared(adjMat)
    {
        #pragma omp for schedule(dynamic)

            // Loop over all users, compare with all users that are further
            // down the list. Complexity: O(n2)
            for (size_t k = 0; k < _clz.size(); k++)
            {
                // Either a vector of single values or a vector of space
                // separated tuples of the form class:frequency, plus
                // delimiter and name of track
                auto current = _clz[k];

            	for (size_t j = k+1; j < _clz.size(); j++)
            	{
                	auto base = getModel(_mtype)();
                    base->bootstrap(&_marg);
                    parseClassification(current, base);

                	auto cmp = getModel (_mtype)();
                	cmp->bootstrap(&_marg);
                    parseClassification(_clz[j], cmp);

                	adjMat[k].push_back(base->compare(cmp));
                	delete base;
                	delete cmp;
            	}
            }
    }

    // Output scores
    ofstream file(_arg[2], ofstream::out|ofstream::trunc);
    for (size_t i = 0; i < adjMat.size(); i++)
    {
        file << "User: " << _dir[i] << "\n" << "        ";
        for (size_t k = 0; k < adjMat[i].size(); k++)
            file << adjMat[i][k] << "  ";
        file << "\n\n"; 
    }
    file.close();
}