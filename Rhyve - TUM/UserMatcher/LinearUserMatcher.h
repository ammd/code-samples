
///////////////////////////////////////////////////////////////////////////////
// LinearUserMatcher.h
///////////////////////////////////////////////////////////////////////////////

#ifndef LINEARUSERMATCHER_H
#define LINEARUSERMATCHER_H

#include <vector>
#include <string>
#include "UserMatcher.h"


// Matches users using a sequential search
class LinearUserMatcher : public UserMatcher
{
public:

    // Constructor, destructor
    LinearUserMatcher() : UserMatcher() { _hasParams = 3; }
    ~LinearUserMatcher() { }

    // Linearly compares every user to every other user, prints out
    // match scores for each pair of users.
    void getMatches();

    // Get all matches for one user (compares user to all other users)
    void getMatches(std::string inputUser);

private:

    // Perform any preprocessing required for the linear search
    bool bootstrap(std::vector<std::string> p);
};

#endif  // USERMATCHER_H

