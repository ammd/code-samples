
///////////////////////////////////////////////////////////////////////////////
// UserMatcher.h
///////////////////////////////////////////////////////////////////////////////

#ifndef USERMATCHER_H
#define USERMATCHER_H

#include <vector>
#include <string>
#include <functional>
#include <unordered_map>
#include "UserModel.h"


// Available model types:
// HIST:    Histogram to model genre distribution
enum modelType { modelType_begin, HIST = modelType_begin, modelType_end };


// Invoker function for all feature extractors
typedef std::function<UserModel* ()> modelBuilder;


// Matches a user to a set of other users based on music preferences
class UserMatcher
{
public:

    // Constructor, binds model enum ids to their constructors
    UserMatcher();

    // Destructor
    virtual ~UserMatcher() { }

    // Bind the given model to this matcher, validate its arguments
    // Expect global parameters [input user tracklist, tracklist directory]
    bool bind(modelType type, std::vector<std::string> p);

    // Prints out the matches for the current user tracklist and
    // user tracklist directory, from best match to worst
    // @aggregate flag signals if a single class value is to be expected,
    // or if the classification is given as a histogram, in the following
    // format: [class:frequency]* = <trackpath>
    virtual void getMatches()=0;

    // As above, but linearly compares a user under the given path
    // with all other users in _dir
    virtual void getMatches(std::string inputUser)=0;


protected:

    // Parse global parameters and perform any global preprocessing, 
    // i.e. parsing the users tracklists and building the user models. 
    // Return false iff parameters are malformed.
    virtual bool bootstrap(std::vector<std::string> p);

    // Returns a model's constructor based on its enum value
    modelBuilder getModel(modelType type) { return _modelFactory[type]; }

    // Dictionary mapping model enum to constructor
    std::unordered_map<modelType, modelBuilder, std::hash<int> > _modelFactory;

    // For each user, store the classifications of each track.
    // If @aggregate is true, then the innermost string consists of a single
    // value. Otherwise it is a list as specified in 'bootstrap'
    std::vector< std::vector<std::string> > _clz;

    // The user directory
    std::vector< std::string > _dir;

    // Classes for input user
    std::vector< std::vector<std::string> > _usrClz;

    // The type of model to create
    modelType _mtype;

    // The arguments given to this matcher, plus global parameters
    std::vector<std::string> _arg;

    // The arguments given to the models
    std::vector<std::string> _marg;    

    // Number of expected global parameters
    int _hasParams;
};

#endif  // USERMATCHER_H

