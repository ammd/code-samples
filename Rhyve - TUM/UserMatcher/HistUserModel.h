
///////////////////////////////////////////////////////////////////////////////
// HistUserModel.h
///////////////////////////////////////////////////////////////////////////////

#ifndef HISTUSERMODEL_H
#define HISTUSERMODEL_H

#include <vector>
#include <string>
#include <unordered_map>
#include <map>
#include "UserModel.h"

// Uses a histogram representation as a model for matching
class HistUserModel : public UserModel
{
public: 

	// Constructor, destructor
    HistUserModel() : UserModel() { _hasParams = 0; }
    ~HistUserModel() {}

    // Bootstrap this model with a set of arguments
    bool bootstrap(std::vector<std::string>* p);

    // Returns a percentage as how to alike this model is compared to mod.
    // Using the euclidean distance between the respective histograms.
    float compare(UserModel* mod);

    // Update the histogram based on a set of observations.
    void updateModel(std::string obs, float weight = 1.0);

    // Clear the histogram
    void reset();

    // Histogram modeling the user's music preference as a genre distribution
    std::unordered_map<std::string, float> _hist;

private:

};

#endif  // HISTUSERMODEL_H