
///////////////////////////////////////////////////////////////////////////////
// UserModel.h
///////////////////////////////////////////////////////////////////////////////

#ifndef USERMODEL_H
#define USERMODEL_H

#include <vector>
#include <string>

// Abstract class representing a user model
class UserModel
{
	friend class UserMatcher;
	
public: 

	// Constructor, destructor
    UserModel() { }
    virtual ~UserModel() { }

    // Bootstrap this model with a set of arguments
    virtual bool bootstrap(std::vector<std::string>* p)=0;

    // Returns a percentage as to how alike this model is compared to mod.
    virtual float compare(UserModel* mod)=0;

    // Update the current model based on an observation and a weight
    virtual void updateModel(std::string obs, float weight = 1.0)=0;

    // Rest the model, as if nothing has been observed.
    virtual void reset()=0;

protected:

	// The number of arguments expected by this model
	int _hasParams;

    // The parameters given to this model
    std::vector<std::string>* _arg;
};

#endif  // USERMODEL_H