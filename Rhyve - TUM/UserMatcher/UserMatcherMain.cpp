
///////////////////////////////////////////////////////////////////////////////
// UserMatcherMain.cpp
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <iostream>
#include <getopt.h>
#include <vector>
#include <string>
#include "UserMatcher.h"
#include "LinearUserMatcher.h"

using namespace std;

// Prints usage (parameters + options) of the program, and then exits
void printUsage()
{
    if (system("man ./assets/usage.1") < 0) {}
    exit(1);
}

// command line options
struct option options[] =
{
    {"help",              0, NULL, 'h'},
    {"histogram",         0, NULL, 'g'},
    {"linear",            0, NULL, 'l'},
    {NULL,                0, NULL,  0 }
};


// Main routine, initialize and test the classification accuracy of the
// specified classifier.
int main(int argc, char** argv)
{
    // Handler to the classifier to be used
    UserMatcher* matcher = nullptr;
    
    // Parse input
    optind = 1;
    int model = -1;
    while(true)
    {
        int c = getopt_long(argc, argv, "hgl", options, NULL);
        if (c == -1) break;
        switch (c)
        {
            case 'h' :  printUsage();
            case 'g' :  if (model != -1) printUsage();
                        model = HIST;
                        break;
            case 'l' :  if (matcher != nullptr) printUsage();
                        matcher = new LinearUserMatcher();
                        break;
            default  :  printUsage();
        }
    }
    if (model == -1 || matcher == nullptr) printUsage();
    vector<string> lineparams(argv+optind, argv+argc);

    // Bind the user model to the matcher, validate its arguments
    // and boot up the matcher
    if(!matcher->bind((modelType)model, lineparams)) printUsage();

    // Perform classification
    //matcher->getMatches("dummy");
    matcher->getMatches();
    return 0;
}
