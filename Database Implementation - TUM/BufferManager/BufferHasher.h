
///////////////////////////////////////////////////////////////////////////////
// BufferHasher.h
//////////////////////////////////////////////////////////////////////////////


#ifndef BUFFERHASHER_H
#define BUFFERHASHER_H

#include "BufferFrame.h"
#include <mutex>
#include <vector>



// Lookup proxy component for external classes, keeps track of
// pages / buffer frames in the page / frame pool.
// Implements a hash table as a lookup mechanism
class BufferHasher
{

public:

	// Constructor, defines how many buckets in the table
	FRIEND_TEST(BufferManagerTest, constructor);
	BufferHasher(uint64_t tableSize);
	
	// Destructor, deletes dynamic storage used for frames
	~BufferHasher();
	
	// Adds a BufferFrame to the hash table according to the
	// pageId value. Makes no assumptions about the frame itself.
	void insert(uint64_t pageId, BufferFrame* bf);
	
	// Removes the BufferFrame in the hash table that has this id.
	// Makes no assumptions about the frame itself.
	void remove(uint64_t pageId);

	// Locks the bucket corresponding to the given non hashed value
	void lockBucket(uint64_t value);

	// Unlocks the bucket corresponding to the given non hashed value
	void unlockBucket(uint64_t value);
	
	// Returns references to all of the BufferFrames associated with
	// the given pageId
	std::vector<BufferFrame*>* lookup(uint64_t pageId);

	// Retrieve the number of frames managed by this hasher
	uint64_t getSize();


private:

	// Given a page id returns the index of the bucket in the
	// hash table, in the range [0, tableSize)
	uint64_t hash(uint64_t pageId);

	// Hash table, at all times contains pointers to all frames
	FRIEND_TEST(BufferManagerTest, fixPageNoReplaceAndDestructor);
	std::vector< std::vector<BufferFrame*> > hashTable;
	
    // Handle concurrent access
	std::vector<std::mutex>* locks;

	// The number of frames managed by this hasher
	uint64_t size;
};


#endif  // BUFFERHASHER_H
