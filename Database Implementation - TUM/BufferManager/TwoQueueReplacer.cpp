
///////////////////////////////////////////////////////////////////////////////
// TwoQueueReplacer.cpp
///////////////////////////////////////////////////////////////////////////////


#include "BufferFrame.h"
#include "BufferHasher.h"
#include "FrameReplacer.h"
#include "BufferManager.h"

#include <sys/mman.h>

using namespace std;

//_____________________________________________________________________________
void TwoQueueReplacer::pageFixedFirstTime(BufferFrame* frame)
{
	fifo.push_front(frame);
}


//_____________________________________________________________________________
void TwoQueueReplacer::pageFixedAgain(BufferFrame* frame)
{
	// frame is in FIFO queue, move to LRU queue
	for(auto it=fifo.begin(); it!=fifo.end(); it++)
	{    
		BufferFrame* bf = *it;
		if (bf == frame)
		{
			fifo.erase(it);
			lru.push_front(frame);
			return;
		}
	}

    // frame is in LRU queue, move to front of LRU queue
	for(auto it=lru.begin(); it!=lru.end(); it++)
	{    
		BufferFrame* bf = *it;
		if (bf == frame)
		{
			lru.erase(it);
			lru.push_front(frame);
			return;
		}
	}
}

//_____________________________________________________________________________
BufferFrame* TwoQueueReplacer::getFrame()
{
	for(auto it=fifo.end(); it != fifo.begin(); )
	{  
		it--;
		BufferFrame* bf = *it;
		if(bf == nullptr) continue;
		if (!bf->pageFixed) return bf;			
	}


	for(auto it=lru.end(); it != lru.begin(); )
	{   
		it--;
		BufferFrame* bf = *it;
		if(bf == nullptr) continue;
		if (!bf->pageFixed) return bf;	
	}

	return nullptr;
}

//_____________________________________________________________________________
void TwoQueueReplacer::replaceFrame(BufferFrame* frame)
{	
	for(auto it=fifo.end(); it != fifo.begin(); )
	{   
		it--; 
		BufferFrame* bf = *it;
		if(bf == nullptr) continue;
		if (bf == frame)
		{
            fifo.erase(it);
            
            // free data in frame, update frame lookup mechanism
            // note: since page in frame is unfixed, it is also clean, since
            // dirty pages are written back to disk when they are unfixed.
            if (munmap(bf->data, BM_CONS::pageSize) < 0)
            {
            	cout << "Failed unmapping main memory: " << errno << endl;
				exit(1);            
            }
            bf->data = nullptr;
			return;
		}
	}

	for(auto it=lru.end(); it != lru.begin(); )
	{   
		it--;
		BufferFrame* bf = *it;
		if(bf == nullptr) continue;
		if (bf == frame)
		{
            lru.erase(it);
            
            // free data in frame, update frame lookup mechanism
            // note: since page in frame is unfixed, it is also clean, since
            // dirty pages are written back to disk when they are unfixed.
            if (munmap(bf->data, BM_CONS::pageSize) < 0)
            {
            	cout << "Failed unmapping main memory: " << errno << endl;
				exit(1);            
            }
            bf->data = nullptr;
			return;
		}
	}
    return;
}

/*
//_____________________________________________________________________________
BufferFrame* TwoQueueReplacer::replaceFrame()
{	
	for(auto it=fifo.end(); it != fifo.begin(); )
	{   
		it--; 
		BufferFrame* bf = *it;
		if(bf == nullptr) continue;
		if (!bf->pageFixed)
		{
			hasher->lockBucket(bf->pageId);

			// Someone fixed the page since last checked as unfixed
			// TODO: lock not necessarily exclusive?
			if(!bf->tryLockFrame(true))
			{
				hasher->unlockBucket(bf->pageId);				
				continue;
			}

            fifo.erase(it);
            
            // free data in frame, update frame lookup mechanism
            // note: since page in frame is unfixed, it is also clean, since
            // dirty pages are written back to disk when they are unfixed.
            if (munmap(bf->data, BM_CONS::pageSize) < 0)
            {
            	cout << "Failed unmapping main memory: " << errno << endl;
				exit(1);            
            }
            bf->data = nullptr;
            hasher->remove(bf->pageId);
            hasher->unlockBucket(bf->pageId);
			return bf;
		}
	}

	for(auto it=lru.end(); it != lru.begin(); )
	{   
		it--;
		BufferFrame* bf = *it;
		if(bf == nullptr) continue;
		if (!bf->pageFixed)
		{
			hasher->lockBucket(bf->pageId);

			// Someone fixed the page since last checked as unfixed
			if(!bf->tryLockFrame(true))
			{
				hasher->unlockBucket(bf->pageId);				
				continue;
			}

			lru.erase(it);
            
            // reset frame
            if (munmap(bf->data, BM_CONS::pageSize) < 0)
            {
            	cout << "Failed unmapping main memory: " << errno << endl;
				exit(1);            
            }
            bf->data = nullptr;
            hasher->remove(bf->pageId);
            hasher->unlockBucket(bf->pageId);
         	return bf;
		}
	}
    return nullptr;
}*/



