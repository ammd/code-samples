// Copyright 2011
// University of Freiburg
// Mauricio Munoz

#include <stdio.h>
#include <gtest/gtest.h>
#include <vector>
#include <utility>
#include "./RoadNetwork.h"


// _____________________________________________________________________________
TEST(RoadNetworkTest, constructor)
{
  // Global variables
  RoadNetwork rn;
  ASSERT_EQ(0, static_cast<int>(rn._nodeVector.size()));
  ASSERT_EQ(0, rn._numArcs);
  ASSERT_EQ(0, rn._dequeued.size());
  ASSERT_EQ(0, rn._heuristicValue.size());
  ASSERT_EQ(0, rn._idMap.size());

  // Check speeds table
  ASSERT_EQ(rn._speeds["motorway"], 110);
  ASSERT_EQ(rn._speeds["trunk"], 110);
  ASSERT_EQ(rn._speeds["primary"], 70);
  ASSERT_EQ(rn._speeds["secondary"], 60);
  ASSERT_EQ(rn._speeds["tertiary"], 50);
  ASSERT_EQ(rn._speeds["motorway_link"], 50);
  ASSERT_EQ(rn._speeds["trunk_link"], 50);
  ASSERT_EQ(rn._speeds["primary_link"], 50);
  ASSERT_EQ(rn._speeds["secondary_link"], 50);
  ASSERT_EQ(rn._speeds["road"], 40);
  ASSERT_EQ(rn._speeds["unclassified"], 40);
  ASSERT_EQ(rn._speeds["residential"], 30);
  ASSERT_EQ(rn._speeds["unsurfaced"], 30);
  ASSERT_EQ(rn._speeds["living_street"], 10);
  ASSERT_EQ(rn._speeds["service"], 5);
}

// _____________________________________________________________________________
TEST(RoadNetworkTest, readFromOsmFile)
{
  // Write simple OSM file.
  FILE* file = fopen("RoadNetworkTest.TMP.xml", "w");
  ASSERT_TRUE(file != NULL);
  fprintf(file, "<?xml blabla>\n"
                "<osm>\n"
                "<node id=\"176\" lat=\"54.1\" lon=\"12.7\" blabliblu />\n"
                "<node id=\"59\" lat=\"40.1\" lon=\"11.7\" blabliblu />\n"
                "<node id=\"37\" lat=\"64.1\" lon=\"13.7\" blabliblu />\n"
                "<way id=\"999\">\n"
                "  <nd ref=\"59\"/>\n"
                "  <nd ref=\"37\"/>\n"
                "  <nd ref=\"176\"/>\n"
                "  <nd ref=\"59\"/>\n"
                "  <tag k=\"highway\" v=\"motorway\"/>\n"
                "</way>\n"
                "</osm>");
  fclose(file);

  RoadNetwork rn;
  rn.readFromOsmFile("RoadNetworkTest.TMP.xml");

  // Sizes of global data structures
  ASSERT_EQ(3, rn._idMap.size());
  ASSERT_EQ(3, rn._nodeVector.size());
  ASSERT_EQ(6, rn._numArcs);
  ASSERT_EQ(3, rn._dequeued.size());
  ASSERT_EQ(3, rn._heuristicValue.size());

  // Content of _heuristicValue should all be -1 (uninitialized)
  ASSERT_EQ(-1.0, rn._heuristicValue[0]);
  ASSERT_EQ(-1.0, rn._heuristicValue[1]);
  ASSERT_EQ(-1.0, rn._heuristicValue[2]);

  // Content of _dequeued should all be false
  ASSERT_FALSE(rn._dequeued[0]);
  ASSERT_FALSE(rn._dequeued[1]);
  ASSERT_FALSE(rn._dequeued[2]);

  // Check contents of the id map
  ASSERT_EQ(0, rn._idMap[176]);
  ASSERT_EQ(1, rn._idMap[59]);
  ASSERT_EQ(2, rn._idMap[37]);

  // Check content of individual nodes
  ASSERT_EQ(0, rn._nodeVector[0].id);
  ASSERT_EQ(54.1, rn._nodeVector[0].latitude);
  ASSERT_EQ(12.7, rn._nodeVector[0].longitude);

  ASSERT_EQ(1, rn._nodeVector[1].id);
  ASSERT_EQ(40.1, rn._nodeVector[1].latitude);
  ASSERT_EQ(11.7, rn._nodeVector[1].longitude);

  ASSERT_EQ(2, rn._nodeVector[2].id);
  ASSERT_EQ(64.1, rn._nodeVector[2].latitude);
  ASSERT_EQ(13.7, rn._nodeVector[2].longitude);

  // Check arcs of the graph
  ASSERT_EQ(2,  rn._nodeVector[0].adjacencyList[0].destId);
  ASSERT_EQ(1,  rn._nodeVector[0].adjacencyList[1].destId);
  ASSERT_EQ(2,  rn._nodeVector[1].adjacencyList[0].destId);
  ASSERT_EQ(0,  rn._nodeVector[1].adjacencyList[1].destId);
  ASSERT_EQ(1,  rn._nodeVector[2].adjacencyList[0].destId);
  ASSERT_EQ(0,  rn._nodeVector[2].adjacencyList[1].destId);
}

// _____________________________________________________________________________
TEST(RoadNetworkTest, makeArcs)
{
  // Make 3 individual sample nodes, insert them into the graph
  RoadNetwork rn;
  Node node0, node1, node2;
  node0.id = 0;
  node1.id = 1;
  node2.id = 2;

  node0.longitude = 2;
  node0.latitude = 0;

  node1.longitude = 5;
  node1.latitude = 0;

  node2.longitude = 9;
  node2.latitude = 0;

  rn._nodeVector.push_back(node0);
  rn._nodeVector.push_back(node1);
  rn._nodeVector.push_back(node2);

  rn._idMap.insert(pair<int, int>(0, 0));
  rn._idMap.insert(pair<int, int>(1, 1));
  rn._idMap.insert(pair<int, int>(2, 2));

  vector<int> nodeIds;
  nodeIds.push_back(node0.id);
  nodeIds.push_back(node1.id);
  nodeIds.push_back(node2.id);

  rn.makeArcs(nodeIds, "motorway");
  double max = rn._speeds["motorway"];

  ASSERT_EQ(4, rn._numArcs);

  // Check the correct formation of arcs between the nodes, including costs
  // for "motorway" as a way type.
  ASSERT_EQ(1, rn._nodeVector[0].adjacencyList[0].destId);
  ASSERT_EQ(0, rn._nodeVector[1].adjacencyList[0].destId);
  ASSERT_EQ(2, rn._nodeVector[1].adjacencyList[1].destId);
  ASSERT_EQ(1, rn._nodeVector[2].adjacencyList[0].destId);

  ASSERT_EQ(double(3*1000000/max), rn._nodeVector[0].adjacencyList[0].cost);
  ASSERT_EQ(double(3*1000000/max), rn._nodeVector[1].adjacencyList[0].cost);
  ASSERT_EQ(double(4*1000000/max), rn._nodeVector[1].adjacencyList[1].cost);
  ASSERT_EQ(double(4*1000000/max), rn._nodeVector[2].adjacencyList[0].cost);
}

// _____________________________________________________________________________
TEST(RoadNetworkTest, computeShortestPath)
{
  // Model of the example graph from the first lecture
  RoadNetwork rn;
  Node node0, node1, node2, node3, node4, node5, node6, node7;
  node0.id = 0;
  node1.id = 1;
  node2.id = 2;
  node3.id = 3;
  node4.id = 4;
  node5.id = 5;
  node6.id = 6;
  node7.id = 7;

  // Best case scenario for A* (in _heuristicValue)
  rn._idMap.insert(pair<int, int>(node0.id, 0));
  rn._nodeVector.push_back(node0);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node1.id, 1));
  rn._nodeVector.push_back(node1);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(10.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node2.id, 2));
  rn._nodeVector.push_back(node2);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(9.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node3.id, 3));
  rn._nodeVector.push_back(node3);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(3.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node4.id, 4));
  rn._nodeVector.push_back(node4);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node5.id, 5));
  rn._nodeVector.push_back(node5);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(0.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node6.id, 6));
  rn._nodeVector.push_back(node6);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(2.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node7.id, 7));
  rn._nodeVector.push_back(node7);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(1.0);
  rn._costToNode.push_back(-1.0);

  // Insert arcs into the graph
  Arc arc1, arc2, arc3, arc4, arc5, arc6, arc7, arc8, arc9, arc10;

  arc1.destId = 1;
  arc1.cost = 1;
  rn._nodeVector[0].adjacencyList.push_back(arc1);

  arc2.destId = 2;
  arc2.cost = 1;
  rn._nodeVector[0].adjacencyList.push_back(arc2);

  arc3.destId = 3;
  arc3.cost = 5;
  rn._nodeVector[0].adjacencyList.push_back(arc3);

  arc4.destId = 4;
  arc4.cost = 2;
  rn._nodeVector[1].adjacencyList.push_back(arc4);

  arc5.destId = 4;
  arc5.cost = 1;
  rn._nodeVector[2].adjacencyList.push_back(arc5);

  arc6.destId = 5;
  arc6.cost = 12;
  rn._nodeVector[2].adjacencyList.push_back(arc6);

  arc7.destId = 6;
  arc7.cost = 1;
  rn._nodeVector[3].adjacencyList.push_back(arc7);

  arc8.destId = 5;
  arc8.cost = 8;
  rn._nodeVector[4].adjacencyList.push_back(arc8);

  arc9.destId = 5;
  arc9.cost = 1;
  rn._nodeVector[7].adjacencyList.push_back(arc9);

  arc10.destId = 7;
  arc10.cost = 1;
  rn._nodeVector[6].adjacencyList.push_back(arc10);

  vector<int> shortestPath;

  // Test: Dijkstra
  // With regular Dijkstra, all nodes in this sample graph are settled
  ASSERT_EQ(8, rn.computeShortestPath(rn.getNode(0), rn.getNode(5), 0, shortestPath)); 
  ASSERT_EQ(0, shortestPath[0]);
  ASSERT_EQ(3, shortestPath[1]);
  ASSERT_EQ(6, shortestPath[2]);
  ASSERT_EQ(7, shortestPath[3]);
  ASSERT_EQ(5, shortestPath[4]);

  shortestPath.clear();

  // This method call will force the next execution of computeShortestPath
  // to call the subprocedure getEuclideanDistance. In this test case
  // the distances between nodes are explicitly given instead of calculated
  // via this procedure, as above. This is why _heuristicValue is set after calling
  // resetNetwork.
  rn.resetNetwork();

  rn._heuristicValue[0] =  8.0;
  rn._heuristicValue[1] =  10.0;
  rn._heuristicValue[2] =  9.0;
  rn._heuristicValue[3] =  3.0;
  rn._heuristicValue[4] =  8.0;
  rn._heuristicValue[5] =  0.0;
  rn._heuristicValue[6] =  2.0;
  rn._heuristicValue[7] =  1.0;


  // Test: A* straight line
  // With A*, only 5 nodes in this graph are settled (Path 0->3->6->7->5)
  ASSERT_EQ(5, rn.computeShortestPath(rn.getNode(0), rn.getNode(5), 1, shortestPath));
  ASSERT_EQ(0, shortestPath[0]);
  ASSERT_EQ(3, shortestPath[1]);
  ASSERT_EQ(6, shortestPath[2]);
  ASSERT_EQ(7, shortestPath[3]);
  ASSERT_EQ(5, shortestPath[4]);

  rn.resetNetwork();

  // Test: A* random landmarks
  // Make graph above non directed
  Arc arc11, arc12, arc13, arc14, arc15, arc16, arc17, arc18, arc19, arc20;

  arc11.destId = 0;
  arc11.cost = 1;
  rn._nodeVector[1].adjacencyList.push_back(arc11);

  arc12.destId = 0;
  arc12.cost = 1;
  rn._nodeVector[2].adjacencyList.push_back(arc12);

  arc13.destId = 0;
  arc13.cost = 5;
  rn._nodeVector[3].adjacencyList.push_back(arc13);

  arc14.destId = 1;
  arc14.cost = 2;
  rn._nodeVector[4].adjacencyList.push_back(arc14);

  arc15.destId = 2;
  arc15.cost = 1;
  rn._nodeVector[4].adjacencyList.push_back(arc15);

  arc16.destId = 2;
  arc16.cost = 12;
  rn._nodeVector[5].adjacencyList.push_back(arc16);

  arc17.destId = 3;
  arc17.cost = 1;
  rn._nodeVector[6].adjacencyList.push_back(arc17);

  arc18.destId = 4;
  arc18.cost = 8;
  rn._nodeVector[5].adjacencyList.push_back(arc18);

  arc19.destId = 7;
  arc19.cost = 1;
  rn._nodeVector[5].adjacencyList.push_back(arc19);

  arc20.destId = 6;
  arc20.cost = 1;
  rn._nodeVector[7].adjacencyList.push_back(arc20);


  // Define nodes 2 and 7 as random landmarks
  Landmark l1, l2;

  l1.node = &node2;
  l2.node = &node7;

  rn.addLandmark(l1);
  rn.resetNetwork();

  rn.addLandmark(l2);
  rn.resetNetwork();

  rn.updateNodeLandmarkVector();

  shortestPath.clear();

  // Test: A* random landmarks
  // With this variant, a total of seven nodes are settled
  // (worked through by hand), and the shortest path is the same as above
  ASSERT_EQ(7, rn.computeShortestPath(rn.getNode(0), rn.getNode(5), 2, shortestPath));
  ASSERT_EQ(0, shortestPath[0]);
  ASSERT_EQ(3, shortestPath[1]);
  ASSERT_EQ(6, shortestPath[2]);
  ASSERT_EQ(7, shortestPath[3]);
  ASSERT_EQ(5, shortestPath[4]);

}

// _____________________________________________________________________________
TEST(RoadNetworkTest, updateNodeLandmarkVector)
{
  // Model of the example graph from the first lecture
  RoadNetwork rn;
  Node node0, node1, node2, node3, node4, node5, node6, node7;
  node0.id = 0;
  node1.id = 1;
  node2.id = 2;
  node3.id = 3;
  node4.id = 4;
  node5.id = 5;
  node6.id = 6;
  node7.id = 7;

  rn._idMap.insert(pair<int, int>(node0.id, 0));
  rn._nodeVector.push_back(node0);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node1.id, 1));
  rn._nodeVector.push_back(node1);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node2.id, 2));
  rn._nodeVector.push_back(node2);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node3.id, 3));
  rn._nodeVector.push_back(node3);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node4.id, 4));
  rn._nodeVector.push_back(node4);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node5.id, 5));
  rn._nodeVector.push_back(node5);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node6.id, 6));
  rn._nodeVector.push_back(node6);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node7.id, 7));
  rn._nodeVector.push_back(node7);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);


  // Insert arcs into the graph
  Arc arc1, arc2, arc3, arc4, arc5, arc6, arc7, arc8, arc9, arc10;

  arc1.destId = 1;
  arc1.cost = 1;
  rn._nodeVector[0].adjacencyList.push_back(arc1);

  arc2.destId = 2;
  arc2.cost = 1;
  rn._nodeVector[0].adjacencyList.push_back(arc2);

  arc3.destId = 3;
  arc3.cost = 5;
  rn._nodeVector[0].adjacencyList.push_back(arc3);

  arc4.destId = 4;
  arc4.cost = 2;
  rn._nodeVector[1].adjacencyList.push_back(arc4);

  arc5.destId = 4;
  arc5.cost = 1;
  rn._nodeVector[2].adjacencyList.push_back(arc5);

  arc6.destId = 5;
  arc6.cost = 12;
  rn._nodeVector[2].adjacencyList.push_back(arc6);

  arc7.destId = 6;
  arc7.cost = 1;
  rn._nodeVector[3].adjacencyList.push_back(arc7);

  arc8.destId = 5;
  arc8.cost = 8;
  rn._nodeVector[4].adjacencyList.push_back(arc8);

  arc9.destId = 5;
  arc9.cost = 1;
  rn._nodeVector[7].adjacencyList.push_back(arc9);

  arc10.destId = 7;
  arc10.cost = 1;
  rn._nodeVector[6].adjacencyList.push_back(arc10);

  // Make graph above non directed
  Arc arc11, arc12, arc13, arc14, arc15, arc16, arc17, arc18, arc19, arc20;

  arc11.destId = 0;
  arc11.cost = 1;
  rn._nodeVector[1].adjacencyList.push_back(arc11);

  arc12.destId = 0;
  arc12.cost = 1;
  rn._nodeVector[2].adjacencyList.push_back(arc12);

  arc13.destId = 0;
  arc13.cost = 5;
  rn._nodeVector[3].adjacencyList.push_back(arc13);

  arc14.destId = 1;
  arc14.cost = 2;
  rn._nodeVector[4].adjacencyList.push_back(arc14);

  arc15.destId = 2;
  arc15.cost = 1;
  rn._nodeVector[4].adjacencyList.push_back(arc15);

  arc16.destId = 2;
  arc16.cost = 12;
  rn._nodeVector[5].adjacencyList.push_back(arc16);

  arc17.destId = 3;
  arc17.cost = 1;
  rn._nodeVector[6].adjacencyList.push_back(arc17);

  arc18.destId = 4;
  arc18.cost = 8;
  rn._nodeVector[5].adjacencyList.push_back(arc18);

  arc19.destId = 7;
  arc19.cost = 1;
  rn._nodeVector[5].adjacencyList.push_back(arc19);

  arc20.destId = 6;
  arc20.cost = 1;
  rn._nodeVector[7].adjacencyList.push_back(arc20);


  Landmark l1;
  l1.node = &node2;
  rn.addLandmark(l1);
  rn.resetNetwork();

  Landmark l2;
  l2.node = &node7;
  rn.addLandmark(l2);
  rn.resetNetwork();

  rn.updateNodeLandmarkVector();

  // Node 0
  ASSERT_EQ(1, rn.getNode(0)->landmarkDist[0]);
  ASSERT_EQ(7, rn.getNode(0)->landmarkDist[1]);

  // Node 1
  ASSERT_EQ(2, rn.getNode(1)->landmarkDist[0]);
  ASSERT_EQ(8, rn.getNode(1)->landmarkDist[1]);

  // Node 2
  ASSERT_EQ(0, rn.getNode(2)->landmarkDist[0]);
  ASSERT_EQ(8, rn.getNode(2)->landmarkDist[1]);

  // Node 3
  ASSERT_EQ(6, rn.getNode(3)->landmarkDist[0]);
  ASSERT_EQ(2, rn.getNode(3)->landmarkDist[1]);

  // Node 4
  ASSERT_EQ(1, rn.getNode(4)->landmarkDist[0]);
  ASSERT_EQ(9, rn.getNode(4)->landmarkDist[1]);

  // Node 5
  ASSERT_EQ(9, rn.getNode(5)->landmarkDist[0]);
  ASSERT_EQ(1, rn.getNode(5)->landmarkDist[1]);

  // Node 6
  ASSERT_EQ(7, rn.getNode(6)->landmarkDist[0]);
  ASSERT_EQ(1, rn.getNode(6)->landmarkDist[1]);

  // Node 7
  ASSERT_EQ(8, rn.getNode(7)->landmarkDist[0]);
  ASSERT_EQ(0, rn.getNode(7)->landmarkDist[1]);
}

// _____________________________________________________________________________
TEST(RoadNetworkTest, addLandmark)
{
  // Model of the example graph from the first lecture
  RoadNetwork rn;
  Node node0, node1, node2, node3, node4, node5, node6, node7;
  node0.id = 0;
  node1.id = 1;
  node2.id = 2;
  node3.id = 3;
  node4.id = 4;
  node5.id = 5;
  node6.id = 6;
  node7.id = 7;

  rn._idMap.insert(pair<int, int>(node0.id, 0));
  rn._nodeVector.push_back(node0);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node1.id, 1));
  rn._nodeVector.push_back(node1);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node2.id, 2));
  rn._nodeVector.push_back(node2);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node3.id, 3));
  rn._nodeVector.push_back(node3);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node4.id, 4));
  rn._nodeVector.push_back(node4);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node5.id, 5));
  rn._nodeVector.push_back(node5);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node6.id, 6));
  rn._nodeVector.push_back(node6);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node7.id, 7));
  rn._nodeVector.push_back(node7);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);


  // Insert arcs into the graph
  Arc arc1, arc2, arc3, arc4, arc5, arc6, arc7, arc8, arc9, arc10;

  arc1.destId = 1;
  arc1.cost = 1;
  rn._nodeVector[0].adjacencyList.push_back(arc1);

  arc2.destId = 2;
  arc2.cost = 1;
  rn._nodeVector[0].adjacencyList.push_back(arc2);

  arc3.destId = 3;
  arc3.cost = 5;
  rn._nodeVector[0].adjacencyList.push_back(arc3);

  arc4.destId = 4;
  arc4.cost = 2;
  rn._nodeVector[1].adjacencyList.push_back(arc4);

  arc5.destId = 4;
  arc5.cost = 1;
  rn._nodeVector[2].adjacencyList.push_back(arc5);

  arc6.destId = 5;
  arc6.cost = 12;
  rn._nodeVector[2].adjacencyList.push_back(arc6);

  arc7.destId = 6;
  arc7.cost = 1;
  rn._nodeVector[3].adjacencyList.push_back(arc7);

  arc8.destId = 5;
  arc8.cost = 8;
  rn._nodeVector[4].adjacencyList.push_back(arc8);

  arc9.destId = 5;
  arc9.cost = 1;
  rn._nodeVector[7].adjacencyList.push_back(arc9);

  arc10.destId = 7;
  arc10.cost = 1;
  rn._nodeVector[6].adjacencyList.push_back(arc10);



  Landmark l1;
  l1.node = &node0;

  // Since regular Dijkstra is being used, all nodes should be settled.
  ASSERT_EQ(8, rn.addLandmark(l1)); 
  ASSERT_EQ(0, rn._landmarks[0][0]);
  ASSERT_EQ(1, rn._landmarks[0][1]);
  ASSERT_EQ(1, rn._landmarks[0][2]);
  ASSERT_EQ(5, rn._landmarks[0][3]);
  ASSERT_EQ(2, rn._landmarks[0][4]);
  ASSERT_EQ(8, rn._landmarks[0][5]);
  ASSERT_EQ(6, rn._landmarks[0][6]);
  ASSERT_EQ(7, rn._landmarks[0][7]);

  rn.resetNetwork();

  Landmark l2;
  l2.node = &node2;

  // Starting from node 2, only nodes 2, 4, 5 should be settled, due to
  // the one way arcs above. The rest of the nodes should be unreachable,
  // leaving a value of -1 in ls cost datastructure in the corresponding places
  ASSERT_EQ(3,  rn.addLandmark(l2)); 
  ASSERT_EQ(-1, rn._landmarks[1][0]);
  ASSERT_EQ(-1, rn._landmarks[1][1]);
  ASSERT_EQ(0,  rn._landmarks[1][2]);
  ASSERT_EQ(-1, rn._landmarks[1][3]);
  ASSERT_EQ(1,  rn._landmarks[1][4]);
  ASSERT_EQ(9,  rn._landmarks[1][5]);
  ASSERT_EQ(-1, rn._landmarks[1][6]);
  ASSERT_EQ(-1, rn._landmarks[1][7]);
}


// _____________________________________________________________________________
TEST(RoadNetworkTest, addLandmarks)
{
  // Model of the example graph from the first lecture
  RoadNetwork rn;
  Node node0, node1, node2, node3, node4, node5, node6, node7;
  node0.id = 0;
  node1.id = 1;
  node2.id = 2;
  node3.id = 3;
  node4.id = 4;
  node5.id = 5;
  node6.id = 6;
  node7.id = 7;

  rn._idMap.insert(pair<int, int>(node0.id, 0));
  rn._nodeVector.push_back(node0);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node1.id, 1));
  rn._nodeVector.push_back(node1);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node2.id, 2));
  rn._nodeVector.push_back(node2);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node3.id, 3));
  rn._nodeVector.push_back(node3);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node4.id, 4));
  rn._nodeVector.push_back(node4);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node5.id, 5));
  rn._nodeVector.push_back(node5);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node6.id, 6));
  rn._nodeVector.push_back(node6);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node7.id, 7));
  rn._nodeVector.push_back(node7);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(8.0);
  rn._costToNode.push_back(-1.0);


  // Insert arcs into the graph
  Arc arc1, arc2, arc3, arc4, arc5, arc6, arc7, arc8, arc9, arc10;

  arc1.destId = 1;
  arc1.cost = 1;
  rn._nodeVector[0].adjacencyList.push_back(arc1);

  arc2.destId = 2;
  arc2.cost = 1;
  rn._nodeVector[0].adjacencyList.push_back(arc2);

  arc3.destId = 3;
  arc3.cost = 5;
  rn._nodeVector[0].adjacencyList.push_back(arc3);

  arc4.destId = 4;
  arc4.cost = 2;
  rn._nodeVector[1].adjacencyList.push_back(arc4);

  arc5.destId = 4;
  arc5.cost = 1;
  rn._nodeVector[2].adjacencyList.push_back(arc5);

  arc6.destId = 5;
  arc6.cost = 12;
  rn._nodeVector[2].adjacencyList.push_back(arc6);

  arc7.destId = 6;
  arc7.cost = 1;
  rn._nodeVector[3].adjacencyList.push_back(arc7);

  arc8.destId = 5;
  arc8.cost = 8;
  rn._nodeVector[4].adjacencyList.push_back(arc8);

  arc9.destId = 5;
  arc9.cost = 1;
  rn._nodeVector[7].adjacencyList.push_back(arc9);

  arc10.destId = 7;
  arc10.cost = 1;
  rn._nodeVector[6].adjacencyList.push_back(arc10);

  // Make graph above non directed
  Arc arc11, arc12, arc13, arc14, arc15, arc16, arc17, arc18, arc19, arc20;

  arc11.destId = 0;
  arc11.cost = 1;
  rn._nodeVector[1].adjacencyList.push_back(arc11);

  arc12.destId = 0;
  arc12.cost = 1;
  rn._nodeVector[2].adjacencyList.push_back(arc12);

  arc13.destId = 0;
  arc13.cost = 5;
  rn._nodeVector[3].adjacencyList.push_back(arc13);

  arc14.destId = 1;
  arc14.cost = 2;
  rn._nodeVector[4].adjacencyList.push_back(arc14);

  arc15.destId = 2;
  arc15.cost = 1;
  rn._nodeVector[4].adjacencyList.push_back(arc15);

  arc16.destId = 2;
  arc16.cost = 12;
  rn._nodeVector[5].adjacencyList.push_back(arc16);

  arc17.destId = 3;
  arc17.cost = 1;
  rn._nodeVector[6].adjacencyList.push_back(arc17);

  arc18.destId = 4;
  arc18.cost = 8;
  rn._nodeVector[5].adjacencyList.push_back(arc18);

  arc19.destId = 7;
  arc19.cost = 1;
  rn._nodeVector[5].adjacencyList.push_back(arc19);

  arc20.destId = 6;
  arc20.cost = 1;
  rn._nodeVector[7].adjacencyList.push_back(arc20);


  // First "random" landmark is node 2
  Landmark l2;
  l2.node = &node2;

  vector<Landmark> temp;
  temp.push_back(l2);

  // Node 5 is 9 units away, thus making it the farthest node from node 2
  ASSERT_EQ(5,  rn.addLandmark(temp));
  rn.resetNetwork();


  Landmark l5;
  l5.node = &node5;

  temp.push_back(l5);
  
  // Node 3 is 3 units away from nodes 2,5 (worked through by hand)
  ASSERT_EQ(3,  rn.addLandmark(temp));
  rn.resetNetwork();


  Landmark l3;
  l3.node = &node3;

  temp.push_back(l3);
  
  // Node 1 is 2 units away from nodes 2,5,3 (worked through by hand)
  ASSERT_EQ(1,  rn.addLandmark(temp));
  rn.resetNetwork();


  // From this point on all remaining nodes are distance 1 away from 2,5,3,1
}


// _____________________________________________________________________________
TEST(RoadNetworkTest, partitionRoadNetwork)
{
  // Simple grid like graph with 9 nodes, one per grid square
  RoadNetwork rn;
  Node node0, node1, node2, node3, node4, node5, node6, node7, node8;
  node0.id = 0;
  node1.id = 1;
  node2.id = 2;
  node3.id = 3;
  node4.id = 4;
  node5.id = 5;
  node6.id = 6;
  node7.id = 7;
  node8.id = 8;

  node0.longitude = 0;
  node0.latitude = 0;

  node1.longitude = 2;
  node1.latitude = 0;

  node2.longitude = 4;
  node2.latitude = 0;

  node3.longitude = 0;
  node3.latitude = 2;

  node4.longitude = 2;
  node4.latitude = 2;

  node5.longitude = 4;
  node5.latitude = 2;

  node6.longitude = 0;
  node6.latitude = 4;

  node7.longitude = 2;
  node7.latitude = 4;

  node8.longitude = 4;
  node8.latitude = 4;

  rn._idMap.insert(pair<int, int>(node0.id, 0));
  rn._nodeVector.push_back(node0);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node1.id, 1));
  rn._nodeVector.push_back(node1);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node2.id, 2));
  rn._nodeVector.push_back(node2);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node3.id, 3));
  rn._nodeVector.push_back(node3);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node4.id, 4));
  rn._nodeVector.push_back(node4);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node5.id, 5));
  rn._nodeVector.push_back(node5);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node6.id, 6));
  rn._nodeVector.push_back(node6);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node7.id, 7));
  rn._nodeVector.push_back(node7);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node8.id, 8));
  rn._nodeVector.push_back(node8);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);


  // Add arcs
  Arc arc1, arc2, arc3, arc4, arc5, arc6, arc7, arc8;

  arc1.destId = 7;
  arc1.sourceId = 6;
  arc1.cost = 2;
  rn._nodeVector[6].adjacencyList.push_back(arc1);

  arc2.destId = 8;
  arc2.sourceId = 7;
  arc2.cost = 2;
  rn._nodeVector[7].adjacencyList.push_back(arc2);

  arc3.destId = 5;
  arc3.sourceId = 8;
  arc3.cost = 2;
  rn._nodeVector[8].adjacencyList.push_back(arc3);

  arc4.destId = 3;
  arc4.sourceId = 6;
  arc4.cost = 2;
  rn._nodeVector[6].adjacencyList.push_back(arc4);

  arc5.destId = 4;
  arc5.sourceId = 3;
  arc5.cost = 2;
  rn._nodeVector[3].adjacencyList.push_back(arc5);

  arc6.destId = 1;
  arc6.sourceId = 4;
  arc6.cost = 2;
  rn._nodeVector[4].adjacencyList.push_back(arc6);

  arc7.destId = 2;
  arc7.sourceId = 1;
  arc7.cost = 2;
  rn._nodeVector[1].adjacencyList.push_back(arc7);

  arc8.destId = 5;
  arc8.sourceId = 2;
  arc8.cost = 2;
  rn._nodeVector[2].adjacencyList.push_back(arc8);

  rn.partitionRoadNetwork(16);

  // Test for correct region positions
  ASSERT_EQ(16,  rn.getNumRegions());
  ASSERT_EQ(0,  rn._regions[0].longitude);
  ASSERT_EQ(4,  rn._regions[0].latitude);
  ASSERT_EQ(1,  rn._regions[1].longitude);
  ASSERT_EQ(4,  rn._regions[1].latitude);
  ASSERT_EQ(2,  rn._regions[2].longitude);
  ASSERT_EQ(4,  rn._regions[2].latitude);
  ASSERT_EQ(3,  rn._regions[3].longitude);
  ASSERT_EQ(4,  rn._regions[3].latitude);
  ASSERT_EQ(3,  rn._regions[15].longitude);
  ASSERT_EQ(1,  rn._regions[15].latitude);

  // Test for correct node assignment
  ASSERT_EQ(6,  rn._regions[0].nodes[0]->id);
  ASSERT_EQ(7,  rn._regions[2].nodes[0]->id);
  ASSERT_EQ(8,  rn._regions[3].nodes[0]->id);
  ASSERT_EQ(3,  rn._regions[8].nodes[0]->id);
  ASSERT_EQ(4,  rn._regions[10].nodes[0]->id);
  ASSERT_EQ(5,  rn._regions[11].nodes[0]->id);
  ASSERT_EQ(0,  rn._regions[12].nodes[0]->id);
  ASSERT_EQ(1,  rn._regions[14].nodes[0]->id);
  ASSERT_EQ(2,  rn._regions[15].nodes[0]->id);

  ASSERT_EQ(0,  rn.getNode(6)->region);
  ASSERT_EQ(2,  rn.getNode(7)->region);
  ASSERT_EQ(3,  rn.getNode(8)->region);
  ASSERT_EQ(8,  rn.getNode(3)->region);
  ASSERT_EQ(10,  rn.getNode(4)->region);
  ASSERT_EQ(11,  rn.getNode(5)->region);
  ASSERT_EQ(12,  rn.getNode(0)->region);
  ASSERT_EQ(14,  rn.getNode(1)->region);
  ASSERT_EQ(15,  rn.getNode(2)->region);

  // Test for correct boundary node evaluation
  ASSERT_EQ(6,  rn._regions[0].boundaryNodes[0]->id);
  ASSERT_EQ(7,  rn._regions[2].boundaryNodes[0]->id);
  ASSERT_EQ(8,  rn._regions[3].boundaryNodes[0]->id);
  ASSERT_EQ(3,  rn._regions[8].boundaryNodes[0]->id);
  ASSERT_EQ(4,  rn._regions[10].boundaryNodes[0]->id);
  ASSERT_EQ(1,  rn._regions[14].boundaryNodes[0]->id);
  ASSERT_EQ(2,  rn._regions[15].boundaryNodes[0]->id);
}

// _____________________________________________________________________________
TEST(RoadNetworkTest, setArcFlags)
{
  // Simple grid like graph with 9 nodes, one per grid square
  RoadNetwork rn;
  Node node0, node1, node2, node3, node4, node5, node6, node7, node8;
  node0.id = 0;
  node1.id = 1;
  node2.id = 2;
  node3.id = 3;
  node4.id = 4;
  node5.id = 5;
  node6.id = 6;
  node7.id = 7;
  node8.id = 8;

  node0.longitude = 0;
  node0.latitude = 0;

  node1.longitude = 2;
  node1.latitude = 0;

  node2.longitude = 4;
  node2.latitude = 0;

  node3.longitude = 0;
  node3.latitude = 2;

  node4.longitude = 2;
  node4.latitude = 2;

  node5.longitude = 4;
  node5.latitude = 2;

  node6.longitude = 0;
  node6.latitude = 4;

  node7.longitude = 2;
  node7.latitude = 4;

  node8.longitude = 4;
  node8.latitude = 4;

  rn._idMap.insert(pair<int, int>(node0.id, 0));
  rn._nodeVector.push_back(node0);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node1.id, 1));
  rn._nodeVector.push_back(node1);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node2.id, 2));
  rn._nodeVector.push_back(node2);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node3.id, 3));
  rn._nodeVector.push_back(node3);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node4.id, 4));
  rn._nodeVector.push_back(node4);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node5.id, 5));
  rn._nodeVector.push_back(node5);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node6.id, 6));
  rn._nodeVector.push_back(node6);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node7.id, 7));
  rn._nodeVector.push_back(node7);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  rn._idMap.insert(pair<int, int>(node8.id, 8));
  rn._nodeVector.push_back(node8);
  rn._dequeued.push_back(false);
  rn._heuristicValue.push_back(-1.0);
  rn._costToNode.push_back(-1.0);

  // Add arcs
  Arc arc1, arc2, arc3, arc4, arc5, arc6, arc7, arc8;

  arc1.destId = 7;
  arc1.sourceId = 6;
  arc1.cost = 2;
  rn._nodeVector[6].adjacencyList.push_back(arc1);

  arc2.destId = 8;
  arc2.sourceId = 7;
  arc2.cost = 2;
  rn._nodeVector[7].adjacencyList.push_back(arc2);

  arc3.destId = 5;
  arc3.sourceId = 8;
  arc3.cost = 2;
  rn._nodeVector[8].adjacencyList.push_back(arc3);

  arc4.destId = 3;
  arc4.sourceId = 6;
  arc4.cost = 2;
  rn._nodeVector[6].adjacencyList.push_back(arc4);

  arc5.destId = 4;
  arc5.sourceId = 3;
  arc5.cost = 2;
  rn._nodeVector[3].adjacencyList.push_back(arc5);

  arc6.destId = 1;
  arc6.sourceId = 4;
  arc6.cost = 2;
  rn._nodeVector[4].adjacencyList.push_back(arc6);

  arc7.destId = 2;
  arc7.sourceId = 1;
  arc7.cost = 2;
  rn._nodeVector[1].adjacencyList.push_back(arc7);

  arc8.destId = 5;
  arc8.sourceId = 2;
  arc8.cost = 2;
  rn._nodeVector[2].adjacencyList.push_back(arc8);


  // Make arcs above bidirectional
  Arc arc11, arc12, arc13, arc14, arc15, arc16, arc17, arc18;

  arc11.destId = 6;
  arc11.sourceId = 7;
  arc11.cost = 2;
  rn._nodeVector[7].adjacencyList.push_back(arc11);

  arc12.destId = 7;
  arc12.sourceId = 8;
  arc12.cost = 2;
  rn._nodeVector[8].adjacencyList.push_back(arc12);

  arc13.destId = 8;
  arc13.sourceId = 5;
  arc13.cost = 2;
  rn._nodeVector[5].adjacencyList.push_back(arc13);

  arc14.destId = 6;
  arc14.sourceId = 3;
  arc14.cost = 2;
  rn._nodeVector[3].adjacencyList.push_back(arc14);

  arc15.destId = 3;
  arc15.sourceId = 4;
  arc15.cost = 2;
  rn._nodeVector[4].adjacencyList.push_back(arc15);

  arc16.destId = 4;
  arc16.sourceId = 1;
  arc16.cost = 2;
  rn._nodeVector[1].adjacencyList.push_back(arc16);

  arc17.destId = 1;
  arc17.sourceId = 2;
  arc17.cost = 2;
  rn._nodeVector[2].adjacencyList.push_back(arc17);

  arc18.destId = 2;
  arc18.sourceId = 5;
  arc18.cost = 2;
  rn._nodeVector[5].adjacencyList.push_back(arc18);

  rn._arcVector.push_back(arc1);
  rn._arcVector.push_back(arc2);
  rn._arcVector.push_back(arc3);
  rn._arcVector.push_back(arc4);
  rn._arcVector.push_back(arc5);
  rn._arcVector.push_back(arc6);
  rn._arcVector.push_back(arc7);
  rn._arcVector.push_back(arc8);
  rn._arcVector.push_back(arc11);
  rn._arcVector.push_back(arc12);
  rn._arcVector.push_back(arc13);
  rn._arcVector.push_back(arc14);
  rn._arcVector.push_back(arc15);
  rn._arcVector.push_back(arc16);
  rn._arcVector.push_back(arc17);
  rn._arcVector.push_back(arc18);

  rn.partitionRoadNetwork(16);
  rn.setArcFlags();

  // Select a random arc, check all of its flag values
  ASSERT_EQ(rn.getNumRegions(),  rn.getNode(8)->adjacencyList[0].flags.size());

  // The 5th node is the source node for the arc to be tested,
  // go through all regions and check whether the current arc appears on any
  // shortest paths in the tree formed starting at the node in the current region
  ASSERT_FALSE(rn._nodeVector[8].adjacencyList[0].flags[0]);
  ASSERT_FALSE(rn._nodeVector[8].adjacencyList[0].flags[1]);
  ASSERT_FALSE(rn._nodeVector[8].adjacencyList[0].flags[2]);
  ASSERT_FALSE(rn._nodeVector[8].adjacencyList[0].flags[3]);
  ASSERT_FALSE(rn._nodeVector[8].adjacencyList[0].flags[4]);
  ASSERT_FALSE(rn._nodeVector[8].adjacencyList[0].flags[5]);
  ASSERT_FALSE(rn._nodeVector[8].adjacencyList[0].flags[6]);
  ASSERT_FALSE(rn._nodeVector[8].adjacencyList[0].flags[7]);
  ASSERT_FALSE(rn._nodeVector[8].adjacencyList[0].flags[8]);
  ASSERT_FALSE(rn._nodeVector[8].adjacencyList[0].flags[9]);
  ASSERT_TRUE(rn._nodeVector[8].adjacencyList[0].flags[10]);
  ASSERT_TRUE(rn._nodeVector[8].adjacencyList[0].flags[11]);
  ASSERT_FALSE(rn._nodeVector[8].adjacencyList[0].flags[12]);
  ASSERT_FALSE(rn._nodeVector[8].adjacencyList[0].flags[13]);
  ASSERT_TRUE(rn._nodeVector[8].adjacencyList[0].flags[14]);
  ASSERT_TRUE(rn._nodeVector[8].adjacencyList[0].flags[15]);

  // Arc: 4->3
  ASSERT_TRUE(rn._nodeVector[4].adjacencyList[1].flags[0]);
  ASSERT_FALSE(rn._nodeVector[4].adjacencyList[1].flags[1]);
  ASSERT_TRUE(rn._nodeVector[4].adjacencyList[1].flags[2]);
  ASSERT_TRUE(rn._nodeVector[4].adjacencyList[1].flags[3]);
  ASSERT_FALSE(rn._nodeVector[4].adjacencyList[1].flags[4]);
  ASSERT_FALSE(rn._nodeVector[4].adjacencyList[1].flags[5]);
  ASSERT_FALSE(rn._nodeVector[4].adjacencyList[1].flags[6]);
  ASSERT_FALSE(rn._nodeVector[4].adjacencyList[1].flags[7]);
  ASSERT_TRUE(rn._nodeVector[4].adjacencyList[1].flags[8]);
  ASSERT_FALSE(rn._nodeVector[4].adjacencyList[1].flags[9]);
  ASSERT_FALSE(rn._nodeVector[4].adjacencyList[1].flags[10]);
  ASSERT_FALSE(rn._nodeVector[4].adjacencyList[1].flags[11]);
  ASSERT_FALSE(rn._nodeVector[4].adjacencyList[1].flags[12]);
  ASSERT_FALSE(rn._nodeVector[4].adjacencyList[1].flags[13]);
  ASSERT_FALSE(rn._nodeVector[4].adjacencyList[1].flags[14]);
  ASSERT_FALSE(rn._nodeVector[4].adjacencyList[1].flags[15]);

  // Test whether a simple computeShortestPath query is managed correctly.
  vector<int> shortestPath;

  rn.resetNetwork();
  ASSERT_EQ(4, rn.computeShortestPath(rn.getNode(6), rn.getNode(5), 3, shortestPath));
  ASSERT_EQ(6, shortestPath[0]);
  ASSERT_EQ(7, shortestPath[1]);
  ASSERT_EQ(8, shortestPath[2]);
  ASSERT_EQ(5, shortestPath[3]);
}
