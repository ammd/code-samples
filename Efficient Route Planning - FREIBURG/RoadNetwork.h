// Copyright 2011
// University of Freiburg
// Mauricio Munoz.

#ifndef _HOME_MUNOZA_EFFICIENTROUTEPLANNING_ROADNETWORK_H_
#define _HOME_MUNOZA_EFFICIENTROUTEPLANNING_ROADNETWORK_H_

#include <gtest/gtest.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <vector>
#include <utility>
#include <queue>
#include <map>

using std::string;
using std::vector;
using std::map;
using std::pair;
using std::priority_queue;

// An arc in the graph modelled by the OSM data. An arc is composed of
// a destination node, a source node, a connecting way, 
// which is identified with the cost
// of getting from the node owning this arc to the destination node,
// and a vector of boolean flags used with the arc flags algorithm.
class Arc
{
 public:
  // The id of the head node of the arc.
  int destId;
  int sourceId;
  // The cost of the arc (time to complete crossing at max speed).
  double cost;

  vector<bool> flags;
};


// Datastructure representing a Node in the OSM geo data file.
// A node is made up of its (internal) id, a latitude value, a longitude value,
// a vector of arcs, which model its output edges, a vector of values
// containing costs to a set of landmarks, and an integer referring to the
// region to which this node belongs (for arc flags algorithm).
class Node
{
 public:
  int id;
  int previous;
  int region;
  double latitude;
  double longitude;

  vector<Arc> adjacencyList;
  vector<double> landmarkDist;
};

// Node wrapper class. A landmark is a special node, for which cost values to
// every other node in the graph are precomputed. landmarkId is the index
// of this landmark in the global data structure _landmarks.
class Landmark
{
 public:
  Node* node;

  int landmarkId;
};

// A region of the network map. Members are vectors of all nodes in the region
// and the regions boundary nodes, as well as the regions id, given by
// the longitude and latitude of its leftmost top corner.
class Region
{
  public:
  vector<Node*> nodes;
  vector<Node*> boundaryNodes;

  double longitude;
  double latitude;
};

// A Road Network as a directed graph, with nodes and edges with costs.
class RoadNetwork
{
 public:
  // Constructor, creates a network with no nodes and no edges.
  RoadNetwork();
  FRIEND_TEST(RoadNetworkTest, constructor);

  // Build graph based on an OSM data file.
  void readFromOsmFile(string fileName);
  FRIEND_TEST(RoadNetworkTest, readFromOsmFile);

  // Makes arcs based on a list of node ids and a type describing
  // this way.
  void makeArcs(const vector<int>& nodeIds, string wayType);
  FRIEND_TEST(RoadNetworkTest, makeArcs);

  // Computes the shortest path in terms of time in a graph, starting
  // from source and ending at dest. Method describes the complementing
  // algorithm used, for the moment either (0) regular dijkstra, 
  // (1) straight line A*, (2) landmarks A*, (3) Arc Flags
  int computeShortestPath(Node* source, Node* dest, int method,
                             vector<int>& shortestPath);
  FRIEND_TEST(RoadNetworkTest, computeShortestPath);


  // Call this method after adding all desired landmarks to update
  // each nodes information regarding its costs to these landmarks. This method
  // is to be called as the last pre-processing step in the landmarks algorithm.
  void updateNodeLandmarkVector();
  FRIEND_TEST(RoadNetworkTest, updateNodeLandmarkVector);

  // This method receives a new landmark with a valid node reference as input
  // The method then adds this landmark to the global data structure
  // _landmarks, and calculates and stores all distances from this landmark
  // to every other node in the network. Unreachable nodes are marked with
  // distances of -1.
  int addLandmark(Landmark& source);
  FRIEND_TEST(RoadNetworkTest, addLandmark);

  // This method adds the last landmark in sources to the total set of landmarks
  // and assumes, that the rest have already been added in previous iterations.
  // Method parameter is a vector, because this method returns the index of the
  // node with the maximum minimum distance to this set of landmarks.
  int addLandmark(vector<Landmark>& sources);
  FRIEND_TEST(RoadNetworkTest, addLandmarks);  

  // This method returns, for a given node source:
  // LandmarkHeuristic(source) = min_{l in L} {dist(source, l)-dist(l, target)}
  double getLandmarkHeuristic(Node* source, Node* target);

  // This method considers the smallest rectangular area around the entire
  // road map and subdivides it into #numRegions regions of equal size.
  // Each node in the node map belongs to exactly one region. Each node
  // receives a reference to the region it belongs to, each region receives
  // a reference to all nodes it contains, plus separate references to all of
  // its boundary nodes.
  // Note: numRegions must be a perfect square.
  void partitionRoadNetwork(int numRegions);
  FRIEND_TEST(RoadNetworkTest, partitionRoadNetwork);

  // This method inspects every arc in the graph and updates its flags
  // based upon the network partition in the method above.
  // The algorithm used here considers only the boundary nodes of each region
  // in order to set the arc flags.
  void setArcFlags();
  FRIEND_TEST(RoadNetworkTest, setArcFlags);

  // Returns the id of the node in _nodes that has the shortest
  // straight line distance to the point given by (lon, lat).
  int getIdOfClosestNode(double lat, double lon);
  FRIEND_TEST(RoadNetworkTest, getIdOfClosestNode);

  // Sets the _dequeued and _heuristicValue data structures back to default settings
  // (see constructor)
  void resetNetwork();

  // Retrieval methods.
  int getNumArcs() { return static_cast<int>(_arcVector.size()); }
  int getNumNodes() { return static_cast<int>(_nodeVector.size()); }
  int getNumRegions() { return static_cast<int>(_regions.size()); }
  int getNumLandmarks() { return static_cast<int>(_landmarks.size()); }
  Node* getNode(int i) { return &_nodeVector[i]; }
  Region* getRegion(int i) { return &_regions[i]; }
  Arc* getArc(int i) { return &_arcVector[i]; }



 private:

  // The number of arcs in the graph
  int _numArcs;

  // The central node managing data structure (map from OSM ids to
  // their corresponding nodes).
  vector<Node> _nodeVector;

  // Data structure containing references to all arcs in the graph.
  // Note: indices are not the base of any particular topological ordering
  vector<Arc> _arcVector;

  vector<double> _costToNode;

  // Storage datastructure for the graphs landmarks, each of which contains
  // a reference to a nodes.
  vector<vector<double> > _landmarks;

  // Central storage for the maps regions.
  vector<Region> _regions;

  // Mapping of osmIds to counter Ids.
  map<int, int> _idMap;

  // Mapping of highway types to max speeds.
  map<string, double> _speeds;

  // A list of boolean values indicated whether node at position i in 
  // idVector has been settled/ taken out of the priority queue.
  vector<bool> _dequeued;

  // Data structure which stores values representing a nodes distance to the
  // destination. Indices correspond with idVector, _dequeued.
  vector<double> _heuristicValue;

  double _globalMaxSpeed;

};

// Comparison class for priority queue
class greaterpair
{
  public:
  greaterpair() {}
  bool operator()(const pair<double, Node*>& pair1, const pair<double,
                                                             Node*>& pair2) const
  {
    return (pair1.first > pair2.first);
  }
};

#endif  // _HOME_MUNOZA_EFFICIENTROUTEPLANNING_ROADNETWORK_H_
