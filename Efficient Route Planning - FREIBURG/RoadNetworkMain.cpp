// Copyright 2011
// University of Freiburg
// Mauricio Munoz.

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include "./RoadNetwork.h"


unsigned randomSeed;
int main(int argc, char** argv)
{
  // Print usage info if wrong number of parameters is detected
  if (argc != 2)
  {
    printf("Usage: ./RoadNetworkMain <XML file with OSM geo-data>\n");
    exit(1);
  }

  // Retrieve name of file with OSM geo data.
  std::string filename = argv[1];

  time_t start, end;

  printf("Reading geo data from file ... ");
  fflush(stdout);

  time(&start);

  // Constructs a graph based on this data.
  RoadNetwork network;
  network.readFromOsmFile(filename);
  time(&end);

  int networkSize = network.getNumNodes();
  int numIterations = 2000;


  // Dijkstra, A*
  //////////////////////////////////////////////////////////////////////////////

/*
  printf("Calculating %d random routes using standard Dijkstra, A* ... \n", numIterations);
  fflush(stdout);

  double counter1 = 0.0;
  double counter2 = 0.0;
  int settled1, settled2;
  double settleCount1 = 0;
  double settleCount2 = 0;
  int validSearch1 = 0;
  int validSearch2 = 0;

  // Dijkstra
  for (int i = 0; i < numIterations; i++)
  {
    printf("Current Iteration (Dijkstra): %d\n", i+1);

    int index1 = rand_r(&randomSeed) % networkSize;
    int index2 = rand_r(&randomSeed) % networkSize;

    Node* start = network.getNode(index1);
    Node* end = network.getNode(index2);

    vector<int> shortestPath;

    time_t begin, finish;

    time(&begin);

    settled1 = network.computeShortestPath(start, end, 0, shortestPath);

    time(&finish);

    if (settled1 != 0) 
    {
       validSearch1++;
       settleCount1 += (double)settled1;
       counter1 += difftime(finish, begin);
    } 

    network.resetNetwork();
  }

  settleCount1 /= (double)validSearch1;

  // A*
  for (int i = 0; i < numIterations; i++)
  {
    printf("Current Iteration (A*): %d\n", i+1);

    int index1 = rand_r(&randomSeed) % networkSize;
    int index2 = rand_r(&randomSeed) % networkSize;

    Node* start = network.getNode(index1);
    Node* end = network.getNode(index2);

    vector<int> shortestPath;

    time_t begin, finish;

    time(&begin);

    settled2 = network.computeShortestPath(start, end, 1, shortestPath);

    time(&finish);

    if (settled2 != 0) 
    {
       validSearch2++;
       settleCount2 += (double)settled2;
       counter2 += difftime(finish, begin);
    } 

    network.resetNetwork();
  }

  settleCount2 /= (double)validSearch2;

  // Output
  printf("\n");

  printf("Reading took %.5lf seconds.\n", difftime(end, start));

  printf("Average comp. time was %f seconds (Dijkstra).\n", counter1/(double)validSearch1);
  printf("Average settled nodes: %d\n", (int)settleCount1);

  printf("Average comp. time was %f seconds (A* straight line).\n", counter2/(double)validSearch2);
  printf("Average settled nodes: %d\n", (int)settleCount2);


  // end Dijkstra, A*
  //////////////////////////////////////////////////////////////////////////////
*/


  // Using landmarks
  //////////////////////////////////////////////////////////////////////////////
/*
  int numLandmarks = 16;

  // Random landmark selection

  int settled3 = 0;
  double counter3 = 0.0;
  int validSearch3 = 0;
  double settleCount3 = 0;
  
  printf("Initializing landmarks ... ");
  fflush(stdout);
  
  for (int i = 0; i < numLandmarks; i++)
  {
    int index = rand_r(&randomSeed) % networkSize;

    Landmark l;
    l.node = network.getNode(index);
    network.addLandmark(l);
    network.resetNetwork();
  }

  printf("done.\n");
  fflush(stdout);

  network.updateNodeLandmarkVector();


// regions test
  printf("Initializing regions ... ");
  fflush(stdout);

    time_t begin, finish;

    time(&begin);

    network.partitionRoadNetwork(100);

    time(&finish);

  printf("done.\n");
  fflush(stdout);

    printf("Time for region initialization: %f\n", difftime(finish, begin));


  for (int i = 0; i < numIterations; i++)
  {
    printf("Current Iteration (Random Landmarks): %d\n", i+1);

    int index1 = rand_r(&randomSeed) % networkSize;
    int index2 = rand_r(&randomSeed) % networkSize;

    Node* start = network.getNode(index1);
    Node* end = network.getNode(index2);

    vector<int> shortestPath;

    time_t begin, finish;

    time(&begin);

    settled3 = network.computeShortestPath(start, end, 2, shortestPath);

    time(&finish);

    if (settled3 != 0) 
    {
       validSearch3++;
       settleCount3 += (double)settled3;
       counter3 += difftime(finish, begin);
    } 

    network.resetNetwork();
  }

  settleCount3 /= (double)validSearch3;

  printf("Average comp. time was %f seconds (A* Random Landmarks).\n", counter3/(double)validSearch3);
  printf("Average settled nodes: %d\n", (int)settleCount3);
*/

/*
  // Farthest landmark selection
  int settled4 = 0;
  double counter4 = 0.0;
  int validSearch4 = 0;
  double settleCount4 = 0;
  int randIndex;
  int nextL = 0;
  vector<Landmark> currentLandmarks;
  
  printf("Initializing landmarks ... ");
  fflush(stdout);
  
  // First landmark is random
  randIndex = rand_r(&randomSeed) % networkSize;
  Landmark l;
  l.node = network.getNode(randIndex);

  // Add first landmark to set of chosen landmarks, store id of next landmark
  currentLandmarks.push_back(l);
  nextL = network.addLandmark(currentLandmarks);
  network.resetNetwork();


  // Repeat for the remaining numLandmarks-1 landmarks to be chosen
  for (int i = 1; i < numLandmarks; i++)
  {
    Landmark nextLandmark;
    nextLandmark.node = network.getNode(nextL);
    currentLandmarks.push_back(nextLandmark);
    nextL = network.addLandmark(currentLandmarks);
    network.resetNetwork();
  }

  printf("done.\n");
  fflush(stdout);

  network.updateNodeLandmarkVector();


  for (int i = 0; i < numIterations; i++)
  {
    printf("Current Iteration (Farthest Landmarks): %d\n", i+1);

    int index1 = rand_r(&randomSeed) % networkSize;
    int index2 = rand_r(&randomSeed) % networkSize;

    Node* start = network.getNode(index1);
    Node* end = network.getNode(index2);

    vector<int> shortestPath;

    time_t begin, finish;

    time(&begin);

    settled4 = network.computeShortestPath(start, end, 2, shortestPath);

    time(&finish);


    if (settled4 != 0) 
    {
       validSearch4++;
       settleCount4 += (double)settled4;
       counter4 += difftime(finish, begin);
    } 


    network.resetNetwork();
  }

  settleCount4 /= (double)validSearch4;



  printf("Average comp. time was %f seconds (A* Farthest Landmarks).\n", counter4/(double)validSearch4);
  printf("Average settled nodes: %d\n", (int)settleCount4);
*/


  // Using arc flags
  //////////////////////////////////////////////////////////////////////////////
  
  // Initialize regions, time method for debug
  printf("Initializing regions ... ");
  fflush(stdout);

  time_t begin1, finish1;

  time(&begin1);
  network.partitionRoadNetwork(100);
  network.setArcFlags();
  time(&finish1);

  printf("done.\n");
  fflush(stdout);

  int settled5 = 0;
  double counter5 = 0.0;
  int validSearch5 = 0;
  double settleCount5 = 0.0;

  for (int i = 0; i < numIterations; i++)
  {
    network.resetNetwork();
    printf("Current Iteration (Arc Flags): %d\n", i+1);

    int index1 = rand_r(&randomSeed) % networkSize;
    int index2 = rand_r(&randomSeed) % networkSize;

    Node* start = network.getNode(index1);
    Node* end = network.getNode(index2);

    vector<int> shortestPath;

    time_t begin2, finish2;

    time(&begin2);

    settled5 = network.computeShortestPath(start, end, 3, shortestPath);

    time(&finish2);

    if (settled5 != 0) 
    {
       validSearch5++;
       settleCount5 += (double)settled5;
       counter5 += difftime(finish2, begin2);
    }  
  }

  settleCount5 /= (double)validSearch5;

//printf("validSearch, settleCount %d, %f, %f\n", validSearch5, settleCount5, counter5);


  printf("Average comp. time was %f seconds (Arc Flags).\n", counter5/(double)validSearch5);
  printf("Average settled nodes: %d\n", (int)settleCount5);
  printf("Time for precomputation: %.2lf min.\n", difftime(finish1, begin1)/60);

  return 0;
}
