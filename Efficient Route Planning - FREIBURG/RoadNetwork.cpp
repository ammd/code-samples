// Copyright 2011
// University of Freiburg
// Mauricio Munoz

#include <vector>
#include <utility>
#include <queue>
#include <map>
#include <string>
#include <algorithm>
#include "./RoadNetwork.h"

// _____________________________________________________________________________
RoadNetwork::RoadNetwork()
{
  _numArcs = 0;

  _speeds["motorway"] =       110;
  _speeds["trunk"] =          110;
  _speeds["primary"] =         70;
  _speeds["secondary"] =       60;
  _speeds["tertiary"] =        50;
  _speeds["motorway_link"] =   50;
  _speeds["trunk_link"] =      50;
  _speeds["primary_link"] =    50;
  _speeds["secondary_link"] =  50;
  _speeds["road"] =            40;
  _speeds["unclassified"] =    40;
  _speeds["residential"] =     30;
  _speeds["unsurfaced"] =      30;
  _speeds["living_street"] =   10;
  _speeds["service"] =          5;

  _globalMaxSpeed = 110;
}

// Returns the euclidean distance between two OSM nodes.
double getEuclideanDistance(double& x1, double& x2, double& y1, double& y2)
{
  double a = 1000000.0 * x1;
  double b = 1000000.0 * x2;
  double c = 1000000.0 * y1;
  double d = 1000000.0 * y2;

  return sqrt(fabs(a-b)*fabs(a-b) + fabs(c-d)*fabs(c-d));
}

// Helper function, returns the cost of the arc between
// these two nodes based on the maximum allowed speed for this arc.
double getEuclideanCost(double& x1, double& x2, double& y1, double& y2,
                              double& maxSpeed)
{
  return (getEuclideanDistance(x1, x2, y1, y2) / maxSpeed);
}

// _____________________________________________________________________________
void RoadNetwork::makeArcs(const vector<int>& nodeIds, string type)
{
  for (int i = 0; i < static_cast<int>(nodeIds.size()); i++)
  {
    double maxSpeed = _speeds.find(type)->second;
    int currentOsmId = nodeIds[i];

    int nodeIndex = _idMap.find(currentOsmId)->second;
    Node* currentNode = &_nodeVector[nodeIndex];

    double x1, x2, y1, y2;

    x1 = currentNode->longitude;
    y1 = currentNode->latitude;

    // Left neighbor in the list
    if (i != 0)
    {
      int predId = _idMap.find(nodeIds[i-1])->second; 
      Node pred = _nodeVector[predId];

      x2 = pred.longitude;
      y2 = pred.latitude;

      Arc arc;
      arc.destId = pred.id;
      arc.sourceId = currentNode->id;
      arc.cost = getEuclideanCost(x1, x2, y1, y2, maxSpeed);

      _arcVector.push_back(arc);
      currentNode->adjacencyList.push_back(arc);
      _numArcs++;
    }

    // Right neighbor int the list
    if (i+1 != static_cast<int>(nodeIds.size()))
    {
      int succId = _idMap.find(nodeIds[i+1])->second;
      Node succ = _nodeVector[succId];

      x2 = succ.longitude;
      y2 = succ.latitude;

      Arc arc;
      arc.destId = succ.id;
      arc.sourceId = currentNode->id;
      arc.cost = getEuclideanCost(x1, x2, y1, y2, maxSpeed);
      
      _arcVector.push_back(arc);
      currentNode->adjacencyList.push_back(arc);
      _numArcs++;
    }
  }
}

// _____________________________________________________________________________
void RoadNetwork::readFromOsmFile(string filename)
{
  // Various variables used in this method:
  // readWay - whether information pertaining to a way is currently being read.
  // highwayTag - whether the current way has a tag marking it as a highway.
  // nodeIds - data structure storing the ids of the nodes of the current way.
  bool readWay = false;
  vector<int> nodeIds;


  // Open the OSM file containing the geo data. This data will only be read,
  // not overwritten.
  FILE * pFile = fopen(filename.c_str(), "r");
  if (pFile == NULL)
  {
    fprintf(stderr, "ERROR: could not open input file \"%s\"\n",
    filename.c_str());
    exit(1);
  }

  // Copy destination for the fgets method below
  const int MAX_LINE_LENGTH = 100;
  char lineBuffer[MAX_LINE_LENGTH + 1];

  // The type of way, e.g. tertiary, secondary, etc.
  string tag;

  int nodeCounter = -1;

  // Main read routine: read the file line by line, find out if the
  // current line describes a node or a way, and act accordingly
  while (true)
  {
    // Read next line and break loop if end of file reached.
    fgets(lineBuffer, MAX_LINE_LENGTH, pFile);
    if (feof(pFile)) break;

    // char array as a string
    string line(lineBuffer);

    // Positions within the string marking the begin of key substrings,
    // such as <node, <nd ref=, <way, <\way, etc.
    size_t nodeFound;
    size_t ndIdFound;
    size_t tagFound;
    size_t wayFound;
    size_t endWay;

    // Case 1: current line describes a node plus its location and id info.
    ///////////////////////////////////////////////////////////////////////
    nodeFound = line.find("<node");

    if (nodeFound != string::npos)
    {
      nodeCounter++;

      // Store first 6 locations within the strings where " appear.
      vector<int> marks;

      size_t found = -1;

      for (int i = 0; i < 6; i++)
      {
        found = line.find("\"", found+1);
        if (found != string::npos) marks.push_back(static_cast<int>(found));
      }

      // Create node, update its internal information.
      Node node;

      // First number in the line is the node's id, followed by
      // its latitude, followed by its longitude.
      node.id = nodeCounter;

      int osmId = atoi(line.substr(marks[0]+1, marks[1]-marks[0]-1).c_str());

      node.latitude = atof(line.substr(marks[2]+1,
                                       marks[3]-marks[2]-1).c_str());
      node.longitude = atof(line.substr(marks[4]+1,
                                        marks[5]-marks[4]-1).c_str());


      // Update data structures.
      _idMap.insert(pair<int, int>(osmId, nodeCounter));
      _nodeVector.push_back(node);
      _heuristicValue.push_back(-1.0);
      _dequeued.push_back(false);
      _costToNode.push_back(-1);
    }

    // Case 2: line contains information pertaining to a way (after way header)
    ////////////////////////////////////////////////////////////////////////////
    else if (readWay)
    {
      // Search the current line for either a reference to a node,
      // a tag classifying the way as a highway, or a reference
      // to the end of way data.
      ndIdFound = line.find("<nd ref=");
      tagFound = line.find("<tag k=\"highway\"");
      endWay = line.find("</way");

      // Case 2.1: a reference to a node is found
      // Then, the next 2 instances of " limit the id of the node
      // due to the syntax of the OSM data file.
      if (ndIdFound != string::npos)
      {
        size_t first, second;

        first = line.find("\"");
        second=  line.find("\"", first+1);

        // Node id is stored in the global data structure, which is
        // erased only after a line signaling the end of way data is read.
        nodeIds.push_back(atoi(line.substr(first+1, second-first-1).c_str()));


      }

      // Case 2.2: a tag marking the way as a highway has been found.
      // Then, store the type of way for further processing.
      else if (tagFound != string::npos)
      {
        size_t begin, first, second;

        begin = line.find("v=");

        first = line.find("\"", begin+1);
        second = line.find("\"", first+1);

        tag = line.substr(first+1, second-first-1);
      }

      // Case 2.3: end of way information data
      else if (endWay != string::npos)
      {
        readWay = false;

        // make arcs if highway was specified
        if (_speeds.find(tag) != _speeds.end()) makeArcs(nodeIds, tag);

        // Reset
        tag="";
        nodeIds.clear();
      }
    }
    // If the current line desribes the beginning of way data,
    // then allow way information to be read in the next interation
    wayFound = line.find("<way");
    if (!readWay && wayFound != string::npos) readWay = true;
  }
}



// _____________________________________________________________________________
int RoadNetwork::computeShortestPath(Node* source, Node* dest,
                              int method, vector<int>& shortestPath)
{

printf("source id: %d\n", source->id);
printf("target id: %d\n", dest->id);

  int numSettled = 0;

  // Initialize algorithm to be used
  bool aStarStraightLine = false;
  bool landmarks = false;
  bool arcFlags = false;

  if (method == 1) aStarStraightLine = true;
  else if (method == 2) landmarks = true;
  else if (method == 3) arcFlags = true;

  // Declare various needed variables by the various methods
  double x2, y2;
  int destRegion = -1;
  x2 = dest->longitude;
  y2 = dest->latitude;

  double initialOffset = 0.0;
  _costToNode[source->id] = 0;


  // Initialize various variables needed by each respective method
  if (aStarStraightLine)
  {
    initialOffset = getEuclideanCost(source->longitude, x2,
                                     source->latitude, y2, _globalMaxSpeed);

    _heuristicValue[source->id] = initialOffset;
  }

  else if (landmarks)
  {
    initialOffset = getLandmarkHeuristic(source, dest);

    _heuristicValue[source->id] = initialOffset;
  }

  else if (arcFlags)
  {
     destRegion = dest->region;
  }

  // Priority queue, stores the graph's frontier as the the algorithm progresses
  priority_queue< pair<double, Node*>, vector< pair<double, Node*> >,
                  greaterpair > q;


  q.push(pair<double, Node*>(initialOffset, source));

  int destId = dest->id;

/*
printf("source id: %d\n", source->id);

printf("number of outgoing arcs: %d\n", (int)source->adjacencyList.size());
for (int i = 0; i < (int)source->adjacencyList.size(); i++)
{
    for (int j = 0; j < getNumRegions(); j++)
         if (source->adjacencyList[i].flags[j])
             printf("arc number %d is flagged true for region number %d\n", i, j);
}
*/

  // While destination node has not been settled
  while (!_dequeued[destId])
  {
    // Extract element with smallest (finite) tentative distance
    if (q.empty())
    {
      return 0;
    }

    pair<double, Node*> top = q.top();

    int topId = top.second->id;

    // If this node has already been taken out of the queue, then it has already
    // been processed and can be removed.
    if (_dequeued[topId])
    {
      q.pop();
      continue;
    }

    // Otherwise settle the node
    q.pop();
    _dequeued[topId] = true;
    numSettled++;

    // Check if arrived at destination
    if (_dequeued[destId]) break;

    // Process all outgoing arcs from this node
    vector<Arc> adjList = getNode(topId)->adjacencyList;

    for (int i = 0; i <
         static_cast<int>(adjList.size()); i++)
    {
      Arc currentArc = adjList[i];
      int arcDestId = currentArc.destId;

      // Arcs leading to settled nodes may be skipped
      if (_dequeued[arcDestId]) continue;

      // Using arc flags
      if (arcFlags && !currentArc.flags[destRegion]) continue;

      Node* nextNode = getNode(arcDestId);

      // Compute total cost for new node
      double alt = top.first + currentArc.cost;

      double nextLong = nextNode->longitude;
      double nextLat = nextNode->latitude;

      // Using straight line A*
      if (aStarStraightLine)
      {
        double temp = _heuristicValue[arcDestId];

        if (temp >= 0) alt += temp;

        else
        {
          double cost = getEuclideanCost(nextLong, x2, nextLat,  y2,
                                         _globalMaxSpeed);
          alt += cost;
          _heuristicValue[arcDestId] = cost;
        }

        alt -= _heuristicValue[topId];
      }

      // Using landmarks A*
      else if (landmarks)
      {
        double temp = _heuristicValue[arcDestId];

        if (temp > 0) alt += temp;

        else
        {
          double cost = getLandmarkHeuristic(nextNode, dest);

          alt += cost;
          _heuristicValue[arcDestId] = cost;
        }

        alt -= _heuristicValue[topId];
      }

      // Add node to the frontier
      q.push(pair<double, Node*>(alt, nextNode));

      // Build shortest path
      double lastCost = _costToNode[nextNode->id]; 

      if (alt <  lastCost || lastCost == -1) 
      {
          _costToNode[nextNode->id] = alt;    
          nextNode->previous = topId;
      }
    }
  }

  int current = destId;
  int sourceId = source->id;

  while (current != sourceId)
  {
    shortestPath.push_back(current);
    current = _nodeVector[current].previous;
  }

  shortestPath.push_back(sourceId);
  reverse(shortestPath.begin(), shortestPath.end());
 
printf("shortest path calculated\n");
 
  return numSettled;
}

// _____________________________________________________________________________
int RoadNetwork::addLandmark(vector<Landmark>& sources)
{

  Landmark currentSource = sources[(int)sources.size()-1];

  currentSource.landmarkId = _landmarks.size();

  vector<double> newCostVector;

  for (int i = 0; i < getNumNodes(); i++) newCostVector.push_back(-1);  

  // Run dijkstras algorithm starting from last landmark in landmark vector
  // Because this method is called iteratively, it is only this last landmark
  // which does not have valid distance values.
  //
  // The first part of this method calculates valid distance values
  // from this last landmark to the rest of the nodes in the graph
  //////////////////////////////////////////////////////////////////////////////

  int numSettled = 0;

  // Priority queue, stores the graph's frontier as the the algorithm progresses
  priority_queue< pair<double, Node*>, vector< pair<double, Node*> >,
                  greaterpair > q;

  q.push(pair<double, Node*>(0, currentSource.node));

  // While not all nodes are settled
  while (numSettled != (int)_nodeVector.size())
  {
    // Extract element with smallest (finite) tentative distance
    if (q.empty())
    {
      break;
    }

    pair<double, Node*> top = q.top();
    int topId = top.second->id;

    // If this node has already been taken out of the queue, then it has already
    // been processed and can be removed.
    if (_dequeued[topId])
    {
      q.pop();
      continue;
    }

    // Otherwise settle the node, store cost from landmark to this node
    q.pop();
    newCostVector[top.second->id] = top.first;
    _dequeued[topId] = true;
    numSettled++;

    // Process all outgoing arcs from this node
    vector<Arc> adjList = _nodeVector[topId].adjacencyList;

    for (int i = 0; i <
         static_cast<int>(adjList.size()); i++)
    {
      Arc currentArc = adjList[i];
      int arcDestId = currentArc.destId;

      // Arcs leading to settled nodes may be skipped
      if (_dequeued[arcDestId]) continue;

      // Compute cost for new node
      double alt = top.first + currentArc.cost;

      Node* nextNode = &_nodeVector[arcDestId];

      // Add the new node to the frontier
      q.push(pair<double, Node*>(alt, nextNode));
    }
  }

  _landmarks.push_back(newCostVector);


  // Now that the distances from this newly added landmark to every other
  // reachable node in the graph have been added to the central landmark
  // data structure, run Dijkstra on the set of all landmarks that are
  // currently in this data structure in order to return the index of the
  // node which maximizes the minimum distance between itself and the set of
  // landmarks chosen up to this point.
  //////////////////////////////////////////////////////////////////////////////

  resetNetwork();

  numSettled = 0;

  // Priority queue, stores the graph's frontier as the the algorithm progresses
  priority_queue< pair<double, Node*>, vector< pair<double, Node*> >,
                  greaterpair> newq;

  priority_queue< pair<double, Node*>, vector< pair<double, Node*> > > tempq;

  for (int i = 0; i < (int)sources.size(); i++)
  {
    newq.push(pair<double, Node*>(0, sources[i].node));
  }

  // While not all nodes are settled
  while (numSettled != (int)_nodeVector.size())
  {
    // Extract element with smallest (finite) tentative distance
    if (newq.empty())
    {
      break;
    }

    pair<double, Node*> top = newq.top();
    int topId = top.second->id;

    // If this node has already been taken out of the queue, then it has already
    // been processed and can be removed.
    if (_dequeued[topId])
    {
      newq.pop();
      continue;
    }

    // Otherwise settle the node, store cost from landmark to this node
    newq.pop();
    tempq.push(pair<double, Node*>(top.first, top.second));
    _dequeued[topId] = true;
    numSettled++;

    // Process all outgoing arcs from this node
    vector<Arc> adjList = _nodeVector[topId].adjacencyList;

    for (int i = 0; i <
         static_cast<int>(adjList.size()); i++)
    {
      Arc currentArc = adjList[i];
      int arcDestId = currentArc.destId;

      // Arcs leading to settled nodes may be skipped
      if (_dequeued[arcDestId]) continue;


      // Compute cost for new node
      double alt = top.first + currentArc.cost;

      Node* nextNode = &_nodeVector[arcDestId];

      // Add the new node to the frontier
      newq.push(pair<double, Node*>(alt, nextNode));
    }
  }

  resetNetwork();
  return tempq.top().second->id;
}

// _____________________________________________________________________________
int RoadNetwork::addLandmark(Landmark& source)
{
  source.landmarkId = _landmarks.size();

  vector<double> newCostVector;

  for (int i = 0; i < getNumNodes(); i++) newCostVector.push_back(-1);  

  // Run dijkstras algorithm starting from this landmark
  //////////////////////////////////////////////////////////////////////////////

  int numSettled = 0;

  // Priority queue, stores the graph's frontier as the the algorithm progresses
  priority_queue< pair<double, Node*>, vector< pair<double, Node*> >,
                  greaterpair > q;

  q.push(pair<double, Node*>(0, source.node));

  // While not all nodes are settled
  while (numSettled != (int)_nodeVector.size())
  {
    // Extract element with smallest (finite) tentative distance
    if (q.empty())
    {
      _landmarks.push_back(newCostVector);
      return numSettled;
    }

    pair<double, Node*> top = q.top();
    int topId = top.second->id;

    // If this node has already been taken out of the queue, then it has already
    // been processed and can be removed.
    if (_dequeued[topId])
    {
      q.pop();
      continue;
    }

    // Otherwise settle the node, store cost from landmark to this node
    q.pop();
    newCostVector[top.second->id] = top.first;
    _dequeued[topId] = true;
    numSettled++;

    // Process all outgoing arcs from this node
    vector<Arc> adjList = _nodeVector[topId].adjacencyList;

    for (int i = 0; i <
         static_cast<int>(adjList.size()); i++)
    {
      Arc currentArc = adjList[i];
      int arcDestId = currentArc.destId;

      // Arcs leading to settled nodes may be skipped
      if (_dequeued[arcDestId]) continue;

      // Compute cost for new node
      double alt = top.first + currentArc.cost;

      Node* nextNode = &_nodeVector[arcDestId];

      // Add the new node to the frontier
      q.push(pair<double, Node*>(alt, nextNode));
    }
  }

  _landmarks.push_back(newCostVector);
  return numSettled;
}

// _____________________________________________________________________________
void RoadNetwork::updateNodeLandmarkVector()
{
  for (int i = 0; i < getNumNodes(); i++)
  {
    Node* current = getNode(i);
    for (int j = 0; j < getNumLandmarks(); j++)
    {
      current->landmarkDist.push_back(_landmarks[j][i]);
    }
  }
}

// _____________________________________________________________________________
double RoadNetwork::getLandmarkHeuristic(Node* source, Node* target)
{
  double max = 0;

  for (int i = 0; i < (int)_landmarks.size(); i++)
  {
    double temp1 = source->landmarkDist[i];
    double temp2 = target->landmarkDist[i];

    if (temp1 == -1 || temp2 == -1) continue;

    if (temp1 - temp2 > max) max = temp1 - temp2;
  }

  return max;
}

// _____________________________________________________________________________
void RoadNetwork::partitionRoadNetwork(int numReg)
{
  // First, determine the road networks bounding box.
  double minX, maxX, minY, maxY;

  Node* n = getNode(0);
  minX = n->longitude;
  maxX = minX;
  minY = n->latitude;
  maxY = minY;

  for (int i = 1; i < getNumNodes(); i++)
  {
    double lon = getNode(i)->longitude;
    double lat = getNode(i)->latitude;

    if (lon < minX) minX = lon;
    if (lon > maxX) maxX = lon;
    if (lat < minY) minY = lat;
    if (lat > maxY) maxY = lat;
  }

  // Create and store all regions
  double currentX = minX;
  double currentY = maxY;

  double spanX = maxX-minX;
  double spanY = maxY-minY;

  int lateral = sqrt(numReg);

  double intervalX = spanX/lateral;
  double intervalY = spanY/lateral;

  while (currentX <= maxX && currentY > minY)
  {
     Region r;
     r.longitude = currentX;
     r.latitude = currentY;

     _regions.push_back(r);

     currentX += intervalX;
     if (fabs(currentX - maxX) < 0.0001)
     {
       currentX = minX;
       currentY -= intervalY;
     }
  }

  // Assign all nodes to a region
  // Node.region is initialized for all nodes
  // All region.nodes are initialized
  for (int i = 0; i < getNumNodes(); i++)
  {
    Node* node = getNode(i);
    double nLon = node->longitude;
    double nLat = node->latitude;

    // First case: node is on the rightmost border
    if (fabs(nLon - maxX) < 0.000001)
    {
       int rowIndex = (maxY - nLat) / intervalY;
       
       if (rowIndex == lateral) rowIndex--;
 
       int regionIndex = (rowIndex+1)*lateral-1;

       Region* r = getRegion(regionIndex);
       node->region= regionIndex;
       r->nodes.push_back(node);       
    }

    // Second case: node is on the bottom-most border
    else if (fabs(nLat - minY) < 0.000001)
    {
       int colIndex = (nLon - minX) / intervalX;
       
       if (colIndex == lateral) colIndex--;

       int regionIndex = lateral*(lateral-1) + colIndex;

       Region* r = getRegion(regionIndex);
       node->region= regionIndex;
       r->nodes.push_back(node);  
    }
    else
    {

      int colIndex = (nLon - minX) / intervalX;
      int rowIndex = (maxY - nLat) / intervalY;

      int regionIndex = rowIndex * lateral + colIndex;
      
      Region* r = getRegion(regionIndex);

      node->region = regionIndex;
      r->nodes.push_back(node);
    }
  }

  // Now that all nodes have been assigned to a region
  // and all respective data structures in the region and node classes
  // have been updated, compute which of the nodes inside each region
  // are boundary nodes.
  for (int i = 0; i < getNumNodes(); i++)
  {

    Node* node = getNode(i);
    vector<Arc> adjList = node->adjacencyList;
    int nodeReg = node->region;

    bool added = false;

    for (int j = 0; j < (int)adjList.size(); j++)
    {
      Node* dest = getNode(adjList[j].destId);
      int destRegion = dest->region;

      if (nodeReg >= 0 && nodeReg < getNumRegions() && destRegion >= 0 &&
          destRegion < getNumRegions() && nodeReg != destRegion && !added)

          getRegion(nodeReg)->boundaryNodes.push_back(node);
          added = true;
    }
  }

}

// _____________________________________________________________________________
void RoadNetwork::setArcFlags()
{
  // First initialize the flag data structures in each arc

  for (int j = 0; j < getNumArcs(); j++)
  {
    Arc* currentArc = getArc(j);
    currentArc->flags.clear();

    Node* n = getNode(currentArc->sourceId);
    
    int i = -1;
    for (int k = 0; k < (int)n->adjacencyList.size(); k++)
      if (n->adjacencyList[k].destId == currentArc->destId)
      {
        i = k;
        break;
      }

    for (int k = 0; k < getNumRegions(); k++)
      n->adjacencyList[i].flags.push_back(false);
  }

  // Now consider the bounding nodes for each region, and update the flags of
  // each arc according to the shortest path calculations starting from each
  // of these nodes
  for (int i = 0; i < getNumRegions(); i++)
  {
    Region* r = getRegion(i);
    int bNodes = r->boundaryNodes.size();
 
    for (int k = 0; k < bNodes; k++)
    {
      resetNetwork();
      Node* source = r->boundaryNodes[k];

      // Perform regular Dijkstra
      //////////////////////////////////////////////////////////////////////////

      vector<double> newCostVector;

      for (int m = 0; m < getNumNodes(); m++) newCostVector.push_back(-1);  

      int numSettled = 0;

      // Priority queue, stores the graph's frontier as the the algorithm progresses
      priority_queue< pair<double, Node*>, vector< pair<double, Node*> >,
                  greaterpair > q;

      q.push(pair<double, Node*>(0, source));

      // While not all nodes are settled
      while (numSettled != (int)_nodeVector.size())
      {
         // Extract element with smallest (finite) tentative distance
         if (q.empty()) break;
         
         pair<double, Node*> top = q.top();
         int topId = top.second->id;

         // If this node has already been taken out of the queue, then it has already
         // been processed and can be removed.
         if (_dequeued[topId])
         {
             q.pop();
             continue;
         }

         // Otherwise settle the node
         q.pop();
         newCostVector[top.second->id] = top.first;
         _dequeued[topId] = true;
         numSettled++;

         // Process all outgoing arcs from this node
         vector<Arc> adjList = _nodeVector[topId].adjacencyList;

         for (int i = 0; i < static_cast<int>(adjList.size()); i++)
         {
           Arc currentArc = adjList[i];
           int arcDestId = currentArc.destId;

           // Arcs leading to settled nodes may be skipped
           if (_dequeued[arcDestId]) continue;

           // Compute cost for new node
           double alt = top.first + currentArc.cost;

           Node* nextNode = &_nodeVector[arcDestId];

           // Add the new node to the frontier
           q.push(pair<double, Node*>(alt, nextNode));
         }
       }

      // end Dijkstra
      //////////////////////////////////////////////////////////////////////////

      // Now consider each arc in the graph
      for (int j = 0; j < getNumArcs(); j++)
      {
        Arc* currentArc = getArc(j);
        int sourceId = currentArc->sourceId;
        int destId = currentArc->destId;
        double cost = currentArc->cost;


        Node* n = getNode(sourceId);
    
        int m = -1;
        for (int s = 0; s < (int)n->adjacencyList.size(); s++)
        if (n->adjacencyList[s].destId == destId)
        {
          m = s;
          break;
        }

        // Index i refers to the current region being considered.
        double c1 = newCostVector[sourceId];
        double c2 = newCostVector[destId];

        if (c1 >= 0 && c2 >= 0 && c1 == c2 + cost)

            getNode(currentArc->sourceId)->adjacencyList[m].flags[i] = true;        
      }
    }
  }
}

// _____________________________________________________________________________
int RoadNetwork::getIdOfClosestNode(double lat, double lon)
{
  int minIndex = 0;
  double min = 10000000;

  for (int i = 0; i < getNumNodes(); i++)
  {
    Node* n = getNode(i);
    double dist = getEuclideanDistance(lon, n->longitude, lat, n->latitude);

    if (dist < min){
      min = dist;
      minIndex = i;
    }
  }

  return minIndex;
}



// _____________________________________________________________________________
void RoadNetwork::resetNetwork()
{
  for (int i = 0; i < (int)getNumNodes(); i++)
  {
     _heuristicValue[i] = -1.0;
     _dequeued[i] = false;
     _costToNode[i] = -1.0;
  }
}
