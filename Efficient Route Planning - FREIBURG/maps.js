  var markersArray = [];
  var sourceImage = 'http://maps.google.com/mapfiles/kml/paddle/red-circle.png';
  var targetImage = 'http://maps.google.com/mapfiles/kml/paddle/blu-circle.png';
  var map;
  $(document).ready(function (){
  
    // Default position
    var latlng = new google.maps.LatLng(49.233, 7);
    
    // Map options
    var myOptions = {
      zoom: 11,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    // Instantiate the map
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    
    // First marker (source) information
    var source = new google.maps.Marker({
        position: latlng,
        map: map,
        draggable: true,
        icon: sourceImage,
        animation: google.maps.Animation.BOUNCE,
    });
    
    // Second marker (target) information
    var target = new google.maps.Marker({
        position: latlng,
        map: map,
        draggable: true,
        icon: targetImage,
        animation: google.maps.Animation.BOUNCE,
    });
    
    // Set markers only once user has clicked on their locations
    source.setMap(null);
    target.setMap(null);
    
    // left click listener -> add either source or target, depending 
    // on whether source has already been added.
    google.maps.event.addListener(map, 'click', function(event) {
    
        if (markersArray[0] == null){
            source.setPosition(event.latLng);
            source.setTitle("Source: " + event.latLng);       
            source.setMap(map);
            markersArray[0] = source;
        } else {
            target.setPosition(event.latLng);
            target.setTitle("Target: " + event.latLng);        
            target.setMap(map);
            markersArray[1] = target;
        }

        if (markersArray[0] != null && markersArray[1] != null)
            sendSelection(markersArray[0].getPosition(), 
                          markersArray[1].getPosition());

    });
    
    // Right click listener -> delete source, if source was right clicked on
    google.maps.event.addListener(source, 'rightclick', function() {
       
        markersArray[0] = null;
        source.setMap(null);
    });
    
    // Right click listener -> delete target, if target was right clicked on
    google.maps.event.addListener(target, 'rightclick', function() {
       
        markersArray[1] = null;
        target.setMap(null);
    });
  });
  

  // Send positions of source and target to server
  function sendSelection(loc1, loc2)
  {
  var url = "http://localhost:8888/" + "$$" + loc1.toUrlValue() + ";" + loc2.toUrlValue() + "$$";
  $.ajax(url, { dataType: "jsonp" });
  }

  // Manage JSON object
  function callback(json, text, xhqr)
  {
    var coordsArray = json.coords.split("$");

    for (var i = 0; i <= coordsArray.length; i++){
         var temp = coordsArray[i].split(";");

         var latlng = new google.maps.LatLng(temp[0], temp[1]);

         var marker = new google.maps.Marker({
             position: latlng,
             map: map,
             animation: google.maps.Animation.DROP
         });   
    }
  }
