#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>
#include <algorithm>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include "./RoadNetwork.h"

using boost::asio::ip::tcp;
using boost::system::error_code;

int port = 8888;

// For formatting ASCII output.
#define BOLD     "\033[1m"
#define BLACK    "\033[30m"
#define RED      "\033[31m"
#define GREEN    "\033[32m"
#define BROWN    "\033[33m"
#define BLUE     "\033[34m"
#define RESET    "\033[0m"

// Main program implementing a simple server loop using boost:asio for the
// socket communication. Expects a simple HTTP get request and returns a json obj
int main(int argc, char** argv)
{
  // Parse command line arguments.
  if (argc != 2)
  {
    std::cout << "Usage: ./MapsDemoServerMain <osm file name>" << std::endl
         << "Start a server answering closest point queries" << std::endl;
    exit(1);
  }
  std::string osmFileName = argv[1];

  // Read road network.
  RoadNetwork rn;
  rn.readFromOsmFile(osmFileName);

  // Now the server part.
  try
  {
    // Create a boost IO service.
    boost::asio::io_service io_service;

    // Create socket and bind to and listen on given port.
    tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), port));

    // Wait for requests and process them.
    while (true)
    {
      // Wait for request.
      std::cout << BLUE << BOLD << "Waiting for query on port " << port
                << " ... " << RESET << std::flush;
      tcp::socket socket(io_service);
      acceptor.accept(socket);
      std::time_t now = std::time(0);
      std::string daytime = std::ctime(&now); // NOLINT
      std::cout << "done. \n Received new request on " << daytime << std::flush;

      // Get the request string.
      std::vector<char> requestBuffer(1000);
      boost::system::error_code read_error;
      socket.read_some(boost::asio::buffer(requestBuffer), read_error);
      std::string request(requestBuffer.size(), 0);
      std::copy(requestBuffer.begin(), requestBuffer.end(), request.begin());
      for (size_t i = 0; i < request.size(); i++)
        request[i] = isspace(request[i]) ? ' ' : request[i];
      std::cout << "request string is \"" << (request.size() < 99 ? request :
          request.substr(0, 87) + "...") << "\"" << std::endl;

      // Extract query = the thing between the GET and the HTTP ...
      std::string query = request.substr(5);
      size_t pos = query.find(" HTTP");
      if (pos != std::string::npos) query.erase(pos);

      // Extract latitude and longitude.
      double lat1 = 0;
      double lon1 = 0;
      double lat2 = 0;
      double lon2 = 0;

      size_t pos1 = query.find("$$");
      size_t pos2 = query.find(",");
      size_t pos3 = query.find(";");
      size_t pos4 = query.find(",", pos2 + 1);
      size_t pos5 = query.find("$$", pos1 + 3);

      // First set of coordinates
      if (pos1 != std::string::npos && pos2 != std::string::npos)
      lat1 = atof((query.substr(pos1 + 2, (pos2-pos1-2))).c_str());
      
      if (pos3 != std::string::npos)
      lon1 = atof((query.substr(pos2 + 1, (pos3-pos2-1))).c_str());

      // Second set of coordinates
      if (pos4 != std::string::npos)
      lat2 = atof((query.substr(pos3 + 1, (pos4-pos3-1))).c_str());
      
      if (pos5 != std::string::npos)
      lon2 = atof((query.substr(pos4 + 1, (pos5-pos4-1))).c_str());

      // Find closest OSM nodes.
      int closestSourceId = rn.getIdOfClosestNode(lat1, lon1);
      int closestTargetId = rn.getIdOfClosestNode(lat2, lon2);
      Node* closestSource;
      Node* closestTarget;

      closestSource = rn.getNode(closestSourceId);
      closestTarget = rn.getNode(closestTargetId);

      std::vector<int> shortestPath;
      if (rn.computeShortestPath(closestSource, closestTarget, 0, shortestPath) == 0)
          std::cout << "No shortest path available.\n" << std::flush;

      // Debug
      shortestPath.clear();
      shortestPath.push_back(closestSourceId);
      shortestPath.push_back(closestTargetId);

      // Send a JSON object containing a string which has the latitudes and 
      // longitudes of all nodes on the shortest path.
      string coords;
      std::stringstream out;
      for (int i = 0; i < (int)shortestPath.size(); i++) {
            Node* n = rn.getNode(shortestPath[i]);
            out << n->latitude << ";" << n->longitude << "$";
      }

      coords = out.str();

      printf("%s\n", coords.c_str());


      std::stringstream jsonp;
      jsonp << "callback(\n"
            << "{\n"
            << "  \"coords\" : \"" << coords << "\"\n"
            << "})\n";

      std::ostringstream answer;
      answer << "HTTP/1.0 200 OK\r\n"
             << "Content-Length: " << jsonp.str().size() << "\r\n"
             << "Content-Type: application/javascript" << "\r\n"
             << "Connection: close\r\n"
             << "\r\n"
             << jsonp.str();
      boost::system::error_code write_error;
      boost::asio::write(socket, boost::asio::buffer(answer.str()),
          boost::asio::transfer_all(), write_error);
    }
  }
  catch (std::exception& e) // NOLINT
  {
    std::cerr << e.what() << std::endl;
  }
}
